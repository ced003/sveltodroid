package it.comerservizi.appsveltodroid;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.nfc.NfcAdapter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.os.StrictMode;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;


class DownloadReceiver extends ResultReceiver {

    public ProgressDialog mProgressDialog;

    public DownloadReceiver(Handler handler) {
        super(handler);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        super.onReceiveResult(resultCode, resultData);
        if (resultCode == DownloadService.UPDATE_PROGRESS) {
            int progress = resultData.getInt("progress");
            mProgressDialog.setProgress(progress);
            if (progress == 100) {
                mProgressDialog.dismiss();
            }
        }
    }
}

public class Act_Accesso extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "it.comerservizi.sveltodroid.MESSAGE";
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    GPSHelper gps;
    ConnectionDetector cd;
    ImageView imgGPS;
    ImageView imgNFC;
    private NfcAdapter mNfcAdapter;
    ImageView imgWEB;
    DatabaseHelper dbHelper;
    TextView txtImei;
    EditText txtCodOperatore;
    TextView lblVersion;
    TextView lblIDTerm;
    TextView lblNomLetturista;
    TextView lblDescAttivita;
    Button BtnLoggati;
    Button BtnAggiornaSoftware;
    AsyncCallWS WebService;
    String DataOraCerta;
    Calendar CalendarCert;
    String Versione;
    String NameApp;
    ProgressDialog mProgressDialog;
    GPClass GPC;
    String OperatoreSim;
    String CodiceSim;
    String PhoneNumber;
    String GiornoAccesso;
    ListView listViewCom;
    public String IDLETT;
    public String _Versioning;


    boolean statusOfGPS;

    public TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /* DA INSERIRE IN TUTTE LE CLASSI DOVE VI E' UN WS */
        if (Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        onTrimMemory(ComponentCallbacks2.TRIM_MEMORY_COMPLETE);
        onTrimMemory(ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN);

        DatabaseHelper db = new DatabaseHelper(Act_Accesso.this);
        /* DA INSERIRE IN TUTTE LE CLASSI DOVE VI E' UN WS */

        int stringId = getApplicationInfo().labelRes;
        NameApp = getString(stringId);
        IDLETT = "";
        Versione = BuildConfig.VERSION_NAME;
        Calendar calendar = Calendar.getInstance();
        GiornoAccesso = Integer.toString(calendar.get(Calendar.DAY_OF_MONTH));

        /* CONTROLLO DATA AUTOMATICA */
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                if (Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME) != 1 || Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME_ZONE) != 1) {
                    Toast.makeText(getApplicationContext(), "ATTENZIONE HAI DISABILITATO LA DATA E ORA AUTOMATICA - L'APP NON PUO' FUNZIONARE", Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        /* CONTROLLO DATA AUTOMATICA */

        setContentView(R.layout.activity_act__accesso);

        statusOfGPS = false;
        //Immagini
        imgGPS = (ImageView) findViewById(R.id.imgGPS);
        imgWEB = (ImageView) findViewById(R.id.imgWeb);
        imgNFC = (ImageView) findViewById(R.id.imgNFC);
        //Testi
        txtImei = (TextView) findViewById(R.id.txtIMEI);
        lblIDTerm = (TextView) findViewById(R.id.txtIDTerm);
        lblNomLetturista = (TextView) findViewById(R.id.txtLett);
        lblVersion = (TextView) findViewById(R.id.lblVersion);
        lblDescAttivita = (TextView) findViewById(R.id.txtDescAttivita);
        BtnLoggati = (Button) findViewById(R.id.btnLoggati);
        listViewCom = (ListView) findViewById(R.id.listComunicazioni);
        BtnAggiornaSoftware = (Button) findViewById(R.id.btnAggiornaSoftware);

        txtCodOperatore = (EditText) findViewById(R.id.txtCodOperatore);

        //POPUP AVVISI


        //Inizializza logo stati web gps

        Date d = new Date();
        CharSequence s = DateFormat.format("dd/MM/yyyy", d.getTime());

        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (mNfcAdapter != null) {
            imgNFC.setImageResource(R.drawable.nfc03green);

        } else {
            imgNFC.setImageResource(R.drawable.nfc02red);
        }


    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

        /* CONTROLLO DATA AUTOMATICA */
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                if (Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME) != 1 || Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME_ZONE) != 1) {
                    Toast.makeText(getApplicationContext(), "ATTENZIONE HAI DISABILITATO LA DATA E ORA AUTOMATICA - L'APP NON PUO' FUNZIONARE", Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        /* CONTROLLO DATA AUTOMATICA */
        Crashlytics.log(null);
        Crashlytics.log("IMEI: " + txtImei.getText().toString() + " - Operatore: " + this.lblNomLetturista.getText().toString().trim());
        if (RichiestaPermessi()) {
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            OperatoreSim = telephonyManager.getNetworkOperatorName().toString();
            CodiceSim = telephonyManager.getSimSerialNumber();
            PhoneNumber = telephonyManager.getLine1Number().toString();

            txtImei.setText("Codice: " + telephonyManager.getDeviceId());
            WebService = new AsyncCallWS(Act_Accesso.this);
            WebService._IMEI = txtImei.getText().toString().replace("Codice:","").trim();
            WebService._CodSIM = CodiceSim;
            WebService._OpSIM = OperatoreSim;
            WebService._PhNum = PhoneNumber;
            WebService._NomeApp = NameApp;
            WebService.MyAct_Accesso = this;

            cd = new ConnectionDetector(getApplicationContext(),imgWEB);
            cd.setAct_AccessoHandler(this);
            IntentFilter fltr_connreceived = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
            registerReceiver(cd, fltr_connreceived);
            WebService.SSID = cd.WifiSSID();
            WebService.execute();
            VerificaGPS();
        }
        else
            Toast.makeText(getApplicationContext(), "Attenzione, vanno abilitati tutti i permessi all'APP affinché funzioni correttamente", Toast.LENGTH_LONG).show();

    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        try{

            unregisterReceiver(cd);
            Log.i("Act_Accesso", "Unregistrato Connection Detector");
        }
        catch(Exception e)
        {
            Log.i("Act_Accesso", "Impossibile unregistrare Connection Detector");
        }

    }

    @Override
    protected void onStop()
    {
        super.onStop();

        try{

            unregisterReceiver(cd);
            Log.i("Act_Accesso", "Unregistrato Connection Detector");
        }
        catch(Exception e)
        {
            Log.i("Act_Accesso", "Impossibile unregistrare Connection Detector");
        }
    }

    public void VerificaGPS()
    {

        gps = new GPSHelper();
        gps.GPSContesto(getApplicationContext());

        statusOfGPS = gps.StatoGPS();
        if (statusOfGPS)
            imgGPS.setImageResource(R.drawable.gpsgreen26);
        else
            Toast.makeText(getApplicationContext(), "ATTENZIONE DEVI ATTIVARE IL GPS", Toast.LENGTH_LONG).show();



    }

    public void sendMessage(View view) throws ExecutionException, InterruptedException {
        //Nascondi Tastiera
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
        //Fine Nascondi Tastiera


        String IMEI_Out = this.txtImei.getText().toString();
        String CodOp_Out = this.txtCodOperatore.getText().toString();
        AsyncLoader task = new AsyncLoader(Act_Accesso.this, "Login");

        int ImeiControllo = Integer.parseInt(IMEI_Out.substring(Math.max(0, IMEI_Out.length() - 5)));
        int GiornoControllo = Integer.parseInt(GiornoAccesso);
        String TotControllo = Integer.toString(ImeiControllo * GiornoControllo);
        if(CodOp_Out.equals(TotControllo))
        {

            Intent intent = new Intent(this, Act_ElencoAttivita.class);
            intent.putExtra("IDTerminale", "0");
            intent.putExtra("IDLetturista", "-");
            intent.putExtra("GiornoAccesso", GiornoAccesso);

            startActivity(intent);

        }
        else {
            task.ResWS = WebService.VerificaAccesso("ControllaAccessoV3", IMEI_Out, CodOp_Out, Versione,NameApp).toString();
            //task.IDLett = WebService._IDLetturista;
            task.IDLett = IDLETT;
            task.IDTerminale = WebService._IDTerminale;
            if (task.IDLett.contains("errore") || task.IDLett.trim().length() < 1 || task.IDLett == "0") {
                Toast.makeText(getApplicationContext(), "ATTENZIONE IL SEGUENTE TERMINALE NON RISULTA AFFIDATO AD UN LETTURISTA - CONTATTARE IL COORDINATORE PER RISOLVERE L'ANOMALIA", Toast.LENGTH_LONG).show();
                finish();
            } else {

                CalendarCert.setTimeInMillis((CalendarCert.getTimeInMillis() + SystemClock.currentThreadTimeMillis()));
                task.DataOraCerta = CalendarCert;
                task.GiornoAccesso = GiornoAccesso;
                task.execute();
            }
        }

    }
    @Override
    public void onBackPressed() {
        Toast.makeText(getApplicationContext(),"Grazie per aver usato SveltoDroid!",Toast.LENGTH_LONG).show();

        finish();
        return;
    }

    private  boolean RichiestaPermessi() {

        int permissionFlashLight = ContextCompat.checkSelfPermission(this,
                Manifest.permission.FLASHLIGHT);
        int permissionInternet = ContextCompat.checkSelfPermission(this,
                Manifest.permission.INTERNET);
        int permissionFineLocation = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        int permissionCamera = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA);
        int permissionReadPhoneState = ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_PHONE_STATE);
        int permissionWriteFile = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionReadPhoneState != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
        }
        if (permissionWriteFile != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionFlashLight != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.FLASHLIGHT);
        }
        if (permissionInternet != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.INTERNET);
        }
        if (permissionFineLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (permissionCamera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


    public void StampaDataOra(boolean a)
    {
        if(a) {
            CalendarCert =WebService._DataEora;

            DataOraCerta =new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(CalendarCert.getTime());

        }

    }

    public void StampaIMEI(boolean a)
    {

        String IMEI = txtImei.getText().toString();
        String Res;
        String Nom;
        String Desc;
        String val;
        if(a) {
            Res = WebService._IDTerminale;
            Nom = WebService._NomeLetturista;
            Desc = WebService._DescAttivita;

            if(Res.equals("TIMEOUT"))
            {
                Toast.makeText(getApplicationContext(), "ATTENZIONE, IMPOSSIBILE CONTATTARE SERVER. TROVARE UNA ZONA CON UN SEGNALE MIGLIORE", Toast.LENGTH_SHORT).show();

            }
            else if(!Res.contains("errore")) {
                lblIDTerm.setText("Terminale N°: " + Res);
                lblNomLetturista.setText("Operatore: " + Nom);
                lblDescAttivita.setText("Attività: " + Desc);
                val = WebService._DatiSIM;

                if(WebService._CiSonoCom) {
                    ListViewAdapters adapter = new ListViewAdapters(this, WebService._list);
                    adapter.cosa = "Comunicazioni";
                    listViewCom.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }

            }

        }
    }

    public void VerificaAggiornamenti()
    {
        Resources res = getResources();
        @SuppressLint({"StringFormatInvalid", "LocalSuppress"}) String text = String.format(res.getString(R.string.app_name), Versione);


        if(_Versioning.equals("TIMEOUT"))
        {
            Toast.makeText(getApplicationContext(), "ATTENZIONE, IMPOSSIBILE CONTATTARE SERVER. TROVARE UNA ZONA CON UN SEGNALE MIGLIORE", Toast.LENGTH_LONG).show();
        }
        else if(_Versioning.contains("Error WS"))
        {
            Toast.makeText(getApplicationContext(), "ATTENZIONE, IMPOSSIBILE CONTATTARE SERVER. TROVARE UNA ZONA CON UN SEGNALE MIGLIORE", Toast.LENGTH_LONG).show();

        }
        else if(_Versioning.equals(Versione))
        {
            lblVersion.setText(Versione);
            BtnLoggati.setEnabled(true);
            BtnAggiornaSoftware.setEnabled(false);
        }

        else if(Double.parseDouble(Versione) > Double.parseDouble(_Versioning))
        {
            lblVersion.setText(Versione);
            BtnLoggati.setEnabled(true);
            BtnAggiornaSoftware.setEnabled(true);
        }

        else
        {
            lblVersion.setText(Versione + " To " + _Versioning);
            BtnLoggati.setEnabled(false);
            BtnAggiornaSoftware.setEnabled(true);
        }
    }

    public void ScaricaAggiornamento(View view)
    {
        String url;
        url = "http://www.comerservizi.it/android/update/update.apk";
        mProgressDialog = new ProgressDialog(Act_Accesso.this);
        mProgressDialog.setMessage("Download Aggiornamenti in corso...");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setCancelable(true);

        mProgressDialog.show();
        DownloadReceiver DWR = new DownloadReceiver(new Handler());
        DWR.mProgressDialog = mProgressDialog;
        Intent intent = new Intent(this, DownloadService.class);
        intent.putExtra("url", url);
        intent.putExtra("receiver", DWR);
        startService(intent);

    }



}