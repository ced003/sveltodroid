package it.comerservizi.appsveltodroid;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by guido.palumbo on 03/12/2015.
 */
public class GPClass {

    DatabaseHelper dbHelper;
    SQLiteDatabase db;
    Context _context;
    public long LastMin;


    public void copy(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }


    public File[] getListFilesExternal() {

        //File sdCard = Environment.getExternalStorageDirectory();
        Integer Counter = 0;

        String storagePath = System.getenv("SECONDARY_STORAGE");

        File sdCard = new File(_context.getFilesDir().getAbsolutePath() );
        //File sdCard = new File(File.separator + "storage" + File.separator + "sdcard1");
        if(!sdCard.isDirectory())
            sdCard = new File(_context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS).getAbsolutePath() );


        File file[],file2[];
        File f = new File(String.valueOf(new File(sdCard.getAbsolutePath()+ File.separator + "SveltoDroidPhoto"+ File.separator)));
        File e = new File(String.valueOf(new File(sdCard.getAbsolutePath()+ File.separator + "SveltoDroidNTB"+ File.separator)));


        if (!f.exists()) {
            if (!f.mkdirs()) {
                Log.i("DB", "Impossibile creare la folder");
            }
        }
        if(f.isDirectory())
            file= f.listFiles();
        else
        {
            boolean success = (new File(String.valueOf(new File(sdCard.getAbsolutePath()+ File.separator + "SveltoDroidPhoto"+ File.separator)))).mkdir();
            file= f.listFiles();
        }

        if (!e.exists()) {
            if (!e.mkdirs()) {
                Log.i("DB", "Impossibile creare la folder");
            }
        }
        if(e.isDirectory())
            file2= e.listFiles();
        else
        {
            boolean success = (new File(String.valueOf(new File(sdCard.getAbsolutePath()+ File.separator + "SveltoDroidPhoto"+ File.separator)))).mkdir();
            file2= e.listFiles();
        }
        File[] finalfile = new File[file.length + file2.length];
        System.arraycopy(file, 0, finalfile, 0, file.length);
        System.arraycopy(file2, 0, finalfile, file.length, file2.length);


        return finalfile;

    }
    public File[] getListFilesExternalWithName(String nome) {

        //File sdCard = Environment.getExternalStorageDirectory();
        Integer Counter = 0;



        File sdCard = new File(_context.getFilesDir().getAbsolutePath() );

        //File sdCard = new File(File.separator + "storage" + File.separator + "sdcard1");

        File dir = new File(sdCard.getAbsolutePath()+ File.separator + "SveltoDroidPhoto"+ File.separator );
        File [] files;

        if (!dir.exists()) {
            if (!dir.mkdirs()) {
                Log.i("DB", "Impossibile creare la folder");
            }
        }
        final String pattName = nome;
        if(dir.isDirectory()){

            files = dir.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.contains(pattName);
                }
            });
        }
        else
        {
            boolean success = (new File(String.valueOf(new File(sdCard.getAbsolutePath()+ File.separator + "SveltoDroidPhoto"+ File.separator )))).mkdir();
            files = dir.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.contains(pattName);
                }
            });
        }




        return files;

    }


    public File[] getListFilesInternallWithName(String nome) {

        //File sdCard = Environment.getExternalStorageDirectory();
        Integer Counter = 0;



        File sdCard = new File(_context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS).getAbsolutePath());

        //File sdCard = new File(File.separator + "storage" + File.separator + "sdcard1");

        File dir = new File(sdCard.getAbsolutePath()+ File.separator + "SveltoDroidPhoto"+ File.separator );
        File [] files;

        if (!dir.exists()) {
            if (!dir.mkdirs()) {
                Log.i("DB", "Impossibile creare la folder");
            }
        }
        final String pattName = nome;
        if(dir.isDirectory()){

            files = dir.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.contains(pattName);
                }
            });
        }
        else
        {
            boolean success = (new File(String.valueOf(new File(sdCard.getAbsolutePath()+ File.separator + "SveltoDroidPhoto"+ File.separator )))).mkdir();
            files = dir.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.contains(pattName);
                }
            });
        }




        return files;

    }

    public File[] getListFiles() {

        //File sdCard = Environment.getExternalStorageDirectory();
        Integer Counter = 0;
        File sdCard = new File(_context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS).getAbsolutePath());
        File intCard = new File(_context.getFilesDir().getAbsolutePath() );

        sdCard.setReadable(Boolean.TRUE);
        sdCard.setWritable(Boolean.TRUE);
        intCard.setReadable(Boolean.TRUE);
        intCard.setWritable(Boolean.TRUE);
        //File sdCard = new File(File.separator + "storage" + File.separator + "sdcard0");
        //File intCard = new File(File.separator + "storage" + File.separator + "sdcard1");
        //if(!sdCard.isDirectory())
        //    sdCard = Environment.getExternalStorageDirectory();
        //if(!intCard.isDirectory())
        //    intCard = new File(_context.getFilesDir().getAbsolutePath());
        File file[], file2[], file3[];
        File f = new File(String.valueOf(new File(sdCard.getAbsolutePath()+ File.separator + "SveltoDroidPhoto"+ File.separator)));
        //File d = new File(String.valueOf(new File(intCard.getAbsolutePath()+ File.separator + "SveltoDroidPhoto"+ File.separator)));
        File e = new File(String.valueOf(new File(sdCard.getAbsolutePath()+ File.separator + "SveltoDroidNTB"+ File.separator)));

        //if(d.isDirectory())
        //    file= d.listFiles();
        //else
        //{
       //     boolean success = (new File(String.valueOf(new File(intCard.getAbsolutePath()+ File.separator + "SveltoDroidPhoto"+ File.separator)))).mkdir();
        //    file= d.listFiles();
        //}

        if(f.isDirectory())
            file2= f.listFiles();
        else
        {
            boolean success = (new File(String.valueOf(new File(sdCard.getAbsolutePath()+ File.separator + "SveltoDroidPhoto"+ File.separator)))).mkdir();
            file2= f.listFiles();
        }
        if(e.isDirectory())
            file3= e.listFiles();
        else
        {
            boolean success = (new File(String.valueOf(new File(sdCard.getAbsolutePath()+ File.separator + "SveltoDroidNTB"+ File.separator)))).mkdir();
            file3= e.listFiles();
        }
        File[] finalfile = new File[file2.length + file3.length];
        //System.arraycopy(file, 0, finalfile, 0, file.length);
        System.arraycopy(file2, 0, finalfile, 0, file2.length);
        System.arraycopy(file3, 0, finalfile, (file2.length), file3.length);


        return finalfile;

    }


    public File[] getListFilesInternal() {

        //File sdCard = Environment.getExternalStorageDirectory();
        Integer Counter = 0;
        File intCardsdCard = new File(_context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS).getAbsolutePath());
        File sdCard = new File(_context.getFilesDir().getAbsolutePath() );
        sdCard.setReadable(Boolean.TRUE);
        sdCard.setWritable(Boolean.TRUE);
        intCardsdCard.setReadable(Boolean.TRUE);
        intCardsdCard.setWritable(Boolean.TRUE);
        //File sdCard = new File(File.separator + "storage" + File.separator + "sdcard0");
        //File intCard = new File(File.separator + "storage" + File.separator + "sdcard1");
        //if(!sdCard.isDirectory())
        //    sdCard = Environment.getExternalStorageDirectory();
        //if(!intCard.isDirectory())
        //    intCard = new File(_context.getFilesDir().getAbsolutePath());
        File file[], file2[], file3[];
        File f = new File(String.valueOf(new File(sdCard.getAbsolutePath()+ File.separator + "SveltoDroidPhoto"+ File.separator)));
        //File d = new File(String.valueOf(new File(intCard.getAbsolutePath()+ File.separator + "SveltoDroidPhoto"+ File.separator)));
        File e = new File(String.valueOf(new File(sdCard.getAbsolutePath()+ File.separator + "SveltoDroidNTB"+ File.separator)));

        //if(d.isDirectory())
        //    file= d.listFiles();
        //else
        //{
        //     boolean success = (new File(String.valueOf(new File(intCard.getAbsolutePath()+ File.separator + "SveltoDroidPhoto"+ File.separator)))).mkdir();
        //    file= d.listFiles();
        //}

        if(f.isDirectory())
            file2= f.listFiles();
        else
        {
            boolean success = (new File(String.valueOf(new File(sdCard.getAbsolutePath()+ File.separator + "SveltoDroidPhoto"+ File.separator)))).mkdir();
            file2= f.listFiles();
        }
        if(e.isDirectory())
            file3= e.listFiles();
        else
        {
            boolean success = (new File(String.valueOf(new File(sdCard.getAbsolutePath()+ File.separator + "SveltoDroidNTB"+ File.separator)))).mkdir();
            file3= e.listFiles();
        }
        File[] finalfile = new File[file2.length + file3.length];
        //System.arraycopy(file, 0, finalfile, 0, file.length);
        System.arraycopy(file2, 0, finalfile, 0, file2.length);
        System.arraycopy(file3, 0, finalfile, (file2.length), file3.length);


        return finalfile;

    }

    public byte[] FileToByteOLD(String photoPath)
    {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap yourbitmap = BitmapFactory.decodeFile(photoPath, options);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Bitmap mutableBitmap = yourbitmap.copy(Bitmap.Config.ARGB_8888, true);
        mutableBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        //this will convert image to byte[]
        byte[] byteArrayImage = baos.toByteArray();
        Log.i("ByteArray", byteArrayImage.toString());
        return byteArrayImage;
    }

    public byte[] FileToByte(String photoPath)
    {

        byte[] byteArray = null;
        try
        {
            InputStream inputStream = new FileInputStream(new File(photoPath));
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] b = new byte[1024*8];
            int bytesRead =0;

            while ((bytesRead = inputStream.read(b)) != -1)
            {
                bos.write(b, 0, bytesRead);
            }

            byteArray = bos.toByteArray();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return byteArray;


    }
    public void RimuoviFotoRiaperti(String IDCarico, String IDAzienda, String CodiceUtente)
    {
        File a[];
        a = getListFiles();
        for(int i = 0; i < a.length; i++)
        {
            if(a[i].getName().toString().contains(IDCarico + "_" + IDAzienda + "_" + CodiceUtente + "_"))
                a[i].delete();
        }
        a = null;
    }
    public boolean appInstalledOrNot(String uri, Context cont) {
        PackageManager pm = cont.getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        }
        catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

    public boolean isAppInstalled(Context context, String packageName) {
        try {
            context.getPackageManager().getApplicationInfo(packageName, 0);
            return true;
        }
        catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public String ConvertiData(String StringData)
    {

        Date ToData;
        String DataResult;

        if(StringData==null)
            DataResult = StringData;
        else {
            try {
                ToData = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(StringData);
                DataResult = new SimpleDateFormat("dd/MM/yyyy").format(ToData);
            } catch (ParseException e) {
                DataResult = StringData;
            }
        }
        return DataResult;
    }
    public String ConvertiDataeOra(String StringData)
    {

        Date ToData;
        String DataResult;

        if(StringData==null)
            DataResult = StringData;
        else {
            try {
                ToData = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(StringData);
                DataResult = new SimpleDateFormat("dd/MM/yyyy HH:mm").format(ToData);
            } catch (ParseException e) {
                DataResult = StringData;
            }
        }
        return DataResult;
    }

    public String ConvertiSoloOra(String StringData)
    {

        Date ToData;
        String DataResult;

        if(StringData==null)
            DataResult = StringData;
        else {
            try {
                ToData = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(StringData);
                DataResult = new SimpleDateFormat("HH:mm:ss").format(ToData);
            } catch (ParseException e) {
                DataResult = StringData;
            }
        }
        return DataResult;
    }
    public String ConvertiSoloData(String StringData)
    {

        Date ToData;
        String DataResult;

        if(StringData==null)
            DataResult = StringData;
        else {
            try {
                ToData = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(StringData);
                DataResult = new SimpleDateFormat("dd/MM/yyyy").format(ToData);
            } catch (ParseException e) {
                DataResult = StringData;
            }
        }
        return DataResult;
    }

    public String RilevaPercentuale(Integer Totali, Integer Quanti)
    {
        return Double.toString((100 * Quanti) / Totali);
    }

    public boolean ControlloGiornoAccesso(Context context, Activity act, String GiornoAccesso)
    {

        Calendar calendar = Calendar.getInstance();
        String Giorno = Integer.toString(calendar.get(Calendar.DAY_OF_MONTH));
        if( !GiornoAccesso.equals(Giorno) )
        {
            Toast.makeText(context.getApplicationContext(), "ATTENZIONE OGNI GIORNO DEVI RIAVVIARE L'APP", Toast.LENGTH_LONG).show();
            return true;

        }
        else
            return false;
    }
    public String NullToEmpty(String val)
    {
        if(val == null)
            return "";
        else
        {
            return val;
        }
    }

    public long MinutiLastSync(Context context, Activity act)
    {
        LastMin = 0;
        Calendar calendari = Calendar.getInstance();
        SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date A1 = new Date();
        Date A2 = new Date();
        String S2="";
        String S1 =new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(calendari.getTime());
        dbHelper = new DatabaseHelper(context);
        db = dbHelper.getWritableDatabase();
        Cursor CLastSync  = dbHelper.query(db, "SELECT IdConfig FROM AndroConfig WHERE Type='LastSync'");
        if(CLastSync.getCount() > 0)
        {
            CLastSync.moveToFirst();
            S2 = CLastSync.getString(CLastSync.getColumnIndex("IdConfig"));
            try {
                A1 = dateformat.parse(S1);
                A2 = dateformat.parse(S2);
                long diff = TimeUnit.MILLISECONDS.toSeconds((A1.getTime() - A2.getTime()));
                long minuti = ((diff / 60));
                LastMin = minuti;
            }
            catch (Exception ex)
            {
                LastMin = 0;
            }
        }
        else
        {
            LastMin = 0;
        }
        return LastMin;
    }

    public void LastSync(Context context, Activity act)
    {
        /*
        long diff = date1.getTime() - date2.getTime();
        long seconds = diff / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;*/
        AlertDialog.Builder dialog = new AlertDialog.Builder(act);dialog
            .setTitle("!!MEMO!!")
            .setMessage("Ricordati di sincronizzare i dati, non lo hai mai fatto...")
            .setCancelable(false)
            .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            })
            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
        Calendar calendari = Calendar.getInstance();
        SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date A1 = new Date();
        Date A2 = new Date();
        String S2="";
        String S1 =new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(calendari.getTime());
        dbHelper = new DatabaseHelper(context);
        db = dbHelper.getWritableDatabase();
        Cursor CLastSync  = dbHelper.query(db, "SELECT IdConfig FROM AndroConfig WHERE Type='LastSync'");
        if(CLastSync.getCount() > 0)
        {
            CLastSync.moveToFirst();
            S2 = CLastSync.getString(CLastSync.getColumnIndex("IdConfig"));
            try
            {
                A1 = dateformat.parse(S1);
                A2 = dateformat.parse(S2);
                long diff = TimeUnit.MILLISECONDS.toSeconds((A1.getTime() - A2.getTime()));
                long minuti = ((diff/60));
                dialog.setMessage("Ricordati di sincronizzare i dati, non lo fai da " + Long.toString(minuti) + " minuti");
                AlertDialog MsgErrore = dialog.create();

                if(minuti>=240)
                {
                    MsgErrore.show();
                }
            }
            catch (Exception ex)
            {
                Toast.makeText(context.getApplicationContext(), "Contattare COMER Errore nel controllo ultima sincronizzazione: " + ex.toString(), Toast.LENGTH_LONG).show();
            }
        }
        else
        {
            AlertDialog MsgErrore = dialog.create();
            MsgErrore.show();

        }
    }


}
