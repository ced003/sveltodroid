package it.comerservizi.appsveltodroid;

/**
 * Created by guido.palumbo on 13/11/2015.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

public class ConnectionDetector extends BroadcastReceiver {

    private Context _context;
    public boolean VerificaInternet = false;
    public Act_Accesso MyAct_Accesso = null;
    private boolean BAccesso = false;
    public Act_ElencoAttivita MyElencoAttivita = null;
    private boolean BElencoAttivita = false;
    public ListaCarico MyListaCarico = null;
    public Act_Statistiche MyStatistiche = null;
    private boolean BListaCarico = false;
    private boolean BStatistiche = false;
    ImageView imgweb;

    public ConnectionDetector(Context context, ImageView iv){
        this._context = context;
        this.imgweb = iv;
    }

    public boolean isConnectingToInternet(){
        ConnectivityManager connectivity = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        return true;
                    }

        }
        return false;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        NetworkInfo info = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
        if(null != info)
        {

            String state = getNetworkStateString(info.getState());
            Log.i("ConnectionDetector", "Stato: "+state);

            if(state=="Disconnected" )
            {

                if(BAccesso)
                {

                    ControlliActAccesso(true);
                    Log.i("ConnectionDetector", "BAccesso è True in Disconnected");
                }
                else if(BElencoAttivita) {
                    Log.i("ConnectionDetector", "BElencoAttivita è True in Disconnected");
                    ControlliElencoAttivita(true);
                }
                else if(BListaCarico) {
                    Log.i("ConnectionDetector", "BListaCarico è True in Disconnected");
                    ControlliListaCarico(true);
                }
                else if(BStatistiche) {
                    Log.i("ConnectionDetector", "BStatistiche è True in Disconnected");
                    ControlliStatistiche(true);
                }

            }
            else
            {
                if(BAccesso)
                {
                    ControlliActAccesso(false);
                    Log.i("ConnectionDetector", "BAccesso è True in Connected");
                }
                else if(BElencoAttivita){
                    Log.i("ConnectionDetector", "BElencoAttivita è True in Connected");
                    ControlliElencoAttivita(false);
                }
                else if(BListaCarico) {
                    Log.i("ConnectionDetector", "BListaCarico è True in Connected");
                    ControlliListaCarico(false);
                }
                else if(BStatistiche) {
                    Log.i("ConnectionDetector", "BStatistiche è True in Connected");
                    ControlliStatistiche(false);
                }

            }
        }
    }


    private String getNetworkStateString(NetworkInfo.State state){
        String stateString = "Unknown";

        switch(state)
        {
            case CONNECTED:         stateString = "Connected";              break;
            case CONNECTING:        stateString = "Connecting";     break;
            case DISCONNECTED:      stateString = "Disconnected";   break;
            case DISCONNECTING:     stateString = "Disconnecting";  break;
            case SUSPENDED:         stateString = "Suspended";              break;
            default:                        stateString = "Unknown";                break;
        }

        return stateString;
    }

    public String WifiSSID()
    {
        ConnectivityManager connManager = (ConnectivityManager)_context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (mWifi.isConnected())
        {

            WifiManager wifiManager = (WifiManager) _context.getSystemService(Context.WIFI_SERVICE);
            WifiInfo info = wifiManager.getConnectionInfo();
            return info.getSSID().toString();
        }
        else
            return "";

    }
    public void setAct_AccessoHandler(Act_Accesso main){
        this.MyAct_Accesso=main;
        this.BAccesso = true;
    }
    public void setListaCarico(ListaCarico main){
        this.MyListaCarico=main;
        this.BListaCarico = true;
    }
    public void setStatisticheHandler(Act_Statistiche main){
        this.MyStatistiche=main;
        this.BStatistiche = true;
    }
    public void setElencoAttivitaHandler(Act_ElencoAttivita main){
        this.MyElencoAttivita=main;
        this.BElencoAttivita = true;
    }

    private void ControlliActAccesso(boolean b)
    {
        if(b)
        {
            MyAct_Accesso.imgWEB.setImageResource(R.drawable.internetred26);
            //MyAct_Accesso.BtnLoggati.setEnabled(false);
            MyAct_Accesso.txtCodOperatore.setEnabled(false);
            //MyAct_Accesso.StampaDataOra(false);
            //MyAct_Accesso.StampaIMEI(false);
            VerificaInternet = false;
            Toast.makeText(MyAct_Accesso.getApplicationContext(), "ATTENZIONE NON C'E' INTERNET!", Toast.LENGTH_LONG).show();
        }
        else
        {
            MyAct_Accesso.imgWEB.setImageResource(R.drawable.internetgreen26);
            //MyAct_Accesso.VerificaAggiornamenti();
            //MyAct_Accesso.BtnLoggati.setEnabled(true);
            MyAct_Accesso.txtCodOperatore.setEnabled(true);
            //MyAct_Accesso.StampaDataOra(true);
            //MyAct_Accesso.StampaIMEI(true);
            VerificaInternet = true;
        }
    }

    private void ControlliElencoAttivita(boolean b) {
        if (b)
        {
            MyElencoAttivita.EAimgWEB.setImageResource(R.drawable.internetred26);
            MyElencoAttivita.connessione = false;
            VerificaInternet = false;
            Toast.makeText(MyElencoAttivita.getApplicationContext(), "ATTENZIONE NON C'E' INTERNET!", Toast.LENGTH_LONG).show();
        } else {
            MyElencoAttivita.EAimgWEB.setImageResource(R.drawable.internetgreen26);
            MyElencoAttivita.connessione = true;
            VerificaInternet = true;
        }
    }
    private void ControlliListaCarico(boolean b) {
        if (b)
        {
            MyListaCarico.LCimgWEB.setImageResource(R.drawable.internetred26);
            VerificaInternet = false;
            Toast.makeText(MyListaCarico.getApplicationContext(), "ATTENZIONE NON C'E' INTERNET!", Toast.LENGTH_LONG).show();
        } else {
            MyListaCarico.LCimgWEB.setImageResource(R.drawable.internetgreen26);
            VerificaInternet = true;
        }
    }
    private void ControlliStatistiche(boolean b) {
        if (b)
        {
            MyStatistiche.STimgWEB.setImageResource(R.drawable.internetred26);
            VerificaInternet = false;
            Toast.makeText(MyStatistiche.getApplicationContext(), "Controllo connessione..", Toast.LENGTH_LONG).show();
        } else {
            MyStatistiche.STimgWEB.setImageResource(R.drawable.internetgreen26);
            VerificaInternet = true;
        }
    }
}