package it.comerservizi.appsveltodroid;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ComponentCallbacks2;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.camera2.CameraManager;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.crashlytics.android.Crashlytics;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Custom_CameraActivity extends Activity {

    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    private Camera mCamera;
    private CameraPreview mCameraPreview;
    ToggleButton btnTorcia;
    DatabaseHelper dbHelper;
    SQLiteDatabase db;
    String DataAttivita;
    String OraAttivita;
    String TipoFoto;
    String Misuratore;
    String Correttore;
    String TestoSuFoto;
    Button captureButton;
    Button NewPhotoButton;
    ImageView ImageView;
    private static File sdCard;
    private static File sdCardBKP;
    private GPClass gpc;
    public static Integer ContatoreNomeFoto = 0;
    Integer TipoFuoco = 0;
    private String operatore;

    String mCurrentPhotoPath;
    Uri fileUri ;
    RatingBar ratingBar;
    private Parameters mCameraParameters;
    private FrameLayout preview;
    private double SumOrientation = 0;
    private boolean sersorrunning;
    private TextView OverlayText,ErrorMessageCamera;
    public static Integer CountPhotoVC, CountPhotoVM, CountPhotoVB, CountPhotoVE, CountPhotoB1, CountPhotoB2,CountPhotoP1;
    public static String IDCarico, IDAzienda, CodiceUtente, FlagFoto, ContatoreFoto;
    LayoutInflater controlInflater = null;
    int a,b,c;
    CameraManager manager;

    String DefLong,GlobalConfig;
    String DefLat;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom__camera);
        onTrimMemory(ComponentCallbacks2.TRIM_MEMORY_COMPLETE);
        onTrimMemory(ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN);

        captureButton = (Button) findViewById(R.id.button_capture);
        NewPhotoButton = (Button) findViewById(R.id.btn_Nuova);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        captureButton.setEnabled(true);
        NewPhotoButton.setEnabled(false);
        CountPhotoVC = 0;
        CountPhotoVM = 0;
        CountPhotoVB = 0;
        CountPhotoVE = 0;
        CountPhotoB1 = 0;
        CountPhotoB2 = 0;
        ContatoreNomeFoto = 0;
        String casa;
        gpc = new GPClass();
        dbHelper = new DatabaseHelper(Custom_CameraActivity.this);
        db = dbHelper.getWritableDatabase();


        sdCard = new File(getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS).getAbsolutePath());
        sdCardBKP = new File(getFilesDir().getAbsolutePath() );
        sdCardBKP.setReadable(Boolean.TRUE);
        sdCardBKP.setWritable(Boolean.TRUE);
        sdCard.setReadable(Boolean.TRUE);
        sdCard.setWritable(Boolean.TRUE);

        Bundle extras = getIntent().getExtras();
        if(extras !=null) {
            CodiceUtente = extras.getString("CodiceUtente");
            IDCarico = extras.getString("IDCarico");
            IDAzienda = extras.getString("IDAzienda");
            DataAttivita = extras.getString("DataAttivita");
            OraAttivita = extras.getString("OraAttivita");
            Misuratore = extras.getString("Misuratore");
            Correttore = extras.getString("Correttore");
            CountPhotoVC = extras.getInt("CountPhotoVC");
            CountPhotoVM = extras.getInt("CountPhotoVM");
            CountPhotoVB = extras.getInt("CountPhotoVB");
            CountPhotoVE = extras.getInt("CountPhotoVE");
            CountPhotoB1 = extras.getInt("CountPhotoB1");
            CountPhotoB2 = extras.getInt("CountPhotoB2");
            CountPhotoP1 = extras.getInt("CountPhotoP1");
            TipoFoto = extras.getString("TipoFoto");
            DefLat = extras.getString("DefLat");
            DefLong = extras.getString("DefLong");
            operatore = extras.getString("operatore");
            GlobalConfig = extras.getString("GlobalConfig");
        }

        CodiceUtente=CodiceUtente.replace(String.valueOf((char) 160), " ").trim();
        IDCarico=IDCarico.replace(String.valueOf((char) 160), " ").trim();
        IDAzienda=IDAzienda.replace(String.valueOf((char) 160), " ").trim();
        DataAttivita=DataAttivita.replace(String.valueOf((char) 160), " ").trim();
        Misuratore=Misuratore.replace(String.valueOf((char) 160), " ").trim();
        Correttore=Correttore.replace(String.valueOf((char) 160), " ").trim();

        OraAttivita = new SimpleDateFormat("HH:mm:ss").format(new Date());
        OraAttivita=OraAttivita.replace(String.valueOf((char) 160), " ").trim();

        if (Misuratore == "" || Misuratore == null || Misuratore.isEmpty())
            Misuratore = "";


        //Confronto per attività - 6 Torino
        if(GlobalConfig.equals("6"))
        {
            //Se attività è torino, aggiungi coordinati sulla foto
            String appo;
            if(!DefLat.equals("0")) {
                appo = DefLat.substring(0, DefLat.indexOf(".")) + DefLat.substring(DefLat.indexOf("."), 5 + (DefLat.indexOf(".") + 1));
                DefLat = appo;
                appo = DefLong.substring(0, DefLong.indexOf(".")) + DefLat.substring(DefLat.indexOf("."), 5 + (DefLat.indexOf(".") + 1));
                DefLong = appo;
            }
            TestoSuFoto = CodiceUtente +" "+ DataAttivita +" " + OraAttivita +" "+ DefLat + " "+DefLong;

        }
        else
        {
            //Altrimenti testo standard (misuratore, corretto, data e ora)
            TestoSuFoto = Misuratore + " [" + Correttore + "] " + DataAttivita + " " + OraAttivita;
            TestoSuFoto = TestoSuFoto.replace("[] ", "");
        }
        btnTorcia = (ToggleButton) findViewById(R.id.btnTorcia);
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics ();
        display.getMetrics(outMetrics);
        float density  = getResources().getDisplayMetrics().density;
        Integer dpWidth  = outMetrics.widthPixels / Math.round(density);
        LinearLayout l = (LinearLayout) findViewById(R.id.LinearButton);
        ImageView = (ImageView) findViewById(R.id.imageView);
        Integer lH = l.getHeight();

        controlInflater = LayoutInflater.from(getBaseContext());
        View viewControl = controlInflater.inflate(R.layout.controlcameraoverlay, null);

        LayoutParams layoutParamsControl
                = new LayoutParams(LayoutParams.FILL_PARENT,
                LayoutParams.FILL_PARENT);
        LinearLayout.LayoutParams a = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);;
        a.setMargins(0,0,0,lH);
        viewControl.setLayoutParams(a);
        this.addContentView(viewControl, layoutParamsControl);


        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = (metrics.heightPixels);


        FrameLayout frame=(FrameLayout) findViewById(R.id.camera_preview);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(width, (height-160));
        lp.addRule(RelativeLayout.BELOW, R.id.OverlayTextCamera);
        frame.setLayoutParams(lp);

        preview = (FrameLayout) findViewById(R.id.camera_preview);
        OverlayText = (TextView) findViewById(R.id.OverlayTextCamera);
        ErrorMessageCamera = (TextView) findViewById(R.id.ErrorMessageCamera);
        ErrorMessageCamera.setText("");
        ErrorMessageCamera.setVisibility(View.INVISIBLE);
        OverlayText.setText(TestoSuFoto);
/*
        mCamera = getCameraInstance();
        mCameraParameters = mCamera.getParameters();
        mCameraParameters.set("orientation", "portrait");

        Camera.Size size=getBestPreviewSize(640, 480, mCameraParameters);
        Camera.Size pictureSize=getSmallestPictureSize(mCameraParameters);

        mCameraParameters.setFlashMode(Parameters.FLASH_MODE_OFF);
        mCameraParameters.setFocusMode(Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        mCameraParameters.setPictureSize(640, 480);
        if (size != null && pictureSize != null) {
            mCameraParameters.setPreviewSize(size.width, size.height);
        }
        mCameraParameters.setZoom(0);
        mCamera.setParameters(mCameraParameters);

        mCameraPreview = new CameraPreview(this, mCamera);
        preview.addView(mCameraPreview);
*/
        InizializzaFotocamera();
        captureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                captureButton.setEnabled(false);
                NewPhotoButton.setEnabled(false);
                ContatoreNomeFoto++;
                FlagFoto = FlagNameFoto();
                OverlayText.setText(TestoSuFoto);
                mCamera.takePicture(shutterCallback, null, mPicture);
            }
        });

        //VERIFICA ROTAZIONE TELEFONO
        Toast.makeText(getApplicationContext(), "Per poter scattare la foto, gira il telefono", Toast.LENGTH_SHORT).show();
        /*
        DA UTILIZZARE SE RIABILITATO IL SENSORE
        SensorManager mySensorManager;
        mySensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        List<Sensor> mySensors = mySensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER);
         */

    }
    private void InizializzaFotocamera()
    {

        OverlayText.setText(TestoSuFoto);
        mCamera = getCameraInstance();
        mCameraParameters = mCamera.getParameters();
        mCameraParameters.set("orientation", "portrait");

        Camera.Size size=getBestPreviewSize(640, 480, mCameraParameters);
        Camera.Size pictureSize=getSmallestPictureSize(mCameraParameters);

        mCameraParameters.setFlashMode(Parameters.FLASH_MODE_OFF);
        mCameraParameters.setFocusMode(Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        mCameraParameters.setPictureSize(640, 480);
        if (size != null && pictureSize != null) {
            mCameraParameters.setPreviewSize(size.width, size.height);
        }
        mCameraParameters.setZoom(0);
        mCamera.setParameters(mCameraParameters);
        try {
            mCameraPreview.getHolder().removeCallback(mCameraPreview);
        }catch (Exception ex)
        {
        }

        mCameraPreview = new CameraPreview(this, mCamera);
        btnTorcia.setChecked(btnTorcia.isChecked());
        if(btnTorcia.isChecked())
        {
            AttivaTorcia(btnTorcia);
        }
        preview.addView(mCameraPreview);
        preview.setVisibility(View.VISIBLE);
        ImageView.setVisibility(View.INVISIBLE);
        preview.setOnLongClickListener(new LinearLayout.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                    captureButton.setEnabled(false);
                    NewPhotoButton.setEnabled(false);
                    ContatoreNomeFoto++;
                    FlagFoto = FlagNameFoto();
                    OverlayText.setText(TestoSuFoto);
                    mCamera.takePicture(shutterCallback, null, mPicture);
                    return true;
            }

        });
    }
/*
    ANALIZZA LA ROTAZIONE DEL DISPOSITIVO PER BLOCCARE LO SCATTO
    private SensorEventListener mySensorEventListener = new SensorEventListener() {

        @Override
        public void onSensorChanged(SensorEvent event) {
            // TODO Auto-generated method stub

            if(event.values[0]<-0.5 && event.values[2]<7)
            {
                ErrorMessageCamera.setVisibility(View.VISIBLE);
                ErrorMessageCamera.setText("Lo stai girando verso destra, devi girare verso sinistra");
            }
            else{
                ErrorMessageCamera.setVisibility(View.INVISIBLE);
                ErrorMessageCamera.setText("");
            }
            a = (int)event.values[0];
            b = (int)event.values[1];
            c = (int)event.values[2];
            if(a>-0.2 && (b>-3 && b<4)) {
                ratingBar.setProgress(6-Math.abs(b));
            }
            else
                ratingBar.setProgress(0);

            if(ratingBar.getProgress()>3) {
                if (!NewPhotoButton.isEnabled())
                    captureButton.setEnabled(true);
            }
            else
                captureButton.setEnabled(false);
        }
        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            // TODO Auto-generated method stub


        }
    };
*/

    @Override
    protected void onResume()
    {
        super.onResume();
        Crashlytics.log(null);
        Crashlytics.log("GlobalConfig: " + GlobalConfig + " - Operatore: " + operatore + " - Foto Scattate: " + CounterFoto());
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();

        if (mCamera != null) {
            //mCamera.setPreviewCallback(null);
            mCameraPreview.getHolder().removeCallback(mCameraPreview);
            mCamera.release();
        }
    }

    @Override
    protected void onStop()
    {
        super.onStop();
    }
    //Suono scatto
    private final Camera.ShutterCallback shutterCallback = new Camera.ShutterCallback() {
        public void onShutter() {
            AudioManager mgr = (AudioManager) getSystemService(getApplicationContext().AUDIO_SERVICE);
            mgr.playSoundEffect(AudioManager.FLAG_PLAY_SOUND);
        }
    };
    /**
     * Helper method to access the camera returns null if it cannot get the
     * camera or does not exist
     *
     * @return
     */
    private Camera getCameraInstance() {
        Camera camera = null;
        try {
            camera = Camera.open();
        } catch (Exception e) {
            // cannot get camera or does not exist
        }
        return camera;
    }

    PictureCallback mPicture = new PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            File pictureFile = getOutputMediaFile(1);
            if (pictureFile == null) {
                Toast.makeText(getApplicationContext(), "ATTENZIONE LA FOTO NON E' VENUTA [Errore F02]", Toast.LENGTH_SHORT).show();
                return;
            }
            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
                captureButton.setEnabled(false);

                btnTorcia.setEnabled(false);
                NewPhotoButton.setEnabled(true);
                ScriviSuFoto(pictureFile.getAbsolutePath());
            } catch (FileNotFoundException e) {


                captureButton.setEnabled(false);
                NewPhotoButton.setEnabled(true);
                Toast.makeText(getApplicationContext(), "ATTENZIONE LA FOTO NON E' VENUTA [Errore F00: "+e.getMessage().toString(), Toast.LENGTH_SHORT).show();

            } catch (IOException e) {

                captureButton.setEnabled(false);
                NewPhotoButton.setEnabled(true);
                Toast.makeText(getApplicationContext(), "ATTENZIONE LA FOTO NON E' VENUTA [Errore F01: "+e.getMessage().toString(), Toast.LENGTH_SHORT).show();
            }
        }
    };

    private static File getOutputMediaFile(Integer type) {

        File mediaStorageDir = new File(sdCard.getAbsolutePath()+ File.separator + "SveltoDroidPhoto");
        File mediaStorageDir2 = new File(sdCardBKP.getAbsolutePath()+ File.separator + "SveltoDroidPhoto");
        File mediaStorageDir3 = new File(sdCardBKP.getAbsolutePath()+ File.separator + "SveltoDroidNTB");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("MyCameraApp", "failed to create directory mediaStorageDir");
                return null;
            }
        }
        if (!mediaStorageDir2.exists()) {
            if (!mediaStorageDir2.mkdirs()) {
                Log.d("MyCameraApp", "failed to create directory mediaStorageDir2");
                return null;
            }
        }
        if (!mediaStorageDir3.exists()) {
            if (!mediaStorageDir3.mkdirs()) {
                Log.d("MyCameraApp", "failed to create directory mediaStorageDir3");
                return null;
            }
        }
        File mediaFile;
        if (type == 1) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + IDCarico + "_" + IDAzienda + "_" + CodiceUtente + "_" + FlagFoto + "_" + ContatoreNomeFoto + ".jpg");
            return mediaFile;
        } else {
            return null;
        }


    }

    private void ScriviSuFoto(String photoPath)
    {

        OraAttivita = new SimpleDateFormat("HH:mm:ss").format(new Date());
        OraAttivita=OraAttivita.replace(String.valueOf((char) 160), " ").trim();

        if(GlobalConfig.equals("6"))
        {
            TestoSuFoto = CodiceUtente +" "+ DataAttivita +" " + OraAttivita +" "+ DefLat + " "+DefLong;
        }
        else
        {
            TestoSuFoto = Misuratore + " [" + Correttore + "] " + DataAttivita + " " + OraAttivita;
            TestoSuFoto = TestoSuFoto.replace("[] ", "");
        }

        String FirstPath;
        FirstPath= "";
        BitmapFactory.Options options = new BitmapFactory.Options();
        //options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        options.inDither = true;
        Bitmap bitmap = BitmapFactory.decodeFile(photoPath, options);

        bitmap.setDensity(DisplayMetrics.DENSITY_280);
        Bitmap mutableBitmap = bitmap.copy(Bitmap.Config.RGB_565, true);
        Canvas canvas = new Canvas(mutableBitmap);
        canvas.setDensity(DisplayMetrics.DENSITY_280);
        Paint paint = new Paint();
        //paint.setColor(Color.BLACK);
        paint.setTextSize(17);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        //Rect rettangolo = new Rect();
        //rettangolo.set(0, 430, 640, 480);
        //canvas.drawRect(rettangolo, paint);
        paint.setColor(Color.RED);
        try {
            mutableBitmap.setDensity(DisplayMetrics.DENSITY_280);
            photoPath = photoPath.replace(".jpg",".NTB");
            photoPath = photoPath.replace(sdCard.toString(),sdCardBKP.toString());
            photoPath = photoPath.replace("SveltoDroidPhoto","SveltoDroidNTB");
            mutableBitmap.compress(Bitmap.CompressFormat.JPEG, 90, new FileOutputStream(new File(photoPath)));
            try
            {
                gpc.copy(new File(photoPath),new File(photoPath.replace(sdCardBKP.toString(),sdCard.toString())));
            }catch(Exception ex)
            {
                Toast.makeText(getApplicationContext(), "ATTENZIONE VERIFICA LA FOTO IN GALLERIA, ALTRIMENTI RISCATTARE [Errore F04: "+ex.getMessage().toString(), Toast.LENGTH_SHORT).show();
            }
            canvas.drawText(TestoSuFoto, 5, 475, paint);
            photoPath = photoPath.replace(".NTB",".jpg");
            photoPath = photoPath.replace("SveltoDroidNTB","SveltoDroidPhoto");
            photoPath = photoPath.replace(sdCardBKP.toString(),sdCard.toString());
            mutableBitmap.compress(Bitmap.CompressFormat.JPEG, 90, new FileOutputStream(new File(photoPath)));

            FirstPath = photoPath;
            try
            {
                gpc.copy(new File(photoPath),new File(photoPath.replace(sdCard.toString(),sdCardBKP.toString())));
            }catch(Exception ex)
            {
                Toast.makeText(getApplicationContext(), "ATTENZIONE VERIFICA LA FOTO IN GALLERIA, ALTRIMENTI RISCATTARE [Errore F05: "+ex.getMessage().toString(), Toast.LENGTH_SHORT).show();

            }
            /*
            if(FirstPath.trim().length()<1)
            FirstPath = photoPath;
            photoPath = photoPath.replace(".jpg","");
            photoPath = photoPath.replace(sdCard.toString(),sdCardBKP.toString());
            mutableBitmap.compress(Bitmap.CompressFormat.JPEG, 90, new FileOutputStream(new File(photoPath)));
*/
            final File image;
            image = new File(FirstPath);
            if(image.exists())
            {
                runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    setPic(image);
                }
            });
            }
            //Il contatore viene incrementato se la foto è stata salvata e sovrascritta

            ContatoreFoto = CounterFoto();
            // dest is Bitmap, if you want to preview the final image, you can display it on screen also before saving
        } catch (FileNotFoundException e) {
            Toast.makeText(getApplicationContext(), "ATTENZIONE LA FOTO NON E' VENUTA [Errore F03: "+e.getMessage().toString(), Toast.LENGTH_SHORT).show();
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void setPic(File image) {
        // Get the dimensions of the View
        int targetW = preview.getWidth();
        int targetH = preview.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inPreferredConfig = Bitmap.Config.RGB_565;
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = 640;
        int photoH = 480;
/*
Prelevare dimensioni da fotocamera
        CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraDevice.getId());
        Size[] jpegSizes = null;
        if (characteristics != null) {
            jpegSizes = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP).getOutputSizes(ImageFormat.JPEG);
        }*/

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;
        try {
            Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(), bmOptions);

            ImageView.setImageBitmap(bitmap);

            OverlayText.setText("");
            preview.removeView(mCameraPreview);
            ImageView.setVisibility(View.VISIBLE);
        }catch( OutOfMemoryError outmemo)
        {
            System.gc();
            Toast.makeText(getApplicationContext(), "Attenzione, verifica la foto in galleria... potrebbe non essere venuta [ " + outmemo.toString() + " ]", Toast.LENGTH_SHORT).show();
        }
    }

    public void AttivaTorcia(View view)
    {
        if(btnTorcia.getText().equals("ON")) {
            Parameters p = mCamera.getParameters();
            p.setFlashMode(Parameters.FLASH_MODE_TORCH);
            mCamera.setParameters(p);
        }
        else
        {
            Parameters p = mCamera.getParameters();
            p.setFlashMode(Parameters.FLASH_MODE_OFF);
            mCamera.setParameters(p);
        }
    }


    public void NuovaFoto(View view)
    {
        InizializzaFotocamera();
        btnTorcia.setEnabled(true);
        captureButton.setEnabled(true);
        NewPhotoButton.setEnabled(false);
    }

    public void TornaIndietro(View view)
    {
        if(ControlloAssenzaScatto(TipoFoto)) {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("B1", Integer.toString(CountPhotoB1));
            returnIntent.putExtra("B2", Integer.toString(CountPhotoB2));
            returnIntent.putExtra("VM", Integer.toString(CountPhotoVM));
            returnIntent.putExtra("VC", Integer.toString(CountPhotoVC));
            returnIntent.putExtra("VE", Integer.toString(CountPhotoVE));
            returnIntent.putExtra("VB", Integer.toString(CountPhotoVB));
            returnIntent.putExtra("P1", Integer.toString(CountPhotoP1));
            setResult(Act_CambiaIndirizzo.RESULT_OK, returnIntent);
            finish();
        }
        else
        {
            QualiFotoMancano();
        }
    }

    private Camera.Size getBestPreviewSize(int width, int height,
                                           Camera.Parameters parameters) {
        Camera.Size result=null;

        for (Camera.Size size : parameters.getSupportedPreviewSizes()) {
            if (size.width <= width && size.height <= height) {
                if (result == null) {
                    result=size;
                }
                else {
                    int resultArea=result.width * result.height;
                    int newArea=size.width * size.height;

                    if (newArea > resultArea) {
                        result=size;
                    }
                }
            }
        }

        return(result);
    }

    private Camera.Size getSmallestPictureSize(Camera.Parameters parameters) {
        Camera.Size result=null;

        for (Camera.Size size : parameters.getSupportedPictureSizes()) {
            if (result == null) {
                result=size;
            }
            else {
                int resultArea=result.width * result.height;
                int newArea=size.width * size.height;

                if (newArea < resultArea) {
                    result=size;
                }
            }
        }

        return(result);
    }

    @Override
    public void onBackPressed() {

        if(ControlloAssenzaScatto(TipoFoto)) {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("B1", Integer.toString(CountPhotoB1));
            returnIntent.putExtra("B2", Integer.toString(CountPhotoB2));
            returnIntent.putExtra("VM", Integer.toString(CountPhotoVM));
            returnIntent.putExtra("VC", Integer.toString(CountPhotoVC));
            returnIntent.putExtra("VE", Integer.toString(CountPhotoVE));
            returnIntent.putExtra("VB", Integer.toString(CountPhotoVB));
            returnIntent.putExtra("P1", Integer.toString(CountPhotoP1));
            setResult(Act_CambiaIndirizzo.RESULT_OK, returnIntent);
            finish();
        }
        else
        {
            QualiFotoMancano();
        }
    }

    private boolean ControlloAssenzaScatto(String TipoFoto)
    {
        boolean ver = false;
        if(TipoFoto.equals("B1") && CountPhotoB1>0)
            ver = true;
        else if(TipoFoto.equals("B2") && CountPhotoB2>0)
            ver = true;
        else if(TipoFoto.equals("VM") && CountPhotoVM>0)
            ver = true;
        else if(TipoFoto.equals("VC") && CountPhotoVC>0)
            ver = true;
        else if(TipoFoto.equals("VE") && CountPhotoVE>0)
            ver = true;
        else if(TipoFoto.equals("VB") && CountPhotoVB>0)
            ver = true;
        else if(TipoFoto.equals("P1") && CountPhotoP1>0)
            ver = true;
        else
            ver = false;

        return ver;
    }

    public void QualiFotoMancano()
    {
        if(CountPhotoB1==0 && TipoFoto.equals("B1"))
            Toast.makeText(getApplicationContext(), "Mancano le foto Nota Uno", Toast.LENGTH_SHORT).show();
        else if(CountPhotoB2==0 && TipoFoto.equals("B2"))
            Toast.makeText(getApplicationContext(), "Mancano le foto Nota Due", Toast.LENGTH_SHORT).show();
        else if(CountPhotoVM==0 && TipoFoto.equals("VM"))
            Toast.makeText(getApplicationContext(), "Mancano le foto VM", Toast.LENGTH_SHORT).show();
        else if(CountPhotoVC==0 && TipoFoto.equals("VC"))
            Toast.makeText(getApplicationContext(), "Mancano le foto VC", Toast.LENGTH_SHORT).show();
        else if(CountPhotoVE==0 && TipoFoto.equals("VE"))
            Toast.makeText(getApplicationContext(), "Mancano le foto VE", Toast.LENGTH_SHORT).show();
        else if(CountPhotoVB==0 && TipoFoto.equals("VB"))
            Toast.makeText(getApplicationContext(), "Mancano le foto VB", Toast.LENGTH_SHORT).show();
        else if(CountPhotoP1==0 && TipoFoto.equals("P1"))
            Toast.makeText(getApplicationContext(), "Mancano le foto della programmazione", Toast.LENGTH_SHORT).show();
    }


    public String CounterFoto()
    {
        String result = "";

        if(TipoFoto.equals("B1")) {
            CountPhotoB1++;
            result = CountPhotoB1.toString();
        }
        else if(TipoFoto.equals("B2")){
            CountPhotoB2++;
            result = CountPhotoB2.toString();
        }
        else if(TipoFoto.equals("VM")){
            CountPhotoVM++;
            result = CountPhotoVM.toString();
        }
        else if(TipoFoto.equals("VC")){
            CountPhotoVC++;
            result = CountPhotoVC.toString();
        }
        else if(TipoFoto.equals("VE")){
            CountPhotoVE++;
            result = CountPhotoVE.toString();
        }
        else if (TipoFoto.equals("VB")){
            CountPhotoVB++;
            result = CountPhotoVB.toString();
        }
        else if (TipoFoto.equals("P1")){
            CountPhotoP1++;
            result = CountPhotoP1.toString();
        }
        else
            result = "0";
        return result;
    }
    public String FlagNameFoto()
    {
        String result;
        if(TipoFoto.equals("B1"))
        {
            result = "B1";
        }
        else if(TipoFoto.equals("B2"))
        {
            result = "B2";
        }
        else if(TipoFoto.equals("VM"))
        {
            result = "VM";
        }
        else if(TipoFoto.equals("VC"))
        {
            result = "VC";
        }
        else if(TipoFoto.equals("VE"))
        {
            result = "VE";
        }
        else if(TipoFoto.equals("VB"))
        {
            result = "VB";
        }
        else if(TipoFoto.equals("P1"))
        {
            result = "P1";
        }
        else
            result = "XX";

        return result;
    }

}