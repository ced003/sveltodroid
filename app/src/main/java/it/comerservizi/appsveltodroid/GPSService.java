package it.comerservizi.appsveltodroid;

/**
 * Created by guido.palumbo on 26/05/2016.
 */

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.LocationManager;
import android.util.Log;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.net.SocketTimeoutException;
import java.util.Calendar;

public class GPSService extends IntentService
{
    private static final String TAG = "GPSService";
    private LocationManager mLocationManager = null;
    private static final int LOCATION_INTERVAL = 1000;
    private static final float LOCATION_DISTANCE = 10f;
    private GPSHelper gps;
    private String IDTerminale;
    private String IDOperatore;
    private String SSID;
    private Context context;
    private DatabaseHelper dbHelper;
    private SQLiteDatabase db;
    private String GlobalConfig;
    private String servizio = "";
    private String response = "";
    private Integer contatore;
    public String _Errore;

    public GPSService() {
        super("LogService");
        Log.i(TAG,"Servizio GPS");

    }
    @Override
    public void onCreate()
    {
        super.onCreate();
        contatore = 0;
        this.context = getApplicationContext();

        dbHelper = new DatabaseHelper(context);
        db = dbHelper.getWritableDatabase();
        Cursor c = db.rawQuery("SELECT IdConfig FROM AndroConfig WHERE Type = 'sGlobalConfiguration'", null);

        if (c != null)
            c.moveToFirst();

        GlobalConfig = c.getString(c.getColumnIndex("IdConfig"));
    }

    @Override
    protected void onHandleIntent(Intent i)
    {
        IDTerminale = i.getStringExtra("IDTerminale");
        IDOperatore = i.getStringExtra("IDOperatore");
        SSID = i.getStringExtra("SSID");
        Log.i(TAG,"Servizio GPS onHandleIntent");
        int n=0;
        while(true)
        {
            System.gc();
            gps = new GPSHelper();
            gps.GPSContesto(getApplicationContext());
            Log.i(TAG,"Servizio GPS While");
            if(gps.StatoGPS())
            {
                if(gps.getLatitude()!=0.0 && gps.getLatitude()!=0)
                {
                    db.execSQL("DELETE FROM AndroConfig WHERE Type='Latitudine' OR Type='Longitudine'");
                    db.execSQL("INSERT INTO AndroConfig VALUES ('" +  Double.toString(gps.getLatitude()) + "', 'Latitudine')");
                    db.execSQL("INSERT INTO AndroConfig VALUES ('" + Double.toString(gps.getLongitude()) + "', 'Longitudine')");
                }
                    SendCoordinate();

            }
            else
                Log.i(TAG, "NO GPS");
            try {
                Thread.sleep(1800000);
            }
            catch (InterruptedException e)
            {
                Log.i(TAG, "ERRORE: " + e.toString());
            }
        }
    }

    @Override
    public void onDestroy()
    {
        Log.i(TAG, "Distruzione Service");
    }

    public Boolean SendCoordinate() {

        this.servizio = "SendCoordinate";
        response = servizio+"Result";
        String SOAP_ACTION = "http://tempuri.org/" + servizio;
        String METHOD_NAME = servizio;
        String NAMESPACE = "http://tempuri.org/";
        String URL;
        Calendar c = Calendar.getInstance();
        if(SSID.contains("cs1"))
            URL = "http://cs-storage/wsTrmLetture/wsTrmLetture.asmx?wsdl";
        else
            URL = "http://web.comerservizi.net:8083/wsTrmLetture/wsTrmLetture.asmx?wsdl";

        try {
            Log.i(TAG, "Inizio WS RecuperaDatiSIM");
            SoapObject Request = new SoapObject(NAMESPACE, METHOD_NAME);
            Request.addProperty("sGlobalConfiguration", this.GlobalConfig);
            Request.addProperty("IDTerminale", this.IDTerminale);
            Request.addProperty("IDOperatore", this.IDOperatore);
            Request.addProperty("Latitudine", Double.toString(gps.getLatitude()));
            Request.addProperty("Longitudine", Double.toString(gps.getLongitude()));

            SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            soapEnvelope.dotNet = true;
            soapEnvelope.setOutputSoapObject(Request);

            HttpTransportSE transport = new HttpTransportSE(URL,30000);
            transport.call(SOAP_ACTION, soapEnvelope);
            Log.i(TAG, "Invia Coordinate per Monitoraggio - POSITIVO");
            return true;
        } catch (SocketTimeoutException et) {

            _Errore = "Error WS: " + et.getMessage() + " - Cause: "+ et.getCause() + " - ex: " + et.toString();
            Log.i(TAG, "TIMEOUT ["+_Errore+"]");
            return false;
        }
        catch( Exception ex )
        {
            _Errore = "Error WS: " + ex.getMessage() + " - Cause: "+ ex.getCause() + " - ex: " + ex.toString();
            Log.i(TAG, "FNEGATIVO ["+_Errore+"]");
            return false;
        }
    }
}