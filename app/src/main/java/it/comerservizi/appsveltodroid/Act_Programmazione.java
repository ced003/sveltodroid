package it.comerservizi.appsveltodroid;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Act_Programmazione extends AppCompatActivity {

    private Integer CountPhotoP1;
    private String CodiceUtente, IDCarico, IDAzienda, DataAttivita, OraAttivita, TipoFoto, DefLat, DefLong, operatore, GlobalConfig;
    GPClass GPC;
    DatabaseHelper dbHelper;
    SQLiteDatabase db;
    Calendar CalendarCert;
    private TextView lblNome;
    private TextView lblIndirizzo;
    private TextView lblMatricola;
    private TextView lblCU;
    private TextView lblComune;
    private EditText txtData;
    private Spinner cmbDaOra;
    private Spinner cmbAOra;
    private List<String> spinnerAOra, spinnerDaOra;
    boolean ConfermatoDato = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__programmazione);

        Bundle extras = getIntent().getExtras();
        if(extras !=null)
        {
            CodiceUtente = extras.getString("CodiceUtente");
            IDCarico = extras.getString("IDCarico");
            IDAzienda = extras.getString("IDAzienda");
            DataAttivita = extras.getString("DataAttivita");
            OraAttivita = extras.getString("OraAttivita");
            CountPhotoP1 = extras.getInt("CountPhotoP1");
            TipoFoto = extras.getString("TipoFoto");
            DefLat = extras.getString("DefLat");
            DefLong = extras.getString("DefLong");
            operatore = extras.getString("operatore");
            GlobalConfig = extras.getString("GlobalConfig");
            CalendarCert = Calendar.getInstance();
        }
        lblComune = (TextView) findViewById(R.id.lblComune);
        lblNome = (TextView) findViewById(R.id.lblNome);
        lblIndirizzo = (TextView) findViewById(R.id.lblIndirizzo);
        lblMatricola = (TextView) findViewById(R.id.lblMatricola);
        lblCU = (TextView) findViewById(R.id.lblCU);
        txtData = (EditText) findViewById(R.id.txtData);
        cmbDaOra = (Spinner) findViewById(R.id.cmbDaOra);
        cmbAOra = (Spinner) findViewById(R.id.cmbAOra);

        dbHelper = new DatabaseHelper(Act_Programmazione.this);
        db = dbHelper.getWritableDatabase();

        Cursor cursor = dbHelper.query(db, "SELECT  MatricolaMis, Nominativo, CAP, Localita, Provincia, Toponimo, Strada, Civico, CodiceUtente FROM utenti WHERE CodiceUtente = '" + CodiceUtente + "' AND idCarico = '"+IDCarico+"' AND  idAzienda = '"+IDAzienda+"'  ");

        if (cursor != null)
        {
            cursor.moveToFirst();
            while (cursor.isAfterLast() == false)
            {
                lblComune.setText(cursor.getString(cursor.getColumnIndex("CAP")) + " "  + cursor.getString(cursor.getColumnIndex("Localita")) + " " +cursor.getString(cursor.getColumnIndex("Provincia")));
                lblNome.setText(cursor.getString(cursor.getColumnIndex("Nominativo")) );
                lblIndirizzo.setText(cursor.getString(cursor.getColumnIndex("Toponimo")) + " "  + cursor.getString(cursor.getColumnIndex("Strada")) + " " +cursor.getString(cursor.getColumnIndex("Civico")));
                lblMatricola.setText(cursor.getString(cursor.getColumnIndex("MatricolaMis")) );
                lblCU.setText(cursor.getString(cursor.getColumnIndex("CodiceUtente")) );
                cursor.moveToNext();
            }
        }
        cursor.close();


        String[] myResArray = getResources().getStringArray(R.array.Orari);
        List<String> myResArrayList = Arrays.asList(myResArray);
        spinnerAOra = new ArrayList<String>(myResArrayList);
        spinnerDaOra = new ArrayList<String>(myResArrayList);
        for (int i = 0; i < 24; i++) {
            spinnerAOra.add(String.format("%02d", (i + 1)));
            spinnerDaOra.add(String.format("%02d", (i + 1)));
        }

        ArrayAdapter<String> SpinnDaOra = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item3, getApplicationContext().getResources().getStringArray(R.array.Orari));
        cmbDaOra.setAdapter(SpinnDaOra);
        ArrayAdapter<String> SpinnAOra = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item3, getApplicationContext().getResources().getStringArray(R.array.Orari));
        cmbAOra.setAdapter(SpinnAOra);

    }


    @Override
    protected void onStart()
    {
        super.onStart();
        Log.i("response", "Act_RilevaDati onStart");
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        Log.i("response", "Act_RilevaDati OnPause");
    }

    @Override
    protected void onResume() {
        super.onResume();


        /* CONTROLLO DATA AUTOMATICA */
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                if(Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME) != 1)
                {
                    Toast.makeText(getApplicationContext(), "ATTENZIONE HAI DISABILITATO LA DATA E ORA AUTOMATICA - L'APP NON PUO' FUNZIONARE", Toast.LENGTH_LONG).show();
                    this.finishAffinity();
                }
                else
                {
                    CalendarCert.setTimeInMillis((CalendarCert.getTimeInMillis() + SystemClock.currentThreadTimeMillis()));
                    Log.i("response", "Act_Programmazione onResume");
                    findViewById(R.id.lblMatricola).requestFocus();

                }
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        /* CONTROLLO DATA AUTOMATICA */



    }

    public void SalvaLavoro(View view) {


        if (CountPhotoP1 < 1)
        {
            Toast.makeText(getApplicationContext(), "Devi scattare almeno una foto..", Toast.LENGTH_LONG).show();
            return;
        }

        AlertDialog.Builder dialogDatoLettura = new AlertDialog.Builder(Act_Programmazione.this);
        dialogDatoLettura
                .setTitle("CONTROLLA PROGRAMMAZIONE")
                .setMessage("Sei sicuro dei dati inseriti? Attenzione, non potrai tornare indietro...")
                .setCancelable(false)
                .setPositiveButton("Si, sono sicuro", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ConfermatoDato = true;
                        SalvaDatiDB();
                        dialog.cancel();
                    }
                })
                .setNegativeButton("No, voglio controllare", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ConfermatoDato = false;
                        SalvaDatiDB();
                        dialog.cancel();
                    }
                });
        AlertDialog MsgDatoLettura = dialogDatoLettura.create();
        MsgDatoLettura.show();

    }

    public void SalvaDatiDB()
    {

        String DaOra, AOra, AffidaOperatore;
        if(ConfermatoDato) {
            if (cmbDaOra.getSelectedItemPosition() > 0)
                DaOra = spinnerDaOra.get(cmbDaOra.getSelectedItemPosition()).toString().trim();
            else
                DaOra = "";

            if (cmbAOra.getSelectedItemPosition() > 0)
                AOra = spinnerAOra.get(cmbAOra.getSelectedItemPosition()).toString().trim();
            else
                AOra = "";

            if (DaOra.compareTo(AOra) >= 0) {
                Toast.makeText(getApplicationContext(), "Attenziona l'ora di inizio non può essere inferiore o uguale alla data di fine appuntamento", Toast.LENGTH_LONG).show();
                return;
            }

            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");


            try {
                Calendar c = Calendar.getInstance();
                df.setLenient(false);
                Date DataDecisa = df.parse(txtData.getText().toString().trim());
                Date DataAdesso = c.getTime();
                if (DataAdesso.compareTo(DataDecisa) >= 0) {
                    Toast.makeText(getApplicationContext(), "Attenzione, la data inserita non può essere inferiore o uguale a quella di oggi", Toast.LENGTH_LONG).show();
                    return;
                }
            } catch (ParseException e) {
                Toast.makeText(getApplicationContext(), "Attenzione, la data inserita non è valida [ " + e.toString() + " ]", Toast.LENGTH_LONG).show();
                return;
            }

            try {
                String sqlUpdate = "UPDATE utentiOUT SET ";
                sqlUpdate = sqlUpdate + "LLatitudine ='" + DefLat + "', ";
                sqlUpdate = sqlUpdate + "LLongitudine ='" + DefLong + "', ";
                sqlUpdate = sqlUpdate + "dtProgrammazione ='" + txtData.getText().toString().trim() + "', ";
                sqlUpdate = sqlUpdate + "daOraLett ='" + DaOra.replace("'", "") + "', ";
                sqlUpdate = sqlUpdate + "aOraLett ='" + AOra.replace("'", "") + "', ";
                sqlUpdate = sqlUpdate + "Stato ='P' WHERE CodiceUtente='" + CodiceUtente + "' AND IDCarico='" + IDCarico + "' AND IDAzienda='" + IDAzienda + "' ";
                db.execSQL(sqlUpdate);
                db.execSQL("UPDATE utenti SET Stato = 'P' WHERE CodiceUtente='" + CodiceUtente + "' AND IDCarico='" + IDCarico + "' AND IDAzienda='" + IDAzienda + "' ");
                Intent returnIntent = new Intent();
                setResult(Act_Censimento.RESULT_OK, returnIntent);
                finish();
            } catch (Exception ex) {
                Toast.makeText(getApplicationContext(), "Attenzione impossibile salvare l'attività [" + ex.toString() + "]", Toast.LENGTH_LONG).show();
            }
        }


    }

    public void FotoP1(View view)
    {
            Intent intent;
            if (android.os.Build.VERSION.SDK_INT > 23) {
                intent = new Intent(Act_Programmazione.this, Act_newCamera.class);
            }
            else
                intent = new Intent(Act_Programmazione.this, Custom_CameraActivity.class);
            intent.putExtra("CodiceUtente", CodiceUtente);
            intent.putExtra("IDCarico", IDCarico);
            intent.putExtra("IDAzienda", IDAzienda);
            intent.putExtra("DataAttivita", DataAttivita);
            intent.putExtra("OraAttivita", OraAttivita);
            intent.putExtra("Misuratore", lblMatricola.getText());
            intent.putExtra("Correttore", "");
            intent.putExtra("TipoFoto", "P1");
            intent.putExtra("CountPhotoB1", 0);
            intent.putExtra("CountPhotoB2", 0);
            intent.putExtra("CountPhotoVM", 0);
            intent.putExtra("CountPhotoVC", 0);
            intent.putExtra("CountPhotoVB", 0);
            intent.putExtra("CountPhotoVE", 0);
            intent.putExtra("CountPhotoP1", CountPhotoP1);
            intent.putExtra("DefLat", DefLat);
            intent.putExtra("DefLong", DefLong);
            intent.putExtra("operatore", operatore);
            intent.putExtra("GlobalConfig",GlobalConfig);

            startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {

        Log.i("response", "Act_Programmazione onActivityResult");
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==1)
        {
            String resultP1=data.getStringExtra("P1");
            CountPhotoP1 = Integer.parseInt(resultP1);
        }
    }//onActivityResult


    public void AnnullaLavoro(View view)
    {
        Intent returnIntent = new Intent();
        setResult(this.RESULT_CANCELED, returnIntent);
        finish();
    }

    @Override
    public void onBackPressed() {

       /* try{
            db.execSQL("DELETE FROM utentiOUT WHERE idAzienda = '"+IDAzienda+"' AND idCarico = '"+IDCarico+"' AND CodiceUtente = '"+CodiceUtente+"'");
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),"Attenzione impossibile cancellare la lavorazione! ["+e.toString()+"]",Toast.LENGTH_LONG).show();
        }*/
        return;
    }

}
