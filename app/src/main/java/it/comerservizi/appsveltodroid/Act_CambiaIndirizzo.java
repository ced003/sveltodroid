package it.comerservizi.appsveltodroid;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Act_CambiaIndirizzo extends AppCompatActivity {

    EditText txtToponimo;
    EditText txtStrada;
    EditText txtCivico;
    String ResultToponimo;
    String ResultStrada;
    String ResultCivico;
    String result;
    String OldToponimo;
    String OldStrada;
    String OldCivico;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__cambia_indirizzo);

        txtToponimo = (EditText) findViewById(R.id.txtToponimo);
        txtStrada = (EditText) findViewById(R.id.txtStrada);
        txtCivico = (EditText) findViewById(R.id.txtCivico);

        Intent I_ActRilevaDati = getIntent();
        OldToponimo = I_ActRilevaDati.getExtras().getString("Toponimo");
        OldStrada = I_ActRilevaDati.getExtras().getString("Strada");
        OldCivico = I_ActRilevaDati.getExtras().getString("Civico");

        txtToponimo.setText(OldToponimo);
        txtStrada.setText(OldStrada);
        txtCivico.setText(OldCivico);
    }

    public void Conferma(View view)
    {
        ResultToponimo = txtToponimo.getText().toString();
        ResultStrada = txtStrada.getText().toString();
        ResultCivico = txtCivico.getText().toString();
        ResultToponimo = ResultToponimo.trim();
        ResultStrada = ResultStrada.trim();
        ResultCivico = ResultCivico.trim();


        if(ResultToponimo.length()<1)
        {
            Toast.makeText(getApplicationContext(), "Attenzione non puoi salvare perchè il Toponimo è vuoto", Toast.LENGTH_LONG).show();
        }
        else if(ResultStrada.length()<1)
        {
            Toast.makeText(getApplicationContext(), "Attenzione non puoi salvare perchè il nome Strada è vuoto", Toast.LENGTH_LONG).show();
        }
        else if(ResultCivico.length()<1)
        {
            Toast.makeText(getApplicationContext(), "Attenzione non puoi salvare perchè il Civico è vuoto", Toast.LENGTH_LONG).show();
        }
        else if (ResultToponimo.trim().toUpperCase().equals(OldToponimo.trim().toUpperCase()) && ResultStrada.trim().toUpperCase().equals(OldStrada.trim().toUpperCase()) && ResultCivico.trim().toUpperCase().equals(OldCivico.trim().toUpperCase()))
        {
            Toast.makeText(getApplicationContext(), "Attenzione non puoi salvare dati uguali ai precedenti", Toast.LENGTH_LONG).show();
        }
        else
        {
            result = ResultToponimo + "-" + ResultStrada + "-" + ResultCivico;
            Log.i("response", "Indirizzo completo verrà restituito il valore: "+result);
            Intent returnIntent = new Intent();
            returnIntent.putExtra("result",result);
            setResult(Act_CambiaIndirizzo.RESULT_OK, returnIntent);
            finish();
        }

    }

    public void Annulla(View view)
    {
        Intent returnIntent = new Intent();
        setResult(this.RESULT_CANCELED, returnIntent);
        finish();
    }

    @Override
    public void onBackPressed() {

        Intent returnIntent = new Intent();
        setResult(this.RESULT_CANCELED, returnIntent);
        finish();
    }

}
