package it.comerservizi.appsveltodroid;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static it.comerservizi.appsveltodroid.Constants.FIRST_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.FOURTH_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.SECOND_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.THIRD_COLUMN;

public class Act_ElencoAttivita extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    int NotificationID;
    ImageView EAimgGPS;
    ImageView EAimgWEB;
    ListView listView;
    EditText txtSearch4Matricola;
    public boolean connessione = false;
    GPSHelper gps;
    GPClass gpClass;
    Menu ElencoMenu;
    ConnectionDetector cd ;
    DatabaseHelper dbHelper;
    SQLiteDatabase db;
    AsyncLoader ElementiAs;
    AsyncCallWS task;
    AsyncUpload SincroOut;
    Switch RaggruppaCivico;
    String IDTerminale = "0";
    String GiornoAccesso;
    String IDLett = "";
    public ArrayList<String> IndiceElenco;
    ArrayList<HashMap<String, String>> list;
    ListViewAdapters adapter;
    Spinner spnCarico;
    Spinner spnComune;
    String IdCaricoScelto;
    String IdComuneScelto;
    String LastQuery;
    String LastOrdinamento;
    ArrayList<String> SelectCarico;
    ArrayList<String> SelectComune;
    private TextView txtNumAttivitaElenco;
    private List<String> spinnerCarico;
    private List<String> spinnerComune;
    private ProgressDialog dialog;
    private Integer LastPosition = 0;
    private Integer NumAttivita = 0;
    Boolean AccessoOffline = false;
    private Context context;
    String GlobalConfig;
    private Boolean AppenaSyncro;

    boolean statusOfGPS;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        /* CONTROLLO DATA AUTOMATICA */
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                if(Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME) != 1 || Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME_ZONE) != 1)
                {
                    Toast.makeText(getApplicationContext(), "ATTENZIONE HAI DISABILITATO LA DATA E ORA AUTOMATICA - L'APP NON PUO' FUNZIONARE", Toast.LENGTH_LONG).show();
                    this.finishAffinity();
                }
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        /* CONTROLLO DATA AUTOMATICA */
        setContentView(R.layout.activity_act__elenco_attivita);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //GUIDO
        onTrimMemory(ComponentCallbacks2.TRIM_MEMORY_COMPLETE);
        onTrimMemory(ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN);
        gpClass = new GPClass();

        txtNumAttivitaElenco = (TextView) findViewById(R.id.txtNumAttivitaElenco);
        EAimgGPS = (ImageView) findViewById(R.id.EAimgGPS);
        EAimgWEB = (ImageView) findViewById(R.id.EAimgWEB);
        txtSearch4Matricola = (EditText) findViewById(R.id.txtSearch4Matricola);
        listView=(ListView)findViewById(R.id.listAttivita);
        RaggruppaCivico=(Switch) findViewById(R.id.swRaggruppaCivico);
        RaggruppaCivico.setChecked(true);
        spnCarico = (Spinner) findViewById(R.id.spnCarico);
        spnComune = (Spinner) findViewById(R.id.spnComune);

        Intent I_ActAccesso = getIntent();
        IDTerminale = I_ActAccesso.getExtras().getString("IDTerminale");
        IDLett = I_ActAccesso.getExtras().getString("IDLetturista");
        GiornoAccesso = I_ActAccesso.getExtras().getString("GiornoAccesso");

        if(IDTerminale.equals("0") && IDLett.equals("-"))
            AccessoOffline = true;



        cd = new ConnectionDetector(getApplicationContext(),EAimgWEB);
        cd.setElencoAttivitaHandler(this);
        IntentFilter fltr_connreceived = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(cd, fltr_connreceived);

        dbHelper = new DatabaseHelper(Act_ElencoAttivita.this);
        db = dbHelper.getWritableDatabase();
        IdCaricoScelto = "nessuno";
        IdComuneScelto = "nessuno";
        AppenaSyncro = false;

        OrganizzaSpinnerComune();

        OrganizzaSpinnerCarico();

        RaggruppaCivico.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    OrdinaGenerico(LastOrdinamento);
                else
                    OrdinaGenerico(LastOrdinamento);

            }
        });
        list=new ArrayList<HashMap<String,String>>();
        adapter=new ListViewAdapters(this, list);

        //Preleva GlobalCOnfigurazione

        Cursor cGlobalConfig = db.rawQuery("SELECT IdConfig FROM AndroConfig WHERE Type = 'sGlobalConfiguration'", null);

        if (cGlobalConfig != null)
            cGlobalConfig.moveToFirst();

        GlobalConfig = cGlobalConfig.getString(cGlobalConfig.getColumnIndex("IdConfig"));
        cGlobalConfig.close();
        NotificationID = 0;
        //Fine

        LastQuery = "SELECT CAP, Localita, Provincia, Toponimo, Strada, Civico, SUM(CASE WHEN NotaAccess = 'Y' THEN 1 ELSE 0 END) as Accessibile , Count(*) as Totali FROM utenti WHERE Stato = 'A' AND (tProgrammazione = '')  GROUP BY CAP, Localita, Toponimo, Strada, Civico ";
        LastOrdinamento = " ORDER BY CAP, Localita, Toponimo, Strada, CAST(Civico AS INTEGER)";
        OrdinaGenerico(LastOrdinamento);

        Intent i = new Intent(this, GPSService.class);
        i.putExtra("IDOperatore", IDLett);
        i.putExtra("IDTerminale", IDTerminale);
        i.putExtra("SSID", cd.WifiSSID());
        startService(i);



    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            dialog
                    .setTitle("CHIUSURA APP")
                    .setMessage("Attenzione, sei sicuro di voler chiudere l'applicazione? ")
                    .setCancelable(false)
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    })
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Toast.makeText(getApplicationContext(), "Grazie per aver usato SveltoDroid!", Toast.LENGTH_LONG).show();

                            finish();
                            dialog.cancel();
                        }
                    });
            AlertDialog MsgErrore = dialog.create();
            //Rimuove il bottone negativo dall'AlertDialog

            MsgErrore.show();
            return;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.ElencoMenu = menu;
        getMenuInflater().inflate(R.menu.act__elenco_attivita, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        //if (id == R.id.action_settings) {
        //    return true;
        //}

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.ord_accessibili) {
            OrdinaAccessibili();
        } else if (id == R.id.ord_indirizzi) {
            OrdinaIndirizzi();
        } else if (id == R.id.ord_totali) {
            OrdinaTotali();
        } else if (id == R.id.btnVerificaSettaggi) {
            ApriPannelloOpzioniAndroid();
        } else if (id == R.id.btnRicerca) {
            ApriPannelloRicerche();
        } else if (id == R.id.btnActProgrammazione) {
            ApriPannelloAppuntamenti();
        } else if (id == R.id.btnActSpeciali) {
            ApriAttivitaSpeciali();
        } else if (id == R.id.btnQRCode) {
            ApriRicercaQCode();
        } else if (id == R.id.btnInstallazioni) {
            ApriInstallazioni();
        } else if (id == R.id.btn_sincronizzafoto) {
            IniziaSincronizzazioneFoto();
        } else if (id == R.id.btnStatistiche) {
            if(AccessoOffline)
                Toast.makeText(getApplicationContext(), "Con l'accesso OFFLINE non puoi interrogare le statistiche", Toast.LENGTH_SHORT).show();
            else
                ApriPannelloStatistiche();
        } else if (id == R.id.btn_sincronizza){
            if(connessione)
                if(AccessoOffline)
                    Toast.makeText(getApplicationContext(), "Con l'accesso OFFLINE non puoi sincronizzare", Toast.LENGTH_SHORT).show();
                else
                    IniziaSincronizzazione();
            else
                Toast.makeText(getApplicationContext(), "Nessuna connessione internet...", Toast.LENGTH_SHORT).show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        try{

            unregisterReceiver(cd);
            stopService(new Intent(this,GPSService.class));
            Log.i("Act_Accesso", "Unregistrato Connection Detector");
        }
        catch(Exception e)
        {
            Log.i("Act_Accesso", "Impossibile unregistrare Connection Detector");
        }

    }
    @Override
    protected void onPause()
    {
        super.onPause();
    }

    @Override
    protected void onRestart()
    {
        super.onRestart();
    }

    @Override
    protected void onStart()
    {

        super.onStart();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);


    }
    @Override
    protected void onResume()
    {
        super.onResume();

        /* CONTROLLO DATA AUTOMATICA */
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                if(Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME) != 1 || Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME_ZONE) != 1)
                {
                    Toast.makeText(getApplicationContext(), "ATTENZIONE HAI DISABILITATO LA DATA E ORA AUTOMATICA - L'APP NON PUO' FUNZIONARE", Toast.LENGTH_LONG).show();
                    this.finishAffinity();
                }
                else
                {
                    if(Settings.System.getInt(context.getContentResolver(),Settings.System.ACCELEROMETER_ROTATION)==1)
                    {

                        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                            dialog
                                    .setTitle("ROTAZIONE DISPOSITIVO")
                                    .setMessage("ATTENZIONE DEVI DISATTIVARE LA ROTAZIONE DEL DISPOSITIVO")
                                    .setCancelable(false)
                                    .setPositiveButton("RECEPITO", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                            AlertDialog MsgErrore = dialog.create();

                            MsgErrore.show();
                    }
                    if(!AccessoOffline) {
                        gpClass.LastSync(getApplicationContext(), this);

                    }
                    gpClass.MinutiLastSync(getApplicationContext(), this);

                    if(gpClass.LastMin>90 || AppenaSyncro) {
                        CallNotificheAttivita();
                        AppenaSyncro = false;
                    }

                    if(gpClass.ControlloGiornoAccesso(getApplicationContext(), this, GiornoAccesso))
                        this.finishAffinity();

                    txtSearch4Matricola.setText("");
                    OrdinaGenerico(LastOrdinamento);
                    listView.setSelection(LastPosition);
                }
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        /* CONTROLLO DATA AUTOMATICA */
        Crashlytics.log(null);
        Crashlytics.log("GlobalConfig: " + GlobalConfig + " - Operatore: " + IDLett );
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        try{

            unregisterReceiver(cd);
            Log.i("ElencoAttivita", "Unregistrato Connection Detector");
        }
        catch(Exception e)
        {
            Log.i("ElencoAttivita", "Impossibile unregistrare Connection Detector");
        }
    }

    public void VerificaGPS()
    {

        gps = new GPSHelper();
        gps.GPSContesto(getApplicationContext());

        statusOfGPS = gps.StatoGPS();
        if (statusOfGPS)
            EAimgGPS.setImageResource(R.drawable.gpsgreen26);
        else
            Toast.makeText(getApplicationContext(), "ATTENZIONE DEVI ATTIVARE IL GPS", Toast.LENGTH_LONG).show();

    }


    public void OrdinaTotali()
    {

        LastOrdinamento =" ORDER BY Totali DESC, Accessibile DESC";
        OrdinaGenerico(LastOrdinamento);
    }


    public void OrdinaAccessibili()
    {


        LastOrdinamento =" ORDER BY Accessibile DESC, Totali DESC";
        OrdinaGenerico(LastOrdinamento);
    }

    public void OrdinaIndirizzi()
    {

        LastOrdinamento =" ORDER BY CAP, Localita, Toponimo, Strada, CAST(Civico AS INTEGER)";
        OrdinaGenerico(LastOrdinamento);
    }


    public void IniziaSincronizzazione()
    {
        task = new AsyncCallWS(Act_ElencoAttivita.this);
        task.SSID = cd.WifiSSID();
        AppenaSyncro = true;

        Boolean AggiornaSveltoDroid = false;
        String Versione = BuildConfig.VERSION_NAME;
        int stringId = getApplicationInfo().labelRes;
        String NameApp = getString(stringId);
        String ControlloVersione = task.VerificaVersioningAsync(NameApp);
        if(ControlloVersione.equals("TIMEOUT") || ControlloVersione.contains("Error WS")) {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                dialog
                        .setTitle("PROBLEMA DI CONNESSIONE")
                        .setMessage("Vi sono stati problemi con la connessione. Ritenta la trasmissione..")
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog MsgErrore = dialog.create();
                //Rimuove il bottone negativo dall'AlertDialog

                MsgErrore.show();
                return;
            }
        }
        else {
            if (Double.parseDouble(Versione) < Double.parseDouble(ControlloVersione))
                AggiornaSveltoDroid = true;

            SincroOut = new AsyncUpload();
            SincroOut.MyElencoAttivita = this;
            SincroOut.AggiornaSveltoDroid = AggiornaSveltoDroid;
            SincroOut.SSID = cd.WifiSSID();
            SincroOut.operatore = IDTerminale;
            SincroOut.execute();

            Log.i("response", "ElencoAttività - Post AsyncLoader.execute");
        }

    }
    public void IniziaSincronizzazioneFoto()
    {
        task = new AsyncCallWS(Act_ElencoAttivita.this);
        task.SSID = cd.WifiSSID();


        SincroOut = new AsyncUpload();
        SincroOut.MyElencoAttivita = this;
        SincroOut.SoloFoto = true;
        SincroOut.SSID = cd.WifiSSID();
        SincroOut.operatore = IDTerminale;
        SincroOut.execute();

        Log.i("response", "ElencoAttività - Post AsyncLoader.execute");

    }

    //Ordinamento per prima scelta carico
    public void OrdinaGenerico(String Ordinamento)
    {
        Integer countElenco = 0;
        dialog = new ProgressDialog(Act_ElencoAttivita.this);
        dialog.setCancelable(false);
        dialog.setMessage("Elaborazione del carico in corso...");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.show();
        String IndRicercaPass;



        list.clear();

        IndiceElenco = new ArrayList<String>();
        IndiceElenco.clear();
        Cursor cursor;
        if(IdCaricoScelto.contains("nessuno")) {
            if(IdComuneScelto.contains("nessuno"))
                if(RaggruppaCivico.isChecked()) {
                    LastQuery = "SELECT CAP, Localita, Provincia, Toponimo, Strada, Civico,SUM(CASE WHEN NotaAccess = 'Y' THEN 1 ELSE 0 END) as Accessibile , Count(*) as Totali FROM utenti WHERE Stato = 'A' AND (dtProgrammazione = '')  GROUP BY CAP, Localita, Toponimo, Strada, Civico";
                    cursor = dbHelper.query(db, LastQuery + Ordinamento);
                }
                else {
                    LastQuery = "SELECT CAP, Localita, Provincia, Toponimo, Strada, Civico,SUM(CASE WHEN NotaAccess = 'Y' THEN 1 ELSE 0 END) as Accessibile , Count(*) as Totali FROM utenti WHERE Stato = 'A' AND (dtProgrammazione = '')  GROUP BY CAP, Localita, Toponimo, Strada";;
                    cursor = dbHelper.query(db, LastQuery+Ordinamento);
                }
            else {
                if(RaggruppaCivico.isChecked()) {
                    LastQuery ="SELECT CAP, Localita, Provincia, Toponimo, Strada, Civico,SUM(CASE WHEN NotaAccess = 'Y' THEN 1 ELSE 0 END) as Accessibile , Count(*) as Totali FROM utenti WHERE Stato = 'A' AND (dtProgrammazione = '') AND Localita='" + IdComuneScelto + "' GROUP BY CAP, Localita, Toponimo, Strada, Civico";
                    cursor = dbHelper.query(db, LastQuery+Ordinamento);
                }
                else {
                    LastQuery="SELECT CAP, Localita, Provincia, Toponimo, Strada, Civico,SUM(CASE WHEN NotaAccess = 'Y' THEN 1 ELSE 0 END) as Accessibile , Count(*) as Totali FROM utenti  WHERE Stato = 'A' AND (dtProgrammazione = '') AND Localita='" + IdComuneScelto + "' GROUP BY CAP, Localita, Toponimo, Strada";
                    cursor = dbHelper.query(db, LastQuery+Ordinamento);
                }
            }
        }
        else {
            if(IdComuneScelto.contains("nessuno")) {
                if(RaggruppaCivico.isChecked())
                {
                    LastQuery="SELECT CAP, Localita, Provincia, Toponimo, Strada, Civico,SUM(CASE WHEN NotaAccess = 'Y' THEN 1 ELSE 0 END) as Accessibile , Count(*) as Totali FROM utenti WHERE Stato = 'A' AND (dtProgrammazione = '') AND IdCarico='" + IdCaricoScelto + "' GROUP BY CAP, Localita, Toponimo, Strada, Civico";
                    cursor = dbHelper.query(db, LastQuery+Ordinamento);
                }
                else
                {
                    LastQuery="SELECT CAP, Localita, Provincia, Toponimo, Strada, Civico,SUM(CASE WHEN NotaAccess = 'Y' THEN 1 ELSE 0 END) as Accessibile , Count(*) as Totali FROM utenti WHERE Stato = 'A' AND (dtProgrammazione = '') AND IdCarico='" + IdCaricoScelto + "' GROUP BY CAP, Localita, Toponimo, Strada";
                    cursor = dbHelper.query(db, LastQuery+Ordinamento);
                }
            }
            else {
                if(RaggruppaCivico.isChecked()) {
                    LastQuery ="SELECT CAP, Localita, Provincia, Toponimo, Strada, Civico,SUM(CASE WHEN NotaAccess = 'Y' THEN 1 ELSE 0 END) as Accessibile , Count(*) as Totali FROM utenti WHERE Stato = 'A' AND (dtProgrammazione = '') AND Localita='" + IdComuneScelto + "' AND IdCarico='" + IdCaricoScelto + "' GROUP BY CAP, Localita, Toponimo, Strada, Civico";
                    cursor = dbHelper.query(db, LastQuery+Ordinamento);
                }
                else
                {
                    LastQuery = "SELECT CAP, Localita, Provincia, Toponimo, Strada, Civico,SUM(CASE WHEN NotaAccess = 'Y' THEN 1 ELSE 0 END) as Accessibile , Count(*) as Totali FROM utenti WHERE Stato = 'A' AND (dtProgrammazione = '') AND Localita='" + IdComuneScelto + "' AND IdCarico='" + IdCaricoScelto + "' GROUP BY CAP, Localita, Toponimo, Strada";
                    cursor = dbHelper.query(db, LastQuery+Ordinamento);
                }
            }
        }
        cursor.moveToFirst();
        NumAttivita = 0;
        while (cursor.isAfterLast() == false) {
            HashMap<String,String> temp=new HashMap<String, String>();
            temp.put(FIRST_COLUMN, gpClass.NullToEmpty(cursor.getString(cursor.getColumnIndex("CAP"))) + " " + gpClass.NullToEmpty(cursor.getString(cursor.getColumnIndex("Localita"))) );

            if(RaggruppaCivico.isChecked())
                temp.put(SECOND_COLUMN, gpClass.NullToEmpty(cursor.getString(cursor.getColumnIndex("Toponimo"))) + " " + gpClass.NullToEmpty(cursor.getString(cursor.getColumnIndex("Strada"))) + " " + gpClass.NullToEmpty(cursor.getString(cursor.getColumnIndex("Civico"))));
            else
                temp.put(SECOND_COLUMN, gpClass.NullToEmpty(cursor.getString(cursor.getColumnIndex("Toponimo"))) + " " + gpClass.NullToEmpty(cursor.getString(cursor.getColumnIndex("Strada")))  );
            temp.put(THIRD_COLUMN, "Totali: " + cursor.getString(cursor.getColumnIndex("Totali")));
            temp.put(FOURTH_COLUMN, "Accessibili: " + cursor.getString(cursor.getColumnIndex("Accessibile")));
            NumAttivita = NumAttivita + Integer.parseInt(cursor.getString(cursor.getColumnIndex("Totali")));
            list.add(temp);

            if(RaggruppaCivico.isChecked())
                IndRicercaPass = gpClass.NullToEmpty(cursor.getString(cursor.getColumnIndex("CAP")))+ " "+gpClass.NullToEmpty(cursor.getString(cursor.getColumnIndex("Localita")))+" " + gpClass.NullToEmpty(cursor.getString(cursor.getColumnIndex("Toponimo"))) + " " + gpClass.NullToEmpty(cursor.getString(cursor.getColumnIndex("Strada"))) + " " + gpClass.NullToEmpty(cursor.getString(cursor.getColumnIndex("Civico")));
            else
                IndRicercaPass = gpClass.NullToEmpty(cursor.getString(cursor.getColumnIndex("CAP")))+ " "+gpClass.NullToEmpty(cursor.getString(cursor.getColumnIndex("Localita")))+" " + gpClass.NullToEmpty(cursor.getString(cursor.getColumnIndex("Toponimo"))) + " " + gpClass.NullToEmpty(cursor.getString(cursor.getColumnIndex("Strada")));
            IndiceElenco.add(IndRicercaPass);
            cursor.moveToNext();
            countElenco++;
        }
        txtNumAttivitaElenco.setText(NumAttivita.toString());
        adapter=new ListViewAdapters(this, list);
        adapter.cosa = "ElencoAttivita";

        listView.setAdapter(null);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                Intent intent = new Intent(Act_ElencoAttivita.this, ListaCarico.class);
                LastPosition = position;
                String IDRicerca = IndiceElenco.get(position);
                Log.i("Ricerca", "Val: " + IDRicerca);
                intent.putExtra("Indirizzo", IDRicerca);
                intent.putExtra("Matricola", "");
                intent.putExtra("GiornoAccesso", GiornoAccesso);
                intent.putExtra("IDLett", IDLett);
                intent.putExtra("SelectCarico", IdCaricoScelto);
                intent.putExtra("ConSenzaCivico", RaggruppaCivico.isChecked());
                startActivity(intent);

            }

        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + IndiceElenco.get(position));
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                LastPosition = position;
                startActivity(mapIntent);
                return true;
            }

        });

        cursor.close();
        System.gc();
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    public void RicercaPerMatricola(View view)
    {
        if(txtSearch4Matricola.getText().length()>0)
        {
            Intent intent = new Intent(this, ListaCarico.class);
            intent.putExtra("Indirizzo", "");
            intent.putExtra("Matricola", txtSearch4Matricola.getText().toString());
            intent.putExtra("SelectCarico", IdCaricoScelto);
            intent.putExtra("IDLett", IDLett.toString());
            intent.putExtra("GiornoAccesso", GiornoAccesso);
            startActivityForResult(intent, 99);
        }
    }
    private void OrganizzaSpinnerCarico()
    {
        //SPINNER CARICO
        Cursor cursorCarico;
        if(IdComuneScelto.contains("nessuno"))
            cursorCarico = dbHelper.query(db, "SELECT IdCarico, Periodicita, Count(*) As TOT FROM utenti WHERE Stato = 'A' AND (dtProgrammazione = '') GROUP BY IdCarico, Periodicita ");
        else
            cursorCarico = dbHelper.query(db, "SELECT IdCarico, Periodicita, Count(*) As TOT FROM utenti WHERE Stato = 'A' AND (dtProgrammazione = '') AND Localita='"+IdComuneScelto+"' GROUP BY IdCarico, Periodicita ");
        spinnerCarico = new ArrayList<String>();
        spinnerCarico.add("TUTTI");
        SelectCarico = new ArrayList<String>();
        SelectCarico.add("");
        cursorCarico.moveToFirst();
        while (cursorCarico.isAfterLast() == false) {
            spinnerCarico.add(cursorCarico.getString(cursorCarico.getColumnIndex("IdCarico")) + " - " + cursorCarico.getString(cursorCarico.getColumnIndex("Periodicita")));
            SelectCarico.add(cursorCarico.getString(cursorCarico.getColumnIndex("IdCarico")));
            cursorCarico.moveToNext();
        }

        cursorCarico.close();
        ArrayAdapter<String> SpinnAdapCarico = new ArrayAdapter<String>(this, R.layout.spinner_item1, spinnerCarico);
        spnCarico.setAdapter(SpinnAdapCarico);
        spnCarico.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0)
                {
                    IdCaricoScelto = SelectCarico.get(position);
                    OrdinaIndirizzi();
                    RiorganizzaSpinnerComune();
                } else {
                    IdCaricoScelto = "nessuno";
                    OrdinaIndirizzi();
                    RiorganizzaSpinnerComune();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                IdCaricoScelto = "nessuno";
                spnCarico.setSelection(0);
            }
        });
    }

    private void OrganizzaSpinnerComune()
    {
        //SPINNER COMUNE
        Cursor cursorComune;
        if(IdCaricoScelto.contains("nessuno"))
            cursorComune = dbHelper.query(db, "SELECT Localita, Count(*) As TOT FROM utenti WHERE Stato = 'A' AND (dtProgrammazione = '') GROUP BY Localita ");
        else
            cursorComune = dbHelper.query(db, "SELECT Localita, Count(*) As TOT FROM utenti WHERE Stato = 'A' AND (dtProgrammazione = '') AND IdCarico = '"+IdCaricoScelto+"' GROUP BY Localita ");
        spinnerComune = new ArrayList<String>();
        spinnerComune.add("TUTTI");
        SelectComune = new ArrayList<String>();
        SelectComune.add("");
        cursorComune.moveToFirst();
        while (cursorComune.isAfterLast() == false) {
            spinnerComune.add(cursorComune.getString(cursorComune.getColumnIndex("Localita")) );
            SelectComune.add(cursorComune.getString(cursorComune.getColumnIndex("Localita")));
            cursorComune.moveToNext();
        }

        cursorComune.close();
        ArrayAdapter<String> SpinnAdapComune = new ArrayAdapter<String>(this, R.layout.spinner_item1, spinnerComune);
        spnComune.setAdapter(SpinnAdapComune);
        spnComune.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    IdComuneScelto = SelectComune.get(position);
                    IdComuneScelto = IdComuneScelto.replace("'", "''");
                    OrdinaIndirizzi();
                    //OrganizzaSpinnerCarico();
                } else {
                    IdComuneScelto = "nessuno";
                    OrdinaIndirizzi();
                    //OrganizzaSpinnerCarico();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                IdComuneScelto = "nessuno";
                spnComune.setSelection(0);
            }
        });

        //FINE SPINNER COMUNE
    }

    private void RiorganizzaSpinnerComune()
    {
        //SPINNER COMUNE
        Cursor cursorComune;
        if(IdCaricoScelto.contains("nessuno"))
            cursorComune = dbHelper.query(db, "SELECT Localita, Count(*) As TOT FROM utenti WHERE Stato = 'A' AND (dtProgrammazione = '') GROUP BY Localita ");
        else
            cursorComune = dbHelper.query(db, "SELECT Localita, Count(*) As TOT FROM utenti WHERE Stato = 'A' AND (dtProgrammazione = '') AND IdCarico = '"+IdCaricoScelto+"' GROUP BY Localita ");
        spinnerComune.clear();
        spinnerComune.add("TUTTI");
        SelectComune.clear();
        SelectComune.add("");
        cursorComune.moveToFirst();
        while (cursorComune.isAfterLast() == false) {
            spinnerComune.add(cursorComune.getString(cursorComune.getColumnIndex("Localita")) );
            SelectComune.add(cursorComune.getString(cursorComune.getColumnIndex("Localita")));
            cursorComune.moveToNext();
        }

        cursorComune.close();
        spnComune.setAdapter(null);
        ArrayAdapter<String> SpinnAdapComune = new ArrayAdapter<String>(this, R.layout.spinner_item1, spinnerComune);
        spnComune.setAdapter(SpinnAdapComune);
    }
    private void ApriPannelloAppuntamenti()
    {
        Intent intent = new Intent(Act_ElencoAttivita.this, Act_ElencoProgrammazione.class);
        intent.putExtra("IDLett", IDLett);
        startActivity(intent);
    }
    private void ApriPannelloRicerche()
    {

        Intent intent = new Intent(Act_ElencoAttivita.this, Act_Ricerca.class);
        intent.putExtra("IDLett", IDLett);
        intent.putExtra("ComuneSelezionato", IdComuneScelto);
        intent.putExtra("GiornoAccesso", GiornoAccesso);
        intent.putExtra("SSID", cd.WifiSSID());
        intent.putExtra("CaricoSelezionato", IdCaricoScelto);
        startActivity(intent);
    }

    private void ApriPannelloOpzioniAndroid()
    {

        Intent intent = new Intent(Act_ElencoAttivita.this, Act_ControlloErrori.class);
        startActivity(intent);
    }
    private void ApriRicercaQCode()
    {

        if(gpClass.isAppInstalled(getApplicationContext(),"com.google.zxing.client.android"))
        {
            Intent intent = new Intent(Act_ElencoAttivita.this, Act_QCode.class);
            intent.putExtra("IDLett", IDLett);
            intent.putExtra("GiornoAccesso", GiornoAccesso);
            startActivity(intent);
        }
        else
            Toast.makeText(getApplicationContext(), "Devi installare la libreria del Barcode Scanner", Toast.LENGTH_LONG).show();
    }
    private void ApriPannelloStatistiche()
    {

        Intent intent = new Intent(Act_ElencoAttivita.this, Act_Statistiche.class);
        intent.putExtra("IDLett", IDLett.toString());
        intent.putExtra("periodo", "mensile");
        intent.putExtra("GiornoAccesso", GiornoAccesso);
        startActivity(intent);
    }
    private void ApriInstallazioni()
    {

        Intent intent = new Intent(Act_ElencoAttivita.this, Act_Impostazioni.class);
        intent.putExtra("IDLett", IDLett.toString());
        startActivity(intent);
    }
    private void ApriAttivitaSpeciali()
    {

        Intent intent = new Intent(Act_ElencoAttivita.this, Act_AttivitaSpeciali.class);
        intent.putExtra("GiornoAccesso", GiornoAccesso);
        intent.putExtra("IDLett", IDLett);
        startActivity(intent);
    }

    private void CallNotificheAttivita()
    {
            String DataOggi = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
            NotificationID++;
            Cursor NumeroAccessibili = db.rawQuery("SELECT Count(*) as TOT FROM utenti WHERE stato!='L' AND NotaAccess = 'Y'",null);
            if(NumeroAccessibili!=null)
            {
                NumeroAccessibili.moveToFirst();
                if(NumeroAccessibili.getInt(NumeroAccessibili.getColumnIndex("TOT"))>0)
                {
                    sendNotificationAccessibili("LAVORI URGENTI","Hai " + Integer.toString(NumeroAccessibili.getInt(NumeroAccessibili.getColumnIndex("TOT"))) + " Accessibili da chiudere!",NotificationID,R.drawable.alert02);
                }
            }
            NumeroAccessibili.close();

            NotificationID++;
            Cursor NumeroSpeciali = db.rawQuery("SELECT Count(*) as TOT FROM utenti WHERE stato!='L' AND (AttivitaSpeciale != '' or AttivitaSpeciale IS NOT NULL)",null);
            if(NumeroSpeciali!=null)
            {
                NumeroSpeciali.moveToFirst();
                if(NumeroSpeciali.getInt(NumeroSpeciali.getColumnIndex("TOT"))>=0)
                {
                    sendNotificationAccessibili("ATTIVITA' SPECIALI","Hai " + Integer.toString(NumeroSpeciali.getInt(NumeroSpeciali.getColumnIndex("TOT"))) + " Attività Speciali!",NotificationID,R.drawable.warning);
                }
            }
            NumeroSpeciali.close();

            Cursor NumeroProgrammazione = db.rawQuery("SELECT Count(*) as TOT FROM utenti WHERE stato!='L' AND (dtProgrammazione = '" + DataOggi + "')",null);
            if(NumeroProgrammazione!=null)
            {
                NumeroProgrammazione.moveToFirst();
                if(NumeroProgrammazione.getInt(NumeroProgrammazione.getColumnIndex("TOT"))>=0)
                {
                    sendNotificationAccessibili("PROGRAMMAZIONE","Hai " + Integer.toString(NumeroProgrammazione.getInt(NumeroProgrammazione.getColumnIndex("TOT"))) + " in Programmazione",NotificationID,R.drawable.alert02);
                }
            }
            NumeroSpeciali.close();



    }

    private void sendNotificationAccessibili(String Titolo, String Messaggio, int NotificationID, int Alert) {
        NotificationManager mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);


        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 })
                        .setSound(alarmSound)
                        .setContentTitle(Titolo)
                        .setContentText(Messaggio)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(""))
                        .setSmallIcon(Alert);

        mNotificationManager.notify(NotificationID, mBuilder.build());
    }
}


