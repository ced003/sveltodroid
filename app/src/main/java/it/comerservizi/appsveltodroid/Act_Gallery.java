package it.comerservizi.appsveltodroid;

import android.app.ActionBar.LayoutParams;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import java.io.File;

public class Act_Gallery extends AppCompatActivity {

    private ImageSwitcher sw;
    private String CodiceUtente;
    private String IDAzienda;
    private String IDCarico;
    private String MatricolaCont;
    private String MatricolaCorr;
    private String NotaUno;
    private String NotaDue;
    private String operatore;
    private File[] listFile;
    private TextView txtDescrizioneFoto;
    private TextView txtMatrciolaContatore;
    private TextView txtMatricolaCorrettore;
    private TextView txtSegnalazioneUno;
    private TextView txtSegnalazioneDue;
    private GPClass gp;
    private String GlobalConfig;
    DatabaseHelper dbHelper;
    SQLiteDatabase db;
    private int quanti, posfoto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__gallery);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            CodiceUtente = extras.getString("CodiceUtente");
            IDAzienda = extras.getString("IDAzienda");
            IDCarico = extras.getString("IDCarico");
            MatricolaCont = extras.getString("Contatore");
            MatricolaCorr = extras.getString("Correttore");
            NotaUno = extras.getString("Nota1");
            NotaDue = extras.getString("Nota2");
            operatore = extras.getString("operatore");
        }
        txtDescrizioneFoto = (TextView) findViewById(R.id.txtDescrizioneFoto);
        txtMatrciolaContatore = (TextView) findViewById(R.id.txtMatrContatore);
        txtMatricolaCorrettore = (TextView) findViewById(R.id.txtMatrCorrettore);
        txtSegnalazioneUno = (TextView) findViewById(R.id.txtNotaUno);
        txtSegnalazioneDue = (TextView) findViewById(R.id.txtNotaDue);

        txtMatrciolaContatore.setText(MatricolaCont);
        txtMatricolaCorrettore.setText(MatricolaCorr);
        txtSegnalazioneUno.setText(NotaUno);
        txtSegnalazioneDue.setText(NotaDue);


        dbHelper = new DatabaseHelper(Act_Gallery.this);
        db = dbHelper.getWritableDatabase();

        Cursor c = db.rawQuery("SELECT IdConfig FROM AndroConfig WHERE Type = 'sGlobalConfiguration'", null);

        if (c != null)
            c.moveToFirst();

        GlobalConfig = c.getString(c.getColumnIndex("IdConfig"));
        c.close();

        quanti = 0;
        posfoto = 0;
        gp = new GPClass();
        gp._context = getApplicationContext();
        try {
            listFile = gp.getListFilesExternalWithName(IDCarico.trim() + "_" + IDAzienda.trim() + "_" + CodiceUtente.trim());
            quanti = listFile.length;

            sw = (ImageSwitcher) findViewById(R.id.imageSwitcher);
            sw.setFactory(new ViewSwitcher.ViewFactory() {
                @Override
                public View makeView() {
                    ImageView myView = new ImageView(getApplicationContext());
                    myView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    myView.setLayoutParams(new
                            ImageSwitcher.LayoutParams(LayoutParams.MATCH_PARENT,
                            LayoutParams.MATCH_PARENT));
                    return myView;
                }
            });
            if (listFile.length < 1) {
                AlertDialog.Builder dialogDatoLettura = new AlertDialog.Builder(Act_Gallery.this);
                dialogDatoLettura
                        .setTitle("NESSUNA FOTO")
                        .setMessage("Attenzione, non vi sono foto per questa scheda")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                finish();
                            }
                        });
                AlertDialog MsgDatoLettura = dialogDatoLettura.create();
                MsgDatoLettura.show();
            } else {
                sw.setImageURI(Uri.fromFile(listFile[posfoto]));
                String nomefile = listFile[posfoto].getName();
                String dato[] = nomefile.split("_");
                txtDescrizioneFoto.setText("Foto " + dato[3] + " N° " + dato[4].toUpperCase().replace(".JPG",""));
            }
            sw.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(listFile[posfoto]), "image/*");
                    startActivity(intent);
                }
            });
        }catch(Exception ex)
        {
            try {
                listFile = gp.getListFilesInternallWithName(IDCarico.trim() + "_" + IDAzienda.trim() + "_" + CodiceUtente.trim());
                quanti = listFile.length;

                sw = (ImageSwitcher) findViewById(R.id.imageSwitcher);
                sw.setFactory(new ViewSwitcher.ViewFactory() {
                    @Override
                    public View makeView() {
                        ImageView myView = new ImageView(getApplicationContext());
                        myView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        myView.setLayoutParams(new
                                ImageSwitcher.LayoutParams(LayoutParams.MATCH_PARENT,
                                LayoutParams.MATCH_PARENT));
                        return myView;
                    }
                });
                if (listFile.length < 1) {
                    AlertDialog.Builder dialogDatoLettura = new AlertDialog.Builder(Act_Gallery.this);
                    dialogDatoLettura
                            .setTitle("NESSUNA FOTO")
                            .setMessage("Attenzione, non vi sono foto per questa scheda")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    finish();
                                }
                            });
                    AlertDialog MsgDatoLettura = dialogDatoLettura.create();
                    MsgDatoLettura.show();
                } else {
                    sw.setImageURI(Uri.fromFile(listFile[posfoto]));
                    String nomefile = listFile[posfoto].getName();
                    String dato[] = nomefile.split("_");
                    txtDescrizioneFoto.setText("Foto " + dato[3] + " N° " + dato[4].toUpperCase().replace(".JPG",""));
                }
                sw.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.fromFile(listFile[posfoto]), "image/*");
                        startActivity(intent);
                    }
                });
            }catch(Exception ex2)
            {
                //DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                //String date = df.format(Calendar.getInstance().getTime());
                //db.execSQL("INSERT INTO LogErrori VALUES ('AsyncUpload', '" + GlobalConfig +"','"+operatore+"','"+ex.getCause().getMessage()+ " - " +ex.getMessage().toString()+"','"+ date +"')");
                finish();
            }
        }
    }
    @Override
    protected void onResume()
    {
        super.onResume();

    }

    public void NextPhoto(View view)
    {
        try
        {
            if(posfoto<(quanti-1))
            {
                posfoto++;
                String nomefile = listFile[posfoto].getName();
                String dato[] = nomefile.split("_");
                txtDescrizioneFoto.setText("Foto " + dato[3] + " N° "+ dato[4].toUpperCase().replace(".JPG",""));
                sw.setImageURI(Uri.fromFile(listFile[posfoto]));
            }
        }catch(Exception ex)
        {
           // DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
           // String date = df.format(Calendar.getInstance().getTime());
           // db.execSQL("INSERT INTO LogErrori VALUES ('AsyncUpload', '" + GlobalConfig +"','"+operatore+"','"+ex.getCause().getMessage()+ " - " +ex.getMessage().toString()+"','"+ date +"')");
            finish();
        }

    }
    public void ReturnPhoto(View view)
    {
        try
        {
            if(posfoto>0)
            {
                posfoto--;
                String nomefile = listFile[posfoto].getName();
                String dato[] = nomefile.split("_");
                txtDescrizioneFoto.setText("Foto " + dato[3] + " N° "+ dato[4].toUpperCase().replace(".JPG",""));
                sw.setImageURI(Uri.fromFile(listFile[posfoto]));
            }
        }catch(Exception ex)
        {
           // DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
          //  String date = df.format(Calendar.getInstance().getTime());
          //  db.execSQL("INSERT INTO LogErrori VALUES ('AsyncUpload', '" + GlobalConfig +"','"+operatore+"','"+ex.getCause().getMessage()+ " - " +ex.getMessage().toString()+"','"+ date +"')");
            finish();
        }
    }


}
