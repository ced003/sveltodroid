package it.comerservizi.appsveltodroid;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Act_Correttore extends AppCompatActivity {
    String CodiceUtente;
    String IDCarico;
    String IDAzienda;
    Boolean ObbligoCorrettore;
    EditText txtVM;
    EditText txtVB;
    EditText txtVE;
    Button btnFotoVM;
    Button btnFotoVB;
    Button btnFotoVE;
    String result;
    String DataAttivita;
    String OraAttivita;
    String Matricola;
    String Correttore;
    String operatore;
    Integer CountPhotoVC;
    Integer CountPhotoVM;
    Integer CountPhotoVB;
    Integer CountPhotoVE;
    Integer CountPhotoB1;
    Integer CountPhotoB2;
    String DefLong,GlobalConfig;
    String DefLat;

    String VerVB, VerVM, VerVE;

    Boolean Integrato = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__correttore);

        /* CONTROLLO DATA AUTOMATICA */
        try {
            if(Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME) != 1)
            {
                Toast.makeText(getApplicationContext(), "ATTENZIONE HAI DISABILITATO LA DATA E ORA AUTOMATICA - L'APP NON PUO' FUNZIONARE", Toast.LENGTH_LONG).show();
                finish();
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        /* CONTROLLO DATA AUTOMATICA */
        txtVM = (EditText) findViewById(R.id.txtVM);
        txtVB = (EditText) findViewById(R.id.txtVB);
        txtVE = (EditText) findViewById(R.id.txtVE);
        btnFotoVM = (Button) findViewById(R.id.btnFotoVM);
        btnFotoVB = (Button) findViewById(R.id.btnFotoVB);
        btnFotoVE = (Button) findViewById(R.id.btnFotoVE);
        CountPhotoVC = 0;
        CountPhotoVM = 0;
        CountPhotoVB = 0;
        CountPhotoVE = 0;
        CountPhotoB1 = 0;
        CountPhotoB2 = 0;
        VerVB = "";
        VerVE = "";
        VerVM = "";
        result = null;
        Bundle extras = getIntent().getExtras();
        if(extras !=null) {
            CodiceUtente = extras.getString("CodiceUtente");
            IDCarico = extras.getString("IDCarico");
            IDAzienda = extras.getString("IDAzienda");
            Integrato = extras.getBoolean("Integrato");
            ObbligoCorrettore = extras.getBoolean("ObbligoCorrettore");
            DataAttivita = extras.getString("DataAttivita");
            OraAttivita = extras.getString("OraAttivita");
            Matricola = extras.getString("Matricola");
            Correttore = extras.getString("Correttore");
            CountPhotoVC = extras.getInt("CountPhotoVC");
            CountPhotoVM = extras.getInt("CountPhotoVM");
            CountPhotoVB = extras.getInt("CountPhotoVB");
            CountPhotoVE = extras.getInt("CountPhotoVE");
            CountPhotoB1 = extras.getInt("CountPhotoB1");
            CountPhotoB2 = extras.getInt("CountPhotoB2");
            VerVB = extras.getString("LettVB");
            VerVE = extras.getString("LettVE");
            VerVM = extras.getString("LettVM");
            DefLat = extras.getString("DefLat");
            DefLong = extras.getString("DefLong");
            operatore = extras.getString("operatore");
            GlobalConfig = extras.getString("GlobalConfig");
        }

        if(VerVM.length()>0)
            txtVM.setText(VerVM);
        if(VerVE.length()>0)
            txtVE.setText(VerVE);
        if(VerVB.length()>0)
            txtVB.setText(VerVB);

        if(Integrato)
        {
            txtVM.setEnabled(false);
            txtVB.setEnabled(true);
            txtVE.setEnabled(true);
            btnFotoVM.setEnabled(false);
            btnFotoVB.setEnabled(true);
            btnFotoVE.setEnabled(true);
        }
        else
        {
            txtVM.setEnabled(true);
            txtVB.setEnabled(true);
            txtVE.setEnabled(true);
            btnFotoVM.setEnabled(true);
            btnFotoVB.setEnabled(true);
            btnFotoVE.setEnabled(true);
        }
    }


    @Override
    protected void onResume()
    {
        super.onResume();

        /* CONTROLLO DATA AUTOMATICA */
        try {
            if(Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME) != 1)
            {
                Toast.makeText(getApplicationContext(), "ATTENZIONE HAI DISABILITATO LA DATA E ORA AUTOMATICA - L'APP NON PUO' FUNZIONARE", Toast.LENGTH_LONG).show();
                finish();
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        /* CONTROLLO DATA AUTOMATICA */

    }

    @Override
    public void onBackPressed() {

       /* try{
            db.execSQL("DELETE FROM utentiOUT WHERE idAzienda = '"+IDAzienda+"' AND idCarico = '"+IDCarico+"' AND CodiceUtente = '"+CodiceUtente+"'");
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),"Attenzione impossibile cancellare la lavorazione! ["+e.toString()+"]",Toast.LENGTH_LONG).show();
        }*/
        if(ObbligoCorrettore && txtVB.getText().toString().length()>0 &&  txtVM.getText().toString().length()>0 &&  txtVE.getText().toString().length()>0 )
            finish();
        else
            Toast.makeText(getApplicationContext(), "Attenzione devi per forza inserire delle letture", Toast.LENGTH_LONG).show();
        return;
    }

    public void AnnullaCorrettore(View view)
    {
        if(ObbligoCorrettore && txtVB.getText().toString().length()>0 &&  txtVM.getText().toString().length()>0 &&  txtVE.getText().toString().length()>0 )
        {
            Intent returnIntent = new Intent();
            setResult(this.RESULT_CANCELED, returnIntent);
            finish();
        }
        else if(!ObbligoCorrettore)
        {
            Intent returnIntent = new Intent();
            setResult(this.RESULT_CANCELED, returnIntent);
            finish();
        }
        else
        Toast.makeText(getApplicationContext(), "Attenzione devi per forza inserire delle letture", Toast.LENGTH_LONG).show();

        return;
    }

    public void ConfermaCorrettore(View view)
    {
        VerVB = txtVB.getText().toString();
        VerVM = txtVM.getText().toString();
        VerVE = txtVE.getText().toString();
        Log.i("VerVB",VerVB);
        Log.i("VerVM", VerVM);
        Log.i("VerVE", VerVE);
        if(ObbligoCorrettore && VerVB.length()>0 &&  VerVM.length()>0 &&  VerVE.length()>0 ) {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("LettVB", VerVB);
            returnIntent.putExtra("LettVM", VerVM);
            returnIntent.putExtra("LettVE", VerVE);
            returnIntent.putExtra("B1", Integer.toString(CountPhotoB1));
            returnIntent.putExtra("B2", Integer.toString(CountPhotoB2));
            returnIntent.putExtra("VM", Integer.toString(CountPhotoVM));
            returnIntent.putExtra("VC", Integer.toString(CountPhotoVC));
            returnIntent.putExtra("VB", Integer.toString(CountPhotoVB));
            returnIntent.putExtra("VE", Integer.toString(CountPhotoVE));
            setResult(Act_Correttore.RESULT_OK, returnIntent);
            finish();
        }
        else if(Integrato && VerVB.length()>0 && VerVE.length()>0 ){
            Intent returnIntent = new Intent();
            returnIntent.putExtra("LettVB", VerVB);
            returnIntent.putExtra("LettVM", VerVM);
            returnIntent.putExtra("LettVE", VerVE);
            returnIntent.putExtra("B1", Integer.toString(CountPhotoB1));
            returnIntent.putExtra("B2", Integer.toString(CountPhotoB2));
            returnIntent.putExtra("VM", Integer.toString(CountPhotoVM));
            returnIntent.putExtra("VC", Integer.toString(CountPhotoVC));
            returnIntent.putExtra("VB", Integer.toString(CountPhotoVB));
            returnIntent.putExtra("VE", Integer.toString(CountPhotoVE));
            setResult(Act_Correttore.RESULT_OK, returnIntent);
            finish();
        }
        else if(Integrato && (VerVB.length()<1 || VerVE.length()<1) ) {
            Toast.makeText(getApplicationContext(), "Attenzione devi per forza inserire delle letture VB e VE", Toast.LENGTH_LONG).show();
        }
        else if(!ObbligoCorrettore && (VerVB.length()>0 &&  VerVM.length()>0 &&  VerVE.length()>0) ){
            Intent returnIntent = new Intent();
            returnIntent.putExtra("LettVB", VerVB);
            returnIntent.putExtra("LettVM", VerVM);
            returnIntent.putExtra("LettVE", VerVE);
            returnIntent.putExtra("B1", Integer.toString(CountPhotoB1));
            returnIntent.putExtra("B2", Integer.toString(CountPhotoB2));
            returnIntent.putExtra("VM", Integer.toString(CountPhotoVM));
            returnIntent.putExtra("VC", Integer.toString(CountPhotoVC));
            returnIntent.putExtra("VB", Integer.toString(CountPhotoVB));
            returnIntent.putExtra("VE", Integer.toString(CountPhotoVE));
            setResult(Act_Correttore.RESULT_OK, returnIntent);
            finish();
        }
        else
            Toast.makeText(getApplicationContext(), "Attenzione devi per forza inserire le letture", Toast.LENGTH_LONG).show();

        return;
    }

    public void FotoVM(View view)
    {

        Intent intent;
        if (android.os.Build.VERSION.SDK_INT > 23) {
            intent = new Intent(Act_Correttore.this, Act_newCamera.class);
        }
        else
            intent = new Intent(Act_Correttore.this, Custom_CameraActivity.class);
        intent.putExtra("CodiceUtente", CodiceUtente);
        intent.putExtra("IDCarico", IDCarico);
        intent.putExtra("IDAzienda", IDAzienda);
        intent.putExtra("DataAttivita", DataAttivita);
        intent.putExtra("OraAttivita", OraAttivita);
        intent.putExtra("Misuratore", Matricola);
        intent.putExtra("Correttore", Correttore);
        intent.putExtra("TipoFoto", "VM");
        intent.putExtra("CountPhotoB1", CountPhotoB1);
        intent.putExtra("CountPhotoB2", CountPhotoB2);
        intent.putExtra("CountPhotoVM", CountPhotoVM);
        intent.putExtra("CountPhotoVC", CountPhotoVC);
        intent.putExtra("CountPhotoVB", CountPhotoVB);
        intent.putExtra("CountPhotoVE", CountPhotoVE);
        intent.putExtra("DefLat", DefLat);
        intent.putExtra("DefLat", DefLong);
        intent.putExtra("operatore", operatore);
        intent.putExtra("GlobalConfig",GlobalConfig);

        startActivityForResult(intent, 2);
    }

    public void FotoVB(View view)
    {

        Intent intent;
        if (android.os.Build.VERSION.SDK_INT > 23) {
            intent = new Intent(Act_Correttore.this, Act_newCamera.class);
        }
        else
            intent = new Intent(Act_Correttore.this, Custom_CameraActivity.class);
        intent.putExtra("CodiceUtente", CodiceUtente);
        intent.putExtra("IDCarico", IDCarico);
        intent.putExtra("IDAzienda", IDAzienda);
        intent.putExtra("DataAttivita", DataAttivita);
        intent.putExtra("OraAttivita", OraAttivita);
        intent.putExtra("Misuratore", Matricola);
        intent.putExtra("Correttore", Correttore);
        intent.putExtra("TipoFoto", "VB");
        intent.putExtra("CountPhotoB1", CountPhotoB1);
        intent.putExtra("CountPhotoB2", CountPhotoB2);
        intent.putExtra("CountPhotoVM", CountPhotoVM);
        intent.putExtra("CountPhotoVC", CountPhotoVC);
        intent.putExtra("CountPhotoVB", CountPhotoVB);
        intent.putExtra("CountPhotoVE", CountPhotoVE);
        intent.putExtra("DefLat", DefLat);
        intent.putExtra("DefLong", DefLong);
        intent.putExtra("operatore", operatore);
        intent.putExtra("GlobalConfig",GlobalConfig);

        startActivityForResult(intent, 2);
    }


    public void FotoVE(View view)
    {

        Intent intent;
        if (android.os.Build.VERSION.SDK_INT > 23) {
            intent = new Intent(Act_Correttore.this, Act_newCamera.class);
        }
        else
            intent = new Intent(Act_Correttore.this, Custom_CameraActivity.class);
        intent.putExtra("CodiceUtente", CodiceUtente);
        intent.putExtra("IDCarico", IDCarico);
        intent.putExtra("IDAzienda", IDAzienda);
        intent.putExtra("DataAttivita", DataAttivita);
        intent.putExtra("OraAttivita", OraAttivita);
        intent.putExtra("Misuratore", Matricola);
        intent.putExtra("Correttore", Correttore);
        intent.putExtra("TipoFoto", "VE");
        intent.putExtra("CountPhotoB1", CountPhotoB1);
        intent.putExtra("CountPhotoB2", CountPhotoB2);
        intent.putExtra("CountPhotoVM", CountPhotoVM);
        intent.putExtra("CountPhotoVC", CountPhotoVC);
        intent.putExtra("CountPhotoVB", CountPhotoVB);
        intent.putExtra("CountPhotoVE", CountPhotoVE);
        intent.putExtra("DefLat", DefLat);
        intent.putExtra("DefLong", DefLong);
        intent.putExtra("operatore", operatore);
        intent.putExtra("GlobalConfig", GlobalConfig);

        startActivityForResult(intent, 2);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.i("response", "Act_Correttore onActivityResult");
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==2)
        {
            String resultB1=data.getStringExtra("B1");
            String resultB2=data.getStringExtra("B2");
            String resultVM=data.getStringExtra("VM");
            String resultVB=data.getStringExtra("VB");
            String resultVE=data.getStringExtra("VE");
            String resultVC=data.getStringExtra("VC");
            CountPhotoB1 = Integer.parseInt(resultB1);
            CountPhotoB2 = Integer.parseInt(resultB2);
            CountPhotoVM = Integer.parseInt(resultVM);
            CountPhotoVC = Integer.parseInt(resultVC);
            CountPhotoVB = Integer.parseInt(resultVB);
            CountPhotoVE = Integer.parseInt(resultVE);
        }
        else
            Log.i("response", "requestCode non classificato");

    }//onActivityResult

}
