package it.comerservizi.appsveltodroid;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class Act_CambioUbicazione extends AppCompatActivity {

    Spinner spnCambioUbicaz;
    TextView txtOldUbicazione;
    private List<String> DatiSpinnerUbicaz;
    private List<String> CodComerUbicaz;
    private String CodiceUtente;
    private String IDCarico;
    private String IDAzienda;
    private String CodBiffa;
    String result;
    DatabaseHelper dbHelper;
    SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__cambio_ubicazione);

        try {
            if(Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME) != 1)
            {
                Toast.makeText(getApplicationContext(), "ATTENZIONE HAI DISABILITATO LA DATA E ORA AUTOMATICA - L'APP NON PUO' FUNZIONARE", Toast.LENGTH_LONG).show();
                finish();
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        /* CONTROLLO DATA AUTOMATICA */

        dbHelper = new DatabaseHelper(Act_CambioUbicazione.this);
        db = dbHelper.getWritableDatabase();
        spnCambioUbicaz = (Spinner) findViewById(R.id.spnCambioUbicaz);
        txtOldUbicazione = (TextView) findViewById(R.id.txtOldUbicazione);
        Bundle extras = getIntent().getExtras();
        if(extras !=null) {
            CodiceUtente = extras.getString("CodiceUtente");
            IDCarico = extras.getString("IdCarico");
            IDAzienda = extras.getString("IdAzienda");
            CodBiffa = extras.getString("CodAzBiffa");
        }
        Cursor cursorOldUb = dbHelper.query(db, "SELECT utenti.Ubicazione as Ubicaz, tabtipo.Descrizione as DescrizioneUbicazione FROM utenti LEFT OUTER JOIN tabtipo ON tabtipo.CodiceDitta = substr(utenti.Ubicazione,-2) WHERE CodiceUtente = '" + CodiceUtente + "' AND idAzienda ='"+IDAzienda+"' AND IdCarico ='"+IDCarico+"' ");

        cursorOldUb.moveToFirst();

        if(cursorOldUb.isNull(cursorOldUb.getColumnIndex("Ubicaz")) || cursorOldUb.isNull(cursorOldUb.getColumnIndex("DescrizioneUbicazione")))
        {
            txtOldUbicazione.setText("-");
        }
        else
        {
            txtOldUbicazione.setText(cursorOldUb.getString(cursorOldUb.getColumnIndex("Ubicaz")) + " - " + cursorOldUb.getString(cursorOldUb.getColumnIndex("DescrizioneUbicazione")));
        }
        cursorOldUb.close();
        DatiSpinnerUbicaz = new ArrayList<String>();
        CodComerUbicaz = new ArrayList<String>();
        DatiSpinnerUbicaz.add("Indicare nuova ubicazione");
        CodComerUbicaz.add("0");
        Cursor cursorCambioUbicaz = dbHelper.query(db, "SELECT CodiceComer, Descrizione FROM tabtipo WHERE IdDitta = '" + IDAzienda + "' AND Tipo = 'UBICAZIONE' AND BiffaturaDitta LIKE '%-" + CodBiffa + "-%' ");

        cursorCambioUbicaz.moveToFirst();
        while (cursorCambioUbicaz.isAfterLast() == false)
        {
            CodComerUbicaz.add(cursorCambioUbicaz.getString(cursorCambioUbicaz.getColumnIndex("CodiceComer")));
            DatiSpinnerUbicaz.add(cursorCambioUbicaz.getString(cursorCambioUbicaz.getColumnIndex("CodiceComer"))+" - " +cursorCambioUbicaz.getString(cursorCambioUbicaz.getColumnIndex("Descrizione")).toUpperCase());
            cursorCambioUbicaz.moveToNext();
        }

        cursorCambioUbicaz.close();
        ArrayAdapter<String> SpinnAdapBiff1 = new ArrayAdapter<String>(this, R.layout.spinner_item_cambio1, DatiSpinnerUbicaz);
        spnCambioUbicaz.setAdapter(SpinnAdapBiff1);

    }

    public void Conferma(View view)
    {
        if(spnCambioUbicaz.getSelectedItemPosition()>0)
        {

            result = CodComerUbicaz.get(spnCambioUbicaz.getSelectedItemPosition());
            Intent returnIntent = new Intent();
            returnIntent.putExtra("result",result);
            setResult(this.RESULT_OK, returnIntent);
            Toast.makeText(getApplicationContext(), "Ricordati di scattare una foto che dimostri l'ubicazione indicata", Toast.LENGTH_LONG).show();
            finish();
        }
        else
            Toast.makeText(getApplicationContext(), "Attenzione non puoi salvare perchè non hai selezionato una nuova ubicazione", Toast.LENGTH_LONG).show();


    }

    public void Annulla(View view)
    {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", result);
        setResult(this.RESULT_CANCELED, returnIntent);
        finish();
    }

    @Override
    public void onBackPressed() {

        Intent returnIntent = new Intent();
        setResult(this.RESULT_CANCELED, returnIntent);
        finish();
    }

}
