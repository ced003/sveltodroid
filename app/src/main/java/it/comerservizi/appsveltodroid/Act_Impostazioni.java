package it.comerservizi.appsveltodroid;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;



class DownloadApp extends ResultReceiver {

    public ProgressDialog mProgressDialog;
    public DownloadApp(Handler handler) {
        super(handler);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        super.onReceiveResult(resultCode, resultData);
        if (resultCode == DownloadService.UPDATE_PROGRESS) {
            int progress = resultData.getInt("progress");
            mProgressDialog.setProgress(progress);
            if (progress == 100) {
                mProgressDialog.dismiss();
            }
        }
    }
}

public class Act_Impostazioni extends AppCompatActivity {

    ImageView LogoInstallQCODE,LogoInstallPRINT;
    TextView DescInstallQCODE,DescInstallPRINT;
    LinearLayout LLayoutQCODE,LLayoutPRINT;
    Boolean AbilitaDLQCode,AbilitaDLPRINT;
    GPClass GPC;
    String LinkQCODE, LinkPRINT;
    AsyncCallWS WebService;
    ConnectionDetector cd ;
    ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__impostazioni);
        AbilitaDLQCode = false;
        AbilitaDLPRINT = false;
        LinkQCODE = "";
        LinkPRINT = "";
        GPC = new GPClass();
        LogoInstallQCODE =  (ImageView) findViewById(R.id.icStatoInstallazioneQCODE);
        DescInstallQCODE =  (TextView) findViewById(R.id.lblStatoInstallazioneQCODE);
        LLayoutQCODE =  (LinearLayout) findViewById(R.id.LLayoutQCODE);
        LogoInstallPRINT =  (ImageView) findViewById(R.id.icStatoInstallazionePRINT);
        DescInstallPRINT =  (TextView) findViewById(R.id.lblStatoInstallazionePRINT);
        LLayoutPRINT =  (LinearLayout) findViewById(R.id.LLayoutPRINT);
        LLayoutQCODE.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        LLayoutQCODE.setBackgroundResource(R.drawable.border_linearlayout_02);
                        break;
                    case MotionEvent.ACTION_UP:

                        //set color back to default
                        ControlloInstallazioni();
                        if(AbilitaDLQCode)
                        {
                            ScaricaAggiornamento(LinkQCODE);
                        }
                        else {
                            Toast.makeText(getApplicationContext(), "L'Applicazione è già installata", Toast.LENGTH_LONG).show();
                        }
                        break;
                }
                return true;
            }
        });
        LLayoutPRINT.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        LLayoutPRINT.setBackgroundResource(R.drawable.border_linearlayout_02);
                        break;
                    case MotionEvent.ACTION_UP:

                        //set color back to default
                        ControlloInstallazioni();
                        if(AbilitaDLPRINT)
                        {
                            ScaricaAggiornamento(LinkPRINT);
                        }
                        else {
                            Toast.makeText(getApplicationContext(), "L'Applicazione è già installata", Toast.LENGTH_LONG).show();
                        }
                        break;
                }
                return true;
            }
        });

    }


    @Override
    protected void onStart()
    {
        super.onStart();
        Log.i("response", "Act_Impostazioni onStart");


    }

    @Override
    protected void onPause()
    {
        super.onPause();
        Log.i("response", "Act_Impostazioni OnPause");
        unregisterReceiver(cd);
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        try{

            unregisterReceiver(cd);
            Log.i("ElencoAttivita", "Unregistrato Connection Detector");
        }
        catch(Exception e)
        {
            Log.i("ElencoAttivita", "Impossibile unregistrare Connection Detector");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("response", "Act_Impostazioni onResume");

        AbilitaDLQCode = false;
        AbilitaDLPRINT = false;
        LinkQCODE = "";
        LinkPRINT = "";
        cd = new ConnectionDetector(getApplicationContext(),null );
        IntentFilter fltr_connreceived = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(cd, fltr_connreceived);
        ControlloInstallazioni();
    }

    public void ControlloInstallazioni()
    {
        //*********************************************************************
        //*********************************************************************
        //*************************QCODE READER********************************
        //*********************************************************************
        //*********************************************************************
        //APPLICAZIONE LETTORE QCODE
        if(GPC.isAppInstalled(getApplicationContext(), "com.google.zxing.client.android")) {

            PackageInfo pinfo = null;
            try {
                pinfo = getPackageManager().getPackageInfo("com.google.zxing.client.android", 0);
                int verCode = pinfo.versionCode;
                String verName = pinfo.versionName;

                WebService = new AsyncCallWS(Act_Impostazioni.this);
                WebService._NomeApp = "com.google.zxing.client.android";
                WebService.MyAct_Impostazioni = this;
                WebService.SSID = cd.WifiSSID();
                WebService.VerificaVersioning();
                if(VerificaAggiornamenti(verName))
                {
                    AbilitaDLQCode = true;
                    DescInstallQCODE.setText(" - AGGIORNAMENTO " + WebService._Versioning + " DISPONIBILE - ");
                    LLayoutQCODE.setBackgroundResource(R.drawable.border_linearlayout_bviolet);
                    WebService = new AsyncCallWS(Act_Impostazioni.this);
                    WebService._NomeApp = "com.google.zxing.client.android";
                    WebService.MyAct_Impostazioni = this;
                    WebService.SSID = cd.WifiSSID();
                    if(WebService.LinkDownloadAPP())
                    {
                        LinkQCODE = WebService._Link;
                        AbilitaDLQCode = true;
                    }
                }
                else
                {
                    LLayoutQCODE.setBackgroundResource(R.drawable.border_linearlayout_03);
                    DescInstallQCODE.setText(" - INSTALLATO V. " + verName + " - ");
                }
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            LogoInstallQCODE.setImageResource(R.drawable.installgreen);
            DescInstallQCODE.setTextColor(Color.BLUE);

        }
        else
        {
            LogoInstallQCODE.setImageResource(R.drawable.installred);
            DescInstallQCODE.setText(" - NON INSTALLATO - ");
            DescInstallQCODE.setTextColor(Color.RED);
            LLayoutQCODE.setBackgroundResource(R.drawable.border_linearlayout_bred);

            WebService = new AsyncCallWS(Act_Impostazioni.this);
            WebService._NomeApp = "com.google.zxing.client.android";
            WebService.MyAct_Impostazioni = this;
            WebService.SSID = cd.WifiSSID();
            if(WebService.LinkDownloadAPP())
            {
                LinkQCODE = WebService._Link;
                AbilitaDLQCode = true;
            }
        }


        //*********************************************************************
        //*********************************************************************
        //***********************STAMPA TERMICA********************************
        //*********************************************************************
        //*********************************************************************
        //Controllo l'installazione della LIB per la stampante termica
        if(GPC.isAppInstalled(getApplicationContext(), "com.loopedlabs.escposprintservice")) {
            //Se installata
            PackageInfo pinfo = null;
            try {
                //Cerco di prendermi il versioning e indicare quale versione è installata
                pinfo = getPackageManager().getPackageInfo("com.loopedlabs.escposprintservice", 0);
                int verCode = pinfo.versionCode;
                String verName = pinfo.versionName;

                DescInstallPRINT.setText(" - INSTALLATO V. " + verName + " - ");

                //disabilito il download
                AbilitaDLPRINT = false;

                //verifico sul server se vi sono versioni più aggiornate disponibili
                WebService = new AsyncCallWS(Act_Impostazioni.this);
                WebService._NomeApp = "com.loopedlabs.escposprintservice";
                WebService.MyAct_Impostazioni = this;
                WebService.SSID = cd.WifiSSID();
                WebService.VerificaVersioning();

                //se la versione presente sui server è diversa, indica la disponibilità di un aggiornamento
                if(VerificaAggiornamenti(verName))
                {
                    AbilitaDLPRINT = true;
                    DescInstallPRINT.setText(" - AGGIORNAMENTO " + WebService._Versioning + " DISPONIBILE - ");
                    LLayoutPRINT.setBackgroundResource(R.drawable.border_linearlayout_bviolet);
                }
                else
                    LLayoutPRINT.setBackgroundResource(R.drawable.border_linearlayout_03);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            //
            LogoInstallPRINT.setImageResource(R.drawable.installgreen);
            DescInstallPRINT.setTextColor(Color.BLUE);
        }
        else {
            LogoInstallPRINT.setImageResource(R.drawable.installred);
            DescInstallPRINT.setText(" - NON INSTALLATO - ");
            DescInstallPRINT.setTextColor(Color.RED);
            LLayoutPRINT.setBackgroundResource(R.drawable.border_linearlayout_bred);
            WebService = new AsyncCallWS(Act_Impostazioni.this);
            WebService._NomeApp = "com.loopedlabs.escposprintservice";
            WebService.MyAct_Impostazioni = this;
            WebService.SSID = cd.WifiSSID();
            if(WebService.LinkDownloadAPP())
            {
                LinkPRINT = WebService._Link;
                AbilitaDLPRINT = true;
            }
        }
    }

    public void InstallaQCODE(View view)
    {
        Toast.makeText(getApplicationContext(), "L'Applicazione è già installata", Toast.LENGTH_LONG).show();
    }
    public void InstallaPRINT(View view)
    {
        if(GPC.isAppInstalled(getApplicationContext(), "com.loopedlabs.escposprintservice"))
        {

            Toast.makeText(getApplicationContext(), "L'Applicazione è già installata", Toast.LENGTH_LONG).show();

        }
    }

    public boolean VerificaAggiornamenti(String versioning)
    {
        Resources res = getResources();
        String ControlloVersione = "";
        ControlloVersione = WebService._Versioning;

        if(!ControlloVersione.equals("TIMEOUT") && !ControlloVersione.contains("Error WS"))
        {
            if(!versioning.equals(ControlloVersione)) {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
            return false;
    }

    public void ScaricaAggiornamento(String url)
    {
        mProgressDialog = new ProgressDialog(Act_Impostazioni.this);
        mProgressDialog.setMessage("Download Libreria in corso...");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setCancelable(true);

        mProgressDialog.show();
        DownloadApp DWR = new DownloadApp(new Handler());
        DWR.mProgressDialog = mProgressDialog;
        Intent intent = new Intent(this, DownloadService.class);
        intent.putExtra("url", url);
        intent.putExtra("receiver", DWR);
        startService(intent);

    }

}
