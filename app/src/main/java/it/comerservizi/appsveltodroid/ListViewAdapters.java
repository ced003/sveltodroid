package it.comerservizi.appsveltodroid;

/**
 * Created by Administrator on 18/11/2015.
 */

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import static it.comerservizi.appsveltodroid.Constants.FIFTH_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.FIRST_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.FOURTH_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.SECOND_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.SEVENTH_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.SIXTH_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.THIRD_COLUMN;

class ViewHolder {

    TextView txtFirst;
    TextView txtSecond;
    TextView txtThird;
    TextView txtFourth;
    TextView txtFifth;
    TextView txtSixth;
    TextView txtSeventh;
}

public class ListViewAdapters extends BaseAdapter{

    public ArrayList<HashMap<String, String>> list;
    public HashMap<Integer, String> IndiceElenco;
    Activity activity;
    public String cosa= "";
    public ListViewAdapters(Activity activity,ArrayList<HashMap<String, String>> list){
        super();
        this.activity=activity;
        this.list=list;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

    TextView app;
        final ViewHolder holder;
        LayoutInflater inflater=activity.getLayoutInflater();

        //if(convertView == null){
        if(cosa=="ElencoAttivita") {
            if ((position & 1) == 0)
                convertView = inflater.inflate(R.layout.column_row, null);
            else
                convertView = inflater.inflate(R.layout.column_row, null);
        }
        else if(cosa=="DettaglioProgrammazione"){
            convertView = inflater.inflate(R.layout.column_row4, null);

            if ((position & 1) == 0)
                convertView.setBackgroundColor(Color.TRANSPARENT);
            else
                convertView.setBackgroundColor(Color.TRANSPARENT);

        }
        else if(cosa=="PrimoAccesso"){
            convertView = inflater.inflate(R.layout.column_row2, null);

            if ((position & 1) == 0)
                convertView.setBackgroundColor(Color.TRANSPARENT);
            else
                convertView.setBackgroundColor(Color.TRANSPARENT);

        }
        else if(cosa=="RicercaDati" || cosa=="QCode"){
            convertView = inflater.inflate(R.layout.column_row3, null);

            if ((position & 1) == 0)
                convertView.setBackgroundColor(Color.TRANSPARENT);
            else
                convertView.setBackgroundColor(Color.TRANSPARENT);

        }
        else if(cosa=="Comunicazioni"){
            convertView = inflater.inflate(R.layout.column_row_comunicazioni, null);

            if ((position & 1) == 0)
                convertView.setBackgroundColor(Color.TRANSPARENT);
            else
                convertView.setBackgroundColor(Color.TRANSPARENT);

        }
            holder = new ViewHolder();
        if(cosa=="RicercaDati" || cosa=="QCode") {
            holder.txtFirst = (TextView) convertView.findViewById(R.id.nominativo);
            holder.txtSecond = (TextView) convertView.findViewById(R.id.indirizzo);
            holder.txtThird = (TextView) convertView.findViewById(R.id.capcom);
            holder.txtFourth = (TextView) convertView.findViewById(R.id.accessibilita);
            holder.txtFifth = (TextView) convertView.findViewById(R.id.matricola);
            holder.txtSixth = (TextView) convertView.findViewById(R.id.datalimite);
        }
        else if(cosa=="DettaglioProgrammazione"){
            holder.txtFirst = (TextView) convertView.findViewById(R.id.nominativo);
            holder.txtSecond = (TextView) convertView.findViewById(R.id.indirizzo);
            holder.txtThird = (TextView) convertView.findViewById(R.id.capcom);
            holder.txtFourth = (TextView) convertView.findViewById(R.id.contatto);
            holder.txtFifth = (TextView) convertView.findViewById(R.id.matricola);
            holder.txtSixth = (TextView) convertView.findViewById(R.id.datalimite);
            holder.txtSeventh = (TextView) convertView.findViewById(R.id.fasciaoraria);

        }
        else if (cosa=="Comunicazioni")
        {
            holder.txtFirst = (TextView) convertView.findViewById(R.id.autore);
            holder.txtSecond = (TextView) convertView.findViewById(R.id.data);
            holder.txtThird = (TextView) convertView.findViewById(R.id.messaggio);
        }
        else
        {
            holder.txtFirst = (TextView) convertView.findViewById(R.id.nominativo);
            holder.txtSecond = (TextView) convertView.findViewById(R.id.indirizzo);
            holder.txtThird = (TextView) convertView.findViewById(R.id.capcom);
            holder.txtFourth = (TextView) convertView.findViewById(R.id.matricola);
            holder.txtFifth = (TextView) convertView.findViewById(R.id.datalimite);
        }


            convertView.setTag(holder);
        /*}
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }*/
        try{
            HashMap<String, String> map=list.get(position);
            if(cosa=="RicercaDati" || cosa=="QCode") {
                holder.txtFirst.setText(map.get(FIRST_COLUMN));
                holder.txtSecond.setText(map.get(SECOND_COLUMN));
                holder.txtThird.setText(map.get(THIRD_COLUMN));
                holder.txtFourth.setText(map.get(FOURTH_COLUMN));
                holder.txtFifth.setText(map.get(FIFTH_COLUMN));
                holder.txtSixth.setText(map.get(SIXTH_COLUMN));
            }
            else if(cosa=="DettaglioProgrammazione")
            {
                holder.txtFirst.setText(map.get(FIRST_COLUMN));
                holder.txtSecond.setText(map.get(SECOND_COLUMN));
                holder.txtThird.setText(map.get(THIRD_COLUMN));
                holder.txtFourth.setText(map.get(FOURTH_COLUMN));
                holder.txtFifth.setText(map.get(FIFTH_COLUMN));
                holder.txtSixth.setText(map.get(SIXTH_COLUMN));
                holder.txtSeventh.setText(map.get(SEVENTH_COLUMN));
            }
            else if(cosa=="Comunicazioni")
            {
                holder.txtFirst.setText(map.get(FIRST_COLUMN));
                holder.txtSecond.setText(map.get(SECOND_COLUMN));
                holder.txtThird.setText(map.get(THIRD_COLUMN));
            }
            else
            {
                holder.txtFirst.setText(map.get(FIRST_COLUMN));
                holder.txtSecond.setText(map.get(SECOND_COLUMN));
                holder.txtThird.setText(map.get(THIRD_COLUMN));
                holder.txtFourth.setText(map.get(FOURTH_COLUMN));
                holder.txtFifth.setText(map.get(FIFTH_COLUMN));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return convertView;
    }

}