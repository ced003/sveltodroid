package it.comerservizi.appsveltodroid;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.graphics.Paint;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class Act_newCamera extends AppCompatActivity {
    private static final String TAG = "Act_newCamera";
    private TextureView textureView;
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();
    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }
    GPClass gpc;
    private String cameraId;
    protected CameraDevice cameraDevice;
    protected CameraCaptureSession cameraCaptureSessions;
    protected CaptureRequest captureRequest;
    protected CaptureRequest.Builder captureRequestBuilder;
    private Size imageDimension;
    private ImageReader imageReader;
    private File file;
    private static final int REQUEST_CAMERA_PERMISSION = 200;
    private boolean mScattaFoto;
    private Handler mBackgroundHandler;
    private HandlerThread mBackgroundThread;
    CameraManager manager;


    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    ToggleButton btnTorcia;
    String DataAttivita;
    String OraAttivita;
    String Misuratore;
    String Correttore;
    String TestoSuFoto;
    Button captureButton;
    Button NewPhotoButton;
    android.widget.ImageView ImageView;
    private static File sdCard;
    private static File sdCardBKP;
    private static File sdCardNTB;
    private static File sdCardBKPNTB;
    public static Integer ContatoreNomeFoto = 0;
    Integer TipoFuoco = 0;
    String ContatoreFoto = "0";
    int width;
    int height;
    int rotation;
    String mCurrentPhotoPath;
    Uri fileUri ;
    RatingBar ratingBar;
    Boolean RefreshCamera;
    Boolean SalvaFoto = true;
    private FrameLayout preview;
    private double SumOrientation = 0;
    private boolean sersorrunning;
    private boolean click = false;
    private TextView OverlayText,ErrorMessageCamera;
    public static Integer CountPhotoUno;
    public static String IDCarico, ODL;
    LayoutInflater controlInflater = null;
    public static Integer CountPhotoVC, CountPhotoVM, CountPhotoVB, CountPhotoVE, CountPhotoB1, CountPhotoB2,CountPhotoP1;
    public static String IDAzienda, CodiceUtente;
    String DefLong,GlobalConfig;
    String DefLat;

    private String operatore,TipoFoto;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_act_new_camera);
        onTrimMemory(ComponentCallbacks2.TRIM_MEMORY_COMPLETE);
        onTrimMemory(ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN);
        System.gc();
        gpc = new GPClass();
        RefreshCamera = false;
        //captureButton = (Button) findViewById(R.id.button_capture);
        //NewPhotoButton = (Button) findViewById(R.id.btn_Nuova);
        //ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        //captureButton.setEnabled(true);
        //NewPhotoButton.setEnabled(false);
        CountPhotoUno = 0;
        ContatoreNomeFoto = 0;
        mScattaFoto = true;

        sdCard = new File(getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS).toString() + File.separator + "SveltoDroidPhoto" );
        sdCardBKP = new File(getApplicationContext().getFilesDir().toString() + File.separator + "SveltoDroidPhoto" );
        //Log.e("CopiaUno_0", "sdCard: " + sdCard.getAbsolutePath().toString() + " - sdCardBKP: " + sdCardBKP.getAbsolutePath().toString());
        sdCardNTB = new File(getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS).toString() + File.separator + "SveltoDroidNTB" );
        sdCardBKPNTB = new File(getApplicationContext().getFilesDir().toString() + File.separator + "SveltoDroidNTB" );
        sdCardBKP.setWritable(true);
        sdCard.setWritable(true);
        sdCardBKPNTB.setWritable(true);
        sdCardNTB.setWritable(true);

        if (!sdCard.exists()) {
            if (!sdCard.mkdirs()) {
                Log.i("DB", "Impossibile creare la folder");
            }
        }
        if (!sdCardBKP.exists()) {
            if (!sdCardBKP.mkdirs()) {
                Log.i("DB", "Impossibile creare la folder");
            }
        }
        if (!sdCardNTB.exists()) {
            if (!sdCardNTB.mkdirs()) {
                Log.i("DB", "Impossibile creare la folder");
            }
        }
        if (!sdCardBKPNTB.exists()) {
            if (!sdCardBKPNTB.mkdirs()) {
                Log.i("DB", "Impossibile creare la folder");
            }
        }

        Bundle extras = getIntent().getExtras();
        if(extras !=null) {
            CodiceUtente = extras.getString("CodiceUtente");
            CodiceUtente = CodiceUtente.trim();
            IDCarico = extras.getString("IDCarico");
            IDAzienda = extras.getString("IDAzienda");
            DataAttivita = extras.getString("DataAttivita");
            OraAttivita = extras.getString("OraAttivita");
            Misuratore = extras.getString("Misuratore");
            Correttore = extras.getString("Correttore");
            CountPhotoVC = extras.getInt("CountPhotoVC");
            CountPhotoVM = extras.getInt("CountPhotoVM");
            CountPhotoVB = extras.getInt("CountPhotoVB");
            CountPhotoVE = extras.getInt("CountPhotoVE");
            CountPhotoB1 = extras.getInt("CountPhotoB1");
            CountPhotoB2 = extras.getInt("CountPhotoB2");
            CountPhotoP1 = extras.getInt("CountPhotoP1");
            TipoFoto = extras.getString("TipoFoto");
            DefLat = extras.getString("DefLat");
            DefLong = extras.getString("DefLong");
            operatore = extras.getString("operatore");
            GlobalConfig = extras.getString("GlobalConfig");
        }
        IDCarico=IDCarico.replace(String.valueOf((char) 160), " ").trim();
        DataAttivita=DataAttivita.replace(String.valueOf((char) 160), " ").trim();
        OraAttivita = new SimpleDateFormat("HH:mm:ss").format(new Date());
        OraAttivita=OraAttivita.replace(String.valueOf((char) 160), " ").trim();

        if(GlobalConfig.equals("6"))
        {
            //Se attività è torino, aggiungi coordinati sulla foto
            String appo;
            if(DefLat!=null) {
                if (!DefLat.equals("0") && (DefLat.length()>=6)) {
                    appo = DefLat.substring(0, DefLat.indexOf(".")) + DefLat.substring(DefLat.indexOf("."), 5 + (DefLat.indexOf(".") + 1));
                    DefLat = appo;
                    appo = DefLong.substring(0, DefLong.indexOf(".")) + DefLat.substring(DefLat.indexOf("."), 5 + (DefLat.indexOf(".") + 1));
                    DefLong = appo;
                }
            }
            TestoSuFoto = CodiceUtente +" "+ DataAttivita +" " + OraAttivita +" "+ DefLat + " "+DefLong;

        }
        else
        {
            //Altrimenti testo standard (misuratore, corretto, data e ora)
            TestoSuFoto = Misuratore + " [" + Correttore + "] " + DataAttivita + " " + OraAttivita;
            TestoSuFoto = TestoSuFoto.replace("[] ", "");
        }
        textureView = (TextureView) findViewById(R.id.texture);
        btnTorcia = (ToggleButton) findViewById(R.id.btnTorcia2);
        ImageView = (ImageView) findViewById(R.id.imageView);
        ImageView.setVisibility(View.INVISIBLE);
        assert textureView != null;
        textureView.setSurfaceTextureListener(textureListener);


        textureView.setOnLongClickListener(new LinearLayout.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v)
            {

                if(SalvaFoto)
                {
                    mScattaFoto = false;
                    takePicture();
                }
                else
                    Toast.makeText(getApplicationContext(), "Devi girare il dispositivo", Toast.LENGTH_SHORT).show();

                return true;
            }

        });

/*
        textureView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(SalvaFoto)
                {
                    takePicture();
                }
            }
        }
        );
*/
/*
        SensorManager mySensorManager;
        mySensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        List<Sensor> mySensors = mySensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER);

        if(mySensors.size() > 0){
            mySensorManager.registerListener(mySensorEventListener, mySensors.get(0), SensorManager.SENSOR_DELAY_GAME);
            sersorrunning = true;
        }
        else{
            sersorrunning = false;
        }
        */
    }
    TextureView.SurfaceTextureListener textureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            //open your camera here
            openCamera();
        }
        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
            // Transform you image captured size according to the surface width and height
        }
        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            return false;
        }
        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {
        }
    };
    private final CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(CameraDevice camera) {
            //This is called when the camera is open
            Log.e(TAG, "onOpened");
            cameraDevice = camera;
            createCameraPreview();
        }
        @Override
        public void onDisconnected(CameraDevice camera) {
            cameraDevice.close();
        }
        @Override
        public void onError(CameraDevice camera, int error) {
            cameraDevice.close();
            cameraDevice = null;
        }
    };
    final CameraCaptureSession.CaptureCallback captureCallbackListener = new CameraCaptureSession.CaptureCallback() {
        @Override
        public void onCaptureCompleted(CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result) {
            super.onCaptureCompleted(session, request, result);
            Toast.makeText(Act_newCamera.this, "Foto Salvata!", Toast.LENGTH_SHORT).show();
            createCameraPreview();
        }
    };
    protected void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("Camera Background");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }
    protected void stopBackgroundThread() {
        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    protected void takePicture() {
        SalvaFoto = false;
        if(null == cameraDevice) {
            Log.e(TAG, "cameraDevice is null");
            return;
        }
            try {
                CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraDevice.getId());
                Size[] jpegSizes = null;
                if (characteristics != null) {
                    jpegSizes = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP).getOutputSizes(ImageFormat.JPEG);
                }
                width = 640;
                height = 480;
                if (jpegSizes != null && 0 < jpegSizes.length) {
                    width = jpegSizes[1].getWidth();
                    height = jpegSizes[1].getHeight();
                }
                //Index 0 : 320
                //Index 1 : 640
                //Index 2 : 1024
                //Index 3 : 1600
                //Index 4 : 2048
                //Index 5 : 2560
                ImageReader reader = ImageReader.newInstance(width, height, ImageFormat.JPEG, 1);
                List<Surface> outputSurfaces = new ArrayList<Surface>(2);
                outputSurfaces.add(reader.getSurface());
                outputSurfaces.add(new Surface(textureView.getSurfaceTexture()));
                final CaptureRequest.Builder captureBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
                captureBuilder.addTarget(reader.getSurface());
                captureBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
                captureRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CameraMetadata.CONTROL_AF_MODE_CONTINUOUS_PICTURE);

                if (btnTorcia.isChecked())
                    captureBuilder.set(CaptureRequest.FLASH_MODE, CameraMetadata.FLASH_MODE_TORCH);
                else
                    captureBuilder.set(captureRequest.FLASH_MODE, CameraMetadata.FLASH_MODE_OFF);
                // Orientation
                rotation = 90;
                captureBuilder.set(CaptureRequest.JPEG_ORIENTATION, ORIENTATIONS.get(rotation));
                final File file = new File(sdCardBKP + File.separator + IDCarico + "_" + IDAzienda + "_" + CodiceUtente + "_" + TipoFoto + "_" + (Integer.parseInt(ContatoreFoto) + 1) + ".jpg");
                Log.e("TakePicture_Dati ", "percorso :" + file.getAbsolutePath().toString());
                ImageReader.OnImageAvailableListener readerListener = new ImageReader.OnImageAvailableListener() {
                    @Override
                    public void onImageAvailable(ImageReader reader) {
                        Image image = null;
                        try {
                            image = reader.acquireLatestImage();
                            ByteBuffer buffer = image.getPlanes()[0].getBuffer();
                            byte[] bytes = new byte[buffer.capacity()];
                            buffer.get(bytes);
                            save(bytes);
                            Log.e("TakePicture_onImageAva", "salvato");
                        } catch (FileNotFoundException e) {
                            Log.e("TakePicture_Errore ", "Errore takePicture FileNotFoundException :" + e.toString());
                            e.printStackTrace();
                        } catch (IOException e) {
                            Log.e("TakePicture_Errore ", "Errore takePicture IOException :" + e.toString());
                            e.printStackTrace();
                        } finally {
                            if (image != null) {
                                image.close();
                            }
                        }
                    }

                    private void save(byte[] bytes) throws IOException {
                        OutputStream output = null;
                        try {
                            output = new FileOutputStream(file);
                            output.write(bytes);
                            Log.e("TakePicture_save", "salvato");
                            ScriviSuFoto(file.getAbsolutePath());
                        } catch(Exception ex)
                        {
                            Log.e("save_Errore ", "Errore save Exception :" + ex.toString());
                        }finally
                         {
                            if (null != output) {
                                output.close();
                            }
                        }
                    }
                };
                reader.setOnImageAvailableListener(readerListener, mBackgroundHandler);
                final CameraCaptureSession.CaptureCallback captureListener = new CameraCaptureSession.CaptureCallback() {
                    @Override
                    public void onCaptureCompleted(CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result) {
                        super.onCaptureCompleted(session, request, result);
                        createCameraPreview();
                    }
                };
                cameraDevice.createCaptureSession(outputSurfaces, new CameraCaptureSession.StateCallback() {
                    @Override
                    public void onConfigured(CameraCaptureSession session) {
                        try {
                            session.capture(captureBuilder.build(), captureListener, mBackgroundHandler);
                        } catch (CameraAccessException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onConfigureFailed(CameraCaptureSession session) {
                    }
                }, mBackgroundHandler);
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
    }
    protected void createCameraPreview() {
        try {
            SurfaceTexture texture = textureView.getSurfaceTexture();
            assert texture != null;
            texture.setDefaultBufferSize(imageDimension.getWidth(), imageDimension.getHeight());
            Surface surface = new Surface(texture);
            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            captureRequestBuilder.addTarget(surface);
            cameraDevice.createCaptureSession(Arrays.asList(surface), new CameraCaptureSession.StateCallback(){
                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                    //The camera is already closed
                    if (null == cameraDevice) {
                        return;
                    }
                    // When the session is ready, we start displaying the preview.
                    cameraCaptureSessions = cameraCaptureSession;

                        updatePreview();
                }
                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                    Toast.makeText(getApplicationContext(), "Configuration change", Toast.LENGTH_SHORT).show();
                }
            }, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }
    private void openCamera() {
        onTrimMemory(ComponentCallbacks2.TRIM_MEMORY_COMPLETE);
        onTrimMemory(ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN);
        System.gc();
        manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        Log.e(TAG, "is camera open");
        try {
            cameraId = manager.getCameraIdList()[0];
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);
            StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            assert map != null;
            imageDimension = map.getOutputSizes(SurfaceTexture.class)[0];
            // Add permission for camera and let user grant the permission
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(Act_newCamera.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA_PERMISSION);
                return;
            }
            manager.openCamera(cameraId, stateCallback, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "openCamera X");
    }
    protected void updatePreview() {
        if(null == cameraDevice) {
            Log.e(TAG, "updatePreview error, return");
        }
        if(mScattaFoto) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    ImageView.setVisibility(View.INVISIBLE);
                }
            });
            captureRequestBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
            captureRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CameraMetadata.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
            if (btnTorcia.isChecked()) {
                captureRequestBuilder.set(CaptureRequest.FLASH_MODE, CameraMetadata.FLASH_MODE_TORCH);
            } else
                captureRequestBuilder.set(captureRequest.FLASH_MODE, CameraMetadata.FLASH_MODE_OFF);

            try {
                cameraCaptureSessions.setRepeatingRequest(captureRequestBuilder.build(), null, mBackgroundHandler);
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }
    }
    private void closeCamera() {
        if (null != cameraDevice) {
            cameraDevice.close();
            cameraDevice = null;
        }
        if (null != imageReader) {
            imageReader.close();
            imageReader = null;
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                // close the app
                Toast.makeText(getApplicationContext(), "ERRORE L'APP NON HA AVUTO I PERMESSI PER USARE LA FOTOCAMERA", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "onResume");
        startBackgroundThread();
        if (textureView.isAvailable()) {

            if(mScattaFoto) {
                openCamera();
            }
        } else {
            textureView.setSurfaceTextureListener(textureListener);
        }
    }
    @Override
    protected void onPause() {
        Log.e(TAG, "onPause");
        //closeCamera();
        stopBackgroundThread();
        super.onPause();
    }



    private void ScriviSuFoto(String photoPath)
    {
        String photopath2 = "";
        String photopathNTB = "";
        String photopathNTB2 = "";
        onTrimMemory(ComponentCallbacks2.TRIM_MEMORY_COMPLETE);
        onTrimMemory(ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN);
        System.gc();


        OraAttivita = new SimpleDateFormat("HH:mm:ss").format(new Date());
        OraAttivita=OraAttivita.replace(String.valueOf((char) 160), " ").trim();

        if(GlobalConfig.equals("6"))
        {
            TestoSuFoto = CodiceUtente +" "+ DataAttivita +" " + OraAttivita +" "+ DefLat + " "+DefLong;
        }
        else
        {
            TestoSuFoto = Misuratore + " [" + Correttore + "] " + DataAttivita + " " + OraAttivita;
            TestoSuFoto = TestoSuFoto.replace("[] ", "");
        }

        String FirstPath;
        FirstPath= "";
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = BitmapFactory.decodeFile(photoPath, options);
        bitmap.setDensity(DisplayMetrics.DENSITY_280);
        Bitmap mutableBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
        Canvas canvas = new Canvas(mutableBitmap);
        canvas.setDensity(DisplayMetrics.DENSITY_280);
        Paint paint = new Paint();
        paint.setTextSize(18);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        //Rect rettangolo = new Rect();
        //rettangolo.set(0 , height-50,width,height );
        //canvas.drawRect(rettangolo, paint);
        paint.setColor(Color.RED);
        try {
            mutableBitmap.setDensity(DisplayMetrics.DENSITY_280);
            photopathNTB = photoPath;
            photopathNTB= photopathNTB.replace(".jpg",".NTB");
            //photoPath = photoPath.replace(sdCard.toString(),sdCardBKP.toString());
            photopathNTB= photopathNTB.replace("SveltoDroidPhoto","SveltoDroidNTB");
            mutableBitmap.compress(Bitmap.CompressFormat.JPEG, 90, new FileOutputStream(new File(photopathNTB)));

            try
            {
                photopathNTB2 = photopathNTB.replace(sdCardBKPNTB.toString(),sdCardNTB.toString());
                Log.e("CopiaUno", "STO - da: " + photopathNTB + " a: " + photopathNTB2);
                gpc.copy(new File(photopathNTB),new File(photopathNTB2));
                Log.e("CopiaUno", "OK - da: " + photopathNTB + " a: " + photopathNTB2);
            }catch(Exception ex)
            {
                //DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                //String date = df.format(Calendar.getInstance().getTime());
                //db.execSQL("INSERT INTO LogErrori VALUES ('AsyncUpload', '" + GlobalConfig +"','"+operatore+"','"+ex.getCause().getMessage()+ " - " +ex.getMessage().toString()+"','"+ date +"')");

                Log.e("CopiaUno", "NO - da: " + photopathNTB + " a: " + photopathNTB2);
            }
            onTrimMemory(ComponentCallbacks2.TRIM_MEMORY_COMPLETE);
            onTrimMemory(ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN);
            onTrimMemory(ComponentCallbacks2.TRIM_MEMORY_COMPLETE);
            onTrimMemory(ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN);
            onTrimMemory(ComponentCallbacks2.TRIM_MEMORY_COMPLETE);
            onTrimMemory(ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN);
            System.gc();
            System.gc();
            System.gc();

            mutableBitmap.setDensity(DisplayMetrics.DENSITY_280);
            //**TESTARE LA ROTAZIONE DEL TESTO
            canvas.drawText(TestoSuFoto, 15, height-5, paint);
            photopath2 = photoPath.replace(sdCardBKP.toString(),sdCard.toString());
            mutableBitmap.compress(Bitmap.CompressFormat.JPEG, 90, new FileOutputStream(new File(photoPath)));
            if(FirstPath.trim().length()<1)
                FirstPath = photoPath;
            gpc.copy(new File(photoPath), new File(photopath2));

            final File image;
            image = new File(photopath2);
            //Il contatore viene incrementato se la foto è stata salvata e sovrascritta

            ContatoreFoto = CounterFoto();
            mutableBitmap.recycle();
            mutableBitmap = null;
            bitmap.recycle();
            bitmap = null;
            onTrimMemory(ComponentCallbacks2.TRIM_MEMORY_COMPLETE);
            onTrimMemory(ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN);
            System.gc();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    setPic(image);
                }
            });
            SalvaFoto = true;
            // dest is Bitmap, if you want to preview the final image, you can display it on screen also before saving
        } catch (FileNotFoundException e) {
            Toast.makeText(getApplicationContext(), "ATTENZIONE LA FOTO NON E' VENUTA [Errore F03: " + e.getMessage().toString(), Toast.LENGTH_SHORT).show();
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch(Exception ex)
        {
            Toast.makeText(getApplicationContext(), "ATTENZIONE LA FOTO NON E' VENUTA [Errore F03: " + ex.getMessage().toString(), Toast.LENGTH_SHORT).show();
            // TODO Auto-generated catch block
            ex.printStackTrace();
        }
    }


    private void setPic(File image) {
        // Get the dimensions of the View
        try {
            int targetW = textureView.getWidth();
            int targetH = textureView.getHeight();
            RefreshCamera = true;
            // Get the dimensions of the bitmap
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
            int photoW = width;
            int photoH = height;

            // Determine how much to scale down the image
            int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

            // Decode the image file into a Bitmap sized to fill the View
            bmOptions.inJustDecodeBounds = false;
            bmOptions.inSampleSize = scaleFactor;
            bmOptions.inPurgeable = true;

            Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(), bmOptions);
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraDevice.getId());
            Size[] jpegSizes = null;
            if (characteristics != null) {
                jpegSizes = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP).getOutputSizes(ImageFormat.JPEG);
            }
            Bitmap resized = Bitmap.createScaledBitmap(bitmap, jpegSizes[5].getWidth(), jpegSizes[5].getHeight(), true);
            ImageView.setImageBitmap(resized);
            mScattaFoto = false;
            ImageView.setVisibility(View.VISIBLE);
            ImageView.setOnClickListener(new View.OnClickListener() {
                                             @Override
                                             public void onClick(View v) {
                                                 ImageView.setVisibility(View.INVISIBLE);
                                                 mScattaFoto = true;
                                                 updatePreview();
                                             }
                                         }
            );
        }catch(Exception ex)
        {

            Toast.makeText(getApplicationContext(), "Errore nel mostrare la foto: " + ex.toString(), Toast.LENGTH_SHORT).show();
        }

    }



    public void AttivaTorcia(View view)
    {
        updatePreview();

    }

/*
    private SensorEventListener mySensorEventListener = new SensorEventListener() {
        int a, b, c;
        @Override
        public void onSensorChanged(SensorEvent event) {
            // TODO Auto-generated method stub


            float X_Axis = event.values[0];
            float Y_Axis = event.values[1];

            if((X_Axis <= 6 && X_Axis >= -6) && Y_Axis > 5){
                mScattaFoto = false;
                SalvaFoto = false;
            }
            else if(X_Axis >= 6 || X_Axis <= -6){
                mScattaFoto = true;
                SalvaFoto = true;
            }

        }
        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            // TODO Auto-generated method stub


        }
    };
*/
    public String CounterFoto()
    {
        String result = "";

        if(TipoFoto.equals("B1")) {
            CountPhotoB1++;
            result = CountPhotoB1.toString();
        }
        else if(TipoFoto.equals("B2")){
            CountPhotoB2++;
            result = CountPhotoB2.toString();
        }
        else if(TipoFoto.equals("VM")){
            CountPhotoVM++;
            result = CountPhotoVM.toString();
        }
        else if(TipoFoto.equals("VC")){
            CountPhotoVC++;
            result = CountPhotoVC.toString();
        }
        else if(TipoFoto.equals("VE")){
            CountPhotoVE++;
            result = CountPhotoVE.toString();
        }
        else if (TipoFoto.equals("VB")){
            CountPhotoVB++;
            result = CountPhotoVB.toString();
        }
        else if (TipoFoto.equals("P1")){
            CountPhotoP1++;
            result = CountPhotoP1.toString();
        }
        else
            result = "0";
        return result;
    }


    public void QualiFotoMancano()
    {
        if(CountPhotoB1==0 && TipoFoto.equals("B1"))
            Toast.makeText(getApplicationContext(), "Mancano le foto Nota Uno", Toast.LENGTH_SHORT).show();
        else if(CountPhotoB2==0 && TipoFoto.equals("B2"))
            Toast.makeText(getApplicationContext(), "Mancano le foto Nota Due", Toast.LENGTH_SHORT).show();
        else if(CountPhotoVM==0 && TipoFoto.equals("VM"))
            Toast.makeText(getApplicationContext(), "Mancano le foto VM", Toast.LENGTH_SHORT).show();
        else if(CountPhotoVC==0 && TipoFoto.equals("VC"))
            Toast.makeText(getApplicationContext(), "Mancano le foto VC", Toast.LENGTH_SHORT).show();
        else if(CountPhotoVE==0 && TipoFoto.equals("VE"))
            Toast.makeText(getApplicationContext(), "Mancano le foto VE", Toast.LENGTH_SHORT).show();
        else if(CountPhotoVB==0 && TipoFoto.equals("VB"))
            Toast.makeText(getApplicationContext(), "Mancano le foto VB", Toast.LENGTH_SHORT).show();
        else if(CountPhotoP1==0 && TipoFoto.equals("P1"))
            Toast.makeText(getApplicationContext(), "Mancano le foto della programmazione", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onBackPressed() {

        if(ControlloAssenzaScatto(TipoFoto))
        {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("B1", Integer.toString(CountPhotoB1));
            returnIntent.putExtra("B2", Integer.toString(CountPhotoB2));
            returnIntent.putExtra("VM", Integer.toString(CountPhotoVM));
            returnIntent.putExtra("VC", Integer.toString(CountPhotoVC));
            returnIntent.putExtra("VE", Integer.toString(CountPhotoVE));
            returnIntent.putExtra("VB", Integer.toString(CountPhotoVB));
            returnIntent.putExtra("P1", Integer.toString(CountPhotoP1));
            setResult(Act_CambiaIndirizzo.RESULT_OK, returnIntent);
            finish();
        }
        else
        {
            QualiFotoMancano();
        }
    }


    private boolean ControlloAssenzaScatto(String TipoFoto)
    {
        boolean ver = false;
        if(TipoFoto.equals("B1") && CountPhotoB1>0)
            ver = true;
        else if(TipoFoto.equals("B2") && CountPhotoB2>0)
            ver = true;
        else if(TipoFoto.equals("VM") && CountPhotoVM>0)
            ver = true;
        else if(TipoFoto.equals("VC") && CountPhotoVC>0)
            ver = true;
        else if(TipoFoto.equals("VE") && CountPhotoVE>0)
            ver = true;
        else if(TipoFoto.equals("VB") && CountPhotoVB>0)
            ver = true;
        else if(TipoFoto.equals("P1") && CountPhotoP1>0)
            ver = true;
        else
            ver = false;

        return ver;
    }


}