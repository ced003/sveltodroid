package it.comerservizi.appsveltodroid;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.regex.Pattern;

import static it.comerservizi.appsveltodroid.Constants.FIFTH_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.FIRST_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.FOURTH_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.SECOND_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.SEVENTH_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.SIXTH_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.THIRD_COLUMN;
/**
 * Created by guido.palumbo on 18/11/2015.
 */
public class AsyncLoader extends AsyncTask<Void, String, Void> {
    String TAG = "response";
    GPSHelper gps;
    boolean statusOfGPS;
    private ProgressDialog dialog;
    GPClass GP;
    Integer LastProgresso;
    Activity MyActivity;
    DatabaseHelper dbHelper;
    SQLiteDatabase db;
    private String cosa = "";
    private AsyncLoader ALoader;
    public Act_Ricerca ActRicerca;
    public Act_ElencoProgrammazione ActProgrammazione;
    public Act_QCode ActQCode;
    public String SubElencoAttivita;
    public String Search4Matricola = "";
    public TextView txtNumAttivita;
    public String operatore;
    public String ResWS = "";
    public String SSID = "";
    public String IDLett = "";
    public String IDTerminale = "";
    public String VersoOrdinato;
    public String GiornoAccesso;
    public String _CodiceUtente;
    public ArrayList ListaArray;
    public ArrayList ListaArray2;
    public ArrayList ListaArray3;
    public ArrayList ListaArray4;
    public String Search4Carico;
    public String Search4Comune;
    public String Search4dtProg;
    public String Search4;
    public String Search4How;
    public String _Errore = "";
    public String Latitudine="";
    public String Longitudine="";
    public Boolean Search4Civico;
    public Boolean EsitoGetCoordinate = false;
    public Boolean Search4Concluse;
    public ArrayList<String> IndiceElencoCodiceUtente;
    public ArrayList<String> IndiceElencoIdCarico;
    public ArrayList<String> IndiceElencoIdAzienza;
    public ArrayList<String> IndiceElencoIndirizzi;
    public Calendar DataOraCerta;
    public Integer LastPosition;
    private Integer PositionOfClick;
    String GlobalConfig;

    public AsyncLoader(Activity activity, String Definizione) {
        Log.i(TAG, "AsyncLoader Costruttore");
        MyActivity = activity;
        cosa = Definizione;
        dialog = new ProgressDialog(activity);
        dialog.setCancelable(false);
        GP = new GPClass();
        dbHelper = new DatabaseHelper(MyActivity);
        db= dbHelper.getWritableDatabase();
        try {
            Cursor c = db.rawQuery("SELECT IdConfig FROM AndroConfig WHERE Type = 'sGlobalConfiguration'", null);

            if (c != null)
                c.moveToFirst();

            GlobalConfig = c.getString(c.getColumnIndex("IdConfig"));
        }catch (Exception e) {
            Log.i("ErrDB", e.toString());
        }
        Crashlytics.log(null);
        Crashlytics.log("GlobalConfig: " + GlobalConfig + " - Operatore: " + operatore );
    }

    @Override
    protected void onPreExecute() {

        if(cosa == "Login")
            dialog.setMessage("Accesso in corso...");
        else if (cosa == "PrimoAccesso" || cosa =="DettaglioAttivita")
        {
                dialog.setMessage("Elaborazione del carico in corso...");
                dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        }
        else if (cosa == "RicercaDati")
        {
            if(Search4Concluse)
                dialog.setMessage("Ricerca Attività già chiuse in corso..");
            else
                dialog.setMessage("Ricerca Attività da chiudere in corso..");

            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        }
        else if (cosa == "DettaglioProgrammazione")
        {
            dialog.setMessage("Ricerca attività su appuntamento..");

            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        }
        else if (cosa == "QCode")
        {
            if(Search4Concluse)
                dialog.setMessage("Ricerca Attività da riaprire in corso....");
            else
                dialog.setMessage("Ricerca Attività in corso....");

            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        }
        else if (cosa == "PrelevaCoordinate")
        {

            dialog.setMessage("Ricerca Coordinate in corso.......");

            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        }
        else
            dialog.setMessage("Elaborazione in corso...");
        try {
            dialog.show();
        }catch(Exception ex)
        {
            Log.e("ERROR",ex.toString());
        }
    }


    @Override
    protected Void doInBackground(Void... params) {
        try {
            if(cosa=="Login") {
                Thread.sleep(1500);
            }
            else if(cosa=="PrimoAccesso" || cosa=="DettaglioAttivita")
            {
                Thread.sleep(2500);
            }
            else if(cosa=="RicercaDati")
            {
                Thread.sleep(2500);
            }
            else if(cosa=="DettaglioProgrammazione")
            {
                Thread.sleep(2000);
            }
            else if(cosa=="QCode")
            {
                Thread.sleep(1500);
            }
            else if (cosa == "PrelevaCoordinate")
            {
                EsitoGetCoordinate = GetCoordinate(GlobalConfig,_CodiceUtente);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
        //Se si tratta di login , l'activity Act_Accesso passa
        //Il risultato del WebService. Il loader è impostato fisso a 5 secondi
        //Se il login ha dato esito positivo chiude la prima activity e lancia la listaCarico
        //Altrimenti apre un popup comunicando l'errore riscontrato
        if(cosa == "Login")
        {
            AlertDialog.Builder dialog = new AlertDialog.Builder(MyActivity);
            if(!ResWS.equals("errore")) {
                Intent intent = new Intent(MyActivity, Act_ElencoAttivita.class);

                intent.putExtra("IDTerminale", ResWS);
                intent.putExtra("IDLetturista", IDLett);
                intent.putExtra("GiornoAccesso", GiornoAccesso);
                //DataOraCerta.setTimeInMillis((DataOraCerta.getTimeInMillis() + SystemClock.currentThreadTimeMillis()));
                intent.putExtra("DataOraCerta", DataOraCerta.getTimeInMillis());

                //Log.i("DataOraCerta - AsyncLoader", new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(DataOraCerta.getTime()));
                MyActivity.startActivity(intent);
                MyActivity.finish();
            }
            else {

                //this.BtnLoggati.setVisibility(View.VISIBLE);

                dialog
                        .setTitle("ERRORE ACCESSO")
                        .setMessage("Attenzione, dati di accesso errati! [" + ResWS + "]")
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });
                AlertDialog MsgErrore = dialog.create();
                //Rimuove il bottone negativo dall'AlertDialog
                MsgErrore.setOnShowListener(new DialogInterface.OnShowListener() {

                    @Override
                    public void onShow(DialogInterface dialog) {
                        ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_NEGATIVE).setVisibility(View.INVISIBLE);
                    }
                });
                MsgErrore.show();
            }
        }
        else if(cosa=="RicercaDati")
        {
            String data;
            String WhereStato = "";
            String WhereCosa = "";
            String FromTabella = "";
            String QueryRicerca = "";
            Integer countElenco = 0;


            ArrayList<HashMap<String, String>> list=new ArrayList<HashMap<String,String>>();
            ListView listView=(ListView)MyActivity.findViewById(R.id.listView1);

            IndiceElencoCodiceUtente = new ArrayList<String>();
            IndiceElencoCodiceUtente.clear();
            IndiceElencoIndirizzi = new ArrayList<String>();
            IndiceElencoIndirizzi.clear();
            IndiceElencoIdCarico = new ArrayList<String>();
            IndiceElencoIdCarico.clear();
            IndiceElencoIdAzienza = new ArrayList<String>();
            IndiceElencoIdAzienza.clear();
            Cursor cursor;
            Search4How = Search4How.replace("'","''");
            if(Search4Concluse)
            {
                WhereStato = "Stato = 'L'";
                FromTabella = "utentiOUT";
            }
            else
            {
                WhereStato = "Stato = 'A'";
                FromTabella = "utenti";
            }

            if(Search4.contains("Indirizzo"))
                WhereCosa= "(Toponimo || ' ' || Strada|| ' ' || Civico) LIKE '%"+Search4How+"%'";
            else if(Search4.contains("Comune"))
                WhereCosa= "Localita LIKE '%"+Search4How+"%'";
            else if(Search4.contains("Nominativo"))
                WhereCosa= "(Nominativo LIKE '%"+Search4How+"%' OR Cognome LIKE '%"+Search4How+"%' OR (Nominativo || ' ' || Cognome) LIKE '%"+Search4How+"%' OR (Cognome || ' ' || Nominativo) LIKE '%"+Search4How+"%')";
            else if(Search4.contains("Matricola"))
                WhereCosa= "MatricolaMis LIKE '%"+Search4How+"%'";
            else if(Search4.contains("Codice Utente"))
                WhereCosa= "CodiceUtente LIKE '%"+Search4How+"%'";
            else if(Search4.contains("Info Aggiuntive Posizione"))
                WhereCosa= "NoteAccesso LIKE '%"+Search4How+"%'";
            else if(Search4Concluse && Search4.contains("Solo Clienti Assenti"))
            {
                if(GlobalConfig.equals("11"))
                    WhereCosa = "(LNota1 = '012' OR LNota1 = '12' OR LNota2='012' OR LNota2='12')";
                else
                    WhereCosa = "(LNota1 = '013' OR LNota1 = '067' OR LNota2='013' OR LNota2='067')";
            }
            if(!Search4Carico.contains("nessuno"))
                WhereCosa = WhereCosa + " AND IdCarico = '" + Search4Carico + "'";
            if(!Search4Comune.contains("nessuno"))
                WhereCosa = WhereCosa + " AND Localita = '" + Search4Comune + "'";

            QueryRicerca = "SELECT * FROM " + FromTabella + " WHERE " + WhereCosa + " AND " + WhereStato + " ORDER BY Localita, Toponimo, Strada, CAST(Civico AS Integer)";
            cursor = dbHelper.query(db,QueryRicerca);

            cursor.moveToFirst();
            while (cursor.isAfterLast() == false)
            {
                HashMap<String,String> temp=new HashMap<String, String>();
                temp.put(FIRST_COLUMN, cursor.getString(cursor.getColumnIndex("Nominativo")) );
                temp.put(SECOND_COLUMN, GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Toponimo"))) + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Strada"))) + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Civico"))) + " - " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("NotaAccesso100"))));
                temp.put(THIRD_COLUMN, GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("CAP"))) + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Localita"))) );
                if(cursor.getString(cursor.getColumnIndex("NotaAccess")).contains("Y"))
                    temp.put(FOURTH_COLUMN, "ACCESSIBILE");

                temp.put(FIFTH_COLUMN, "Matricola: " + cursor.getString(cursor.getColumnIndex("MatricolaMis")));
                list.add(temp);
                IndiceElencoCodiceUtente.add(cursor.getString(cursor.getColumnIndex("CodiceUtente")));
                IndiceElencoIdCarico.add(cursor.getString(cursor.getColumnIndex("IdCarico")));
                IndiceElencoIdAzienza.add(cursor.getString(cursor.getColumnIndex("idAzienda")));
                IndiceElencoIndirizzi.add(GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("CAP")))+ " "+GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Localita")))+" "+GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Toponimo"))) + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Strada"))) + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Civico"))));
                cursor.moveToNext();
                countElenco++;
            }
            txtNumAttivita = (TextView) this.MyActivity.findViewById(R.id.txtNumAttivita);
            txtNumAttivita.setText(countElenco.toString());
            ListViewAdapters adapter=new ListViewAdapters(MyActivity, list);
            adapter.cosa = cosa;
            listView.setAdapter(adapter);
            adapter.notifyDataSetChanged();

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                    VerificaGPS();
                    if(Search4Concluse && statusOfGPS)
                    {
                        PositionOfClick = position;
                        AlertDialog.Builder dialogRiaperti = new AlertDialog.Builder(ActRicerca);
                        dialogRiaperti
                                .setTitle("!!!!ATTENZIONE!!!")
                                .setMessage("Attenzione, sei sicuro di voler riaprire la scheda? Se clicchi su si i dati precedenti verrano DEFINITIVAMENTE RIMOSSI ")
                                .setCancelable(false)
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                })
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        Intent intent = new Intent(MyActivity, Act_RilevaDati.class);
                                        String IDRicerca = IndiceElencoCodiceUtente.get(PositionOfClick);
                                        String IDCarico = IndiceElencoIdCarico.get(PositionOfClick);
                                        String IDAzienda = IndiceElencoIdAzienza.get(PositionOfClick);
                                        intent.putExtra("CodiceUtente", IDRicerca);
                                        intent.putExtra("IDCarico", IDCarico.trim());
                                        intent.putExtra("IDLett", IDLett.toString());
                                        intent.putExtra("IDAzienda", IDAzienda.trim());
                                        if (PositionOfClick > 0)
                                            ActRicerca.LastPosition = PositionOfClick;
                                        MyActivity.startActivity(intent);
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog MsgErrore = dialogRiaperti.create();

                        MsgErrore.show();
                    }
                    else if (!Search4Concluse && statusOfGPS) {
                        Intent intent = new Intent(MyActivity, Act_RilevaDati.class);
                        String IDRicerca = IndiceElencoCodiceUtente.get(position);
                        String IDCarico = IndiceElencoIdCarico.get(position);
                        String IDAzienda = IndiceElencoIdAzienza.get(position);
                        intent.putExtra("CodiceUtente", IDRicerca);
                        intent.putExtra("IDCarico", IDCarico.trim());
                        intent.putExtra("IDLett", IDLett.toString());
                        intent.putExtra("IDAzienda", IDAzienda.trim());
                        if (position > 0)
                            ActRicerca.LastPosition = position;
                        MyActivity.startActivity(intent);
                    } else {

                        Toast.makeText(MyActivity.getApplicationContext(), "Attenzione per poter lavorare devi attivare il GPS", Toast.LENGTH_LONG).show();
                    }

                }
            });


            listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    final int mposition = position;
                    AlertDialog.Builder dialogRiaperti = new AlertDialog.Builder(ActRicerca);
                    dialogRiaperti
                            .setTitle("TROVA POSIZIONE")
                            .setMessage("Puoi farti navigare direttamente sul posto cercando per indirizzo (scelta più veloce) o cercando l'ultima posizione rilevata in fase di lettura.")
                            .setCancelable(false)
                            .setNegativeButton("PER INDIRIZZO", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + IndiceElencoIndirizzi.get(mposition));
                                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                                    mapIntent.setPackage("com.google.android.apps.maps");
                                    if (mposition > 0)
                                        ActRicerca.LastPosition = mposition;
                                    MyActivity.startActivity(mapIntent);
                                }
                            })
                            .setPositiveButton("CERCA COORDINATE", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                   /* if(GetCoordinate(GlobalConfig,IndiceElencoCodiceUtente.get(mposition)))
                                    {
                                        dialog.cancel();
                                        Uri gmmIntentUri = Uri.parse("geo:0,0?q="+Latitudine+","+Longitudine);
                                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                                        mapIntent.setPackage("com.google.android.apps.maps");
                                        if (mposition > 0)
                                            ActRicerca.LastPosition = mposition;
                                        MyActivity.startActivity(mapIntent);
                                    }
                                    else
                                        Toast.makeText(MyActivity.getApplicationContext(), "Coordinate non trovate..", Toast.LENGTH_LONG).show();
                                        */

                                    ALoader = new AsyncLoader(MyActivity,"PrelevaCoordinate");
                                    ALoader._CodiceUtente = IndiceElencoCodiceUtente.get(mposition);
                                    ALoader.SSID = SSID;
                                    ALoader.execute();
                                    dialog.cancel();


                                }
                            });
                    AlertDialog MsgErrore = dialogRiaperti.create();

                    MsgErrore.show();

                    return true;
                }

            });

            ActRicerca.listView.setSelection(LastPosition);
            cursor.close();
            System.gc();
            //Close the Database and the Helper

        }

        else if(cosa=="QCode")
        {
            String data;
            String WhereStato = "";
            String WhereCosa = "";
            String FromTabella = "";
            String QueryRicerca = "";
            Integer countElenco = 0;


            ArrayList<HashMap<String, String>> list=new ArrayList<HashMap<String,String>>();
            ListView listView=(ListView)MyActivity.findViewById(R.id.listView1);

            IndiceElencoCodiceUtente = new ArrayList<String>();
            IndiceElencoCodiceUtente.clear();
            IndiceElencoIndirizzi = new ArrayList<String>();
            IndiceElencoIndirizzi.clear();
            IndiceElencoIdCarico = new ArrayList<String>();
            IndiceElencoIdCarico.clear();
            IndiceElencoIdAzienza = new ArrayList<String>();
            IndiceElencoIdAzienza.clear();
            Cursor cursor;
            Search4How = Search4How.replace("'","''");
            if(Search4Concluse)
            {
                WhereStato = "Stato = 'L'";
                FromTabella = "utentiOUT";
            }
            else
            {
                WhereStato = "Stato = 'A'";
                FromTabella = "utenti";
            }

            WhereCosa= "MatricolaMis LIKE '%"+Search4How+"%'";

            QueryRicerca = "SELECT * FROM " + FromTabella + " WHERE " + WhereCosa + " AND " + WhereStato + " ORDER BY Localita, Toponimo, Strada, CAST(Civico AS Integer)";
            cursor = dbHelper.query(db,QueryRicerca);

            cursor.moveToFirst();
            while (cursor.isAfterLast() == false) {
                HashMap<String,String> temp=new HashMap<String, String>();
                temp.put(FIRST_COLUMN, cursor.getString(cursor.getColumnIndex("Nominativo")) );
                temp.put(SECOND_COLUMN, GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Toponimo"))) + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Strada"))) + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Civico"))) + " - " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("NotaAccesso100"))));
                temp.put(THIRD_COLUMN, GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("CAP"))) + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Localita"))) );
                if(cursor.getString(cursor.getColumnIndex("NotaAccess")).contains("Y"))
                    temp.put(FOURTH_COLUMN, "ACCESSIBILE");

                temp.put(FIFTH_COLUMN, "Matricola: " + cursor.getString(cursor.getColumnIndex("MatricolaMis")));
                //temp.put(SIXTH_COLUMN, "Data Limite: " + cursor.getString(cursor.getColumnIndex("LetturaA")));
                list.add(temp);
                IndiceElencoCodiceUtente.add(cursor.getString(cursor.getColumnIndex("CodiceUtente")));
                IndiceElencoIdCarico.add(cursor.getString(cursor.getColumnIndex("IdCarico")));
                IndiceElencoIdAzienza.add(cursor.getString(cursor.getColumnIndex("idAzienda")));
                IndiceElencoIndirizzi.add(GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("CAP")))+ " "+GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Localita")))+" "+GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Toponimo"))) + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Strada"))) + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Civico"))));
                cursor.moveToNext();
                countElenco++;
            }
            ListViewAdapters adapter=new ListViewAdapters(MyActivity, list);
            adapter.cosa = cosa;
            listView.setAdapter(adapter);
            adapter.notifyDataSetChanged();

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                    VerificaGPS();
                    if(Search4Concluse && statusOfGPS)
                    {
                        PositionOfClick = position;
                        AlertDialog.Builder dialogRiaperti = new AlertDialog.Builder(ActQCode);
                        dialogRiaperti
                                .setTitle("!!!!ATTENZIONE!!!")
                                .setMessage("Attenzione, sei sicuro di voler riaprire la scheda? Se clicchi su si i dati precedenti verrano DEFINITIVAMENTE RIMOSSI ")
                                .setCancelable(false)
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                })
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        Intent intent = new Intent(MyActivity, Act_RilevaDati.class);
                                        String IDRicerca = IndiceElencoCodiceUtente.get(PositionOfClick);
                                        String IDCarico = IndiceElencoIdCarico.get(PositionOfClick);
                                        String IDAzienda = IndiceElencoIdAzienza.get(PositionOfClick);
                                        intent.putExtra("CodiceUtente", IDRicerca);
                                        intent.putExtra("IDCarico", IDCarico.trim());
                                        intent.putExtra("IDLett", IDLett.toString());
                                        intent.putExtra("IDAzienda", IDAzienda.trim());
                                        if (PositionOfClick > 0)
                                            ActQCode.LastPosition = PositionOfClick;
                                        MyActivity.startActivity(intent);
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog MsgErrore = dialogRiaperti.create();

                        MsgErrore.show();
                    }
                    else if (!Search4Concluse && statusOfGPS) {
                        Intent intent = new Intent(MyActivity, Act_RilevaDati.class);
                        String IDRicerca = IndiceElencoCodiceUtente.get(position);
                        String IDCarico = IndiceElencoIdCarico.get(position);
                        String IDAzienda = IndiceElencoIdAzienza.get(position);
                        intent.putExtra("CodiceUtente", IDRicerca);
                        intent.putExtra("IDCarico", IDCarico.trim());
                        intent.putExtra("IDLett", IDLett.toString());
                        intent.putExtra("IDAzienda", IDAzienda.trim());
                        if (position > 0)
                            ActQCode.LastPosition = position;
                        MyActivity.startActivity(intent);
                    } else {

                        Toast.makeText(MyActivity.getApplicationContext(), "Attenzione per poter lavorare devi attivare il GPS", Toast.LENGTH_LONG).show();
                    }

                }
            });


            listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + IndiceElencoIndirizzi.get(position));
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    if (position > 0)
                        ActQCode.LastPosition = position;
                    MyActivity.startActivity(mapIntent);
                    return true;
                }

            });

            ActQCode.listView.setSelection(LastPosition);
            cursor.close();
            System.gc();
            //Close the Database and the Helper

        }
        else if(cosa=="DettaglioProgrammazione")
        {
            String WhereCosa = "";
            String QueryRicerca = "";
            Integer countElenco = 0;
            String IndRicercaPass;
            String Nome = "";
            String Cognome = "";



            ArrayList<HashMap<String, String>> list=new ArrayList<HashMap<String,String>>();
            ListView listView=(ListView)MyActivity.findViewById(R.id.listView1);

            IndiceElencoCodiceUtente = new ArrayList<String>();
            IndiceElencoCodiceUtente.clear();
            IndiceElencoIdCarico = new ArrayList<String>();
            IndiceElencoIdCarico.clear();
            IndiceElencoIdAzienza = new ArrayList<String>();
            IndiceElencoIdAzienza.clear();;
            Cursor cursor;

            if(Search4dtProg.contains("nessuno"))
                WhereCosa= "(dtProgrammazione != '' and dtProgrammazione IS NULL)";
            else if(Search4dtProg.contains("Comune"))
                WhereCosa= "dtProgrammazione = '"+Search4How+"'";

            QueryRicerca = "SELECT * FROM utenti WHERE " + WhereCosa + " AND Stato = 'A' ORDER BY Localita, Toponimo, Strada, CAST(Civico AS Integer)";
            cursor = dbHelper.query(db,QueryRicerca);

            cursor.moveToFirst();
            while (cursor.isAfterLast() == false) {
                HashMap<String,String> temp=new HashMap<String, String>();
                if(cursor.isNull(cursor.getColumnIndex("Nominativo")))
                    Nome = "";
                else
                    Nome = cursor.getString(cursor.getColumnIndex("Nominativo"));
                if(cursor.isNull(cursor.getColumnIndex("Cognome")))
                    Cognome = "";
                else
                    Cognome = cursor.getString(cursor.getColumnIndex("Cognome"));
                temp.put(FIRST_COLUMN, Nome + " " + Cognome );
                temp.put(SECOND_COLUMN, cursor.getString(cursor.getColumnIndex("Toponimo")) + " " + cursor.getString(cursor.getColumnIndex("Strada")) + " " + cursor.getString(cursor.getColumnIndex("Civico"))+ " - " + cursor.getString(cursor.getColumnIndex("NotaAccesso100")));
                temp.put(THIRD_COLUMN, GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("CAP")))+ " " +GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Localita"))) + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Provincia"))));
                temp.put(FOURTH_COLUMN, "Telefono: " + cursor.getString(cursor.getColumnIndex("Telefono")));
                temp.put(FIFTH_COLUMN, cursor.getString(cursor.getColumnIndex("MatricolaMis")));
                temp.put(SIXTH_COLUMN, cursor.getString(cursor.getColumnIndex("dtProgrammazione")));
                temp.put(SEVENTH_COLUMN, cursor.getString(cursor.getColumnIndex("daOraLett")) + " - " + cursor.getString(cursor.getColumnIndex("aOraLett")));
                list.add(temp);
                IndiceElencoCodiceUtente.add(cursor.getString(cursor.getColumnIndex("CodiceUtente")));
                IndiceElencoIdCarico.add(cursor.getString(cursor.getColumnIndex("IdCarico")));
                IndiceElencoIdAzienza.add(cursor.getString(cursor.getColumnIndex("idAzienda")));
                cursor.moveToNext();
                countElenco++;
            }
            txtNumAttivita = (TextView) this.MyActivity.findViewById(R.id.txtNumAttivita);
            txtNumAttivita.setText(countElenco.toString());
            ListViewAdapters adapter=new ListViewAdapters(MyActivity, list);
            adapter.cosa = cosa;
            listView.setAdapter(adapter);
            adapter.notifyDataSetChanged();

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                    VerificaGPS();
                    if (!Search4Concluse && statusOfGPS) {
                        Intent intent = new Intent(MyActivity, Act_RilevaDati.class);
                        String IDRicerca = IndiceElencoCodiceUtente.get(position);
                        String IDCarico = IndiceElencoIdCarico.get(position);
                        String IDAzienda = IndiceElencoIdAzienza.get(position);
                        intent.putExtra("CodiceUtente", IDRicerca);
                        intent.putExtra("IDCarico", IDCarico.trim());
                        intent.putExtra("IDLett", IDLett.toString());
                        intent.putExtra("IDAzienda", IDAzienda.trim());
                        if (position > 0)
                            ActRicerca.LastPosition = position;
                        ActProgrammazione.startActivity(intent);
                    } else {

                        Toast.makeText(MyActivity.getApplicationContext(), "Attenzione per poter lavorare devi attivare il GPS", Toast.LENGTH_LONG).show();
                    }

                }
            });


            listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    final int mposition = position;
                    AlertDialog.Builder dialogRiaperti = new AlertDialog.Builder(ActRicerca);
                    dialogRiaperti
                            .setTitle("TROVA POSIZIONE")
                            .setMessage("Puoi farti navigare direttamente sul posto cercando per indirizzo (scelta più veloce) o cercando l'ultima posizione rilevata in fase di lettura.")
                            .setCancelable(false)
                            .setNegativeButton("PER INDIRIZZO", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + IndiceElencoIndirizzi.get(mposition));
                                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                                    mapIntent.setPackage("com.google.android.apps.maps");
                                    if (mposition > 0)
                                        ActRicerca.LastPosition = mposition;
                                    MyActivity.startActivity(mapIntent);
                                }
                            })
                            .setPositiveButton("CERCA COORDINATE", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                   /* if(GetCoordinate(GlobalConfig,IndiceElencoCodiceUtente.get(mposition)))
                                    {
                                        dialog.cancel();
                                        Uri gmmIntentUri = Uri.parse("geo:0,0?q="+Latitudine+","+Longitudine);
                                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                                        mapIntent.setPackage("com.google.android.apps.maps");
                                        if (mposition > 0)
                                            ActRicerca.LastPosition = mposition;
                                        MyActivity.startActivity(mapIntent);
                                    }
                                    else
                                        Toast.makeText(MyActivity.getApplicationContext(), "Coordinate non trovate..", Toast.LENGTH_LONG).show();
                                        */

                                    ALoader = new AsyncLoader(MyActivity,"PrelevaCoordinate");
                                    ALoader._CodiceUtente = IndiceElencoCodiceUtente.get(mposition);
                                    ALoader.SSID = SSID;
                                    ALoader.execute();
                                    dialog.cancel();


                                }
                            });
                    AlertDialog MsgErrore = dialogRiaperti.create();

                    MsgErrore.show();

                    return true;
                }

            });

            ActRicerca.listView.setSelection(LastPosition);
            cursor.close();
            System.gc();
            //Close the Database and the Helper

        }
        else if(cosa=="DettaglioAttivita")
        {

            String IndRicercaPass;


            ArrayList<HashMap<String, String>> list=new ArrayList<HashMap<String,String>>();
            ListView listView=(ListView)MyActivity.findViewById(R.id.listAttivita);

            IndiceElencoCodiceUtente = new ArrayList<String>();
            IndiceElencoCodiceUtente.clear();
            Cursor cursor;
            if(VersoOrdinato =="Totali")
                cursor = dbHelper.query(db, "SELECT CAP, Localita, Provincia, Toponimo, Strada, Civico, SUM(CASE WHEN NotaAccess = 'Y' THEN 1 ELSE 0 END) as Accessibile , Count(*) as Totali FROM utenti WHERE Stato = 'A'  GROUP BY CAP, Localita, Toponimo, Strada, Civico ORDER BY Totali DESC, Accessibile DESC");
            else
                cursor = dbHelper.query(db, "SELECT CAP, Localita, Provincia, Toponimo, Strada, Civico, SUM(CASE WHEN NotaAccess = 'Y' THEN 1 ELSE 0 END) as Accessibile , Count(*) as Totali FROM utenti WHERE Stato = 'A' GROUP BY CAP, Localita, Toponimo, Strada, Civico ORDER BY Accessibile DESC, Totali DESC");

            cursor.moveToFirst();
            while (cursor.isAfterLast() == false) {
                HashMap<String,String> temp=new HashMap<String, String>();
                temp.put(FIRST_COLUMN, GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("CAP")))+ " " +GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Localita"))) );
                temp.put(SECOND_COLUMN, GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Toponimo"))) + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Strada"))) + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Civico")))  );
                temp.put(THIRD_COLUMN, "Totali: " + cursor.getString(cursor.getColumnIndex("Totali")));
                temp.put(FOURTH_COLUMN, "Accessibili: " + cursor.getString(cursor.getColumnIndex("Accessibile")));

                list.add(temp);
                IndRicercaPass = GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("CAP")))+ " "+GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Localita")))+" "+GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Toponimo"))) + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Strada"))) + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Civico")));
                IndiceElencoCodiceUtente.add(IndRicercaPass);
                cursor.moveToNext();
            }
            ListViewAdapters adapter=new ListViewAdapters(MyActivity, list);
            adapter.cosa = "ElencoAttivita";
            listView.setAdapter(adapter);
            adapter.notifyDataSetChanged();


            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                    Intent intent = new Intent(MyActivity, ListaCarico.class);
                    String IDRicerca = IndiceElencoCodiceUtente.get(position);
                    intent.putExtra("Indirizzo", IDRicerca);
                    intent.putExtra("Matricola", "");
                    intent.putExtra("IDLett", IDLett.toString());
                    intent.putExtra("SelectCarico", "nessuno");
                    //DataOraCerta.setTimeInMillis((DataOraCerta.getTimeInMillis() + SystemClock.currentThreadTimeMillis()));
                    intent.putExtra("DataOraCerta", DataOraCerta.getTimeInMillis());
                    MyActivity.startActivityForResult(intent,99);

                }

            });
            cursor.close();
            System.gc();
            //Close the Database and the Helper
            Toast.makeText(MyActivity.getApplicationContext(), "Lettura Carico Completata", Toast.LENGTH_SHORT).show();

        }
        else if (cosa == "PrelevaCoordinate")
        {
            if(!EsitoGetCoordinate)
            {
                Toast.makeText(MyActivity.getApplicationContext(), "Coordinate non trovate..", Toast.LENGTH_LONG).show();
            }
            else
            {
                Uri gmmIntentUri = Uri.parse("geo:0,0?q="+Latitudine+","+Longitudine);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                MyActivity.startActivity(mapIntent);
            }
        }
    }



    protected void onProgressUpdate(String... progress) {

        if(cosa=="PrelevaInterventi") {
            dialog.setProgress(Integer.parseInt(progress[0]));
        }
    }

    public void VerificaGPS()
    {

        gps = new GPSHelper();
        gps.GPSContesto(MyActivity.getApplicationContext());

        statusOfGPS = gps.StatoGPS();

    }

    public Boolean GetCoordinate(String sGlobal, String CodiceUtente) {

        String servizio = "GetLastCoordinate";
        String response = servizio+"Result";
        String SOAP_ACTION = "http://tempuri.org/" + servizio;
        String METHOD_NAME = servizio;
        String NAMESPACE = "http://tempuri.org/";
        String URL;

        SoapPrimitive resultString;

        Calendar c = Calendar.getInstance();
        if(SSID.contains("cs1"))
            URL = "http://cs-storage/wsTrmLetture/wsTrmLetture.asmx?wsdl";
        else
            URL = "http://web.comerservizi.net:8083/wsTrmLetture/wsTrmLetture.asmx?wsdl";

        try {
            Log.i(TAG, "Inizio WS RecuperaDatiSIM");
            SoapObject Request = new SoapObject(NAMESPACE, METHOD_NAME);
            Request.addProperty("sGlobalConfiguration", sGlobal);
            Request.addProperty("CodiceUtente", CodiceUtente);

            SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            soapEnvelope.dotNet = true;
            soapEnvelope.setOutputSoapObject(Request);

            HttpTransportSE transport = new HttpTransportSE(URL,30000);
            transport.call(SOAP_ACTION, soapEnvelope);

            resultString = (SoapPrimitive) soapEnvelope.getResponse();

            String Controllo = resultString.toString();
            if(Controllo.contains("|"))
            {
                String[] parts = Controllo.split(Pattern.quote("|"));
                Latitudine = parts[0];
                Longitudine = parts[1];
                Log.i(TAG, "Finito GetCoordinate - POSITIVO");
                return true;
            }
            else
            {
                _Errore = Controllo;
                Latitudine ="";
                Longitudine = "";
                Log.i(TAG, "Finito GetCoordinate - NON TROVATE");
                return false;
            }

        } catch (SocketTimeoutException et) {

            _Errore = "Error WS: " + et.getMessage() + " - Cause: "+ et.getCause() + " - ex: " + et.toString();
            Log.i(TAG, "GetCoordinate TIMEOUT ["+_Errore+"]");
            return false;
        }
        catch( Exception ex )
        {
            _Errore = "Error WS: " + ex.getMessage() + " - Cause: "+ ex.getCause() + " - ex: " + ex.toString();
            Log.i(TAG, "GetCoordinate NEGATIVO ["+_Errore+"]");
            return false;
        }
    }
}
