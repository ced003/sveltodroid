package it.comerservizi.appsveltodroid;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class Act_NuovoContatore extends AppCompatActivity {

    EditText txtNewMatricola;
    String ResultMatricola;
    String GlobalConfig;
    String result;
    private GPClass GPC;
    private ImageView ButtonQR;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__nuovo_contatore);

        GPC = new GPClass();
        Bundle extras = getIntent().getExtras();
        if(extras !=null) {
            GlobalConfig = extras.getString("GlobalConfig");
        }
        txtNewMatricola = (EditText) findViewById(R.id.txtNewMatricola);
        ButtonQR = (ImageView) findViewById(R.id.qCodeButton);
        ButtonQR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("AAAAA", "Cliccato!");
                CercaMatricola();
            }
        });
    }

    public void CercaMatricola()
    {
        Log.e("Func", "Sono entrato!");
        if(GPC.isAppInstalled(getApplicationContext(),"com.google.zxing.client.android"))
        {
            Log.e("Func", "Sono installato!");
            Intent intent = new Intent("com.google.zxing.client.android.SCAN");
            //intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
            intent.putExtra("SCAN_FORMATS", "CODE_39,CODE_93,CODE_128,DATA_MATRIX,ITF,CODABAR,EAN_13,EAN_8,UPC_A,QR_CODE");
            Log.e("BBB", "Pronto a partire!");
            startActivityForResult(intent, 0);
        }
        else
            Toast.makeText(getApplicationContext(), "Devi installare la libreria del Barcode Scanner", Toast.LENGTH_LONG).show();
    }



    public void Conferma(View view)
    {
        ResultMatricola = txtNewMatricola.getText().toString();
        ResultMatricola = ResultMatricola.trim();
        if(ResultMatricola.length()<1)
        {
            Toast.makeText(getApplicationContext(), "Attenzione non puoi salvare perchè il numero matricola del nuovo contatore è vuoto", Toast.LENGTH_LONG).show();
        }
        else if((GlobalConfig.equals("1") || GlobalConfig.equals("2")) && (ResultMatricola.length()!=10 && ResultMatricola.length()!=16))
        {
               Toast.makeText(getApplicationContext(), "Attenzione la matricola può essere di 10 o 16 caratteri", Toast.LENGTH_LONG).show();
        }
        else
        {
            result = ResultMatricola;
            Intent returnIntent = new Intent();
            returnIntent.putExtra("result",result);
            setResult(this.RESULT_OK, returnIntent);
            finish();
        }

    }

    public void Annulla(View view)
    {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", result);
        setResult(this.RESULT_CANCELED, returnIntent);
        finish();
    }

    @Override
    public void onBackPressed() {

        Intent returnIntent = new Intent();
        setResult(this.RESULT_CANCELED, returnIntent);
        finish();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        String MatricolaSparata = "";
        if (requestCode == 0) {
            if (resultCode == RESULT_OK)
            {
                MatricolaSparata = intent.getStringExtra("SCAN_RESULT");
                txtNewMatricola.setText("");
                txtNewMatricola.setText(MatricolaSparata);
            } else if (resultCode == RESULT_CANCELED)
            {
                MatricolaSparata = "";
            }
        }
    }

}



