package it.comerservizi.appsveltodroid;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.net.SocketTimeoutException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by guido.palumbo on 03/12/2015.
 */
public class AsyncDownload extends AsyncTask<Void, String, Void> {

    private String TAG = "AsyncDownload";
    ProgressDialog dialog;
    public Act_ElencoAttivita MyElencoAttivita = null;
    private ArrayList ListTBUtenti;
    private ArrayList ListTBUtentiOUT;
    private ArrayList ListTBtabtipo;
    private ArrayList ListTBControlloNote;
    private ArrayList ListTBNoteLettDitta;
    private DatabaseHelper dbHelper;
    private SQLiteDatabase db;
    private long startTime;
    public long TempoTrascorso;
    private long endTime;
    private String servizio = "";
    private String response = "";
    private GPClass GPC ;
    public String service;
    public String PeriodoDal, PeriodoAl;
    private String MessErrore;
    public String operatore;
    public String SSID;
    private Integer LastProgresso;

    private ArrayList<String> IndiceElencoCodiceUtente;
    private ArrayList<String> IndiceElencoIdCarico;
    private ArrayList<String> IndiceElencoIdAzienza;

    private boolean ControlloUtenti = false, ControlloTabTipo = false, ControlloCNote = false, ControlloNoteLettDitta = false;
    private boolean NessunLavoro = false;
    private boolean NessunDownload = false;
    private boolean Timeout = false;
    String GlobalConfig;

    @Override
    protected void onPreExecute()
    {
        startTime = System.currentTimeMillis();
        Log.i(TAG, "entrato in OnPreExecute");
        dbHelper = new DatabaseHelper(MyElencoAttivita);
        GPC = new GPClass();
        db= dbHelper.getWritableDatabase();
        dialog = new ProgressDialog(MyElencoAttivita);
        dialog.setCancelable(false);

        Cursor c = db.rawQuery("SELECT IdConfig FROM AndroConfig WHERE Type = 'sGlobalConfiguration'", null);

        if (c != null)
            c.moveToFirst();

        GlobalConfig = c.getString(c.getColumnIndex("IdConfig"));

        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialog.setProgress(0);
        dialog.setMax(4);
        dialog.setMessage("Connessione in corso...");
        dialog.show();
        Crashlytics.log(null);
        Crashlytics.log("GlobalConfig: " + GlobalConfig + " - Operatore: " + operatore );




    }

    @Override
    protected Void doInBackground(Void... params) {
        Log.i(TAG, "doInBackground - Step1");
        ControlloUtenti = gettallRecordUtenti();
        publishProgress("uno");
        Log.i(TAG, "doInBackground - Step2");
        ControlloTabTipo = gettallRecordtabtipo();
        publishProgress("due");
        Log.i(TAG, "doInBackground - Step3");
        ControlloCNote = gettallRecordControlloNote();
        publishProgress("tre");
        Log.i(TAG, "doInBackground - Step4");
        ControlloNoteLettDitta = gettallRecordNoteLettDitta();
        publishProgress("quattro");
        Log.i(TAG, "doInBackground - Vai ai Controlli");
        if(ControlloUtenti && ControlloTabTipo && ControlloCNote && ControlloNoteLettDitta)
        {
            Log.i(TAG, "doInBackground - Finiti Controlli");

            Log.i(TAG, "doInBackground - Change Showdialog");
            dbHelper.Deleteutenti(db);
            dbHelper.DeleteutentiOUT(db);
            dbHelper.Deletetabtipo(db);
            dbHelper.DeleteControlloNote(db);
            dbHelper.DeleteNoteLettDitta(db);
            if (!NessunLavoro) {
                publishProgress("change");
                LastProgresso = 0;
                dbHelper.Deleteutenti(db);
                Log.i(TAG, "doInBackground - ListTBUtenti");
                for (Integer i = 0; i < this.ListTBUtenti.size(); i++) {
                    publishProgress(Integer.toString(LastProgresso));
                    LastProgresso++;
                    db.insert("utenti", null, (ContentValues) ListTBUtenti.get(i));
                    //dbHelper.SimpleQuery(db, ListTBUtenti.get(i).toString());

                }
                ListTBUtenti.clear();

                Log.i(TAG, "doInBackground - ListTBUtentiOUT");
                for (Integer i = 0; i < this.ListTBUtentiOUT.size(); i++) {
                    publishProgress(Integer.toString(LastProgresso));
                    LastProgresso++;

                    db.insert("utentiOUT", null, (ContentValues) ListTBUtentiOUT.get(i));
                    //dbHelper.SimpleQuery(db, ListTBUtenti.get(i).toString());

                }
                ListTBUtentiOUT.clear();


                dbHelper.Deletetabtipo(db);
                Log.i(TAG, "doInBackground - ListTBtabtipo");
                for (Integer i = 0; i < this.ListTBtabtipo.size(); i++) {
                    publishProgress(Integer.toString(LastProgresso));
                    LastProgresso++;
                    db.insert("tabtipo", null, (ContentValues) ListTBtabtipo.get(i));
                    //dbHelper.SimpleQuery(db, ListTBtabtipo.get(i).toString());

                }
                ListTBtabtipo.clear();

                dbHelper.DeleteControlloNote(db);
                Log.i(TAG, "doInBackground - ListTBControlloNote");
                for (Integer i = 0; i < this.ListTBControlloNote.size(); i++) {
                    publishProgress(Integer.toString(LastProgresso));
                    LastProgresso++;
                    db.insert("ControlloNote", null, (ContentValues) ListTBControlloNote.get(i));
                    //dbHelper.SimpleQuery(db, ListTBControlloNote.get(i).toString());

                }
                ListTBControlloNote.clear();

                dbHelper.DeleteNoteLettDitta(db);
                Log.i(TAG, "doInBackground - ListTBNoteLettDitta");
                for (Integer i = 0; i < this.ListTBNoteLettDitta.size(); i++) {
                    publishProgress(Integer.toString(LastProgresso));
                    LastProgresso++;
                    db.insert("NoteLettDitta", null, (ContentValues) ListTBNoteLettDitta.get(i));
                    //dbHelper.SimpleQuery(db, ListTBNoteLettDitta.get(i).toString());

                }
                ListTBNoteLettDitta.clear();
            }
            else
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
        }
        else
        {
            NessunDownload = true;
        }

        return null;

    }

    @Override
    protected void onPostExecute(Void result)
    {
        Log.i(TAG, "onPostExecute");
        System.gc();
        String IndRicercaPass;
        if(NessunLavoro) {
            Toast.makeText(MyElencoAttivita.getApplicationContext(), "Sincronizzazione Completata - ma non vi sono attività da svolgere", Toast.LENGTH_LONG).show();
            MyElencoAttivita.finish();
            MyElencoAttivita.startActivity(MyElencoAttivita.getIntent());
        }
        else if(Timeout)
        {
            AlertDialog.Builder dialog = new AlertDialog.Builder(MyElencoAttivita);dialog
                .setTitle("Errore di Rete")
                .setMessage("Invio Completato ma non è stato possibile scaricare a causa della qualità del segnale, cambia zona o cerca un WIFI! " + " [ Tempo Trascorso : " + Double.toString(TempoTrascorso) + " minuti ]")
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();

                                MyElencoAttivita.finish();
                                MyElencoAttivita.startActivity(MyElencoAttivita.getIntent());
                            }
                        });
            AlertDialog MsgErrore = dialog.create();
            MsgErrore.show();
        }
        else if(NessunDownload)
        {
            AlertDialog.Builder dialog = new AlertDialog.Builder(MyElencoAttivita);dialog
                .setTitle("Errore di Sincronizzazione")
                .setMessage("Invio Completato ma non è stato possibile scaricare i nuovi dati comunicare il seguente errore: " + MessErrore + " [ Tempo Trascorso : " + Double.toString(TempoTrascorso) + " minuti ]")
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                        MyElencoAttivita.finish();
                        MyElencoAttivita.startActivity(MyElencoAttivita.getIntent());
                    }
                });
            AlertDialog MsgErrore = dialog.create();
            MsgErrore.show();
        }
        else {
            Toast.makeText(MyElencoAttivita.getApplicationContext(), "Sincronizzazione Completata", Toast.LENGTH_SHORT).show();
            MyElencoAttivita.finish();
            MyElencoAttivita.startActivity(MyElencoAttivita.getIntent());
        }


        if (dialog.isShowing()) {
            Log.i(TAG, "onPostExecute - Dismissis DIALOG");
            dialog.dismiss();
        }

        Log.i(TAG, "onPostExecute - Finita");

    }

    protected void onProgressUpdate(String... progress) {

        if(progress[0]=="change")
        {
            Log.i(TAG, "onProgressUpdate Change");
            dialog.setMessage("Sincronizzazione in corso..");
            dialog.setProgress(0);
            dialog.setMax(ListTBUtenti.size() + ListTBUtentiOUT.size() + ListTBtabtipo.size() + ListTBControlloNote.size()+ ListTBNoteLettDitta.size());
            dialog.show();
        }
        else if(progress[0]=="uno")
            dialog.setProgress(1);
        else if(progress[0]=="due")
            dialog.setProgress(2);
        else if(progress[0]=="tre")
            dialog.setProgress(3);
        else if(progress[0]=="quattro")
            dialog.setProgress(4);
        else
            dialog.setProgress(Integer.parseInt(progress[0]));

    }

    public void SetActElenco(Act_ElencoAttivita a)
    {
        this.MyElencoAttivita = a;
    }



    //PRIVATE

    private String PulisciStringa(String val, Boolean NullToEmpty)
    {
        String res;
        if(val==null)
        {
            if(NullToEmpty)
                return "";
            else
                return val;
        }
        else
        {
            res = val.replace("'","''");
            return res;
        }
    }
    private String ConvertiData(String StringData)
    {

        Date ToData;
        String DataResult;

        if(StringData==null)
            DataResult = StringData;
        else {
            try {
                ToData = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(StringData);
                DataResult = new SimpleDateFormat("dd/MM/yyyy").format(ToData);
            } catch (ParseException e) {
                DataResult = StringData;
            }
        }
        return DataResult;
    }

    private boolean gettallRecordtabtipo() {

        service = "TabelleVarie";
        this.servizio = service;
        String SOAP_ACTION = "http://tempuri.org/" + servizio;
        String METHOD_NAME = servizio;
        String NAMESPACE = "http://tempuri.org/";
        String URL;
        if(SSID.contains("cs1"))
            URL = "http://cs-storage/wsTrmLetture/wsTrmLetture.asmx?wsdl";
        else
            URL = "http://web.comerservizi.net:8083/wsTrmLetture/wsTrmLetture.asmx?wsdl";
        try {
            String appo;
            SoapObject client = null;                        // Its the client petition to the web service
            SoapObject responseBody = null;                    // Contains XML content of dataset
            HttpTransportSE transport = new HttpTransportSE(URL,180000);
            SoapSerializationEnvelope sse = null;

            sse = new SoapSerializationEnvelope(SoapEnvelope.VER11);

            sse.dotNet = true; // if WebService written .Net is result=true
            sse.implicitTypes = false;
            client = new SoapObject(NAMESPACE, METHOD_NAME);
            client.addProperty("sGlobalConfiguration", GlobalConfig);
            sse.setOutputSoapObject(client);
            sse.bodyOut = client;
            transport.call(SOAP_ACTION, sse);

            // This step: get file XML
            responseBody = (SoapObject) sse.getResponse();
            // remove information XML,only retrieved results that returned
            responseBody = (SoapObject) responseBody.getProperty(1);
            // get information XMl of tables that is returned
            SoapObject table = (SoapObject) responseBody.getProperty("Spool");
            //Get information each row in table,0 is first row
            PropertyInfo pi = new PropertyInfo();
            String query = "";
            SoapObject Spool;

            ListTBtabtipo= new ArrayList<>(table.getPropertyCount());
            for (Integer i = 0; i < table.getPropertyCount(); i++) {
                Spool = null;
                Spool = (SoapObject) table.getProperty(i);
                ContentValues values = new ContentValues();
                values.put("IdDitta", PulisciStringa(Spool.getPrimitivePropertySafely("IdDitta").toString(), false) );
                values.put("Tipo", PulisciStringa(Spool.getPrimitivePropertySafely("Tipo").toString(), false) );
                values.put("CodiceComer", PulisciStringa(Spool.getPrimitivePropertySafely("Codice").toString(), false) );
                values.put("Descrizione", PulisciStringa(Spool.getPrimitivePropertySafely("Descrizione").toString(), false) );
                values.put("CodiceDitta", PulisciStringa(Spool.getPrimitivePropertySafely("CodiceDitta").toString(), false) );
                values.put("Accessibilita", PulisciStringa(Spool.getPrimitivePropertySafely("Accessibilita").toString(), false) );
                values.put("BiffaturaDitta", PulisciStringa(Spool.getPrimitivePropertySafely("BiffaturaDitta").toString(), false) );


                ListTBtabtipo.add(values);
            }

            Log.i(TAG, "Finito gettallRecordtabtipo - POSITIVO");
            return true;

        } catch (SocketTimeoutException et) {

            endTime = System.currentTimeMillis();
            TempoTrascorso = ((endTime - startTime)/60000);
            Log.i(TAG, "Finito gettallRecordtabtipo - TIMEOUT");
            MessErrore = "ErroreWS TIMEOUT: " + et.getMessage() + " - " + et.getCause() + " - " + et.toString();
            Timeout = true;
            return false;
        } catch (Exception e) {
            endTime = System.currentTimeMillis();
            TempoTrascorso = ((endTime - startTime)/60000);
            if(e.toString().contains("illegal property: Spool"))
            {
                NessunLavoro = true;
                Log.i(TAG, "Finito gettallRecordtabtipo - NESSUN VALORE");
                return true;
            }
            else {
                Log.i(TAG, "Finito gettallRecordtabtipo - NEGATIVO");
                MessErrore = "ErroreWS: " + e.getMessage() + " - " + e.getCause() + " - " + e.toString();
                return false;
            }
        }


    }

    private boolean gettallRecordControlloNote() {

        service = "TabellaControlloNote";
        this.servizio = service;
        String SOAP_ACTION = "http://tempuri.org/" + servizio;
        String METHOD_NAME = servizio;
        String NAMESPACE = "http://tempuri.org/";
        String URL;
        if(SSID.contains("cs1"))
            URL = "http://cs-storage/wsTrmLetture/wsTrmLetture.asmx?wsdl";
        else
            URL = "http://web.comerservizi.net:8083/wsTrmLetture/wsTrmLetture.asmx?wsdl";

        try {
            String appo;
            SoapObject client = null;                        // Its the client petition to the web service
            SoapObject responseBody = null;                    // Contains XML content of dataset
            HttpTransportSE transport = new HttpTransportSE(URL,180000);
            SoapSerializationEnvelope sse = null;

            sse = new SoapSerializationEnvelope(SoapEnvelope.VER11);

            sse.dotNet = true; // if WebService written .Net is result=true
            sse.implicitTypes = false;
            client = new SoapObject(NAMESPACE, METHOD_NAME);
            client.addProperty("sGlobalConfiguration", GlobalConfig);
            sse.setOutputSoapObject(client);
            sse.bodyOut = client;
            transport.call(SOAP_ACTION, sse);

            // This step: get file XML
            responseBody = (SoapObject) sse.getResponse();
            // remove information XML,only retrieved results that returned
            responseBody = (SoapObject) responseBody.getProperty(1);
            // get information XMl of tables that is returned
            SoapObject table = (SoapObject) responseBody.getProperty("Spool");
            //Get information each row in table,0 is first row
            PropertyInfo pi = new PropertyInfo();
            String query = "";
            SoapObject Spool;

            ListTBControlloNote= new ArrayList<>(table.getPropertyCount());
            for (Integer i = 0; i < table.getPropertyCount(); i++) {
                Spool = null;
                Spool = (SoapObject) table.getProperty(i);
                ContentValues values = new ContentValues();
                values.put("IdDitta", PulisciStringa(Spool.getPrimitivePropertySafely("IdDitta").toString(), false) );
                values.put("Nota", PulisciStringa(Spool.getPrimitivePropertySafely("Nota").toString(), false) );
                values.put("Accessibilita", PulisciStringa(Spool.getPrimitivePropertySafely("Accessibilita").toString(), false) );
                values.put("PresenzaLettura", PulisciStringa(Spool.getPrimitivePropertySafely("PresenzaLettura").toString(), false) );
                values.put("NoteAccessibile", PulisciStringa(Spool.getPrimitivePropertySafely("NoteAccessibile").toString(), false) );
                values.put("NoteParzAccessibile", PulisciStringa(Spool.getPrimitivePropertySafely("NoteParzAccessibile").toString(), false) );
                values.put("NoteNonAccessibile", PulisciStringa(Spool.getPrimitivePropertySafely("NoteNonAccessibile").toString(), false) );
                values.put("PresenzaFoto", PulisciStringa(Spool.getPrimitivePropertySafely("PresenzaFoto").toString(), false) );


                ListTBControlloNote.add(values);
            }

            Log.i(TAG, "Finito gettallRecordControlloNote - POSITIVO");
            return true;

        }catch (SocketTimeoutException et) {

            endTime = System.currentTimeMillis();
            TempoTrascorso = ((endTime - startTime)/60000);
            Log.i(TAG, "Finito gettallRecordControlloNote - TIMEOUT");
            MessErrore = "ErroreWS TIMEOUT: " + et.getMessage() + " - " + et.getCause() + " - " + et.toString();
            Timeout = true;
            return false;
        } catch (Exception e) {
            endTime = System.currentTimeMillis();
            TempoTrascorso = ((endTime - startTime)/60000);
            if(e.toString().contains("illegal property: Spool"))
            {
                NessunLavoro = true;
                Log.i(TAG, "Finito gettallRecordControlloNote - NESSUN VALORE");
                return true;
            }
            else {
                Log.i(TAG, "Finito gettallRecordControlloNote - NEGATIVO");
                MessErrore = "ErroreWS: " + e.getMessage() + " - " + e.getCause() + " - " + e.toString();
                return false;
            }
        }


    }

    private boolean gettallRecordNoteLettDitta() {

        service = "TabellaCodiciNote";
        this.servizio = service;
        String SOAP_ACTION = "http://tempuri.org/" + servizio;
        String METHOD_NAME = servizio;
        String NAMESPACE = "http://tempuri.org/";
        String URL;
        if(SSID.contains("cs1"))
            URL = "http://cs-storage/wsTrmLetture/wsTrmLetture.asmx?wsdl";
        else
            URL = "http://web.comerservizi.net:8083/wsTrmLetture/wsTrmLetture.asmx?wsdl";
        try {
            String appo;
            SoapObject client = null;                        // Its the client petition to the web service
            SoapObject responseBody = null;                    // Contains XML content of dataset
            HttpTransportSE transport = new HttpTransportSE(URL,180000);
            SoapSerializationEnvelope sse = null;

            sse = new SoapSerializationEnvelope(SoapEnvelope.VER11);

            sse.dotNet = true;
            sse.implicitTypes = false;

            client = new SoapObject(NAMESPACE, METHOD_NAME);
            client.addProperty("sGlobalConfiguration", GlobalConfig);
            sse.setOutputSoapObject(client);
            sse.bodyOut = client;
            transport.call(SOAP_ACTION, sse);

            // This step: get file XML
            responseBody = (SoapObject) sse.getResponse();
            // remove information XML,only retrieved results that returned
            responseBody = (SoapObject) responseBody.getProperty(1);
            // get information XMl of tables that is returned
            SoapObject table = (SoapObject) responseBody.getProperty("Spool");
            //Get information each row in table,0 is first row
            PropertyInfo pi = new PropertyInfo();
            String query = "";
            SoapObject Spool;
            query = "INSERT INTO NoteLettDitta VALUES (";

            ListTBNoteLettDitta= new ArrayList<>(table.getPropertyCount());
            for (Integer i = 0; i < table.getPropertyCount(); i++) {
                Spool = null;
                Spool = (SoapObject) table.getProperty(i);

                ContentValues values = new ContentValues();
                values.put("IdDitta", PulisciStringa(Spool.getPrimitivePropertySafely("IdDitta").toString(), false) );
                values.put("Codice", PulisciStringa(Spool.getPrimitivePropertySafely("Codice").toString(), false) );
                values.put("CodiceDitta", PulisciStringa(Spool.getPrimitivePropertySafely("CodiceDitta").toString(), false) );
                values.put("DescrSupp", PulisciStringa(Spool.getPrimitivePropertySafely("DescrSupp").toString(), false) );
                values.put("FatturataDitta", PulisciStringa(Spool.getPrimitivePropertySafely("FatturataDitta").toString(), false) );
                values.put("FatturataLett", PulisciStringa(Spool.getPrimitivePropertySafely("FatturataLett").toString(), false) );
                values.put("ObbLettura", PulisciStringa(Spool.getPrimitivePropertySafely("ObbLettura").toString(), false) );
                values.put("ObbData", PulisciStringa(Spool.getPrimitivePropertySafely("ObbData").toString(), false) );
                values.put("ConfrontaPrecLet", PulisciStringa(Spool.getPrimitivePropertySafely("ConfrontaPrecLet").toString(), false) );
                values.put("ULettP", PulisciStringa(Spool.getPrimitivePropertySafely("ULettP").toString(), false) );
                values.put("genSegnalazione", PulisciStringa(Spool.getPrimitivePropertySafely("genSegnalazione").toString(), false) );
                values.put("CodSegn", PulisciStringa(Spool.getPrimitivePropertySafely("CodSegn").toString(), false) );
                values.put("Descrizione", PulisciStringa(Spool.getPrimitivePropertySafely("Descrizione").toString(), false) );
                values.put("SopprimiLett", PulisciStringa(Spool.getPrimitivePropertySafely("SopprimiLett").toString(), false) );
                values.put("mSegnalazione", PulisciStringa(Spool.getPrimitivePropertySafely("mSegnalazione").toString(), false) );
                values.put("UtAssente", PulisciStringa(Spool.getPrimitivePropertySafely("UtAssente").toString(), false) );
                values.put("SoppRec", PulisciStringa(Spool.getPrimitivePropertySafely("SoppRec").toString(), false) );
                values.put("ScartaFoto", PulisciStringa(Spool.getPrimitivePropertySafely("ScartaFoto").toString(), false) );
                values.put("ValFlagFoto", PulisciStringa(Spool.getPrimitivePropertySafely("ValFlagFoto").toString(), false) );
                values.put("ImpostaBiff2", PulisciStringa(Spool.getPrimitivePropertySafely("ImpostaBiff2").toString(), false) );
                values.put("FileFoto", PulisciStringa(Spool.getPrimitivePropertySafely("FileFoto").toString(), false) );
                values.put("NumNota", PulisciStringa(Spool.getPrimitivePropertySafely("NumNota").toString(), false) );
                values.put("SopprimiDtLett", PulisciStringa(Spool.getPrimitivePropertySafely("SopprimiDtLett").toString(), false) );
                values.put("ObbRMatricola", PulisciStringa(Spool.getPrimitivePropertySafely("ObbRMatricola").toString(), false) );
                values.put("Accorpatore", PulisciStringa(Spool.getPrimitivePropertySafely("Accorpatore").toString(), false) );
                values.put("Activity", PulisciStringa(Spool.getPrimitivePropertySafely("Activity").toString(), false) );
                values.put("Autolettura", PulisciStringa(Spool.getPrimitivePropertySafely("Autolettura").toString(), false) );
                values.put("RitornaLettura", PulisciStringa(Spool.getPrimitivePropertySafely("RitornaLettura").toString(), false) );
                values.put("CampoNota", PulisciStringa(Spool.getPrimitivePropertySafely("CampoNota").toString(), false) );
                values.put("ObbFoto", PulisciStringa(Spool.getPrimitivePropertySafely("ObbFoto").toString(), false) );


                ListTBNoteLettDitta.add(values);
            }

            Log.i(TAG, "Finito gettallRecordNoteLettDitta - POSITIVO");
            return true;

        }catch (SocketTimeoutException et) {

            endTime = System.currentTimeMillis();
            TempoTrascorso = ((endTime - startTime)/60000);
            Timeout = true;
            Log.i(TAG, "Finito gettallRecordNoteLettDitta - TIMEOUT");
            MessErrore = "ErroreWS TIMEOUT: " + et.getMessage() + " - " + et.getCause() + " - " + et.toString();
            return false;
        }  catch (Exception e) {
            endTime = System.currentTimeMillis();
            TempoTrascorso = ((endTime - startTime)/60000);

            if(e.toString().contains("illegal property: Spool"))
            {
                NessunLavoro = true;
                Log.i(TAG, "Finito gettallRecordNoteLettDitta - NESSUN VALORE");
                return true;
            }
            else {
                Log.i(TAG, "Finito gettallRecordNoteLettDitta - NEGATIVO");
                MessErrore = "ErroreWS: " + e.getMessage() + " - " + e.getCause() + " - " + e.toString();
                return false;
            }
        }


    }

    private boolean gettallRecordUtenti() {

        this.servizio = service;
        response = servizio + "Result";
        String SOAP_ACTION = "http://tempuri.org/" + servizio;
        String METHOD_NAME = servizio;
        String NAMESPACE = "http://tempuri.org/";
        String URL;
        if(SSID.contains("cs1"))
            URL = "http://cs-storage/wsTrmLetture/wsTrmLetture.asmx?wsdl";
        else
            URL = "http://web.comerservizi.net:8083/wsTrmLetture/wsTrmLetture.asmx?wsdl";

        try {
            SoapObject client = null;                        // Its the client petition to the web service
            SoapObject responseBody = null;                    // Contains XML content of dataset
            HttpTransportSE transport = new HttpTransportSE(URL,180000);
            SoapSerializationEnvelope sse = null;

            sse = new SoapSerializationEnvelope(SoapEnvelope.VER11);

            sse.dotNet = true; // if WebService written .Net is result=true
            sse.implicitTypes = false;
            client = new SoapObject(NAMESPACE, METHOD_NAME);
            client.addProperty("sGlobalConfiguration", GlobalConfig);
            client.addProperty("nPalm",operatore);
            client.addProperty("DataGG", "");
            sse.setOutputSoapObject(client);
            sse.bodyOut = client;
            transport.call(SOAP_ACTION, sse);

            // This step: get file XML
            responseBody = (SoapObject) sse.getResponse();
            // remove information XML,only retrieved results that returned
            responseBody = (SoapObject) responseBody.getProperty(1);
            // get information XMl of tables that is returned
            SoapObject table = (SoapObject) responseBody.getProperty("Spool");
            //Get information each row in table,0 is first row
            PropertyInfo pi = new PropertyInfo();
            String query = "";
            String queryOUT = "";
            SoapObject Spool;
            query = "INSERT INTO utenti VALUES (";
            queryOUT = "INSERT INTO utentiOUT VALUES (";

            if(table.getPropertyCount()<1)
            {

                ListTBUtenti= new ArrayList<>(1);
                ListTBUtenti.add("Nessun Lavoro");
                ListTBUtentiOUT= new ArrayList<>(1);
                ListTBUtentiOUT.add("Nessun Lavoro");
            }
            else
            {
                ListTBUtenti= new ArrayList<>(table.getPropertyCount());
                ListTBUtentiOUT= new ArrayList<>(table.getPropertyCount());
                for (Integer i = 0; i < table.getPropertyCount(); i++) {
                    Spool = null;
                    Spool = (SoapObject) table.getProperty(i);
                    if(Spool.getPrimitivePropertySafely("Stato").toString().trim().equals("L"))
                    {
                        ContentValues values = new ContentValues();
                        values.put("idAzienda", PulisciStringa(Spool.getPrimitivePropertySafely("idAzienda").toString(), false) );
                        values.put("IdCarico", PulisciStringa(Spool.getPrimitivePropertySafely("IdCarico").toString(), false) );
                        values.put("CodiceUtente", PulisciStringa(Spool.getPrimitivePropertySafely("CodiceUtente").toString(), false) );
                        values.put("Nominativo", PulisciStringa(Spool.getPrimitivePropertySafely("Nominativo").toString(), false) );
                        values.put("Cognome", PulisciStringa(Spool.getPrimitivePropertySafely("Cognome").toString(), false) );
                        values.put("Toponimo", PulisciStringa(Spool.getPrimitivePropertySafely("Toponimo").toString(), true) );
                        values.put("Strada", PulisciStringa(Spool.getPrimitivePropertySafely("Strada").toString(), true) );
                        values.put("Civico", PulisciStringa(Spool.getPrimitivePropertySafely("Civico").toString(), true) );
                        values.put("Isolato", PulisciStringa(Spool.getPrimitivePropertySafely("Isolato").toString(), true) );
                        values.put("Scala", PulisciStringa(Spool.getPrimitivePropertySafely("Scala").toString(), true) );
                        values.put("Piano", PulisciStringa(Spool.getPrimitivePropertySafely("Piano").toString(), true) );
                        values.put("Interno", PulisciStringa(Spool.getPrimitivePropertySafely("Interno").toString(), true) );
                        values.put("Localita", PulisciStringa(Spool.getPrimitivePropertySafely("Localita").toString(), true) );
                        values.put("CAP", PulisciStringa(Spool.getPrimitivePropertySafely("CAP").toString(), true) );
                        values.put("CodicePercorso", PulisciStringa(Spool.getPrimitivePropertySafely("CodicePercorso").toString(), false) );
                        values.put("NoteAccesso", PulisciStringa(Spool.getPrimitivePropertySafely("NoteAccesso").toString(), false) );
                        values.put("MatricolaMis", PulisciStringa(Spool.getPrimitivePropertySafely("MatricolaMis").toString(), false) );
                        values.put("CifreContatore", PulisciStringa(Spool.getPrimitivePropertySafely("CifreContatore").toString(), false) );
                        values.put("CategoriaUtente", PulisciStringa(Spool.getPrimitivePropertySafely("CategoriaUtente").toString(), false) );
                        values.put("DataUltLettura", ConvertiData(PulisciStringa(Spool.getPrimitivePropertySafely("DataUltLettura").toString(), false)) );
                        values.put("UltimaLettura", PulisciStringa(Spool.getPrimitivePropertySafely("UltimaLettura").toString(), true) );
                        values.put("LetturaPrevista", PulisciStringa(Spool.getPrimitivePropertySafely("LetturaPrevista").toString(), true) );
                        values.put("telefono", PulisciStringa(Spool.getPrimitivePropertySafely("telefono").toString(), true) );
                        values.put("IdUtente", PulisciStringa(Spool.getPrimitivePropertySafely("IdUtente").toString(), false) );
                        values.put("idLetturista", PulisciStringa(Spool.getPrimitivePropertySafely("idLetturista").toString(), false) );
                        values.put("NomeLetturista", PulisciStringa(Spool.getPrimitivePropertySafely("NomeLetturista").toString(), false) );
                        values.put("Terminale", PulisciStringa(Spool.getPrimitivePropertySafely("Terminale").toString(), false) );
                        values.put("DtAffidamento", ConvertiData(PulisciStringa(Spool.getPrimitivePropertySafely("DtAffidamento").toString(), false)) );
                        values.put("DtRientro", ConvertiData(PulisciStringa(Spool.getPrimitivePropertySafely("DtRientro").toString(), false)) );
                        values.put("Stato", PulisciStringa(Spool.getPrimitivePropertySafely("Stato").toString(), false) );
                        values.put("NotaAccess", PulisciStringa(Spool.getPrimitivePropertySafely("NotaAccess").toString(), true) );
                        values.put("PuntoFornitura", PulisciStringa(Spool.getPrimitivePropertySafely("PuntoFornitura").toString(), false) );
                        values.put("NotaAccesso100", PulisciStringa(Spool.getPrimitivePropertySafely("NotaAccesso100").toString(), false) );
                        values.put("LetturaDa", ConvertiData(PulisciStringa(Spool.getPrimitivePropertySafely("LetturaDa").toString(), false)) );
                        values.put("LetturaA", ConvertiData(PulisciStringa(Spool.getPrimitivePropertySafely("LetturaA").toString(), false)) );
                        values.put("PresCorrettore", PulisciStringa(Spool.getPrimitivePropertySafely("PresCorrettore").toString(), false) );
                        values.put("ClasseContatore", PulisciStringa(Spool.getPrimitivePropertySafely("ClasseContatore").toString(), false) );
                        values.put("dtLimiteInt1", ConvertiData(PulisciStringa(Spool.getPrimitivePropertySafely("dtLimiteInt1").toString(), false)) );
                        values.put("dtLimiteInt2", ConvertiData(PulisciStringa(Spool.getPrimitivePropertySafely("dtLimiteInt2").toString(), false)) );
                        values.put("Ubicazione", PulisciStringa(Spool.getPrimitivePropertySafely("Ubicazione").toString(), false) );
                        values.put("Periodicita", PulisciStringa(Spool.getPrimitivePropertySafely("Periodicita").toString(), false) );
                        values.put("StatoMisuratore", PulisciStringa(Spool.getPrimitivePropertySafely("StatoMisuratore").toString(), false) );
                        values.put("Provincia", PulisciStringa(Spool.getPrimitivePropertySafely("Provincia").toString(), false) );
                        values.put("LCifreLettura", PulisciStringa(Spool.getPrimitivePropertySafely("LCifreLettura").toString(), false) );
                        values.put("LDataLettura", GPC.ConvertiSoloData(PulisciStringa(Spool.getPrimitivePropertySafely("LDataLettura").toString(), false)) );
                        values.put("LOraLettura", GPC.ConvertiSoloOra(PulisciStringa(Spool.getPrimitivePropertySafely("LOraLettura").toString(), false)) );
                        values.put("LNota1", PulisciStringa(Spool.getPrimitivePropertySafely("LNota1").toString(), false) );
                        values.put("LNota2", PulisciStringa(Spool.getPrimitivePropertySafely("LNota2").toString(), false) );
                        values.put("LSegnalazione", PulisciStringa(Spool.getPrimitivePropertySafely("LSegnalazione").toString(), false) );
                        values.put("LSegnalazione1", PulisciStringa(Spool.getPrimitivePropertySafely("LSegnalazione1").toString(), false) );
                        values.put("LLetturaCorrettore", PulisciStringa(Spool.getPrimitivePropertySafely("LLetturaCorrettore").toString(), false) );
                        values.put("LLetturaCorrettore1", PulisciStringa(Spool.getPrimitivePropertySafely("LLetturaCorrettore1").toString(), false) );
                        values.put("LLetturaCorrettore2", PulisciStringa(Spool.getPrimitivePropertySafely("LLetturaCorrettore2").toString(), false) );
                        values.put("LLatitudine", PulisciStringa(Spool.getPrimitivePropertySafely("LLatitudine").toString(), false) );
                        values.put("LLongitudine", PulisciStringa(Spool.getPrimitivePropertySafely("LLongitudine").toString(), false) );
                        values.put("LAutolettura", PulisciStringa(Spool.getPrimitivePropertySafely("LAutolettura").toString(), false) );
                        values.put("LNoteLibere", PulisciStringa(Spool.getPrimitivePropertySafely("LNoteLibere").toString(), false) );
                        values.put("DtLetturaTrm", GPC.ConvertiSoloData(PulisciStringa(Spool.getPrimitivePropertySafely("DtLetturaTrm").toString(), false)) );
                        values.put("OraLetturaTrm", GPC.ConvertiSoloOra(PulisciStringa(Spool.getPrimitivePropertySafely("OraLetturaTrm").toString(), false)) );
                        values.put("dtLimiteInfLett", PulisciStringa(Spool.getPrimitivePropertySafely("dtLimiteInfLett").toString(), false) );
                        values.put("DtLimiteLettura", PulisciStringa(Spool.getPrimitivePropertySafely("DtLimiteLettura").toString(), false) );
                        values.put("NoteAttivita", PulisciStringa(Spool.getPrimitivePropertySafely("NoteAttivita").toString(), false) );
                        values.put("AttivitaSpeciale", PulisciStringa(Spool.getPrimitivePropertySafely("AttivitaSpeciale").toString(), false) );
                        values.put("DtPrimoPassaggio", PulisciStringa(Spool.getPrimitivePropertySafely("DtPrimoPassaggio").toString(), false) );
                        values.put("OraPrimoPassaggio", PulisciStringa(Spool.getPrimitivePropertySafely("OraPrimoPassaggio").toString(), false) );
                        values.put("LettMinimaCalcolata", PulisciStringa(Spool.getPrimitivePropertySafely("LettMinimaCalcolata").toString(), false) );
                        values.put("LettMassimaCalcolata", PulisciStringa(Spool.getPrimitivePropertySafely("LettMassimaCalcolata").toString(), false) );
                        values.put("FDLettura", PulisciStringa(Spool.getPrimitivePropertySafely("FDLettura").toString(), false) );
                        values.put("ObbNotaUno", PulisciStringa(Spool.getPrimitivePropertySafely("ObbNotaUno").toString(), false) );
                        values.put("ObbNotaDue", PulisciStringa(Spool.getPrimitivePropertySafely("ObbNotaDue").toString(), false) );
                        values.put("Ritrasmetti", "N");
                        values.put("Info", PulisciStringa(Spool.getPrimitivePropertySafely("Info").toString(), false) );
                        values.put("dtProgrammazione", PulisciStringa(Spool.getPrimitivePropertySafely("dtProgrammazione").toString(), false) );
                        values.put("daOraLett", PulisciStringa(Spool.getPrimitivePropertySafely("daOraLett").toString(), false) );
                        values.put("aOraLett", PulisciStringa(Spool.getPrimitivePropertySafely("aOraLett").toString(), false) );
                        values.put("Nota3", PulisciStringa(Spool.getPrimitivePropertySafely("Nota3").toString(), false));

                        ListTBUtentiOUT.add(values);

                    }
                        ContentValues values = new ContentValues();
                        values.put("idAzienda", PulisciStringa(Spool.getPrimitivePropertySafely("idAzienda").toString(), false));
                        values.put("IdCarico", PulisciStringa(Spool.getPrimitivePropertySafely("IdCarico").toString(), false));
                        values.put("CodiceUtente", PulisciStringa(Spool.getPrimitivePropertySafely("CodiceUtente").toString(), false));
                        values.put("Nominativo", PulisciStringa(Spool.getPrimitivePropertySafely("Nominativo").toString(), false));
                        values.put("Cognome", PulisciStringa(Spool.getPrimitivePropertySafely("Cognome").toString(), false));
                        values.put("Toponimo", PulisciStringa(Spool.getPrimitivePropertySafely("Toponimo").toString(), true));
                        values.put("Strada", PulisciStringa(Spool.getPrimitivePropertySafely("Strada").toString(), true));
                        values.put("Civico", PulisciStringa(Spool.getPrimitivePropertySafely("Civico").toString(), true));
                        values.put("Isolato", PulisciStringa(Spool.getPrimitivePropertySafely("Isolato").toString(), true));
                        values.put("Scala", PulisciStringa(Spool.getPrimitivePropertySafely("Scala").toString(), true));
                        values.put("Piano", PulisciStringa(Spool.getPrimitivePropertySafely("Piano").toString(), true));
                        values.put("Interno", PulisciStringa(Spool.getPrimitivePropertySafely("Interno").toString(), true));
                        values.put("Localita", PulisciStringa(Spool.getPrimitivePropertySafely("Localita").toString(), true));
                        values.put("CAP", PulisciStringa(Spool.getPrimitivePropertySafely("CAP").toString(), true));
                        values.put("CodicePercorso", PulisciStringa(Spool.getPrimitivePropertySafely("CodicePercorso").toString(), false));
                        values.put("NoteAccesso", PulisciStringa(Spool.getPrimitivePropertySafely("NoteAccesso").toString(), false));
                        values.put("MatricolaMis", PulisciStringa(Spool.getPrimitivePropertySafely("MatricolaMis").toString(), false));
                        values.put("CifreContatore", PulisciStringa(Spool.getPrimitivePropertySafely("CifreContatore").toString(), false));
                        values.put("CategoriaUtente", PulisciStringa(Spool.getPrimitivePropertySafely("CategoriaUtente").toString(), false));
                        values.put("DataUltLettura", ConvertiData(PulisciStringa(Spool.getPrimitivePropertySafely("DataUltLettura").toString(), false)));
                        values.put("UltimaLettura", ConvertiData(PulisciStringa(Spool.getPrimitivePropertySafely("UltimaLettura").toString(), false)));
                        values.put("LetturaPrevista", ConvertiData(PulisciStringa(Spool.getPrimitivePropertySafely("LetturaPrevista").toString(), false)));
                        values.put("telefono", PulisciStringa(Spool.getPrimitivePropertySafely("telefono").toString(), true));
                        values.put("IdUtente", PulisciStringa(Spool.getPrimitivePropertySafely("IdUtente").toString(), false));
                        values.put("idLetturista", PulisciStringa(Spool.getPrimitivePropertySafely("idLetturista").toString(), false));
                        values.put("NomeLetturista", PulisciStringa(Spool.getPrimitivePropertySafely("NomeLetturista").toString(), false));
                        values.put("Terminale", PulisciStringa(Spool.getPrimitivePropertySafely("Terminale").toString(), false));
                        values.put("DtAffidamento", ConvertiData(PulisciStringa(Spool.getPrimitivePropertySafely("DtAffidamento").toString(), false)));
                        values.put("DtRientro", ConvertiData(PulisciStringa(Spool.getPrimitivePropertySafely("DtRientro").toString(), false)));
                        values.put("Stato", PulisciStringa(Spool.getPrimitivePropertySafely("Stato").toString(), false));
                        values.put("NotaAccess", PulisciStringa(Spool.getPrimitivePropertySafely("NotaAccess").toString(), true));
                        values.put("PuntoFornitura", PulisciStringa(Spool.getPrimitivePropertySafely("PuntoFornitura").toString(), false));
                        values.put("NotaAccesso100", PulisciStringa(Spool.getPrimitivePropertySafely("NotaAccesso100").toString(), false));
                        values.put("LetturaDa", ConvertiData(PulisciStringa(Spool.getPrimitivePropertySafely("LetturaDa").toString(), false)));
                        values.put("LetturaA", ConvertiData(PulisciStringa(Spool.getPrimitivePropertySafely("LetturaA").toString(), false)));
                        values.put("PresCorrettore", PulisciStringa(Spool.getPrimitivePropertySafely("PresCorrettore").toString(), false));
                        values.put("ClasseContatore", PulisciStringa(Spool.getPrimitivePropertySafely("ClasseContatore").toString(), false));
                        values.put("dtLimiteInt1", ConvertiData(PulisciStringa(Spool.getPrimitivePropertySafely("dtLimiteInt1").toString(), false)));
                        values.put("dtLimiteInt2", ConvertiData(PulisciStringa(Spool.getPrimitivePropertySafely("dtLimiteInt2").toString(), false)));
                        values.put("Ubicazione", PulisciStringa(Spool.getPrimitivePropertySafely("Ubicazione").toString(), false));
                        values.put("Periodicita", PulisciStringa(Spool.getPrimitivePropertySafely("Periodicita").toString(), false));
                        values.put("StatoMisuratore", PulisciStringa(Spool.getPrimitivePropertySafely("StatoMisuratore").toString(), false));
                        values.put("Provincia", PulisciStringa(Spool.getPrimitivePropertySafely("Provincia").toString(), false));
                        values.put("LLatitudine", PulisciStringa(Spool.getPrimitivePropertySafely("LLatitudine").toString(), false));
                        values.put("LLongitudine", PulisciStringa(Spool.getPrimitivePropertySafely("LLongitudine").toString(), false));
                        values.put("LAutolettura", PulisciStringa(Spool.getPrimitivePropertySafely("LAutolettura").toString(), false));
                        values.put("LNoteLibere", PulisciStringa(Spool.getPrimitivePropertySafely("LNoteLibere").toString(), false));
                        values.put("DtLetturaTrm", GPC.ConvertiSoloData(PulisciStringa(Spool.getPrimitivePropertySafely("DtLetturaTrm").toString(), false)));
                        values.put("OraLetturaTrm", GPC.ConvertiSoloOra(PulisciStringa(Spool.getPrimitivePropertySafely("OraLetturaTrm").toString(), false)));
                        values.put("dtLimiteInfLett", PulisciStringa(Spool.getPrimitivePropertySafely("dtLimiteInfLett").toString(), false));
                        values.put("DtLimiteLettura", PulisciStringa(Spool.getPrimitivePropertySafely("DtLimiteLettura").toString(), false));
                        values.put("NoteAttivita", PulisciStringa(Spool.getPrimitivePropertySafely("NoteAttivita").toString(), false));
                        values.put("AttivitaSpeciale", PulisciStringa(Spool.getPrimitivePropertySafely("AttivitaSpeciale").toString(), false));
                        values.put("DtPrimoPassaggio", PulisciStringa(Spool.getPrimitivePropertySafely("DtPrimoPassaggio").toString(), false));
                        values.put("OraPrimoPassaggio", PulisciStringa(Spool.getPrimitivePropertySafely("OraPrimoPassaggio").toString(), false));
                        values.put("LettMinimaCalcolata", PulisciStringa(Spool.getPrimitivePropertySafely("LettMinimaCalcolata").toString(), false));
                        values.put("LettMassimaCalcolata", PulisciStringa(Spool.getPrimitivePropertySafely("LettMassimaCalcolata").toString(), false));
                        values.put("FDLettura", PulisciStringa(Spool.getPrimitivePropertySafely("FDLettura").toString(), false));
                        values.put("ObbNotaUno", PulisciStringa(Spool.getPrimitivePropertySafely("ObbNotaUno").toString(), false));
                        values.put("ObbNotaDue", PulisciStringa(Spool.getPrimitivePropertySafely("ObbNotaDue").toString(), false));
                        values.put("Ritrasmetti", "S");
                        values.put("Info", PulisciStringa(Spool.getPrimitivePropertySafely("Info").toString(), false));
                        values.put("dtProgrammazione", GPC.ConvertiSoloData(PulisciStringa(Spool.getPrimitivePropertySafely("dtProgrammazione").toString(), true)));
                        values.put("daOraLett", GPC.ConvertiSoloOra(PulisciStringa(Spool.getPrimitivePropertySafely("daOraLett").toString(), true)));
                        values.put("aOraLett", GPC.ConvertiSoloOra(PulisciStringa(Spool.getPrimitivePropertySafely("aOraLett").toString(), true)));
                        values.put("Nota3", PulisciStringa(Spool.getPrimitivePropertySafely("Nota3").toString(), false));
                        ListTBUtenti.add(values);
                }
            }

            Log.i(TAG, "Finito gettallRecordUtenti - POSITIVO - ListTBUtenti.Size=" + ListTBUtenti.size() + " ListTBUtentiOUT.Size="+ListTBUtentiOUT.size());
            NessunLavoro = false;
            return true;

        }catch (SocketTimeoutException et) {

            endTime = System.currentTimeMillis();
            TempoTrascorso = ((endTime - startTime)/60000);
            Timeout = true;
            Log.i(TAG, "Finito gettallRecordUtenti - TIMEOUT");
            MessErrore = "ErroreWS TIMEOUT: " + et.getMessage() + " - " + et.getCause() + " - " + et.toString();
            return false;
        } catch (Exception e) {
            endTime = System.currentTimeMillis();
            TempoTrascorso = ((endTime - startTime)/60000);
            if(e.toString().contains("illegal property: Spool"))
            {
                NessunLavoro = true;
                Log.i(TAG, "Non sono stati trovati carici in UTENTI");
                return true;
            }
            else
            {
                NessunLavoro = false;
                MessErrore = "AsyncWS_ErroreWS: " + e.getMessage() + " - " + e.getCause() + " - " + e.toString();
                Log.i(TAG, "Finito gettallRecordUtenti - NEGATIVO - "+ MessErrore);
                return false;
            }
        }


    }
}
