package it.comerservizi.appsveltodroid;

import android.content.ComponentCallbacks2;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static it.comerservizi.appsveltodroid.Constants.FIFTH_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.FIRST_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.FOURTH_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.SECOND_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.SEVENTH_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.SIXTH_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.THIRD_COLUMN;

public class Act_ElencoProgrammazione extends AppCompatActivity {

    DatabaseHelper dbHelper;
    SQLiteDatabase db;
    Spinner spnDataProg;
    TextView txtNumAttivitaElenco;
    ListView listAttivitaProg;
    ListViewAdapters adapter;
    AsyncLoader ElementiAs;

    GPSHelper gps;
    GPClass GP;

    ArrayList<String> SelectDataProg;
    private List<String> spinnerDataProg;
    ArrayList<HashMap<String, String>> list;
    public ArrayList<String> IndiceElencoCodiceUtente;
    public ArrayList<String> IndiceElencoIdCarico;
    public ArrayList<String> IndiceElencoIdAzienza;
    boolean statusOfGPS;
    String IddtProgScelto;
    Integer NumOperatore = 0;
    Integer countElenco = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__elenco_programmazione);
        onTrimMemory(ComponentCallbacks2.TRIM_MEMORY_COMPLETE);
        onTrimMemory(ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN);

        spnDataProg = (Spinner) findViewById(R.id.spnDataProg);
        txtNumAttivitaElenco = (TextView) findViewById(R.id.txtNumAttivitaElenco);
        listAttivitaProg = (ListView) findViewById(R.id.listAttivitaProg);


        GP = new GPClass();
        dbHelper = new DatabaseHelper(Act_ElencoProgrammazione.this);
        db = dbHelper.getWritableDatabase();

        Intent I_ActElencoAttivita = getIntent();
        NumOperatore = I_ActElencoAttivita.getExtras().getInt("NumOperatore");

        spinnerDataProg = new ArrayList<String>();
        spinnerDataProg.add("TUTTI");
        SelectDataProg = new ArrayList<String>();
        SelectDataProg.add("");
        IddtProgScelto = "nessuno";


        Cursor cursorDataProg = dbHelper.query(db, "SELECT dtProgrammazione FROM utenti WHERE dtProgrammazione!='' and dtProgrammazione IS NOT NULL GROUP BY dtProgrammazione ORDER BY dtProgrammazione");

        cursorDataProg.moveToFirst();
        while (cursorDataProg.isAfterLast() == false) {
            spinnerDataProg.add(GP.ConvertiData(cursorDataProg.getString(cursorDataProg.getColumnIndex("dtProgrammazione"))));
            SelectDataProg.add(GP.ConvertiData(cursorDataProg.getString(cursorDataProg.getColumnIndex("dtProgrammazione"))));
            cursorDataProg.moveToNext();
        }
        cursorDataProg.close();

        ArrayAdapter<String> SpinnAdapCarico = new ArrayAdapter<String>(this, R.layout.spinner_item1, spinnerDataProg);
        spnDataProg.setAdapter(SpinnAdapCarico);
        spnDataProg.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                if (position > 0)
                {
                    IddtProgScelto = SelectDataProg.get(position);
                    OrdinaGenerico();
                }
                else
                {
                    IddtProgScelto = "nessuno";
                    OrdinaGenerico();
                }

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {
                IddtProgScelto = "nessuno";
                spnDataProg.setSelection(0);
            }
        });
        list=new ArrayList<HashMap<String,String>>();
        adapter=new ListViewAdapters(this, list);
        //ElementiAs = new AsyncLoader(Act_ElencoProgrammazione.this, "DettaglioProgrammazione");
        //ElementiAs.operatore = NumOperatore.toString();
        //ElementiAs.Search4dtProg = IddtProgScelto;
        //ElementiAs.execute();


    }

    @Override
    protected void onPause()
    {
        super.onPause();
    }

    @Override
    protected void onRestart()
    {
        super.onRestart();
    }

    @Override
    protected void onStart()
    {
        super.onStart();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        VerificaGPS();
        //ElementiAs = new AsyncLoader(Act_ElencoProgrammazione.this, "DettaglioProgrammazione");
        //ElementiAs.operatore = NumOperatore.toString();
        //ElementiAs.Search4dtProg = IddtProgScelto;
        //ElementiAs.execute();
        OrdinaGenerico();
    }

    @Override
    public void onBackPressed() {
        finish();
        return;
    }


    public void VerificaGPS()
    {

        gps = new GPSHelper();
        gps.GPSContesto(getApplicationContext());

        statusOfGPS = gps.StatoGPS();
        if (!statusOfGPS)
            Toast.makeText(getApplicationContext(), "ATTENZIONE DEVI ATTIVARE IL GPS", Toast.LENGTH_LONG).show();

    }

    //Ordinamento per prima scelta carico
    public void OrdinaGenerico()
    {
        String IndRicercaPass;
        String Nome = "";
        String Cognome = "";

        countElenco = 0;

        list.clear();
        ListView listView=(ListView)findViewById(R.id.listAttivitaProg);

        IndiceElencoCodiceUtente = new ArrayList<String>();
        IndiceElencoCodiceUtente.clear();
        IndiceElencoIdCarico = new ArrayList<String>();
        IndiceElencoIdCarico.clear();
        IndiceElencoIdAzienza = new ArrayList<String>();
        IndiceElencoIdAzienza.clear();
        Cursor cursor;

        if(IddtProgScelto.contains("nessuno"))
            cursor = dbHelper.query(db, "SELECT idCarico, idAzienda, CodiceUtente,Nominativo, Cognome, CAP, Localita, Provincia, Toponimo, Strada, Civico,NotaAccesso100,dtProgrammazione, telefono,MatricolaMis,daOraLett,aOraLett FROM utenti WHERE (dtProgrammazione !='') AND Stato = 'A' ORDER BY dtProgrammazione, daOraLett ");
        else
            cursor = dbHelper.query(db, "SELECT idCarico, idAzienda, CodiceUtente,Nominativo, Cognome,  CAP, Localita, Provincia, Toponimo, Strada, Civico,NotaAccesso100,dtProgrammazione, telefono,MatricolaMis,daOraLett,aOraLett FROM utenti WHERE (dtProgrammazione = '" + IddtProgScelto + "') AND Stato = 'A' ORDER BY dtProgrammazione, daOraLett");

        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {
            HashMap<String,String> temp=new HashMap<String, String>();
            if(cursor.isNull(cursor.getColumnIndex("Nominativo")))
                Nome = "";
            else
                Nome = cursor.getString(cursor.getColumnIndex("Nominativo"));
            if(cursor.isNull(cursor.getColumnIndex("Cognome")))
                Cognome = "";
            else
                Cognome = cursor.getString(cursor.getColumnIndex("Cognome"));
            temp.put(FIRST_COLUMN, Nome + " " + Cognome );
            temp.put(SECOND_COLUMN, cursor.getString(cursor.getColumnIndex("Toponimo")).trim() + " " + cursor.getString(cursor.getColumnIndex("Strada")).trim() + " " + cursor.getString(cursor.getColumnIndex("Civico")).trim()+ '\n' + cursor.getString(cursor.getColumnIndex("NotaAccesso100")).trim());
            temp.put(THIRD_COLUMN, GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("CAP")).trim())+ " " +GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Localita")).trim()) + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Provincia"))).trim());
            temp.put(FOURTH_COLUMN, "Telefono: " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("telefono")).trim()));
            temp.put(FIFTH_COLUMN, "Matricola: " + cursor.getString(cursor.getColumnIndex("MatricolaMis")).trim());
            temp.put(SIXTH_COLUMN, cursor.getString(cursor.getColumnIndex("dtProgrammazione")));
            temp.put(SEVENTH_COLUMN, cursor.getString(cursor.getColumnIndex("daOraLett")) + " - " + cursor.getString(cursor.getColumnIndex("aOraLett")));
            list.add(temp);
            IndiceElencoCodiceUtente.add(cursor.getString(cursor.getColumnIndex("CodiceUtente")));
            IndiceElencoIdCarico.add(cursor.getString(cursor.getColumnIndex("IdCarico")));
            IndiceElencoIdAzienza.add(cursor.getString(cursor.getColumnIndex("idAzienda")));
            cursor.moveToNext();
            countElenco++;
        }
        txtNumAttivitaElenco.setText(countElenco.toString());
        adapter=new ListViewAdapters(this, list);
        adapter.cosa = "DettaglioProgrammazione";

        listView.setAdapter(null);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                        VerificaGPS();
                        if (statusOfGPS) {
                            Intent intent = new Intent(Act_ElencoProgrammazione.this, Act_RilevaDati.class);
                            String IDRicerca = IndiceElencoCodiceUtente.get(position);
                            String IDCarico = IndiceElencoIdCarico.get(position);
                            String IDAzienda = IndiceElencoIdAzienza.get(position);
                            intent.putExtra("CodiceUtente", IDRicerca);
                            intent.putExtra("IDCarico", IDCarico.trim());
                            intent.putExtra("IDLett", NumOperatore.toString());
                            intent.putExtra("IDAzienda", IDAzienda.trim());
                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), "Attenzione per poter lavorare devi attivare il GPS", Toast.LENGTH_LONG).show();
                        }

            }

        });
        cursor.close();
        System.gc();
    }
}
