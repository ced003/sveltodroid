package it.comerservizi.appsveltodroid;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

public class Act_Censimento extends AppCompatActivity {

    private EditText txtCensNominativo;
    private EditText txtCensMatricola;
    private EditText txtCensSiglaMatricola;
    private EditText txtCensAnnoMatricola;
    private EditText txtCensImpegnativo;
    private EditText txtCensCifreMisuratore;
    private EditText txtCensIndirizzo;
    private EditText txtCensCivico;
    private EditText txtCensBarra;
    private EditText txtCensScala;
    private EditText txtCensPiano;
    private EditText txtCensInterno;
    private EditText txtCensComune;
    private EditText txtCensSigillo;
    private EditText txtCensNotaSigillo;
    private EditText txtCensTelefono;
    private Button btnSalva, btnCancella;

    private String CensNominativo;
    private String CensMatricola;
    private String CensSiglaMatricola;
    private String CensAnnoMatricola;
    private String CensImpegnativo;
    private String CensCifreMisuratore;
    private String CensIndirizzo;
    private String CensCivico;
    private String CensBarra;
    private String CensScala;
    private String CensPiano;
    private String CensInterno;
    private String CensComune;
    private String CensNotaSigillo;
    private String CensSigillo;
    private String CensTelefono;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__censimento);

        /* CONTROLLO DATA AUTOMATICA */
        try {
            if(Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME) != 1 || Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME_ZONE)!=1)
            {
                Toast.makeText(getApplicationContext(), "ATTENZIONE HAI DISABILITATO LA DATA E ORA AUTOMATICA - L'APP NON PUO' FUNZIONARE", Toast.LENGTH_LONG).show();
                finish();
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        /* CONTROLLO DATA AUTOMATICA */
        txtCensNominativo=(EditText) findViewById(R.id.txtCensNominativo);
        txtCensMatricola=(EditText) findViewById(R.id.txtCensMatricola);
        txtCensSiglaMatricola=(EditText) findViewById(R.id.txtCensSiglaMatricola);
        txtCensAnnoMatricola=(EditText) findViewById(R.id.txtCensAnnoMatricola);
        txtCensImpegnativo=(EditText) findViewById(R.id.txtCensImpegnativo);
        txtCensCifreMisuratore=(EditText) findViewById(R.id.txtCensCifreMisuratore);
        txtCensIndirizzo=(EditText) findViewById(R.id.txtCensIndirizzo);
        txtCensCivico=(EditText) findViewById(R.id.txtCensCivico);
        txtCensBarra=(EditText) findViewById(R.id.txtCensBarra);
        txtCensScala=(EditText) findViewById(R.id.txtCensScala);
        txtCensPiano=(EditText) findViewById(R.id.txtCensPiano);
        txtCensInterno=(EditText) findViewById(R.id.txtCensInterno);
        txtCensComune=(EditText) findViewById(R.id.txtCensComune);
        txtCensSigillo=(EditText) findViewById(R.id.txtCensSigillo);
        txtCensNotaSigillo=(EditText) findViewById(R.id.txtCensNotaSigillo);
        txtCensTelefono=(EditText) findViewById(R.id.txtCensTelefono);

        btnSalva = (Button) findViewById(R.id.btnSalvaCensimento);
        btnCancella = (Button) findViewById(R.id.btnAnnullaCensimento);


        Bundle extras = getIntent().getExtras();
        if(extras !=null) {
            CensNominativo = extras.getString("CensNominativo");
            CensMatricola = extras.getString("CensMatricola");
            CensAnnoMatricola = extras.getString("CensAnnoMatricola");
            CensSiglaMatricola = extras.getString("CensSiglaMatricola");
            CensImpegnativo = extras.getString("CensImpegnativo");
            CensCifreMisuratore = extras.getString("CensCifreMisuratore");
            CensIndirizzo = extras.getString("CensIndirizzo");
            CensCivico = extras.getString("CensCivico");
            CensBarra = extras.getString("CensBarra");
            CensScala = extras.getString("CensScala");
            CensPiano = extras.getString("CensPiano");
            CensInterno = extras.getString("CensInterno");
            CensComune = extras.getString("CensComune");
            CensSigillo = extras.getString("CensSigillo");
            CensNotaSigillo = extras.getString("CensNotaSigillo");
            CensTelefono = extras.getString("CensTelefono");

            txtCensNominativo.setText(CensNominativo);
            txtCensMatricola.setText(CensMatricola);
            txtCensAnnoMatricola.setText(CensAnnoMatricola);
            txtCensSiglaMatricola.setText(CensSiglaMatricola);
            txtCensImpegnativo.setText(CensImpegnativo);
            txtCensCifreMisuratore.setText(CensCifreMisuratore);
            txtCensIndirizzo.setText(CensIndirizzo);
            txtCensCivico.setText(CensCivico);
            txtCensBarra.setText(CensBarra);
            txtCensScala.setText(CensScala);
            txtCensPiano.setText(CensPiano);
            txtCensInterno.setText(CensInterno);
            txtCensComune.setText(CensComune);
            txtCensSigillo.setText(CensSigillo);
            txtCensNotaSigillo.setText(CensNotaSigillo);
            txtCensTelefono.setText(CensTelefono);

        }

    }

    @Override
    protected void onResume()
    {
        super.onResume();

        /* CONTROLLO DATA AUTOMATICA */
        try {
            if(Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME) != 1 || Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME_ZONE)!=1)
            {
                Toast.makeText(getApplicationContext(), "ATTENZIONE HAI DISABILITATO LA DATA E ORA AUTOMATICA - L'APP NON PUO' FUNZIONARE", Toast.LENGTH_LONG).show();
                finish();
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        /* CONTROLLO DATA AUTOMATICA */

    }

    public void SalvaCensimento(View view)
    {
        Integer Controllo = 0;
        CensNominativo = txtCensNominativo.getText().toString();
        CensMatricola = txtCensMatricola.getText().toString();
        CensAnnoMatricola = txtCensAnnoMatricola.getText().toString();
        CensSiglaMatricola = txtCensSiglaMatricola.getText().toString();
        CensImpegnativo = txtCensImpegnativo.getText().toString();
        CensCifreMisuratore = txtCensCifreMisuratore.getText().toString();
        CensIndirizzo = txtCensIndirizzo.getText().toString();
        CensCivico = txtCensCivico.getText().toString();
        CensBarra = txtCensBarra.getText().toString();
        CensScala = txtCensScala.getText().toString();
        CensPiano = txtCensPiano.getText().toString();
        CensInterno = txtCensInterno.getText().toString();
        CensComune = txtCensComune.getText().toString();
        CensSigillo = txtCensSigillo.getText().toString();
        CensNotaSigillo = txtCensNotaSigillo.getText().toString();
        CensTelefono = txtCensTelefono.getText().toString();

        LinearLayout myScroll = (LinearLayout) findViewById(R.id.Linear_Cens);
        LinearLayout subScroll;
        EditText bb;
        for( int i = 0; i < myScroll.getChildCount(); i++ ) {
            if (myScroll.getChildAt(i) instanceof LinearLayout)
            {
                subScroll = (LinearLayout) myScroll.getChildAt(i);
                for( int j = 0; j < subScroll.getChildCount(); j++)
                {
                    if(subScroll.getChildAt(j) instanceof EditText)
                    {
                        bb = (EditText) subScroll.getChildAt(j);
                        if(!bb.getText().toString().isEmpty())
                        {
                            Controllo++;
                        }
                    }
                }
            }
        }


        if(Controllo==0)
        {
            Toast.makeText(getApplicationContext(), "Attenzione non puoi salvare senza aver inserito dati", Toast.LENGTH_LONG).show();
        }
        else
        {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("CensNominativo",CensNominativo);
            returnIntent.putExtra("CensMatricola",CensMatricola);
            returnIntent.putExtra("CensAnnoMatricola",CensAnnoMatricola);
            returnIntent.putExtra("CensSiglaMatricola",CensSiglaMatricola);
            returnIntent.putExtra("CensImpegnativo",CensImpegnativo);
            returnIntent.putExtra("CensCifreMisuratore",CensCifreMisuratore);
            returnIntent.putExtra("CensIndirizzo",CensIndirizzo);
            returnIntent.putExtra("CensCivico",CensCivico);
            returnIntent.putExtra("CensBarra",CensBarra);
            returnIntent.putExtra("CensScala",CensScala);
            returnIntent.putExtra("CensPiano",CensPiano);
            returnIntent.putExtra("CensInterno",CensInterno);
            returnIntent.putExtra("CensComune",CensComune);
            returnIntent.putExtra("CensSigillo",CensSigillo);
            returnIntent.putExtra("CensNotaSigillo",CensNotaSigillo);
            returnIntent.putExtra("CensTelefono", CensTelefono);
            setResult(Act_Censimento.RESULT_OK, returnIntent);
            finish();
        }

    }


    public void AnnullaCensimento(View view)
    {

        Intent returnIntent = new Intent();
        setResult(this.RESULT_CANCELED, returnIntent);
        finish();

        return;
    }

    @Override
    public void onBackPressed() {
        return;
    }


}
