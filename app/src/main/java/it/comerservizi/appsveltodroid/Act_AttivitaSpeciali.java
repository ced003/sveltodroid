package it.comerservizi.appsveltodroid;

import android.app.ProgressDialog;
import android.content.ComponentCallbacks2;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

import static it.comerservizi.appsveltodroid.Constants.FIFTH_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.FIRST_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.FOURTH_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.SECOND_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.THIRD_COLUMN;

public class Act_AttivitaSpeciali extends AppCompatActivity {

    GPSHelper gps;
    GPClass gpClass;
    private ProgressDialog dialog;
    private String GiornoAccesso;
    public ArrayList<String> IndiceElencoCodiceUtente;
    public ArrayList<String> IndiceElencoIdCarico;
    public ArrayList<String> IndiceElencoIdAzienza;
    Integer countElenco = 0;
    boolean statusOfGPS;
    ListView listView;
    DatabaseHelper dbHelper;
    SQLiteDatabase db;
    ListViewAdapters adapter;
    String IDLett="";
    TextView txtTotAccessibili, txtTotSpeciali;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__attivita_speciali);
        onTrimMemory(ComponentCallbacks2.TRIM_MEMORY_COMPLETE);
        onTrimMemory(ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN);
        /* INIZIO CONTROLLO DATA AUTOMATICA */
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                if(Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME) != 1 || Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME_ZONE) != 1)
                {
                    Toast.makeText(getApplicationContext(), "ATTENZIONE HAI DISABILITATO LA DATA E ORA AUTOMATICA - L'APP NON PUO' FUNZIONARE", Toast.LENGTH_LONG).show();
                    this.finishAffinity();
                }
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        /* FINE CONTROLLO DATA AUTOMATICA */


        gpClass = new GPClass();
        Intent I_ActElencoAttivita = getIntent();
        GiornoAccesso = I_ActElencoAttivita.getExtras().getString("GiornoAccesso");
        IDLett = I_ActElencoAttivita.getExtras().getString("IDLett");
        statusOfGPS = false;
        countElenco = 0;
        listView=(ListView)findViewById(R.id.listAttivita);
        txtTotAccessibili = (TextView) findViewById(R.id.txtTotAccessibili);
        txtTotSpeciali = (TextView) findViewById(R.id.txtTotSpeciali);



        dbHelper = new DatabaseHelper(Act_AttivitaSpeciali.this);
        db = dbHelper.getWritableDatabase();
    }
    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        setResult(ListaCarico.RESULT_OK, returnIntent);
        finish();
        return;
    }
    @Override
    protected void onPause()
    {
        super.onPause();
        //db.close();
        //dbHelper.close();
    }

    @Override
    protected void onRestart()
    {
        super.onRestart();
    }

    @Override
    protected void onStart()
    {
        super.onStart();
    }
    @Override
    protected void onResume()
    {
        super.onResume();

        /* CONTROLLO DATA AUTOMATICA */
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                if(Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME) != 1 || Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME_ZONE) != 1)
                {
                    Toast.makeText(getApplicationContext(), "ATTENZIONE HAI DISABILITATO LA DATA E ORA AUTOMATICA - L'APP NON PUO' FUNZIONARE", Toast.LENGTH_LONG).show();
                    finish();
                }
                else
                {
                    ElencoLista(1);
                }
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        /* CONTROLLO DATA AUTOMATICA */
    }


    @Override
    protected void onStop()
    {
        super.onStop();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
    }

    public void VerificaGPS()
    {
        gps = new GPSHelper();
        gps.GPSContesto(getApplicationContext());

        statusOfGPS = gps.StatoGPS();

    }
    public void ElencoLista(int StartOrResume)
    {
        int contAccessibili = 0, contSpeciali = 0;
        dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        dialog.setMessage("Elaborazione del carico in corso...");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.show();
        String data;
        Integer countElenco = 0;


        ArrayList<HashMap<String, String>> list=new ArrayList<HashMap<String,String>>();
        ListView listView=(ListView) findViewById(R.id.listAttivita);

        IndiceElencoCodiceUtente = new ArrayList<String>();
        IndiceElencoCodiceUtente.clear();
        IndiceElencoIdCarico = new ArrayList<String>();
        IndiceElencoIdCarico.clear();
        IndiceElencoIdAzienza = new ArrayList<String>();
        IndiceElencoIdAzienza.clear();
        Cursor cursor;
        cursor = dbHelper.query(db, "SELECT * FROM utenti WHERE (NotaAccess = 'Y' OR AttivitaSpeciale IS NOT NULL) AND Stato = 'A' ORDER BY NotaAccess DESC, AttivitaSpeciale, Toponimo, Strada, CAST(Civico AS Integer)");

        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {
            HashMap<String,String> temp=new HashMap<String, String>();
            temp.put(FIRST_COLUMN, cursor.getString(cursor.getColumnIndex("Nominativo")) );
            temp.put(SECOND_COLUMN, cursor.getString(cursor.getColumnIndex("Toponimo")) + " " + cursor.getString(cursor.getColumnIndex("Strada")) + " " + cursor.getString(cursor.getColumnIndex("Civico"))+ " - " + cursor.getString(cursor.getColumnIndex("NotaAccesso100")));
            if(cursor.getString(cursor.getColumnIndex("NotaAccess")).contains("Y") &&  !cursor.isNull(cursor.getColumnIndex("AttivitaSpeciale")))
            {
                if(cursor.getString(cursor.getColumnIndex("AttivitaSpeciale")).equals("C"))
                    temp.put(THIRD_COLUMN, "ACCESSIBILE & NOTA 051");

                contAccessibili++;
                contSpeciali++;
            }
            else if(cursor.getString(cursor.getColumnIndex("NotaAccess")).equals("Y")) {
                temp.put(THIRD_COLUMN, "ACCESSIBILE");
                contAccessibili++;
            }
            else if(!cursor.isNull(cursor.getColumnIndex("AttivitaSpeciale")))
            {
                if(cursor.getString(cursor.getColumnIndex("AttivitaSpeciale")).equals("C"))
                    temp.put(THIRD_COLUMN, "CENSIMENTO - NOTA 051");
                if(cursor.getString(cursor.getColumnIndex("AttivitaSpeciale")).equals("S"))
                    temp.put(THIRD_COLUMN, "RISIGILLATURA");


                contSpeciali++;
            }

            temp.put(FOURTH_COLUMN, "Matricola: " + cursor.getString(cursor.getColumnIndex("MatricolaMis")));
            temp.put(FIFTH_COLUMN, "Data Limite: " + cursor.getString(cursor.getColumnIndex("LetturaA")));
            list.add(temp);
            IndiceElencoCodiceUtente.add(cursor.getString(cursor.getColumnIndex("CodiceUtente")));
            IndiceElencoIdCarico.add(cursor.getString(cursor.getColumnIndex("IdCarico")));
            IndiceElencoIdAzienza.add(cursor.getString(cursor.getColumnIndex("idAzienda")));
            cursor.moveToNext();
            countElenco++;
        }
        ListViewAdapters adapter=new ListViewAdapters(this, list);
        adapter.cosa = "PrimoAccesso";
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        txtTotAccessibili.setText(Integer.toString(contAccessibili));
        txtTotSpeciali.setText(Integer.toString(contSpeciali));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                VerificaGPS();
                if (statusOfGPS) {
                    Intent intent = new Intent(Act_AttivitaSpeciali.this, Act_RilevaDati.class);
                    String IDRicerca = IndiceElencoCodiceUtente.get(position);
                    String IDCarico = IndiceElencoIdCarico.get(position);
                    String IDAzienda = IndiceElencoIdAzienza.get(position);
                    intent.putExtra("CodiceUtente", IDRicerca);
                    intent.putExtra("IDCarico", IDCarico.trim());
                    intent.putExtra("IDLett", IDLett.toString());
                    intent.putExtra("IDAzienda", IDAzienda.trim());
                    //DataOraCerta.setTimeInMillis((DataOraCerta.getTimeInMillis() + SystemClock.currentThreadTimeMillis()));
                    startActivity(intent);
                } else {

                    Toast.makeText(getApplicationContext(), "Attenzione per poter lavorare devi attivare il GPS", Toast.LENGTH_LONG).show();
                }

            }

        });

        cursor.close();
        System.gc();
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }

}
