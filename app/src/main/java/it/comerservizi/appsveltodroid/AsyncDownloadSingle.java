package it.comerservizi.appsveltodroid;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;

/**
 * Created by guido.palumbo on 21/01/2016.
 */
public class AsyncDownloadSingle extends AsyncTask<Void, String, Void> {

    String GlobalConfig;
    private String TAG = "AsyncDownloadSingle";
    ProgressDialog dialog;
    private ArrayList ListTBStatistiche;
    private DatabaseHelper dbHelper;
    private SQLiteDatabase db;
    public String service;
    public String operatore;
    public String PeriodoDal, PeriodoAl;
    public Act_ElencoAttivita MyElencoAttivita = null;
    public Act_Statistiche MyStatistiche = null;
    Boolean ControlloStatistiche = false;
    Boolean DatiStatistici = false;
    private Integer LastProgresso;
    private String servizio = "";
    private String response = "";
    private String sQuery, sWhere;
    public String SSID;
    private GPClass GPC;
    private Integer TotaleLette = 0;
    private String MessErrore;
    @Override
    protected void onPreExecute()
    {
        if(service=="ReturnStatistiche2") {
            GPC = new GPClass();
            Log.i(TAG, "entrato in OnPreExecute");
            dbHelper = new DatabaseHelper(MyStatistiche);
            db = dbHelper.getWritableDatabase();
            dialog = new ProgressDialog(MyStatistiche);
            dialog.setCancelable(false);
            db.execSQL("DELETE FROM statistiche");

            Cursor c = db.rawQuery("SELECT IdConfig FROM AndroConfig WHERE Type = 'sGlobalConfiguration'", null);

            if (c != null)
                c.moveToFirst();

            GlobalConfig = c.getString(c.getColumnIndex("IdConfig"));

            dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            dialog.setProgress(0);
            dialog.setMax(1);
            dialog.setMessage("Connessione in corso...");
            dialog.show();
        }
    }

    @Override
    protected Void doInBackground(Void... params) {

        if(service=="ReturnStatistiche2") {
            ControlloStatistiche = getStatistiche();
            if (ControlloStatistiche && DatiStatistici) {
                publishProgress("change");
                LastProgresso = 0;
                for (Integer i = 0; i < this.ListTBStatistiche.size(); i++) {
                    publishProgress(Integer.toString(LastProgresso));
                    LastProgresso++;
                    dbHelper.SimpleQuery(db, ListTBStatistiche.get(i).toString());

                }
                ListTBStatistiche.clear();
            } else {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result)
    {

        if(service=="ReturnStatistiche2") {
            if (DatiStatistici) {
                Toast.makeText(MyStatistiche.getApplicationContext(), "Statistiche Scaricate", Toast.LENGTH_LONG).show();
                MyStatistiche.RiOrganizzaSpinnerData();
                MyStatistiche.RiOrganizzaSpinnerIdCarico();
                MyStatistiche.PopolaStatistiche("0","0");
            }
            else
                Toast.makeText(MyStatistiche.getApplicationContext(), "Non vi sono dati da inizio mese ad oggi", Toast.LENGTH_SHORT).show();


            if (dialog.isShowing()) {
                Log.i(TAG, "onPostExecute - Dismissis DIALOG");
                dialog.dismiss();
            }

            Log.i(TAG, "onPostExecute - Finita");
        }

    }

    protected void onProgressUpdate(String... progress) {

        if(progress[0]=="change")
        {
            Log.i(TAG, "onProgressUpdate Change");
            dialog.setMessage("Download statistiche in corso..");
            dialog.setProgress(0);
            dialog.setMax(ListTBStatistiche.size());
            dialog.show();
        }
        else if(progress[0]=="uno")
            dialog.setProgress(1);
        else if(progress[0]=="due")
            dialog.setProgress(2);
        else if(progress[0]=="tre")
            dialog.setProgress(3);
        else if(progress[0]=="quattro")
            dialog.setProgress(4);
        else
            dialog.setProgress(Integer.parseInt(progress[0]));

    }
    private String PulisciStringa(String val)
    {
        String res;
        if(val==null)
            return val;
        else
        {
            res = val.replace("'","''");
            return res;
        }
    }


    private boolean getStatistiche() {

        this.servizio = service;
        response = servizio + "Result";
        String SOAP_ACTION = "http://tempuri.org/" + servizio;
        String METHOD_NAME = servizio;
        String NAMESPACE = "http://tempuri.org/";
        String URL;
        if(SSID.contains("cs1"))
            URL = "http://cs-storage/wsTrmLetture/wsTrmLetture.asmx?wsdl";
        else
            URL = "http://web.comerservizi.net:8083/wsTrmLetture/wsTrmLetture.asmx?wsdl";
        SoapObject client = null;                        // Its the client petition to the web service
        SoapObject responseBody = null;                    // Contains XML content of dataset
        HttpTransportSE transport = new HttpTransportSE(URL,60000);
        SoapSerializationEnvelope sse = null;

        sse = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        sse.dotNet = true; // if WebService written .Net is result=true
        sse.implicitTypes = false;

        try {
            Log.i("Statistiche","Entrato nel try");
            client = new SoapObject(NAMESPACE, METHOD_NAME);
            client.addProperty("sGlobalConfiguration", GlobalConfig);
            client.addProperty("idLetturista", operatore);
            client.addProperty("PeriodoDal", PeriodoAl);
            sse.setOutputSoapObject(client);
            sse.bodyOut = client;
            transport.call(SOAP_ACTION, sse);

            // This step: get file XML
            responseBody = (SoapObject) sse.getResponse();
            // remove information XML,only retrieved results that returned
            responseBody = (SoapObject) responseBody.getProperty(1);
            // get information XMl of tables that is returned
            SoapObject table = (SoapObject) responseBody.getProperty("Spool");
            //Get information each row in table,0 is first row
            PropertyInfo pi = new PropertyInfo();
            String query = "";
            SoapObject Spool;
            query = "INSERT INTO statistiche VALUES (";
            Log.i("Statistiche",Integer.toString(table.getPropertyCount()));
            if(table.getPropertyCount()<1)
            {
                DatiStatistici = false;

                ListTBStatistiche= new ArrayList<>(1);
                ListTBStatistiche.add("Nessuna statistica");
            }
            else
            {
                ListTBStatistiche= new ArrayList<>(table.getPropertyCount());
                for (Integer i = 0; i < table.getPropertyCount(); i++) {
                    Spool = null;
                    Spool = (SoapObject) table.getProperty(i);
                    query = query + "'" + PulisciStringa(Spool.getPrimitivePropertySafely("IdCarico").toString()) + "', ";
                    query = query + "'" +  PulisciStringa(Spool.getPrimitivePropertySafely("IdAzienda").toString()) + "', ";
                    query = query + "'" +  PulisciStringa(Spool.getPrimitivePropertySafely("LNota1").toString()) + "', ";
                    query = query + "'" +  PulisciStringa(Spool.getPrimitivePropertySafely("CodiceDitta").toString()) + "', ";
                    query = query + "'" +  PulisciStringa(Spool.getPrimitivePropertySafely("DescrSupp").toString()) + "', ";
                    query = query + "'" +  PulisciStringa(Spool.getPrimitivePropertySafely("FatturataLett").toString()) + "', ";
                    query = query + "'" +  PulisciStringa(Spool.getPrimitivePropertySafely("Totale").toString()) + "', ";
                    query = query + "'" +  PulisciStringa(Spool.getPrimitivePropertySafely("TotLetture").toString()) + "', ";
                    query = query + "'" +  PulisciStringa(Spool.getPrimitivePropertySafely("Periodicita").toString()) + "', ";
                    query = query + "'" +  PulisciStringa(Spool.getPrimitivePropertySafely("Stato").toString()) + "', ";
                    query = query + "'" +  PulisciStringa(Spool.getPrimitivePropertySafely("LAutolettura").toString()) + "', ";
                    query = query + "'" +  PulisciStringa(Spool.getPrimitivePropertySafely("DtLetturaTrm").toString()) + "', ";
                    query = query + "'" +  PulisciStringa(Spool.getPrimitivePropertySafely("LastSync").toString()) + "', ";
                    query = query + "'" +  PulisciStringa(Spool.getPrimitivePropertySafely("NotaAccess").toString()) + "') ";

                    query = query.replace("'null'", "null");
                    ListTBStatistiche.add(query);
                    query = "INSERT INTO statistiche VALUES (";
                }
            }

            Log.i(TAG, "Finito getStatistiche - POSITIVO - ListTBStatistiche.Size=" + ListTBStatistiche.size());
            DatiStatistici = true;
            return true;

        } catch (Exception e) {
            if(e.toString().contains("illegal property: Spool"))
            {
                DatiStatistici = false;
                Log.i(TAG, "Nessuna statistica trovata - " + e.toString());
                return true;
            }
            else
            {
                DatiStatistici = false;
                MessErrore = "AsyncWS_ErroreWS: " + e.getMessage() + " - " + e.getCause() + " - " + e.toString();
                Log.i(TAG, "Finito getStatistiche - NEGATIVO");
                return false;
            }
        }


    }
}