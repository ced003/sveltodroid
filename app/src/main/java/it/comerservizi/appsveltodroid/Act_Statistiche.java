package it.comerservizi.appsveltodroid;

import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Act_Statistiche extends AppCompatActivity {

    private String GiornoDa, GiornoAl, Mese, Anno;
    private String IDLett;
    private String periodo;
    private AsyncDownloadSingle AsDownload;
    public TextView txtDatiStatistici;
    public TextView lblLastSync,lblTotLetture, lblPercLettureValide, lblTotLettureValide, lblPercLettureFatte, lblTotLettureFatte,lblTotAutoLetture,lblPercAutoLetture, lblPercAccessibili, lblTotAccessibili;
    public ProgressBar pBarLettureFatte, pBarLettureValide, pBarAutoletture, pBarAccessibili;
    public LinearLayout LayoutPercentuali;
    public ImageView STimgWEB;
    private GPClass GPC;
    private String sQuery, sWhere;
    private String idCaricoRicerca, DataLetturaRicerca;
    Spinner spnIdCarico;
    Spinner spnDataLettura;
    private String GiornoAccesso;
    private List<String> spinnerData;
    private List<String> spinnerCarico;
    private ArrayList<String> ValoriSpinnerCarico;
    private ArrayList<String> ValoriSpinnerData;
    public
    DatabaseHelper dbHelper;
    SQLiteDatabase db;
    ConnectionDetector cd ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__statistiche);
        STimgWEB = (ImageView) findViewById(R.id.STimgWEB);
        txtDatiStatistici = (TextView) findViewById(R.id.txtDatiStatistici);
        lblLastSync = (TextView) findViewById(R.id.lblLastSync);
        lblTotLetture = (TextView) findViewById(R.id.lblTotLetture);
        lblPercLettureValide = (TextView) findViewById(R.id.lblPercLettureValide);
        lblTotLettureValide = (TextView) findViewById(R.id.lblTotLettureValide);
        lblPercLettureFatte = (TextView) findViewById(R.id.lblPercLettureFatte);
        lblTotLettureFatte = (TextView) findViewById(R.id.lblTotLettureFatte);
        lblPercAutoLetture = (TextView) findViewById(R.id.lblPercAutoLetture);
        lblTotAutoLetture = (TextView) findViewById(R.id.lblTotAutoLetture);
        lblPercAccessibili = (TextView) findViewById(R.id.lblPercAccessibili);
        lblTotAccessibili = (TextView) findViewById(R.id.lblTotAccessibili);
        pBarLettureFatte = (ProgressBar) findViewById(R.id.pBarLettureFatte);
        pBarLettureValide = (ProgressBar) findViewById(R.id.pBarLettureValide);
        pBarAutoletture = (ProgressBar) findViewById(R.id.pBarAutoLetture);
        pBarAccessibili = (ProgressBar) findViewById(R.id.pBarAccessibili);
        LayoutPercentuali = (LinearLayout) findViewById(R.id.LayoutPercentuali);
        spnIdCarico = (Spinner) findViewById(R.id.spnIdCarico);
        spnDataLettura = (Spinner) findViewById(R.id.spnDataLetture);
        cd = new ConnectionDetector(getApplicationContext(),STimgWEB);
        cd.setStatisticheHandler(this);
        IntentFilter fltr_connreceived = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(cd, fltr_connreceived);
        dbHelper = new DatabaseHelper(Act_Statistiche.this);
        db = dbHelper.getWritableDatabase();
        GPC = new GPClass();
        idCaricoRicerca = "0";
        DataLetturaRicerca = "0";


        Intent I_ActAccesso = getIntent();
        IDLett = I_ActAccesso.getExtras().getString("IDLett");
        periodo = I_ActAccesso.getExtras().getString("periodo");
        GiornoAccesso = I_ActAccesso.getExtras().getString("GiornoAccesso");

        GiornoDa = "01";

        Calendar calendar = Calendar.getInstance();
        Anno = Integer.toString(calendar.get(Calendar.YEAR));
        GiornoAl = Integer.toString(calendar.get(Calendar.DAY_OF_MONTH));
        Mese = Integer.toString(calendar.get(Calendar.MONTH) + 1);

        Cursor DataSync;
        DataSync = dbHelper.query(db, "SELECT LastSync FROM statistiche");
        if(DataSync.getCount()>0) {
            DataSync.moveToFirst();
            lblLastSync.setText("Ultima sincronizzazione: " + GPC.ConvertiDataeOra(DataSync.getString(DataSync.getColumnIndex("LastSync"))));
        }
        else
            lblLastSync.setText("Statistiche mai scaricate...");

        OrganizzaSpinnerIdCarico();
        OrganizzaSpinnerData();





        PopolaStatistiche("0", "0");


    }


    @Override
    protected void onStop()
    {
        super.onStop();
        try{

            unregisterReceiver(cd);
            Log.i("Act_Statistiche", "Unregistrato Connection Detector");
        }
        catch(Exception e)
        {
            Log.i("Act_Statistiche", "Impossibile unregistrare Connection Detector");
        }
    }
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        try{

            unregisterReceiver(cd);
            Log.i("Act_Statistiche", "Unregistrato Connection Detector");
        }
        catch(Exception e)
        {
            Log.i("Act_Statistiche", "Impossibile unregistrare Connection Detector");
        }

    }
    @Override
    protected void onPause()
    {
        super.onPause();
    }

    @Override
    protected void onResume(){
        super.onResume();

        if(GPC.ControlloGiornoAccesso(getApplicationContext(), this, GiornoAccesso)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                this.finishAffinity();
            }
        }
    }

    public void SincronizzaStatistiche(View view)
    {

        AsDownload = new AsyncDownloadSingle();
        AsDownload.operatore = IDLett;
        AsDownload.SSID = cd.WifiSSID();
        AsDownload.MyStatistiche = Act_Statistiche.this;
        AsDownload.PeriodoAl = GiornoAl+"/"+Mese+"/"+Anno;

        AsDownload.service ="ReturnStatistiche2";
        AsDownload.execute();
    }


    public void PopolaStatistiche(String wCarico, String wData)
    {
        Cursor cursStat;
        sWhere = "";
        String testo;
        if(!wCarico.equals("0"))
            sWhere = sWhere + " IdCarico = '"+ wCarico + "' ";

        if(!wData.equals("0"))
        {
            if(sWhere.contains("IdCarico"))
            {
                sWhere = sWhere + "AND strftime('%d/%m/%Y', date(dtLetturaTrm, '+1 day'))='" + wData + "'";
            }
            else
            {
                sWhere = sWhere + " strftime('%d/%m/%Y', date(dtLetturaTrm, '+1 day'))='" + wData + "'";
            }
        }
        else
        {
            if(!sWhere.contains("IdCarico"))
                sWhere = " 1 ";
        }

        sQuery = "SELECT SUM(CASE WHEN Stato = 'L' THEN Totale  ELSE 0 END) As Lette, SUM(CASE WHEN FatturataLett = 'S' AND LAutolettura='N' THEN Totale  ELSE 0 END) As Percent,SUM(CASE WHEN LAutolettura = 'Y' THEN Totale  ELSE 0 END) As AutoLett, TotLetture, LastSync FROM statistiche WHERE "+ sWhere + " GROUP BY TotLetture" ;
        cursStat = dbHelper.query(db, sQuery);
        sQuery = "";
        sQuery = "SELECT LNota1, DescrSupp, SUM(Totale) as Tot FROM statistiche WHERE Stato = 'L' AND "+ sWhere + " GROUP BY LNota1, DescrSupp ORDER BY SUM(Totale) Desc" ;
        Cursor cursorNote = dbHelper.query(db,sQuery);
        sQuery = "";
        try {
            //Query Percentuali totali
            cursStat.moveToFirst();
            if(!wCarico.equals("0"))
            {
                Cursor CursTotAccessibili = dbHelper.query(db,"SELECT SUM(Totale) As TotAccess FROM statistiche WHERE NotaAccess='Y' AND IdCarico = '"+ wCarico + "'");
                Cursor CursLettAccessibili = dbHelper.query(db,"SELECT SUM(Totale) As LettAccess FROM statistiche WHERE NotaAccess='Y' AND Stato = 'L' AND IdCarico = '"+ wCarico + "'");
                CursTotAccessibili.moveToFirst();
                CursLettAccessibili.moveToFirst();
                Cursor TotCarico;
                sQuery = "";
                sQuery = "SELECT SUM(Totale) as TotLetture FROM statistiche WHERE IdCarico = '"+ wCarico + "' ";
                TotCarico = dbHelper.query(db, sQuery);
                TotCarico.moveToFirst();



                String nLettAccess;
                if(CursLettAccessibili.isNull(CursLettAccessibili.getColumnIndex("LettAccess")))
                    nLettAccess = "0";
                else
                    nLettAccess = CursLettAccessibili.getString(CursLettAccessibili.getColumnIndex("LettAccess"));

                String nTotAccess;
                if(CursTotAccessibili.isNull(CursTotAccessibili.getColumnIndex("TotAccess")))
                    nTotAccess = "0";
                else
                    nTotAccess = CursTotAccessibili.getString(CursTotAccessibili.getColumnIndex("TotAccess"));


                lblTotLetture.setText("Totali Letture da fare: " + TotCarico.getString(TotCarico.getColumnIndex("TotLetture")));
                lblTotAccessibili.setText(nLettAccess + "/"+nTotAccess);
                if(!CursTotAccessibili.isNull(CursTotAccessibili.getColumnIndex("TotAccess")) && !CursLettAccessibili.isNull(CursLettAccessibili.getColumnIndex("LettAccess")) )
                    lblPercAccessibili.setText(GPC.RilevaPercentuale(Integer.parseInt(CursTotAccessibili.getString(CursTotAccessibili.getColumnIndex("TotAccess"))), CursLettAccessibili.getInt(CursLettAccessibili.getColumnIndex("LettAccess"))) + "%");
                else
                    lblPercAccessibili.setText("0%");
                if(!TotCarico.isNull(TotCarico.getColumnIndex("TotLetture")) && !cursStat.isNull(cursStat.getColumnIndex("Lette")))
                    lblPercLettureFatte.setText(GPC.RilevaPercentuale(Integer.parseInt(TotCarico.getString(TotCarico.getColumnIndex("TotLetture"))), cursStat.getInt(cursStat.getColumnIndex("Lette"))) + "%");
                else
                    lblPercLettureFatte.setText("0%");

                if(!TotCarico.isNull(TotCarico.getColumnIndex("TotLetture")) && !cursStat.isNull(cursStat.getColumnIndex("Percent")))
                    lblPercLettureValide.setText(GPC.RilevaPercentuale(Integer.parseInt(TotCarico.getString(TotCarico.getColumnIndex("TotLetture"))), cursStat.getInt(cursStat.getColumnIndex("Percent"))) + "%");
                else
                    lblPercLettureValide.setText("0%");

                if(!TotCarico.isNull(TotCarico.getColumnIndex("TotLetture")) && !cursStat.isNull(cursStat.getColumnIndex("AutoLett")))
                    lblPercAutoLetture.setText(GPC.RilevaPercentuale(Integer.parseInt(TotCarico.getString(TotCarico.getColumnIndex("TotLetture"))), cursStat.getInt(cursStat.getColumnIndex("AutoLett"))) + "%");
                else
                    lblPercAutoLetture.setText("0%");


                if(CursLettAccessibili.isNull(CursLettAccessibili.getColumnIndex("LettAccess")))
                    pBarAccessibili.setProgress(0);
                else
                    pBarAccessibili.setProgress(CursLettAccessibili.getInt(CursLettAccessibili.getColumnIndex("LettAccess")));

                if(TotCarico.isNull(TotCarico.getColumnIndex("TotLetture")))
                    pBarLettureFatte.setProgress(0);
                else
                    pBarLettureFatte.setMax(Integer.parseInt(TotCarico.getString(TotCarico.getColumnIndex("TotLetture"))));

                if(TotCarico.isNull(TotCarico.getColumnIndex("TotLetture")))
                    pBarLettureValide.setProgress(0);
                else
                    pBarLettureValide.setMax(Integer.parseInt(TotCarico.getString(TotCarico.getColumnIndex("TotLetture"))));

                if(CursTotAccessibili.isNull(CursTotAccessibili.getColumnIndex("TotAccess")))
                    pBarAccessibili.setProgress(0);
                else
                    pBarAccessibili.setMax(Integer.parseInt(CursTotAccessibili.getString(CursTotAccessibili.getColumnIndex("TotAccess"))));

            }
            else
            {
                Cursor CursTotAccessibili = dbHelper.query(db,"SELECT SUM(Totale) As TotAccess FROM statistiche WHERE NotaAccess='Y' ");
                Cursor CursLettAccessibili = dbHelper.query(db,"SELECT SUM(Totale) As LettAccess FROM statistiche WHERE NotaAccess='Y' AND Stato = 'L'");
                CursLettAccessibili.moveToFirst();
                CursTotAccessibili.moveToFirst();


                String nLettAccess;
                if(CursLettAccessibili.isNull(CursLettAccessibili.getColumnIndex("LettAccess")))
                    nLettAccess = "0";
                else
                    nLettAccess = CursLettAccessibili.getString(CursLettAccessibili.getColumnIndex("LettAccess"));

                String nTotAccess;
                if(CursTotAccessibili.isNull(CursTotAccessibili.getColumnIndex("TotAccess")))
                    nTotAccess = "0";
                else
                    nTotAccess = CursTotAccessibili.getString(CursTotAccessibili.getColumnIndex("TotAccess"));

                lblTotLetture.setText("Totali Letture da fare: " + cursStat.getString(cursStat.getColumnIndex("TotLetture")));
                lblTotAccessibili.setText(nLettAccess + "/"+nTotAccess);
                if(!CursTotAccessibili.isNull(CursTotAccessibili.getColumnIndex("TotAccess")) && !cursStat.isNull(cursStat.getColumnIndex("Lette")))
                    lblPercAccessibili.setText(GPC.RilevaPercentuale(Integer.parseInt(CursTotAccessibili.getString(CursTotAccessibili.getColumnIndex("TotAccess"))), CursLettAccessibili.getInt(CursLettAccessibili.getColumnIndex("LettAccess"))) + "%");
                else
                    lblPercAccessibili.setText("0%");

                if(!cursStat.isNull(cursStat.getColumnIndex("TotLetture")) && !cursStat.isNull(cursStat.getColumnIndex("Lette")))
                    lblPercLettureFatte.setText(GPC.RilevaPercentuale(Integer.parseInt(cursStat.getString(cursStat.getColumnIndex("TotLetture"))), cursStat.getInt(cursStat.getColumnIndex("Lette"))) + "%");
                else
                    lblPercLettureFatte.setText("0%");
                if(!cursStat.isNull(cursStat.getColumnIndex("TotLetture")) && !cursStat.isNull(cursStat.getColumnIndex("Percent")))
                    lblPercLettureValide.setText(GPC.RilevaPercentuale(Integer.parseInt(cursStat.getString(cursStat.getColumnIndex("TotLetture"))), cursStat.getInt(cursStat.getColumnIndex("Percent"))) + "%");
                else
                    lblPercLettureValide.setText("0%");

                if(!cursStat.isNull(cursStat.getColumnIndex("TotLetture")) && !cursStat.isNull(cursStat.getColumnIndex("AutoLett")))
                    lblPercAutoLetture.setText(GPC.RilevaPercentuale(Integer.parseInt(cursStat.getString(cursStat.getColumnIndex("TotLetture"))), cursStat.getInt(cursStat.getColumnIndex("AutoLett"))) + "%");
                else
                    lblPercAutoLetture.setText("0%");


                if(CursLettAccessibili.isNull(CursLettAccessibili.getColumnIndex("LettAccess")))
                    pBarAccessibili.setProgress(0);
                else
                    pBarAccessibili.setProgress(CursLettAccessibili.getInt(CursLettAccessibili.getColumnIndex("LettAccess")));

                if(cursStat.isNull(cursStat.getColumnIndex("TotLetture")))
                    pBarLettureFatte.setProgress(0);
                else
                    pBarLettureFatte.setMax(Integer.parseInt(cursStat.getString(cursStat.getColumnIndex("TotLetture"))));

                if(cursStat.isNull(cursStat.getColumnIndex("TotLetture")))
                    pBarLettureValide.setProgress(0);
                else
                    pBarLettureValide.setMax(Integer.parseInt(cursStat.getString(cursStat.getColumnIndex("TotLetture"))));

                if(CursTotAccessibili.isNull(CursTotAccessibili.getColumnIndex("TotAccess")))
                    pBarAccessibili.setProgress(0);
                else
                    pBarAccessibili.setMax(Integer.parseInt(CursTotAccessibili.getString(CursTotAccessibili.getColumnIndex("TotAccess"))));
            }
            lblTotLettureValide.setText(cursStat.getString(cursStat.getColumnIndex("Percent")));
            lblTotLettureFatte.setText(cursStat.getString(cursStat.getColumnIndex("Lette")));
            lblTotAutoLetture.setText(cursStat.getString(cursStat.getColumnIndex("AutoLett")));
            pBarLettureFatte.setProgress(cursStat.getInt(cursStat.getColumnIndex("Lette")));
            pBarLettureValide.setProgress(cursStat.getInt(cursStat.getColumnIndex("Percent")));
            pBarAutoletture.setProgress(cursStat.getInt(cursStat.getColumnIndex("AutoLett")));
            LayoutPercentuali.setVisibility(LinearLayout.VISIBLE);
            cursStat.close();
            //Fine Query Percentuali Totali

            //Inizio Riepiloghi Tipo Letture
            String testoDescrizione = "";
            cursorNote.moveToFirst();
            while (cursorNote.isAfterLast() == false) {
                testoDescrizione = testoDescrizione + cursorNote.getString(cursorNote.getColumnIndex("LNota1")) + " - " +cursorNote.getString(cursorNote.getColumnIndex("DescrSupp")) + " : " + cursorNote.getString(cursorNote.getColumnIndex("Tot"));
                testoDescrizione = testoDescrizione + System.getProperty("line.separator") + System.getProperty("line.separator");
                cursorNote.moveToNext();
            }

            cursorNote.close();
            txtDatiStatistici.setText(testoDescrizione);
            //FineRiepiloghiTipoLetture


        }catch(Exception ex)
        {
            Toast.makeText(getApplicationContext(), "Nessun risultato", Toast.LENGTH_LONG).show();
            lblTotLetture.setText("");
            pBarLettureFatte.setEnabled(false);
            pBarLettureValide.setEnabled(false);
            LayoutPercentuali.setVisibility(LinearLayout.INVISIBLE);
            txtDatiStatistici.setText("");
            cursStat.close();
            cursorNote.close();
        }
    }

    public void OrganizzaSpinnerIdCarico()
    {
        Cursor cursorSpnCarico;
        if(DataLetturaRicerca.equals("0"))
            cursorSpnCarico = dbHelper.query(db,"SELECT idCarico, Periodicita FROM Statistiche GROUP BY idCarico, Periodicita");
        else
            cursorSpnCarico = dbHelper.query(db,"SELECT idCarico, Periodicita FROM Statistiche WHERE strftime('%d/%m/%Y', date(dtLetturaTrm, '+1 day'))='" + DataLetturaRicerca + "' GROUP BY idCarico, Periodicita");

        spinnerCarico = new ArrayList<String>();
        ValoriSpinnerCarico = new ArrayList<String>();
        ValoriSpinnerCarico.add("TIPO CARICO");
        spinnerCarico.add("");
        //Inizio Query Spinner Carico
        cursorSpnCarico.moveToFirst();
        while (cursorSpnCarico.isAfterLast() == false) {
            ValoriSpinnerCarico.add(cursorSpnCarico.getString(cursorSpnCarico.getColumnIndex("IdCarico")) +" - " + cursorSpnCarico.getString(cursorSpnCarico.getColumnIndex("Periodicita")));
            spinnerCarico.add(cursorSpnCarico.getString(cursorSpnCarico.getColumnIndex("IdCarico")));
            cursorSpnCarico.moveToNext();
        }

        cursorSpnCarico.close();
        ArrayAdapter<String> SpinnAdapCarico = new ArrayAdapter<String>(this, R.layout.spinner_item2, ValoriSpinnerCarico);
        spnIdCarico.setAdapter(SpinnAdapCarico);
        //Fine query Spinner Carico

        spnIdCarico.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    idCaricoRicerca = spinnerCarico.get(position);
                    PopolaStatistiche(idCaricoRicerca, DataLetturaRicerca);
                    RiOrganizzaSpinnerData();
                } else {
                    idCaricoRicerca = "0";
                    PopolaStatistiche(idCaricoRicerca, DataLetturaRicerca);
                    RiOrganizzaSpinnerData();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                idCaricoRicerca = "0";
                PopolaStatistiche(idCaricoRicerca, DataLetturaRicerca);
                spnIdCarico.setSelection(0);
            }
        });


    }
    public void RiOrganizzaSpinnerIdCarico()
    {
        Cursor cursorSpnCarico;
        if(DataLetturaRicerca.equals("0"))
            cursorSpnCarico = dbHelper.query(db,"SELECT idCarico, Periodicita FROM Statistiche GROUP BY idCarico, Periodicita");
        else
            cursorSpnCarico = dbHelper.query(db,"SELECT idCarico, Periodicita FROM Statistiche WHERE strftime('%d/%m/%Y', date(dtLetturaTrm, '+1 day'))='" + DataLetturaRicerca + "' GROUP BY idCarico, Periodicita");

        ValoriSpinnerCarico.clear();
        ValoriSpinnerCarico.add("TIPO CARICO");
        spinnerCarico.clear();
        spinnerCarico.add("");
        //Inizio Query Spinner Carico
        cursorSpnCarico.moveToFirst();
        while (cursorSpnCarico.isAfterLast() == false) {
            ValoriSpinnerCarico.add(cursorSpnCarico.getString(cursorSpnCarico.getColumnIndex("IdCarico")) +" - " + cursorSpnCarico.getString(cursorSpnCarico.getColumnIndex("Periodicita")));
            spinnerCarico.add(cursorSpnCarico.getString(cursorSpnCarico.getColumnIndex("IdCarico")));
            cursorSpnCarico.moveToNext();
        }

        cursorSpnCarico.close();
        spnIdCarico.setAdapter(null);
        ArrayAdapter<String> SpinnAdapCarico = new ArrayAdapter<String>(this, R.layout.spinner_item2, ValoriSpinnerCarico);
        spnIdCarico.setAdapter(SpinnAdapCarico);
        //Fine query Spinner Carico

    }

    public void OrganizzaSpinnerData()
    {
        Cursor cursorSpnData;
        spinnerData = new ArrayList<String>();
        ValoriSpinnerData = new ArrayList<String>();
        ValoriSpinnerData.add("GIORNO LETTURA");
        spinnerData.add("");

        //Inizio Query Spinner Data
        if(idCaricoRicerca.equals("0"))
            cursorSpnData = dbHelper.query(db,"SELECT strftime('%d/%m/%Y', date(dtLetturaTrm, '+1 day')) as myDate FROM Statistiche WHERE dtLetturaTrm IS NOT NULL GROUP BY dtLetturaTrm");
        else
            cursorSpnData = dbHelper.query(db,"SELECT strftime('%d/%m/%Y', date(dtLetturaTrm, '+1 day')) as myDate FROM Statistiche WHERE dtLetturaTrm IS NOT NULL AND IdCarico = '"+idCaricoRicerca+"' GROUP BY dtLetturaTrm");

        cursorSpnData.moveToFirst();
        while (cursorSpnData.isAfterLast() == false) {
            ValoriSpinnerData.add(cursorSpnData.getString(cursorSpnData.getColumnIndex("myDate")));
            spinnerData.add(cursorSpnData.getString(cursorSpnData.getColumnIndex("myDate")));
            cursorSpnData.moveToNext();
        }

        cursorSpnData.close();
        ArrayAdapter<String> SpinnAdapData = new ArrayAdapter<String>(this, R.layout.spinner_item2, ValoriSpinnerData);
        spnDataLettura.setAdapter(SpinnAdapData);
        //Fine query Spinner Data


        spnDataLettura.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    DataLetturaRicerca = spinnerData.get(position);
                    PopolaStatistiche(idCaricoRicerca, DataLetturaRicerca);
                    //RiOrganizzaSpinnerIdCarico();
                } else {
                    DataLetturaRicerca = "0";
                    PopolaStatistiche(idCaricoRicerca, DataLetturaRicerca);
                    //RiOrganizzaSpinnerIdCarico();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                DataLetturaRicerca = "0";
                PopolaStatistiche(idCaricoRicerca, DataLetturaRicerca);
                spnDataLettura.setSelection(0);
            }
        });
    }
    public void RiOrganizzaSpinnerData()
    {
        Cursor cursorSpnData;
        spinnerData.clear();
        ValoriSpinnerData.clear();
        ValoriSpinnerData.add("GIORNO LETTURA");
        spinnerData.add("");

        //Inizio Query Spinner Data
        if(idCaricoRicerca.equals("0"))
            cursorSpnData = dbHelper.query(db,"SELECT strftime('%d/%m/%Y', date(dtLetturaTrm, '+1 day')) as myDate FROM Statistiche WHERE dtLetturaTrm IS NOT NULL GROUP BY dtLetturaTrm");
        else
            cursorSpnData = dbHelper.query(db,"SELECT strftime('%d/%m/%Y', date(dtLetturaTrm, '+1 day')) as myDate FROM Statistiche WHERE dtLetturaTrm IS NOT NULL AND IdCarico = '"+idCaricoRicerca+"' GROUP BY dtLetturaTrm");

        cursorSpnData.moveToFirst();
        while (cursorSpnData.isAfterLast() == false) {
            ValoriSpinnerData.add(cursorSpnData.getString(cursorSpnData.getColumnIndex("myDate")));
            spinnerData.add(cursorSpnData.getString(cursorSpnData.getColumnIndex("myDate")));
            cursorSpnData.moveToNext();
        }

        cursorSpnData.close();
        spnDataLettura.setAdapter(null);
        ArrayAdapter<String> SpinnAdapData = new ArrayAdapter<String>(this, R.layout.spinner_item2, ValoriSpinnerData);
        spnDataLettura.setAdapter(SpinnAdapData);
        //Fine query Spinner Data

    }
}


