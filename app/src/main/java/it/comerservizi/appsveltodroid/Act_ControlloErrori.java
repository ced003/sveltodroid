package it.comerservizi.appsveltodroid;

import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Act_ControlloErrori extends AppCompatActivity {

    private TextView txtVerificaOra;
    private TextView txtTimeZone;
    private TextView txtGPS;
    private TextView txtModAereo;
    private TextView txtConnessione;
    private TextView txtRotazione;

    private ImageView imgVerificaOra;
    private ImageView imgTimeZone;
    private ImageView imgGPS;
    private ImageView imgModAereo;
    private ImageView imgConnessione;
    private ImageView imgRotazione;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__controllo_errori);
        txtVerificaOra = (TextView) findViewById(R.id.txtVerificaOra);
        txtTimeZone = (TextView) findViewById(R.id.txtTimeZone);
        txtGPS = (TextView) findViewById(R.id.txtGPS);
        txtModAereo = (TextView) findViewById(R.id.txtModAereo);
        txtConnessione = (TextView) findViewById(R.id.txtConnessione);
        txtRotazione = (TextView) findViewById(R.id.txtRotazione);

        imgVerificaOra = (ImageView) findViewById(R.id.imgVerificaOra);
        imgTimeZone = (ImageView) findViewById(R.id.imgTimeZone);
        imgGPS = (ImageView) findViewById(R.id.imgGPS);
        imgModAereo = (ImageView) findViewById(R.id.imgModAereo);
        imgConnessione = (ImageView) findViewById(R.id.imgConnessione);
        imgRotazione = (ImageView) findViewById(R.id.imgRotazione);
    }


    @Override
    protected void onResume() {
        super.onResume();


        /* CONTROLLO DATA AUTOMATICA */
        try
        {
            ConnectivityManager manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
            {
                if (Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME) != 1) {
                    txtVerificaOra.setText("Disabilitato");
                    imgVerificaOra.setImageResource(R.drawable.xred001);
                } else {
                    txtVerificaOra.setText("Abilitato");
                    imgVerificaOra.setImageResource(R.drawable.checkgreen001);
                }
                if (Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME_ZONE) != 1) {
                    txtTimeZone.setText("Disabilitato");
                    imgTimeZone.setImageResource(R.drawable.xred001);
                } else {
                    txtTimeZone.setText("Abilitato");
                    imgTimeZone.setImageResource(R.drawable.checkgreen001);
                }
                if (Settings.System.getInt(getContentResolver(), Settings.System.AIRPLANE_MODE_ON) == 1) {
                    txtModAereo.setText("Abilitato");
                    imgModAereo.setImageResource(R.drawable.xred001);
                } else {
                    txtModAereo.setText("Disabilitato");
                    imgModAereo.setImageResource(R.drawable.checkgreen001);
                }
            }

            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                txtGPS.setText("Abilitato");
                imgGPS.setImageResource(R.drawable.checkgreen001);
            } else {
                txtGPS.setText("Disabilitato");
                imgGPS.setImageResource(R.drawable.xred001);
            }

            if (Settings.System.getInt(getContentResolver(), Settings.System.ACCELEROMETER_ROTATION) == 1) {
                txtRotazione.setText("Abilitato");
                imgRotazione.setImageResource(R.drawable.xred001);
            } else {
                txtRotazione.setText("Disabilitato");
                imgRotazione.setImageResource(R.drawable.checkgreen001);
            }

            ConnectivityManager cm = (ConnectivityManager) getSystemService(getApplicationContext().CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = cm.getActiveNetworkInfo();
            // if no network is available networkInfo will be null
            // otherwise check if we are connected
            if (networkInfo != null && networkInfo.isConnected()) {
                txtConnessione.setText("Abilitato");
                imgConnessione.setImageResource(R.drawable.checkgreen001);
            }
            else
            {
                txtConnessione.setText("Disabilitato");
                imgConnessione.setImageResource(R.drawable.xred001);
            }
        } catch (Settings.SettingNotFoundException e) {
            Toast.makeText(getApplicationContext(), "Impossibile aprire la schermata", Toast.LENGTH_LONG).show();
            finish();
        }
        /* CONTROLLO DATA AUTOMATICA */



    }

}
