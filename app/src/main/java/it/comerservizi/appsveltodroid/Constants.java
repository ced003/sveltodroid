package it.comerservizi.appsveltodroid;

/**
 * Created by Administrator on 18/11/2015.
 */

public class Constants {

    public static final String FIRST_COLUMN="First";
    public static final String SECOND_COLUMN="Second";
    public static final String THIRD_COLUMN="Third";
    public static final String FOURTH_COLUMN="Fourth";
    public static final String FIFTH_COLUMN="Fifth";
    public static final String SIXTH_COLUMN="Sixth";
    public static final String SEVENTH_COLUMN="Seventh";
}