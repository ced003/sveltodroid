package it.comerservizi.appsveltodroid;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.Toast;

public class Act_QCode extends AppCompatActivity {


    private GPClass GPC;
    private String GiornoAccesso;
    private ImageView ButtonQR;
    private String MatricolaSparata;
    private Switch swAttivitaConcluse;
    public ListView listView;
    String IDLett="";
    public Integer LastPosition = 0;
    private AsyncLoader ElementiAs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__qcode);

        GPC = new GPClass();
        MatricolaSparata = "";
        ButtonQR = (ImageView) findViewById(R.id.qCodeButton);
        swAttivitaConcluse = (Switch) findViewById(R.id.swAttivitaConcluse);
        swAttivitaConcluse.setChecked(false);
        Intent I_ActElencoAttivita = getIntent();
        IDLett = I_ActElencoAttivita.getExtras().getString("IDLett");
        GiornoAccesso = I_ActElencoAttivita.getExtras().getString("GiornoAccesso");
        listView=(ListView)findViewById(R.id.listView1);

        ButtonQR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("AAAAA", "Cliccato!");
                CercaMatricola();
            }
        });
    }

    public void CercaMatricola()
    {
        Log.e("Func", "Sono entrato!");
        if(GPC.isAppInstalled(getApplicationContext(),"com.google.zxing.client.android"))
        {
            Log.e("Func", "Sono installato!");
            Intent intent = new Intent("com.google.zxing.client.android.SCAN");
            //intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
            intent.putExtra("SCAN_FORMATS", "CODE_39,CODE_93,CODE_128,DATA_MATRIX,ITF,CODABAR,EAN_13,EAN_8,UPC_A,QR_CODE");
            Log.e("BBB", "Pronto a partire!");
            startActivityForResult(intent, 0);
        }
        else
            Toast.makeText(getApplicationContext(), "Devi installare la libreria del Barcode Scanner", Toast.LENGTH_LONG).show();
    }


    @Override
    protected void onResume()
    {
        super.onResume();

        GPC.LastSync(getApplicationContext(), this);
        if(GPC.ControlloGiornoAccesso(getApplicationContext(), this, GiornoAccesso))
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
            {
                this.finishAffinity();
            }
        }
        else
        {
            if(MatricolaSparata!="")
            {
                ElementiAs = new AsyncLoader(Act_QCode.this, "QCode");
                ElementiAs.IDLett = IDLett;
                ElementiAs.LastPosition = LastPosition;
                ElementiAs.ActQCode = this;
                ElementiAs.Search4How = MatricolaSparata;
                ElementiAs.Search4Concluse = swAttivitaConcluse.isChecked();
                ElementiAs.execute();

            }
        }
        //if(LastPosition>0)
        //    listView.setSelection(LastPosition);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        MatricolaSparata = "";
        if (requestCode == 0) {
            if (resultCode == RESULT_OK)
            {
                MatricolaSparata = intent.getStringExtra("SCAN_RESULT");
            } else if (resultCode == RESULT_CANCELED)
            {
                MatricolaSparata = "";
            }
        }
    }
}
