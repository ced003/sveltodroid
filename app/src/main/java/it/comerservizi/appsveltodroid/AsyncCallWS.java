package it.comerservizi.appsveltodroid;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.net.SocketTimeoutException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Pattern;

import static it.comerservizi.appsveltodroid.Constants.FIRST_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.SECOND_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.THIRD_COLUMN;

/**
 * Created by guido.palumbo on 16/11/2015.
 */
public class AsyncCallWS extends AsyncTask<Void, String, Void> {

    String TAG = "AsyncCallWS";
    SoapPrimitive resultString;
    String servizio = "";
    String response = "";
    DatabaseHelper dbHelper;
    SQLiteDatabase db;
    public String SSID;
    Context context;
    String GlobalConfig;
    public ProgressDialog dialogAvvio;
    public String _Versioning;
    public String _Link;
    public String _Errore;
    public Calendar _DataEora;
    public String _DatiSIM;
    public String _IDTerminale;
    public String _IDLetturista;
    public String _NomeLetturista;
    public String _NomeApp;
    public String _CodSIM;
    public String _OpSIM;
    public String _PhNum;
    public String _IMEI;
    public String _idConfing;
    public Boolean _CiSonoCom;
    public ArrayList<HashMap<String, String>> _list;
    public String _DescAttivita;
    private String CausaleErrore;
    private Boolean SegnaleErrore = false;
    public Act_Accesso MyAct_Accesso = null;
    public Act_Impostazioni MyAct_Impostazioni = null;

    public AsyncCallWS(Context context) {
        this.context = context;

    }
    @Override
    protected void onPreExecute() {
        CausaleErrore = "";
        _CiSonoCom = false;
        dbHelper = new DatabaseHelper(context);
        db = dbHelper.getWritableDatabase();
        Cursor c = db.rawQuery("SELECT IdConfig FROM AndroConfig WHERE Type = 'sGlobalConfiguration'", null);

        dialogAvvio = new ProgressDialog(context);
        dialogAvvio.setCancelable(false);

        dialogAvvio.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialogAvvio.setMessage("Attendere Sincronizzazione. In base alla qualità del segnale potrebbe volerci qualche secondo [1 di 8]");
        if (c != null)
            c.moveToFirst();

        dialogAvvio.show();
        GlobalConfig = c.getString(c.getColumnIndex("IdConfig"));
        Log.i(TAG, "onPreExecute");
        Crashlytics.log(null);
        Crashlytics.log("GlobalConfig: " + GlobalConfig + " - Operatore: " + _IDLetturista );
    }

    @Override
    protected Void doInBackground(Void... params) {
        Log.i(TAG, "doInBackground");
        int i = 1;
        if(VerificaVersioning())
        {
            i++;
            publishProgress(Integer.toString(i));
            if(ReturnGlobalConfig()) {
                db.execSQL("UPDATE AndroConfig SET IdConfig='"+_idConfing+"' WHERE Type = 'sGlobalConfiguration'");
                i++;
                publishProgress(Integer.toString(i));
                if (ReturnIMEI()) {
                    i++;
                    publishProgress(Integer.toString(i));
                    if (RecuperaDatiSIM())
                    {
                        i++;
                        publishProgress(Integer.toString(i));
                        if (ReturnLetturista())
                        {
                            i++;
                            publishProgress(Integer.toString(i));
                            if(ReturnDescAttivita())
                            {
                                i++;
                                publishProgress(Integer.toString(i));
                                if(ReturnComunicazioni())
                                {
                                    i++;
                                    publishProgress(Integer.toString(i));
                                    if (ReturnDataOra()) {
                                        SegnaleErrore = false;
                                    } else {
                                        SegnaleErrore = true;
                                        CausaleErrore = "Impossibile verificare data e ora server";
                                    }
                                }
                                else
                                {
                                    SegnaleErrore = false;
                                    CausaleErrore = "Impossibile scaricare comunicazioni";
                                }
                            }
                            else
                            {
                                SegnaleErrore = true;
                                CausaleErrore = "Impossibile trovare l'Attività";
                            }
                        }
                        else
                        {
                            SegnaleErrore = true;
                            CausaleErrore = "Impossibile trovare l'ID letturista";
                        }
                    }
                    else
                    {
                        SegnaleErrore = true;
                        CausaleErrore = "Impossibile registrare i dati della SIM";
                    }
                }
                else
                {
                    SegnaleErrore = true;
                    CausaleErrore = "Impossibile trovare l'ID Terminale";
                }
            }
            else
            {
                SegnaleErrore = true;
                CausaleErrore = "Impossibile trovare l'ID Attività";
            }
        }
        else
        {
            SegnaleErrore = true;
            CausaleErrore = "Impossibile verificare la versione dell'APP";
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {

        Log.i(TAG, "onPostExecute");

        if (dialogAvvio.isShowing()) {
            dialogAvvio.dismiss();
        }

        if(SegnaleErrore)
        {

            if (_Errore.equals("TIMEOUT"))
            {
                AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                dialog
                        .setTitle("Errore di Rete")
                        .setMessage("Impossibile contattare i server. Verifica la tua connessione o cerca una zona con un campo migliore.")
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog MsgErrore = dialog.create();
                MsgErrore.show();
            }
            else
            {
                AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                dialog
                        .setTitle("Errore di Rete")
                        .setMessage("Impossibile contattare i server. Errore: [" + CausaleErrore + "]")
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog MsgErrore = dialog.create();
                MsgErrore.show();

            }
        }
        else
        {
            MyAct_Accesso.VerificaAggiornamenti();
            MyAct_Accesso.StampaIMEI(true);
            MyAct_Accesso.StampaDataOra(true);
        }


        Log.i(TAG, "onPostExecute");
    }

    public String ReturnSimpleString(String service) {

        this.servizio = service;
        response = servizio+"Result";
        String SOAP_ACTION = "http://tempuri.org/" + servizio;
        String METHOD_NAME = servizio;
        String NAMESPACE = "http://tempuri.org/";
        String URL;
        if(SSID.contains("cs1"))
            URL = "http://cs-storage/wsTrmLetture/wsTrmLetture.asmx?wsdl";
        else
            URL = "http://web.comerservizi.net:8083/wsTrmLetture/wsTrmLetture.asmx?wsdl";

        try {
            SoapObject Request = new SoapObject(NAMESPACE, METHOD_NAME);
            //Request.addProperty(servizio, getCel);

            SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            soapEnvelope.dotNet = true;
            soapEnvelope.setOutputSoapObject(Request);

            HttpTransportSE transport = new HttpTransportSE(URL,180000);

            transport.call(SOAP_ACTION, soapEnvelope);
            String Controllo = resultString.toString();
            Controllo = Controllo.trim();
            String dato="";
            String idConfing="";
            if(Controllo.contains("|"))
            {
                String[] parts = Controllo.split(Pattern.quote("|"));
                dato = parts[0];
                idConfing = parts[1];
                db.execSQL("DELETE FROM AndroConfig WHERE Type = 'sGlobalConfiguration' ");
                db.execSQL("INSERT INTO AndroConfig (IdConfig, Type) VALUES ('" + idConfing + "','sGlobalConfiguration')");

            }
            else {
                dato = resultString.toString();
            }

            return dato;
        } catch (Exception ex) {
            return "Error WS: " + ex.getMessage() + " - Cause: "+ ex.getCause() + " - ex: " + ex.toString();
        }
    }
    private boolean ReturnComunicazioni() {

        this.servizio = "ReturnComunicazioniAndroid";
        response = servizio+"Result";
        String SOAP_ACTION = "http://tempuri.org/" + servizio;
        String METHOD_NAME = servizio;
        String NAMESPACE = "http://tempuri.org/";
        String URL;
        _list=new ArrayList<HashMap<String,String>>();
        if(SSID.contains("cs1"))
            URL = "http://cs-storage/wsTrmLetture/wsTrmLetture.asmx?wsdl";
        else
            URL = "http://web.comerservizi.net:8083/wsTrmLetture/wsTrmLetture.asmx?wsdl";
        try {
            String appo;
            SoapObject client = null;                        // Its the client petition to the web service
            SoapObject responseBody = null;                    // Contains XML content of dataset
            HttpTransportSE transport = new HttpTransportSE(URL,180000);
            SoapSerializationEnvelope sse = null;

            sse = new SoapSerializationEnvelope(SoapEnvelope.VER11);

            sse.dotNet = true; // if WebService written .Net is result=true
            sse.implicitTypes = false;
            client = new SoapObject(NAMESPACE, METHOD_NAME);
            client.addProperty("sGlobalConfiguration", GlobalConfig);
            sse.setOutputSoapObject(client);
            sse.bodyOut = client;
            transport.call(SOAP_ACTION, sse);

            // This step: get file XML
            responseBody = (SoapObject) sse.getResponse();
            // remove information XML,only retrieved results that returned
            responseBody = (SoapObject) responseBody.getProperty(1);
            // get information XMl of tables that is returned
            SoapObject table = (SoapObject) responseBody.getProperty("Spool");
            //Get information each row in table,0 is first row
            PropertyInfo pi = new PropertyInfo();
            String query = "";
            SoapObject Spool;

            for (Integer i = 0; i < table.getPropertyCount(); i++) {

                Spool = null;
                Spool = (SoapObject) table.getProperty(i);
                HashMap<String,String> temp=new HashMap<String, String>();

                temp.put(FIRST_COLUMN, PulisciStringa(Spool.getPrimitivePropertySafely("Autore").toString()) );
                temp.put(SECOND_COLUMN, ConvertiDataT(PulisciStringa(Spool.getPrimitivePropertySafely("Data").toString())));


                temp.put(THIRD_COLUMN, PulisciStringa(Spool.getPrimitivePropertySafely("Comunicazioni").toString()) );
                _list.add(temp);
                _CiSonoCom = true;
            }



            Log.i(TAG, "Finito gettallRecordtabtipo - POSITIVO");
            return true;

        } catch (SocketTimeoutException et) {

            Log.i(TAG, "Fine WS VerificaVersioning - TIMEOUT");
            _Errore = "TIMEOUT";
            _CiSonoCom = false;
            return false;
        }catch (Exception ex) {

            Log.i(TAG, "Fine WS VerificaVersioning - NEGATIVO");
            _Errore = "Error WS: " + ex.getMessage() + " - Cause: "+ ex.getCause() + " - ex: " + ex.toString();
            _CiSonoCom = false;
            return true;
        }


    }
    //Controllo Versioning da WebService
    public Boolean VerificaVersioning() {

        this.servizio = "ControllaVersioning";
        response = servizio+"Result";
        String SOAP_ACTION = "http://tempuri.org/" + servizio;
        String METHOD_NAME = servizio;
        String NAMESPACE = "http://tempuri.org/";
        String URL;
        Calendar c = Calendar.getInstance();
        if(SSID.contains("cs1"))
            URL = "http://cs-storage/wsTrmLetture/wsTrmLetture.asmx?wsdl";
        else
            URL = "http://web.comerservizi.net:8083/wsTrmLetture/wsTrmLetture.asmx?wsdl";

        try {
            Log.i(TAG, "Inizio WS VerificaVersioning");
            SoapObject Request = new SoapObject(NAMESPACE, METHOD_NAME);
            //Request.addProperty(servizio, getCel);
            Request.addProperty("software", _NomeApp);

            SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            soapEnvelope.dotNet = true;
            soapEnvelope.setOutputSoapObject(Request);

            HttpTransportSE transport = new HttpTransportSE(URL,180000);

            transport.call(SOAP_ACTION, soapEnvelope);
            resultString = (SoapPrimitive) soapEnvelope.getResponse();
            Log.i(TAG, "Fine WS VerificaVersioning - POSITIVO");
            _Versioning = resultString.toString();
            MyAct_Accesso._Versioning = _Versioning;
            return true;

        } catch (SocketTimeoutException et) {

            Log.i(TAG, "Fine WS VerificaVersioning - TIMEOUT");
            _Errore = "TIMEOUT";
            return false;
        }catch (Exception ex) {

            Log.i(TAG, "Fine WS VerificaVersioning - NEGATIVO");
            _Errore = "Error WS: " + ex.getMessage() + " - Cause: "+ ex.getCause() + " - ex: " + ex.toString();
            return false;
        }
    }

    public Boolean LinkDownloadAPP() {

        this.servizio = "LinkDownloadAPP";
        response = servizio+"Result";
        String SOAP_ACTION = "http://tempuri.org/" + servizio;
        String METHOD_NAME = servizio;
        String NAMESPACE = "http://tempuri.org/";
        String URL;
        Calendar c = Calendar.getInstance();
        if(SSID.contains("cs1"))
            URL = "http://cs-storage/wsTrmLetture/wsTrmLetture.asmx?wsdl";
        else
            URL = "http://web.comerservizi.net:8083/wsTrmLetture/wsTrmLetture.asmx?wsdl";

        try {
            Log.i(TAG, "Inizio WS VerificaVersioning");
            SoapObject Request = new SoapObject(NAMESPACE, METHOD_NAME);
            //Request.addProperty(servizio, getCel);
            Request.addProperty("software", _NomeApp);

            SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            soapEnvelope.dotNet = true;
            soapEnvelope.setOutputSoapObject(Request);

            HttpTransportSE transport = new HttpTransportSE(URL,180000);

            transport.call(SOAP_ACTION, soapEnvelope);
            resultString = (SoapPrimitive) soapEnvelope.getResponse();
            Log.i(TAG, "Fine WS VerificaVersioning - POSITIVO");
            _Link = resultString.toString();
            return true;

        } catch (SocketTimeoutException et) {

            Log.i(TAG, "Fine WS VerificaVersioning - TIMEOUT");
            _Errore = "TIMEOUT";
            return false;
        }catch (Exception ex) {

            Log.i(TAG, "Fine WS VerificaVersioning - NEGATIVO");
            _Errore = "Error WS: " + ex.getMessage() + " - Cause: "+ ex.getCause() + " - ex: " + ex.toString();
            return false;
        }
    }


    //Controllo Versioning da WebService
    public String VerificaVersioningAsync(String NomeApp) {

        this.servizio = "ControllaVersioning";
        response = servizio+"Result";
        String SOAP_ACTION = "http://tempuri.org/" + servizio;
        String METHOD_NAME = servizio;
        String NAMESPACE = "http://tempuri.org/";
        String URL;
        Calendar c = Calendar.getInstance();
        if(SSID.contains("cs1"))
            URL = "http://cs-storage/wsTrmLetture/wsTrmLetture.asmx?wsdl";
        else
            URL = "http://web.comerservizi.net:8083/wsTrmLetture/wsTrmLetture.asmx?wsdl";

        try {
            Log.i(TAG, "Inizio WS VerificaVersioning");
            SoapObject Request = new SoapObject(NAMESPACE, METHOD_NAME);
            //Request.addProperty(servizio, getCel);
            Request.addProperty("software", NomeApp);

            SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            soapEnvelope.dotNet = true;
            soapEnvelope.setOutputSoapObject(Request);

            HttpTransportSE transport = new HttpTransportSE(URL,180000);

            transport.call(SOAP_ACTION, soapEnvelope);
            resultString = (SoapPrimitive) soapEnvelope.getResponse();
            Log.i(TAG, "Fine WS VerificaVersioning - POSITIVO");
            return resultString.toString();

        } catch (SocketTimeoutException et) {

            Log.i(TAG, "Fine WS VerificaVersioning - TIMEOUT");
            return "TIMEOUT";
        }catch (Exception ex) {

            Log.i(TAG, "Fine WS VerificaVersioning - NEGATIVO");
            return "Error WS: " + ex.getMessage() + " - Cause: "+ ex.getCause() + " - ex: " + ex.toString();
        }
    }

    public Boolean ReturnDataOra() {

        this.servizio = "DataOraServer";
        response = servizio+"Result";
        String SOAP_ACTION = "http://tempuri.org/" + servizio;
        String METHOD_NAME = servizio;
        String NAMESPACE = "http://tempuri.org/";
        String URL;
        Calendar c = Calendar.getInstance();
        if(SSID.contains("cs1"))
            URL = "http://cs-storage/wsTrmLetture/wsTrmLetture.asmx?wsdl";
        else
            URL = "http://web.comerservizi.net:8083/wsTrmLetture/wsTrmLetture.asmx?wsdl";

        try {
            SoapObject Request = new SoapObject(NAMESPACE, METHOD_NAME);
            //Request.addProperty(servizio, getCel);

            SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            soapEnvelope.dotNet = true;
            soapEnvelope.setOutputSoapObject(Request);

            HttpTransportSE transport = new HttpTransportSE(URL,180000);

            transport.call(SOAP_ACTION, soapEnvelope);
            resultString = (SoapPrimitive) soapEnvelope.getResponse();
            Log.i("DataEOraServer", ConvertToDate(resultString.toString()).toString());
            c.setTime(ConvertToDate(resultString.toString()));
            _DataEora = c;
            return true;

        } catch (SocketTimeoutException et) {

            Log.i(TAG, "Fine WS DataEOraServer - TIMEOUT");
            _Errore = "TIMEOUT";
            return false;
        }catch (Exception ex) {

            Log.i(TAG, "Fine WS DataEOraServer - AltroErrore");
            c.setTime(ConvertToDate("25/04/1985 14:30"));
            _Errore = ex.toString();
            return false;
        }
    }

    public Boolean RecuperaDatiSIM() {

        this.servizio = "RegistraSIMV2";
        response = servizio+"Result";
        String SOAP_ACTION = "http://tempuri.org/" + servizio;
        String METHOD_NAME = servizio;
        String NAMESPACE = "http://tempuri.org/";
        String URL;
        Calendar c = Calendar.getInstance();
        if(SSID.contains("cs1"))
            URL = "http://cs-storage/wsTrmLetture/wsTrmLetture.asmx?wsdl";
        else
            URL = "http://web.comerservizi.net:8083/wsTrmLetture/wsTrmLetture.asmx?wsdl";

        try {
            Log.i(TAG, "Inizio WS RecuperaDatiSIM");
            SoapObject Request = new SoapObject(NAMESPACE, METHOD_NAME);
            //Request.addProperty(servizio, getCel);
            Request.addProperty("IDTerminale", _IDTerminale);
            Request.addProperty("SIMSeriale", _CodSIM);
            Request.addProperty("Operatore", _OpSIM);
            Request.addProperty("PhoneNumber", _PhNum);

            SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            soapEnvelope.dotNet = true;
            soapEnvelope.setOutputSoapObject(Request);

            HttpTransportSE transport = new HttpTransportSE(URL,180000);
            transport.call(SOAP_ACTION, soapEnvelope);
            resultString = (SoapPrimitive) soapEnvelope.getResponse();
            Log.i(TAG, "WS RecuperaDatiSIM - Registro dato SIM - POSITIVO");
            _DatiSIM = resultString.toString();
            return true;
        } catch (SocketTimeoutException et) {

            Log.i(TAG, "Fine WS RecuperaDatiSIM - TIMEOUT");
            _Errore = "TIMEOUT";
            return false;
        }
        catch( Exception ex )
        {
            Log.i(TAG, "Fine WS RecuperaDatiSIM - NEGATIVO");
            _Errore = "Error WS: " + ex.getMessage() + " - Cause: "+ ex.getCause() + " - ex: " + ex.toString();
            return false;
        }
    }

    public Boolean ReturnIMEI() {

        this.servizio = "ReturnIDTerminale";
        response = servizio+"Result";
        String SOAP_ACTION = "http://tempuri.org/" + servizio;
        String METHOD_NAME = servizio;
        String NAMESPACE = "http://tempuri.org/";
        String URL;
        Calendar c = Calendar.getInstance();
        if(SSID.contains("cs1"))
            URL = "http://cs-storage/wsTrmLetture/wsTrmLetture.asmx?wsdl";
        else
            URL = "http://web.comerservizi.net:8083/wsTrmLetture/wsTrmLetture.asmx?wsdl";

        try {
            Log.i(TAG, "Inizio WS ReturnIMEI");
            SoapObject Request = new SoapObject(NAMESPACE, METHOD_NAME);
            //Request.addProperty(servizio, getCel);
            Request.addProperty("IMEI", _IMEI);

            SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            soapEnvelope.dotNet = true;
            soapEnvelope.setOutputSoapObject(Request);

            HttpTransportSE transport = new HttpTransportSE(URL,180000);

            transport.call(SOAP_ACTION, soapEnvelope);
            resultString = (SoapPrimitive) soapEnvelope.getResponse();
            Log.i(TAG, "Fine WS ReturnIMEI - POSITIVO");
            _IDTerminale = resultString.toString();
            return true;
        } catch (SocketTimeoutException et) {

            Log.i(TAG, "Fine WS ReturnIMEI - TIMEOUT");
            _Errore = "TIMEOUT";
            return false;
        }catch (Exception ex) {

            Log.i(TAG, "Fine WS ReturnIMEI - NEGATIVO");
            _Errore = "errore";
            return false;
        }
    }

    public Boolean ReturnGlobalConfig() {

        this.servizio = "ReturnGlobalConfigV2";
        response = servizio+"Result";
        String SOAP_ACTION = "http://tempuri.org/" + servizio;
        String METHOD_NAME = servizio;
        String NAMESPACE = "http://tempuri.org/";
        String URL;
        Calendar c = Calendar.getInstance();
        if(SSID.contains("cs1"))
            URL = "http://cs-storage/wsTrmLetture/wsTrmLetture.asmx?wsdl";
        else
            URL = "http://web.comerservizi.net:8083/wsTrmLetture/wsTrmLetture.asmx?wsdl";

        try {
            Log.i(TAG, "Inizio WS ReturnGlobalConfig");
            SoapObject Request = new SoapObject(NAMESPACE, METHOD_NAME);
            //Request.addProperty(servizio, getCel);
            Request.addProperty("IMEI", _IMEI);
            Request.addProperty("APP", _NomeApp);

            SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            soapEnvelope.dotNet = true;
            soapEnvelope.setOutputSoapObject(Request);

            HttpTransportSE transport = new HttpTransportSE(URL,180000);

            transport.call(SOAP_ACTION, soapEnvelope);
            resultString = (SoapPrimitive) soapEnvelope.getResponse();
            Log.i(TAG, "Fine WS ReturnGlobalConfig - POSITIVO");
            _idConfing = resultString.toString();
            if(_idConfing.trim().isEmpty() || _idConfing.trim().equals("errore") || _idConfing.trim().equals("0"))
            {
                _Errore ="Nessun ID Config impostato per il codice IMEI";
                return false;
            }
            return true;
        } catch (SocketTimeoutException et) {

            Log.i(TAG, "Fine WS ReturnGlobalConfig - TIMEOUT");
            _Errore = "TIMEOUT";
            return false;
        }catch (Exception ex) {

            Log.i(TAG, "Fine WS ReturnGlobalConfig - NEGATIVO");
            _Errore = "errore";
            return false;
        }
    }
    public Boolean ReturnLetturista()
    {
        if(ReturnIDLetturista())
        {
            if(ReturnNomeLetturista())
                return true;
            else
                return false;
        }
        else
            return false;
    }

    public Boolean ReturnIDLetturista() {

        this.servizio = "ReturnIDLetturista";
        response = servizio+"Result";
        String SOAP_ACTION = "http://tempuri.org/" + servizio;
        String METHOD_NAME = servizio;
        String NAMESPACE = "http://tempuri.org/";
        String URL;
        Calendar c = Calendar.getInstance();
        if(SSID.contains("cs1"))
            URL = "http://cs-storage/wsTrmLetture/wsTrmLetture.asmx?wsdl";
        else
            URL = "http://web.comerservizi.net:8083/wsTrmLetture/wsTrmLetture.asmx?wsdl";

        try {
            Log.i(TAG, "Inizio WS ReturnIDLetturista");
            SoapObject Request = new SoapObject(NAMESPACE, METHOD_NAME);
            //Request.addProperty(servizio, getCel);
            Request.addProperty("sGlobalConfiguration", GlobalConfig);
            Request.addProperty("IDTerminale", _IDTerminale);

            SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            soapEnvelope.dotNet = true;
            soapEnvelope.setOutputSoapObject(Request);

            HttpTransportSE transport = new HttpTransportSE(URL,180000);

            transport.call(SOAP_ACTION, soapEnvelope);
            resultString = (SoapPrimitive) soapEnvelope.getResponse();
            Log.i(TAG, "Fine WS ReturnIDLetturista - POSITIVO");
            _IDLetturista = resultString.toString();
            MyAct_Accesso.IDLETT = _IDLetturista;
            return true;
        }  catch (SocketTimeoutException et) {

            Log.i(TAG, "Fine WS ReturnIDLetturista - TIMEOUT");
            _Errore = "TIMEOUT";
            return false;
        }catch (Exception ex) {

            Log.i(TAG, "Fine WS ReturnIDLetturista - NEGATIVO");
            _Errore = "errore: " + ex.getMessage();
            return false;
        }
    }

    public Boolean ReturnNomeLetturista() {

        this.servizio = "ReturnNomeLetturista";
        response = servizio+"Result";
        String SOAP_ACTION = "http://tempuri.org/" + servizio;
        String METHOD_NAME = servizio;
        String NAMESPACE = "http://tempuri.org/";
        String URL;
        Calendar c = Calendar.getInstance();
        if(SSID.contains("cs1"))
            URL = "http://cs-storage/wsTrmLetture/wsTrmLetture.asmx?wsdl";
        else
            URL = "http://web.comerservizi.net:8083/wsTrmLetture/wsTrmLetture.asmx?wsdl";

        try {
            Log.i(TAG, "Inizio WS ReturnNomeLetturista");
            SoapObject Request = new SoapObject(NAMESPACE, METHOD_NAME);
            //Request.addProperty(servizio, getCel);
            Request.addProperty("sGlobalConfiguration", GlobalConfig);
            Request.addProperty("IDLetturista", _IDLetturista);

            SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            soapEnvelope.dotNet = true;
            soapEnvelope.setOutputSoapObject(Request);

            HttpTransportSE transport = new HttpTransportSE(URL,180000);

            transport.call(SOAP_ACTION, soapEnvelope);
            resultString = (SoapPrimitive) soapEnvelope.getResponse();
            Log.i(TAG, "Fine WS ReturnNomeLetturista - POSITIVO");
            _NomeLetturista = resultString.toString();
            return true;
        }  catch (SocketTimeoutException et) {

            Log.i(TAG, "Fine WS ReturnNomeLetturista - TIMEOUT");
            _Errore = "TIMEOUT";
            return false;
        }catch (Exception ex) {

            Log.i(TAG, "Fine WS ReturnNomeLetturista - NEGATIVO");
            _Errore = "errore: " + ex.getMessage();
            return false;
        }
    }

    public Boolean ReturnDescAttivita() {

        this.servizio = "ReturnDescAttivita";
        response = servizio+"Result";
        String SOAP_ACTION = "http://tempuri.org/" + servizio;
        String METHOD_NAME = servizio;
        String NAMESPACE = "http://tempuri.org/";
        String URL;
        Calendar c = Calendar.getInstance();
        if(SSID.contains("cs1"))
            URL = "http://cs-storage/wsTrmLetture/wsTrmLetture.asmx?wsdl";
        else
            URL = "http://web.comerservizi.net:8083/wsTrmLetture/wsTrmLetture.asmx?wsdl";

        try {
            Log.i(TAG, "Inizio WS ReturnDescAttivita");
            SoapObject Request = new SoapObject(NAMESPACE, METHOD_NAME);
            //Request.addProperty(servizio, getCel);
            Request.addProperty("sGlobalConfiguration", GlobalConfig);
            Request.addProperty("IDLetturista", _IDLetturista);

            SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            soapEnvelope.dotNet = true;
            soapEnvelope.setOutputSoapObject(Request);

            HttpTransportSE transport = new HttpTransportSE(URL,180000);

            transport.call(SOAP_ACTION, soapEnvelope);
            resultString = (SoapPrimitive) soapEnvelope.getResponse();
            Log.i(TAG, "Fine WS ReturnDescAttivita - POSITIVO");
            _DescAttivita = resultString.toString();
            return true;
        }  catch (SocketTimeoutException et) {

            Log.i(TAG, "Fine WS ReturnDescAttivita - TIMEOUT");
            _Errore = "TIMEOUT";
            return false;
        }catch (Exception ex) {

            Log.i(TAG, "Fine WS ReturnDescAttivita - NEGATIVO");
            _Errore = "errore: " + ex.getMessage();
            return false;
        }
    }
    //Chiama il WS per verificare i dati di login

    public String VerificaAccesso(String service, String IMEI_In, String Operatore_In, String Versione, String Software) {

        this.servizio = service;
        response = servizio+"Result";
        String SOAP_ACTION = "http://tempuri.org/" + servizio;
        String METHOD_NAME = servizio;
        String NAMESPACE = "http://tempuri.org/";
        String URL;
        if(SSID.contains("cs1"))
            URL = "http://cs-storage/wsTrmLetture/wsTrmLetture.asmx?wsdl";
        else
            URL = "http://web.comerservizi.net:8083/wsTrmLetture/wsTrmLetture.asmx?wsdl";

        try {
            SoapObject Request = new SoapObject(NAMESPACE, METHOD_NAME);
            Request.addProperty("sGlobalConfiguration", GlobalConfig);
            Request.addProperty("IMEI", _IMEI);
            Request.addProperty("Operatore", Operatore_In);
            Request.addProperty("Versione", Versione);
            Request.addProperty("Software", Software);

            SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            soapEnvelope.dotNet = true;
            soapEnvelope.setOutputSoapObject(Request);

            HttpTransportSE transport = new HttpTransportSE(URL,180000);

            transport.call(SOAP_ACTION, soapEnvelope);

            resultString = (SoapPrimitive) soapEnvelope.getResponse();

            String Controllo = resultString.toString();
            String dato="";
            String idConfing="";
            if(Controllo.contains("|"))
            {
                String[] parts = Controllo.split(Pattern.quote("|"));
                dato = parts[0];
                idConfing = parts[1];
                String sqlUpdate = "UPDATE AndroConfig SET IdConfig='"+idConfing+"' WHERE Type = 'sGlobalConfiguration' ";
                db.execSQL(sqlUpdate);
            }
            else
                dato = resultString.toString();

            Log.i(TAG, "Finito il Verifica Accesso - POSITIVO");
            return dato;
        } catch (Exception ex) {

            Log.i(TAG, "Finito il Verifica Accesso - NEGATIVO");
            return "Error WS: " + ex.getMessage() + " - Cause: "+ ex.getCause() + " - ex: " + ex.toString();
        }
    }


    private String PulisciStringa(String val)
    {
        String res;
        if(val==null)
            return val;
        else
        {
            res = val.replace("'","''");
            return res;
        }
    }

    private String ConvertiData(String StringData)
    {

        Date ToData;
        String DataResult;

        if(StringData==null)
            DataResult = StringData;
        else {
            try {
                ToData = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(StringData);
                DataResult = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(ToData);
            } catch (ParseException e) {
                DataResult = StringData;
            }
        }
        return DataResult;
    }

    private Date ConvertToDate(String dateString){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH.mm.ss");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            Log.i("DataEOraServer",e.toString());
            e.printStackTrace();
        }
        return convertedDate;
    }


    private String ConvertiDataT(String StringData)
    {

        Date ToData;
        String DataResult;

        if(StringData==null)
            DataResult = StringData;
        else {
            try {
                ToData = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(StringData);
                DataResult = new SimpleDateFormat("dd/MM/yyyy").format(ToData);
            } catch (ParseException e) {
                DataResult = StringData;
            }
        }
        return DataResult;
    }
    protected void onProgressUpdate(String... progress) {

        dialogAvvio.setMessage("Sincronizzazione dati in corso.." + progress[0].toString());
        dialogAvvio.setMessage("Attendere Sincronizzazione. In base alla qualità del segnale potrebbe volerci qualche secondo [ " + progress[0].toString() + " di 8 ]");
        dialogAvvio.show();
    }


}
