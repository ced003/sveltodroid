package it.comerservizi.appsveltodroid;

import android.content.ComponentCallbacks2;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static it.comerservizi.appsveltodroid.Constants.FIRST_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.FOURTH_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.SECOND_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.THIRD_COLUMN;

public class ElencoAttivita extends AppCompatActivity {

    ImageView EAimgGPS;
    ImageView EAimgWEB;
    ListView listView;
    Button btnSincronizza;
    GPSHelper gps;
    GPClass GP;
    ConnectionDetector cd ;
    DatabaseHelper dbHelper;
    SQLiteDatabase db;
    AsyncLoader ElementiAs;
    AsyncCallWS task;
    AsyncUpload SincroOut;
    Integer NumOperatore = 0;
    public ArrayList<String> IndiceElenco;
    ArrayList<HashMap<String, String>> list;
    ListViewAdapters adapter;
    String IdCaricoScelto;
    Spinner spnCarico;
    ArrayList<String> SelectCarico;
    private List<String> spinnerCarico;

    boolean statusOfGPS;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_elenco_attivita);
        onTrimMemory(ComponentCallbacks2.TRIM_MEMORY_COMPLETE);
        onTrimMemory(ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN);

        EAimgGPS = (ImageView) findViewById(R.id.EAimgGPS);
        EAimgWEB = (ImageView) findViewById(R.id.EAimgWEB);
        listView=(ListView)findViewById(R.id.listAttivita);
        btnSincronizza=(Button)findViewById(R.id.btnSincronizza);
        spnCarico = (Spinner) findViewById(R.id.spnCarico);

        Intent I_ActAccesso = getIntent();
        NumOperatore = I_ActAccesso.getExtras().getInt("NumOperatore");
        IdCaricoScelto ="nessuno";
        GP = new GPClass();
        cd = new ConnectionDetector(getApplicationContext(),EAimgWEB);
        //cd.setElencoAttivitaHandler(this);
        IntentFilter fltr_connreceived = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(cd, fltr_connreceived);

        dbHelper = new DatabaseHelper(ElencoAttivita.this);
        db = dbHelper.getWritableDatabase();
        //SPINNER CARICO
        Cursor cursorCarico = dbHelper.query(db, "SELECT IdCarico, Periodicita, Count(*) As TOT FROM utenti GROUP BY IdCarico, Periodicita WHERE Stato = 'A' ");
        spinnerCarico = new ArrayList<String>();
        spinnerCarico.add("ALL");
        SelectCarico = new ArrayList<String>();
        SelectCarico.add("");
        cursorCarico.moveToFirst();
        while (cursorCarico.isAfterLast() == false) {
            spinnerCarico.add(cursorCarico.getString(cursorCarico.getColumnIndex("IdCarico")) + " - " + cursorCarico.getString(cursorCarico.getColumnIndex("Periodicita")));
            SelectCarico.add(cursorCarico.getString(cursorCarico.getColumnIndex("IdCarico")));
            cursorCarico.moveToNext();
        }

        cursorCarico.close();
        ArrayAdapter<String> SpinnAdapCarico = new ArrayAdapter<String>(this, R.layout.spinner_item1, spinnerCarico);
        spnCarico.setAdapter(SpinnAdapCarico);
        spnCarico.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                if (position > 0)
                {
                    IdCaricoScelto = SelectCarico.get(position);
                    OrdinaGenerico();
                }
                else
                {
                    IdCaricoScelto = "nessuno";
                    OrdinaGenerico();
                }

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {
                IdCaricoScelto = "nessuno";
                spnCarico.setSelection(0);
            }
        });
        list=new ArrayList<HashMap<String,String>>();
        adapter=new ListViewAdapters(this, list);
        ElementiAs = new AsyncLoader(ElencoAttivita.this, "DettaglioAttivita");
        ElementiAs.operatore = NumOperatore.toString();
        ElementiAs.execute();
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(getApplicationContext(),"Grazie per aver usato SveltoDroid!",Toast.LENGTH_LONG).show();

        finish();
        return;
    }


    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        try{

            unregisterReceiver(cd);
            Log.i("Act_Accesso", "Unregistrato Connection Detector");
        }
        catch(Exception e)
        {
            Log.i("Act_Accesso", "Impossibile unregistrare Connection Detector");
        }

    }
    @Override
    protected void onPause()
    {
        super.onPause();
    }

    @Override
    protected void onRestart()
    {
        super.onRestart();
    }

    @Override
    protected void onStart()
    {
        super.onStart();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        VerificaGPS();
        ElementiAs = new AsyncLoader(ElencoAttivita.this, "DettaglioAttivita");
        ElementiAs.operatore = NumOperatore.toString();
        ElementiAs.execute();
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        try{

            unregisterReceiver(cd);
            Log.i("ElencoAttivita", "Unregistrato Connection Detector");
        }
        catch(Exception e)
        {
            Log.i("ElencoAttivita", "Impossibile unregistrare Connection Detector");
        }
    }

    public void VerificaGPS()
    {

        gps = new GPSHelper();
        gps.GPSContesto(getApplicationContext());

        statusOfGPS = gps.StatoGPS();
        if (statusOfGPS)
            EAimgGPS.setImageResource(R.drawable.gpsgreen26);
        else
            Toast.makeText(getApplicationContext(), "ATTENZIONE DEVI ATTIVARE IL GPS", Toast.LENGTH_LONG).show();

    }

    public void OrdinaTotali(View view)
    {
        String IndRicercaPass;



        list.clear();
        ListView listView=(ListView)findViewById(R.id.listAttivita);

        IndiceElenco = new ArrayList<String>();
        IndiceElenco.clear();
        Cursor cursor;
        if(IdCaricoScelto.contains("nessuno"))
            cursor = dbHelper.query(db, "SELECT CAP, Localita, Provincia, Toponimo, Strada, Civico,SUM(CASE WHEN NotaAccess = 'Y' THEN 1 ELSE 0 END) as Accessibile , Count(*) as Totali FROM utenti WHERE Stato = 'A' GROUP BY CAP, Localita, Toponimo, Strada, Civico ORDER BY Totali DESC, Accessibile DESC");
        else
            cursor = dbHelper.query(db, "SELECT CAP, Localita, Provincia, Toponimo, Strada, Civico,SUM(CASE WHEN NotaAccess = 'Y' THEN 1 ELSE 0 END) as Accessibile , Count(*) as Totali FROM utenti WHERE Stato = 'A' AND IdCarico='"+IdCaricoScelto+"' GROUP BY CAP, Localita, Toponimo, Strada, Civico ORDER BY Totali DESC, Accessibile DESC");

        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {
            HashMap<String,String> temp=new HashMap<String, String>();
            temp.put(FIRST_COLUMN, GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("CAP")))+ " " +GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Localita")))+ " " +GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Provincia"))));
            temp.put(SECOND_COLUMN, GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Toponimo"))) + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Strada"))  + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Civico"))))  );
            temp.put(THIRD_COLUMN, "Totali: " + cursor.getString(cursor.getColumnIndex("Totali")));
            temp.put(FOURTH_COLUMN, "Accessibili: " + cursor.getString(cursor.getColumnIndex("Accessibile")));

            list.add(temp);
            IndRicercaPass = GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("CAP")))+ " "+GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Localita")))+" "+GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Provincia")))+" "+GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Toponimo"))) + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Strada"))) + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Civico"))) ;
            IndiceElenco.add(IndRicercaPass);
            cursor.moveToNext();
        }
        adapter=new ListViewAdapters(this, list);
        adapter.cosa = "ElencoAttivita";

        listView.setAdapter(null);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                Intent intent = new Intent(ElencoAttivita.this, ListaCarico.class);
                String IDRicerca = IndiceElenco.get(position);
                intent.putExtra("Indirizzo", IDRicerca);
                startActivity(intent);

            }

        });

        cursor.close();
        System.gc();
        Toast.makeText(getApplicationContext(), "Sincronizzazione Completata", Toast.LENGTH_LONG).show();
    }

    public void OrdinaAccessibili(View view)
    {
        String IndRicercaPass;


        list.clear();
        ListView listView=(ListView)findViewById(R.id.listAttivita);
        IndiceElenco = new ArrayList<String>();
        IndiceElenco.clear();
        Cursor cursor;
        if(IdCaricoScelto.contains("nessuno"))
            cursor = dbHelper.query(db, "SELECT CAP, Localita, Provincia, Toponimo, Strada, Civico, SUM(CASE WHEN NotaAccess = 'Y' THEN 1 ELSE 0 END) as Accessibile , Count(*) as Totali FROM utenti WHERE Stato = 'A' GROUP BY CAP, Localita, Toponimo, Strada, Civico ORDER BY Accessibile DESC, Totali DESC");
        else
            cursor = dbHelper.query(db, "SELECT CAP, Localita, Provincia, Toponimo, Strada, Civico,SUM(CASE WHEN NotaAccess = 'Y' THEN 1 ELSE 0 END) as Accessibile , Count(*) as Totali FROM utenti WHERE Stato = 'A' AND IdCarico='"+IdCaricoScelto+"' GROUP BY CAP, Localita, Toponimo, Strada, Civico ORDER BY Accessibile DESC, Totali DESC");

        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {
            HashMap<String,String> temp=new HashMap<String, String>();
            temp.put(FIRST_COLUMN, GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("CAP")))+ " " +GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Localita"))) + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Provincia"))));
            temp.put(SECOND_COLUMN, GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Toponimo"))) + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Strada")))  + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Civico")))  );
            temp.put(THIRD_COLUMN, "Totali: " + cursor.getString(cursor.getColumnIndex("Totali")));
            temp.put(FOURTH_COLUMN, "Accessibili: " + cursor.getString(cursor.getColumnIndex("Accessibile")) );

            list.add(temp);
            IndRicercaPass = GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("CAP")))+ " "+GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Localita")))+" "+GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Provincia")))+" "+GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Toponimo"))) + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Strada"))) + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Civico"))) ;
            IndiceElenco.add(IndRicercaPass);
            cursor.moveToNext();
        }
        adapter=new ListViewAdapters(this, list);
        adapter.cosa = "ElencoAttivita";

        listView.setAdapter(null);
        listView.setAdapter(adapter);

        adapter.notifyDataSetChanged();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                Intent intent = new Intent(ElencoAttivita.this, ListaCarico.class);
                String IDRicerca = IndiceElenco.get(position);
                intent.putExtra("Indirizzo", IDRicerca);
                startActivity(intent);

            }

        });

        cursor.close();
        System.gc();
        Toast.makeText(getApplicationContext(), "Elaborazione Completata", Toast.LENGTH_LONG).show();
    }

    public void OrdinaIndirizzi(View view)
    {
        String IndRicercaPass;

        list.clear();
        ListView listView=(ListView)findViewById(R.id.listAttivita);
        IndiceElenco = new ArrayList<String>();
        IndiceElenco.clear();
        Cursor cursor;

        if(IdCaricoScelto.contains("nessuno"))
            cursor = dbHelper.query(db, "SELECT CAP, Localita, Provincia, Toponimo, Strada, Civico, SUM(CASE WHEN NotaAccess = 'Y' THEN 1 ELSE 0 END) as Accessibile , Count(*) as Totali FROM utenti WHERE Stato = 'A' GROUP BY CAP, Localita, Toponimo, Strada, Civico ORDER BY  CAP, Localita, Provincia, Toponimo, Strada, CAST(Civico AS INTEGER)");
        else
            cursor = dbHelper.query(db, "SELECT CAP, Localita, Provincia, Toponimo, Strada, Civico, SUM(CASE WHEN NotaAccess = 'Y' THEN 1 ELSE 0 END) as Accessibile , Count(*) as Totali FROM utenti WHERE Stato = 'A' AND IdCarico='"+IdCaricoScelto+"' GROUP BY CAP, Localita, Toponimo, Strada, Civico ORDER BY  CAP, Localita, Provincia, Toponimo, Strada, CAST(Civico AS INTEGER)");

        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {
            HashMap<String,String> temp=new HashMap<String, String>();
            temp.put(FIRST_COLUMN, GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("CAP")))+ " " +GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Localita"))) + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Provincia"))));
            temp.put(SECOND_COLUMN, GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Toponimo"))) + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Strada")))  + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Civico")))  );
            temp.put(THIRD_COLUMN, "Totali: " + cursor.getString(cursor.getColumnIndex("Totali")));
            temp.put(FOURTH_COLUMN, "Accessibili: " + cursor.getString(cursor.getColumnIndex("Accessibile")) );

            list.add(temp);
            IndRicercaPass = GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("CAP")))+ " "+GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Localita")))+" "+GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Provincia")))+" "+GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Toponimo"))) + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Strada"))) + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Civico"))) ;
            IndiceElenco.add(IndRicercaPass);
            cursor.moveToNext();
        }
        adapter=new ListViewAdapters(this, list);
        adapter.cosa = "ElencoAttivita";

        listView.setAdapter(null);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                Intent intent = new Intent(ElencoAttivita.this, ListaCarico.class);
                String IDRicerca = IndiceElenco.get(position);
                intent.putExtra("Indirizzo", IDRicerca);
                startActivity(intent);

            }

        });

        cursor.close();
        System.gc();

        Toast.makeText(getApplicationContext(), "Sincronizzazione Completata", Toast.LENGTH_LONG).show();
    }

    public void IniziaSincronizzazione(View view)
    {



        SincroOut = new AsyncUpload();
       // SincroOut.MyElencoAttivita = this;
        SincroOut.SSID = cd.WifiSSID();
        SincroOut.operatore = NumOperatore.toString();
        SincroOut.execute();

        Log.i("response", "ElencoAttività - Post AsyncLoader.execute");

    }

    //Ordinamento per prima scelta carico
    public void OrdinaGenerico()
    {
        String IndRicercaPass;



        list.clear();
        ListView listView=(ListView)findViewById(R.id.listAttivita);

        IndiceElenco = new ArrayList<String>();
        IndiceElenco.clear();
        Cursor cursor;
        if(IdCaricoScelto.contains("nessuno"))
            cursor = dbHelper.query(db, "SELECT CAP, Localita, Provincia, Toponimo, Strada, Civico,SUM(CASE WHEN NotaAccess = 'Y' THEN 1 ELSE 0 END) as Accessibile , Count(*) as Totali FROM utenti GROUP BY CAP, Localita, Toponimo, Strada, Civico ORDER BY Totali DESC, Accessibile DESC");
        else
            cursor = dbHelper.query(db, "SELECT CAP, Localita, Provincia, Toponimo, Strada, Civico,SUM(CASE WHEN NotaAccess = 'Y' THEN 1 ELSE 0 END) as Accessibile , Count(*) as Totali FROM utenti WHERE IdCarico='"+IdCaricoScelto+"' GROUP BY CAP, Localita, Toponimo, Strada, Civico ORDER BY Totali DESC, Accessibile DESC");

        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {
            HashMap<String,String> temp=new HashMap<String, String>();
            temp.put(FIRST_COLUMN, GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("CAP")))+ " " +GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Localita"))) + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Provincia"))));
            temp.put(SECOND_COLUMN, GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Toponimo"))) + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Strada")))  + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Civico")))  );
            temp.put(THIRD_COLUMN, "Totali: " + cursor.getString(cursor.getColumnIndex("Totali")));
            temp.put(FOURTH_COLUMN, "Accessibili: " + cursor.getString(cursor.getColumnIndex("Accessibile")));

            list.add(temp);
            IndRicercaPass = GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("CAP")))+ " "+GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Localita")))+" "+GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Provincia")))+" "+GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Toponimo"))) + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Strada"))) + " " + GP.NullToEmpty(cursor.getString(cursor.getColumnIndex("Civico"))) ;
            IndiceElenco.add(IndRicercaPass);
            cursor.moveToNext();
        }
        adapter=new ListViewAdapters(this, list);
        adapter.cosa = "ElencoAttivita";

        listView.setAdapter(null);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                Intent intent = new Intent(ElencoAttivita.this, ListaCarico.class);
                String IDRicerca = IndiceElenco.get(position);
                intent.putExtra("Indirizzo", IDRicerca);
                startActivity(intent);

            }

        });

        cursor.close();
        System.gc();
        Toast.makeText(getApplicationContext(), "Sincronizzazione Completata", Toast.LENGTH_LONG).show();
    }
}
