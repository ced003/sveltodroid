package it.comerservizi.appsveltodroid;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class Act_DatiUtenza extends AppCompatActivity {

    TextView Nominativo;
    TextView CodUtePDR;
    TextView Cognome;
    TextView Comune;
    TextView Indirizzo;
    TextView IndCompl;
    TextView Misuratore;
    TextView Correttore;
    TextView Classe;
    TextView Stato;
    TextView Ubicazione;
    TextView Accessibilita;
    TextView Telefono;
    TextView Causale;
    TextView Altro;
    TextView Info;
    TextView Periodicita;
    TextView UltLettura;
    DatabaseHelper dbHelper;
    SQLiteDatabase db;
    String CodiceUtente, IDCarico, IDAzienda;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__dati_utenza);

        dbHelper = new DatabaseHelper(Act_DatiUtenza.this);
        db = dbHelper.getWritableDatabase();

        Bundle extras = getIntent().getExtras();
        if(extras !=null) {
            CodiceUtente = extras.getString("CodiceUtente");
            IDCarico = extras.getString("IDCarico");
            IDAzienda = extras.getString("IDAzienda");
        }
        Nominativo = (TextView) findViewById(R.id.DatiUtenza_lblNominativo);
        CodUtePDR = (TextView) findViewById(R.id.DatiUtenza_lblCodUte);
        Cognome = (TextView) findViewById(R.id.DatiUtenza_lblCognome);
        Comune = (TextView) findViewById(R.id.DatiUtenza_lblComune);
        Indirizzo = (TextView) findViewById(R.id.DatiUtenza_lblIndirizzo);
        IndCompl = (TextView) findViewById(R.id.DatiUtenza_lblIndCompl);
        Misuratore = (TextView) findViewById(R.id.DatiUtenza_lblMisuratore);
        Correttore = (TextView) findViewById(R.id.DatiUtenza_lblCorrettore);
        Classe = (TextView) findViewById(R.id.DatiUtenza_lblClasse);
        Stato = (TextView) findViewById(R.id.DatiUtenza_lblStato);
        Ubicazione = (TextView) findViewById(R.id.DatiUtenza_lblUbicazione);
        Accessibilita = (TextView) findViewById(R.id.DatiUtenza_lblAccessibilita);
        Telefono = (TextView) findViewById(R.id.DatiUtenza_lblTelefono);
        Causale = (TextView) findViewById(R.id.DatiUtenza_lblCausale);
        Altro = (TextView) findViewById(R.id.DatiUtenza_lblAltro);
        Periodicita = (TextView) findViewById(R.id.DatiUtenza_lblPeriodicita);
        UltLettura = (TextView) findViewById(R.id.DatiUtenza_lblUltLett);
        Info = (TextView) findViewById(R.id.DatiUtenza_Info);


        Cursor cursor = dbHelper.query(db, "SELECT Nominativo,Cognome, CAP, Localita, Provincia, Toponimo, Strada, Civico, NoteAccesso, NotaAccesso100, Periodicita, Ubicazione, NotaAccess, MatricolaMis, ClasseContatore, PresCorrettore, StatoMisuratore, tabtipo.Descrizione as DescrizioneUbicazione, UltimaLettura, Info FROM utenti LEFT OUTER JOIN tabtipo ON tabtipo.CodiceDitta = utenti.Ubicazione WHERE CodiceUtente = '" + CodiceUtente + "' AND idAzienda ='"+IDAzienda+"' AND IdCarico ='"+IDCarico+"' ");

        cursor.moveToFirst();
        //while (cursor.isAfterLast() == false) {
        Nominativo.setText(cursor.getString(cursor.getColumnIndex("Nominativo")));
        CodUtePDR.setText(CodiceUtente);
        Cognome.setText(cursor.getString(cursor.getColumnIndex("Cognome")));
        Comune.setText(cursor.getString(cursor.getColumnIndex("CAP")) + " " + cursor.getString(cursor.getColumnIndex("Localita")) + " " + cursor.getString(cursor.getColumnIndex("Provincia")));
        Indirizzo.setText(cursor.getString(cursor.getColumnIndex("Toponimo")) + " " + cursor.getString(cursor.getColumnIndex("Strada")) + " " + cursor.getString(cursor.getColumnIndex("Civico")));
        IndCompl.setText(cursor.getString(cursor.getColumnIndex("NoteAccesso")));
        Misuratore.setText(cursor.getString(cursor.getColumnIndex("MatricolaMis")));
        Correttore.setText(cursor.getString(cursor.getColumnIndex("PresCorrettore")));
        Classe.setText(cursor.getString(cursor.getColumnIndex("ClasseContatore")));
        Stato.setText(cursor.getString(cursor.getColumnIndex("StatoMisuratore")));
        Ubicazione.setText(cursor.getString(cursor.getColumnIndex("DescrizioneUbicazione")));
        Accessibilita.setText(cursor.getString(cursor.getColumnIndex("NotaAccess")));
        Altro.setText(cursor.getString(cursor.getColumnIndex("NotaAccesso100")));
        Periodicita.setText(cursor.getString(cursor.getColumnIndex("Periodicita")));
        UltLettura.setText(cursor.getString(cursor.getColumnIndex("UltimaLettura")));
        Info.setText(cursor.getString(cursor.getColumnIndex("Info")));
        //}

        cursor.close();
    }
}
