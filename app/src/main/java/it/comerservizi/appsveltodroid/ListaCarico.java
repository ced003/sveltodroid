package it.comerservizi.appsveltodroid;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentCallbacks2;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import static it.comerservizi.appsveltodroid.Constants.FIFTH_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.FIRST_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.FOURTH_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.SECOND_COLUMN;
import static it.comerservizi.appsveltodroid.Constants.THIRD_COLUMN;

public class ListaCarico extends AppCompatActivity {

    AsyncLoader ALoader;
    GPSHelper gps;
    GPClass gpClass;
    ConnectionDetector cd ;
    ImageView LCimgGPS;
    public ImageView LCimgWEB;
    Integer countElenco = 0;
    boolean statusOfGPS;
    ListView listView;
    TextView txtNumAttivitaElenco;
    ArrayList<String> listDitta;
    EditText txtFind;
    Integer NumOperatore = 0;
    String Indirizzo;
    String Matricola;
    String SelectCarico;
    String SelectComune;
    Boolean ConSenzaCivico;
    AsyncLoader ElementiAs;
    DatabaseHelper dbHelper;
    SQLiteDatabase db;
    ListViewAdapters adapter;
    Calendar CalendarCert;
    boolean AccessoOffline = false;
    private ProgressDialog dialog;
    private String GiornoAccesso;
    public ArrayList<String> IndiceElencoCodiceUtente;
    public ArrayList<String> IndiceElencoIndirizzi;
    public ArrayList<String> IndiceElencoIdCarico;
    public ArrayList<String> IndiceElencoIdAzienza;
    String IDLett="";
    String GlobalConfig;

    ArrayList<HashMap<String, String>> list;





    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_carico);
        onTrimMemory(ComponentCallbacks2.TRIM_MEMORY_COMPLETE);
        onTrimMemory(ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN);


        /* CONTROLLO DATA AUTOMATICA */
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                if(Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME) != 1)
                {
                    Toast.makeText(getApplicationContext(), "ATTENZIONE HAI DISABILITATO LA DATA E ORA AUTOMATICA - L'APP NON PUO' FUNZIONARE", Toast.LENGTH_LONG).show();
                    this.finishAffinity();
                }
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        /* CONTROLLO DATA AUTOMATICA */
        gpClass = new GPClass();
        Button btTrova = (Button) findViewById(R.id.btTrova);
        Intent I_ActElencoAttivita = getIntent();
        IDLett = I_ActElencoAttivita.getExtras().getString("IDLett");


        statusOfGPS = false;
        countElenco = 0;
        LCimgGPS = (ImageView) findViewById(R.id.LCimgGPS);
        LCimgWEB = (ImageView) findViewById(R.id.LCimgWEB);
        txtFind = (EditText) findViewById(R.id.txtFind);
        txtNumAttivitaElenco = (TextView) findViewById(R.id.txtNumAttivitaElenco);
        listView=(ListView)findViewById(R.id.listView1);
        //DataOraCerta
        CalendarCert = Calendar.getInstance();
        //Prende il dato dall'intent passato da Act_Accesso.java
        NumOperatore = I_ActElencoAttivita.getExtras().getInt("NumOperatore");
        Indirizzo = I_ActElencoAttivita.getExtras().getString("Indirizzo");
        GiornoAccesso = I_ActElencoAttivita.getExtras().getString("GiornoAccesso");
        if(I_ActElencoAttivita.hasExtra("Matricola"))
            Matricola = I_ActElencoAttivita.getExtras().getString("Matricola");
        else
            Matricola = "";
        if(I_ActElencoAttivita.hasExtra("SelectCarico"))
            SelectCarico = I_ActElencoAttivita.getExtras().getString("SelectCarico");
        else
            SelectCarico = "nessuno";
        if(I_ActElencoAttivita.hasExtra("SelectComune"))
            SelectComune = I_ActElencoAttivita.getExtras().getString("SelectComune");
        else
            SelectComune = "nessuno";
        if(I_ActElencoAttivita.hasExtra("ConSenzaCivico"))
            ConSenzaCivico = I_ActElencoAttivita.getExtras().getBoolean("ConSenzaCivico");
        else
            ConSenzaCivico = false;

        SelectComune=SelectComune.replace("'","''");
        Indirizzo=Indirizzo.replace("'","''");
        Log.i("Ricerca Lista", "Val: " + Indirizzo);
        Long Var = I_ActElencoAttivita.getExtras().getLong("DataOraCerta");
        CalendarCert.setTimeInMillis(Var);

        //Lo Spinner è la combobox di visual, ho generato oggetto associandolo a quello creato graficamente






        cd = new ConnectionDetector(getApplicationContext(),LCimgWEB);
        cd.setListaCarico(this);
        IntentFilter fltr_connreceived = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(cd, fltr_connreceived);


        dbHelper = new DatabaseHelper(ListaCarico.this);
        //list=new ArrayList<HashMap<String,String>>();
        //listItems=new ArrayList<String>();
        adapter=new ListViewAdapters(this, list);
        db = dbHelper.getWritableDatabase();



        ElencoLista(0);

        Cursor cGlobalConfig = db.rawQuery("SELECT IdConfig FROM AndroConfig WHERE Type = 'sGlobalConfiguration'", null);

        if (cGlobalConfig != null)
            cGlobalConfig.moveToFirst();

        GlobalConfig = cGlobalConfig.getString(cGlobalConfig.getColumnIndex("IdConfig"));
        cGlobalConfig.close();


        if(NumOperatore.equals(0) && IDLett.equals("-"))
            AccessoOffline = true;

    }
    @Override
     public void onBackPressed() {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("DataOraCerta", CalendarCert.getTimeInMillis());
        setResult(ListaCarico.RESULT_OK, returnIntent);
        finish();
        return;
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        //db.close();
        //dbHelper.close();
    }

    @Override
    protected void onRestart()
    {
        super.onRestart();
    }

    @Override
    protected void onStart()
    {
        super.onStart();
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        /* CONTROLLO DATA AUTOMATICA */
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                if(Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME) != 1)
                {
                    Toast.makeText(getApplicationContext(), "ATTENZIONE HAI DISABILITATO LA DATA E ORA AUTOMATICA - L'APP NON PUO' FUNZIONARE", Toast.LENGTH_LONG).show();
                    finish();
                }
                else
                {

                    if(!AccessoOffline)
                        gpClass.LastSync(getApplicationContext(), this);
                    if(gpClass.ControlloGiornoAccesso(getApplicationContext(), this, GiornoAccesso))
                        this.finishAffinity();
                    /*
                    CalendarCert.setTimeInMillis((CalendarCert.getTimeInMillis() + SystemClock.currentThreadTimeMillis()));
                    Log.i("DataOraCerta - ListaCarico Resume", new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(CalendarCert.getTime()));
                    VerificaGPS();
                    ElementiAs = new AsyncLoader(ListaCarico.this, "PrimoAccesso");
                    ElementiAs.SubElencoAttivita = Indirizzo;
                    ElementiAs.IDLetturista = IDLett;
                    ElementiAs.Search4Matricola = Matricola;
                    ElementiAs.Search4Carico = SelectCarico;
                    ElementiAs.DataOraCerta = CalendarCert;
                    ElementiAs.execute();
                    */
                    ElencoLista(1);
                }
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        /* CONTROLLO DATA AUTOMATICA */
        Crashlytics.log(null);
        Crashlytics.log("GlobalConfig: " + GlobalConfig + " - Operatore: " + IDLett );
    }

    @Override
    protected void onStop()
    {
        super.onStop();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        unregisterReceiver(cd);
    }

    public void VerificaGPS()
    {

        gps = new GPSHelper();
        gps.GPSContesto(getApplicationContext());

        statusOfGPS = gps.StatoGPS();
        if (statusOfGPS)
            LCimgGPS.setImageResource(R.drawable.gpsgreen26);

    }

    public void RiordinaLista(View view)
    {

    }

    public void ElencoLista(int StartOrResume)
    {
        String Nome, Cognome;
        dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        dialog.setMessage("Elaborazione del carico in corso...");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.show();
        String data;
        Integer countElenco = 0;


        ArrayList<HashMap<String, String>> list=new ArrayList<HashMap<String,String>>();
        ListView listView=(ListView) findViewById(R.id.listView1);

        IndiceElencoCodiceUtente = new ArrayList<String>();
        IndiceElencoCodiceUtente.clear();
        IndiceElencoIndirizzi = new ArrayList<String>();
        IndiceElencoIndirizzi.clear();
        IndiceElencoIdCarico = new ArrayList<String>();
        IndiceElencoIdCarico.clear();
        IndiceElencoIdAzienza = new ArrayList<String>();
        IndiceElencoIdAzienza.clear();
        Cursor cursor;
        if(Matricola.length()<1) {
            if(ConSenzaCivico)
            {
                if (SelectCarico.contains("nessuno") && SelectComune.contains("nessuno"))
                    cursor = dbHelper.query(db, "SELECT * FROM utenti WHERE (CAP || ' ' || Localita || ' ' || Toponimo || ' ' || Strada|| ' ' || Civico) = '" + Indirizzo + "' AND Stato = 'A' AND dtProgrammazione = '' ORDER BY Toponimo, Strada, CAST(Civico AS Integer)");
                else if (!SelectCarico.contains("nessuno") && SelectComune.contains("nessuno"))
                    cursor = dbHelper.query(db, "SELECT * FROM utenti WHERE (CAP || ' ' || Localita || ' ' || Toponimo || ' ' || Strada|| ' ' || Civico) = '" + Indirizzo + "' AND IdCarico = '" + SelectCarico + "' AND Stato = 'A' AND dtProgrammazione = ''  ORDER BY Toponimo, Strada, CAST(Civico AS Integer)");
                else if (SelectCarico.contains("nessuno") && !SelectComune.contains("nessuno"))
                    cursor = dbHelper.query(db, "SELECT * FROM utenti WHERE (CAP || ' ' || Localita || ' ' || Toponimo || ' ' || Strada|| ' ' || Civico) = '" + Indirizzo + "' AND Localita = '" + SelectComune + "' AND Stato = 'A' AND dtProgrammazione = ''  ORDER BY Toponimo, Strada, CAST(Civico AS Integer)");
                else
                    cursor = dbHelper.query(db, "SELECT * FROM utenti WHERE (CAP || ' ' || Localita || ' ' || Toponimo || ' ' || Strada|| ' ' || Civico) = '" + Indirizzo + "' AND Localita = '" + SelectComune + "' AND IdCarico = '" + SelectCarico + "' AND Stato = 'A' AND dtProgrammazione = ''  ORDER BY Toponimo, Strada, CAST(Civico AS Integer)");

            }
            else
            {
                if (SelectCarico.contains("nessuno") && SelectComune.contains("nessuno"))
                    cursor = dbHelper.query(db, "SELECT * FROM utenti WHERE (CAP || ' ' || Localita || ' ' || Toponimo || ' ' || Strada) = '" + Indirizzo + "' AND Stato = 'A'  ORDER BY Toponimo, Strada, CAST(Civico AS Integer)");
                else if (!SelectCarico.contains("nessuno") && SelectComune.contains("nessuno"))
                    cursor = dbHelper.query(db, "SELECT * FROM utenti WHERE (CAP || ' ' || Localita || ' ' || Toponimo || ' ' || Strada) = '" + Indirizzo + "' AND IdCarico = '" + SelectCarico + "' AND Stato = 'A' AND dtProgrammazione = ''  ORDER BY Toponimo, Strada, CAST(Civico AS Integer)");
                else if (SelectCarico.contains("nessuno") && !SelectComune.contains("nessuno"))
                    cursor = dbHelper.query(db, "SELECT * FROM utenti WHERE (CAP || ' ' || Localita || ' ' || Toponimo || ' ' || Strada) = '" + Indirizzo + "' AND Localita = '" + SelectComune + "' AND Stato = 'A' AND dtProgrammazione = ''  ORDER BY Toponimo, Strada, CAST(Civico AS Integer)");
                else
                    cursor = dbHelper.query(db, "SELECT * FROM utenti WHERE (CAP || ' ' || Localita || ' ' || Toponimo || ' ' || Strada) = '" + Indirizzo + "' AND Localita = '" + SelectComune + "' AND IdCarico = '" + SelectCarico + "' AND Stato = 'A' AND dtProgrammazione = ''  ORDER BY Toponimo, Strada, CAST(Civico AS Integer)");
            }
        }
        else
        {
            if (StartOrResume==0)
            Toast.makeText(getApplicationContext(), "La ricerca per matricola non prende in considerazione il carico ed il comune selezionato", Toast.LENGTH_SHORT).show();
            if(SelectCarico.contains("nessuno"))
                cursor = dbHelper.query(db, "SELECT * FROM utenti WHERE MatricolaMis LIKE '%"+ Matricola+"' AND Stato = 'A' AND dtProgrammazione = ''  ");
            else
                cursor = dbHelper.query(db, "SELECT * FROM utenti WHERE MatricolaMis LIKE '%"+ Matricola+"' AND IdCarico = '" + SelectCarico + "' AND Stato = 'A' AND dtProgrammazione = ''  ");
        }

        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {
            HashMap<String,String> temp=new HashMap<String, String>();
            if(cursor.isNull(cursor.getColumnIndex("Nominativo")))
                Nome = "";
            else
                Nome = cursor.getString(cursor.getColumnIndex("Nominativo"));
            if(cursor.isNull(cursor.getColumnIndex("Cognome")))
                Cognome = "";
            else
                Cognome = cursor.getString(cursor.getColumnIndex("Cognome"));
            temp.put(FIRST_COLUMN, Nome + " " + Cognome );
            temp.put(SECOND_COLUMN, cursor.getString(cursor.getColumnIndex("Toponimo")) + " " + cursor.getString(cursor.getColumnIndex("Strada")) + " " + cursor.getString(cursor.getColumnIndex("Civico"))+ " - " + cursor.getString(cursor.getColumnIndex("NotaAccesso100")));
            if(cursor.getString(cursor.getColumnIndex("NotaAccess")).contains("Y"))
                temp.put(THIRD_COLUMN, "ACCESSIBILE");
            else if(!cursor.isNull(cursor.getColumnIndex("Nota3"))){
                if(cursor.getString(cursor.getColumnIndex("Nota3")).contains("AFF"))
                temp.put(THIRD_COLUMN, "PROGRAMMABILE");
            }
            temp.put(FOURTH_COLUMN, "Matricola: " + cursor.getString(cursor.getColumnIndex("MatricolaMis")));
            temp.put(FIFTH_COLUMN, "Data Limite: " + cursor.getString(cursor.getColumnIndex("DtRientro")));
            list.add(temp);
            IndiceElencoIndirizzi.add(gpClass.NullToEmpty(cursor.getString(cursor.getColumnIndex("CAP")))+ " "+gpClass.NullToEmpty(cursor.getString(cursor.getColumnIndex("Localita")))+" " + gpClass.NullToEmpty(cursor.getString(cursor.getColumnIndex("Toponimo"))) + " " + gpClass.NullToEmpty(cursor.getString(cursor.getColumnIndex("Strada"))) + " " + gpClass.NullToEmpty(cursor.getString(cursor.getColumnIndex("Civico"))));
            IndiceElencoCodiceUtente.add(cursor.getString(cursor.getColumnIndex("CodiceUtente")));
            IndiceElencoIdCarico.add(cursor.getString(cursor.getColumnIndex("IdCarico")));
            IndiceElencoIdAzienza.add(cursor.getString(cursor.getColumnIndex("idAzienda")));
            cursor.moveToNext();
            countElenco++;
        }
        ListViewAdapters adapter=new ListViewAdapters(this, list);
        adapter.cosa = "PrimoAccesso";
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        txtNumAttivitaElenco.setText(Integer.toString(cursor.getCount()) + " di " +Integer.toString(listView.getCount()));


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                VerificaGPS();
                if (statusOfGPS) {
                    Intent intent = new Intent(ListaCarico.this, Act_RilevaDati.class);
                    String IDRicerca = IndiceElencoCodiceUtente.get(position);
                    String IDCarico = IndiceElencoIdCarico.get(position);
                    String IDAzienda = IndiceElencoIdAzienza.get(position);
                    intent.putExtra("CodiceUtente", IDRicerca);
                    intent.putExtra("IDCarico", IDCarico.trim());
                    intent.putExtra("IDLett", IDLett.toString());
                    intent.putExtra("IDAzienda", IDAzienda.trim());
                    //DataOraCerta.setTimeInMillis((DataOraCerta.getTimeInMillis() + SystemClock.currentThreadTimeMillis()));
                    startActivity(intent);
                } else {

                    Toast.makeText(getApplicationContext(), "Attenzione per poter lavorare devi attivare il GPS", Toast.LENGTH_LONG).show();
                }

            }

        });


                listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final int mposition = position;
                AlertDialog.Builder dialogRiaperti = new AlertDialog.Builder(ListaCarico.this);
                dialogRiaperti
                        .setTitle("TROVA POSIZIONE")
                        .setMessage("Puoi farti navigare direttamente sul posto cercando per indirizzo (scelta più veloce) o cercando l'ultima posizione rilevata in fase di lettura.")
                        .setCancelable(false)
                        .setNegativeButton("PER INDIRIZZO", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + IndiceElencoIndirizzi.get(mposition));
                                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                                mapIntent.setPackage("com.google.android.apps.maps");
                                startActivity(mapIntent);
                            }
                        })
                        .setPositiveButton("CERCA COORDINATE", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                ALoader = new AsyncLoader(ListaCarico.this,"PrelevaCoordinate");
                                ALoader._CodiceUtente = IndiceElencoCodiceUtente.get(mposition);
                                ALoader.SSID = cd.WifiSSID();
                                ALoader.execute();
                                dialog.cancel();

                            }
                        });
                AlertDialog MsgErrore = dialogRiaperti.create();

                MsgErrore.show();

                return true;
            }

        });

        cursor.close();
        System.gc();
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }

}
