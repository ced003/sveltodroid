package it.comerservizi.appsveltodroid;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.File;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class AsyncUpload extends AsyncTask<Void, String, Void> {

    private String TAG = "AsyncUpload";
    private DatabaseHelper dbHelper;
    private SQLiteDatabase db;
    private File[] ElencoFile;
    private File[] ElencoFileInternal;
    private String servizio = "";
    private String response = "";
    private Cursor CDati;
    private Cursor CCensiti;
    private Cursor CAccessori;
    private boolean AvviaDownload = false;
    public String service;
    public String SSID;
    private List<String> res ;
    private List<String> resCensiti ;
    private List<String> resAccessori ;
    private Integer CDatiLen = 0;
    private Integer CCensitiLen = 0;
    private Integer CAccessoriLen = 0;
    AsyncDownload Sincro;
    public String operatore;
    public Boolean AggiornaSveltoDroid = false;
    public Boolean SoloFoto = false;
    public Integer FotoTrasmesse = 0, FotoErrate = 0;
    public Act_ElencoAttivita MyElencoAttivita = null;
    ProgressDialog dialog;
    String valResponse;
    private GPClass GPC;
    private Integer ContaFotoTrasmesse;
    String GlobalConfig;
    @Override
    protected void onPreExecute()
    {
        Log.i(TAG, "entrato in OnPreExecute");
        dbHelper = new DatabaseHelper(MyElencoAttivita);
        db= dbHelper.getWritableDatabase();
        Cursor c = db.rawQuery("SELECT IdConfig FROM AndroConfig WHERE Type = 'sGlobalConfiguration'", null);

        if (c != null)
            c.moveToFirst();

        dialog = new ProgressDialog(MyElencoAttivita);
        GlobalConfig = c.getString(c.getColumnIndex("IdConfig"));
        if (dialog.isShowing())
        {
            dialog.dismiss();
        }

        dialog.setCancelable(false);
        dialog.setMessage("Sincronizzazione foto in corso...");
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialog.setProgress(0);
        try {
            dialog.show();
        }catch(Exception ex)
        {
            Log.e("ERROR",ex.toString());
        }
        GPC = new GPClass();
        GPC._context = MyElencoAttivita.getApplicationContext();
        ElencoFile = GPC.getListFiles();
        ElencoFileInternal = GPC.getListFilesInternal();
        ContaFotoTrasmesse = 0;
        Crashlytics.log(null);
        Crashlytics.log("GlobalConfig: " + GlobalConfig + " - Operatore: " + operatore );

        if(!SoloFoto) {
            CDati = dbHelper.query(db, "SELECT IdCarico, CodiceUtente, Stato, LCifreLettura , LDataLettura , LOraLettura , LNota1 , LNota2 , LNota3 , LSegnalazione , LSegnalazione1 , LCifreM , LCodTpUtilizzo , LDesTpUtilizzo , LUnAbit , LUnComm , LNota50 , LLetturaCorrettore , LLetturaCorrettore1 , LLetturaCorrettore2 , LLetturaCorrettore3 , LInfo , Lat_Ditta , Lon_Ditta , LLatitudine , LLongitudine , LAutolettura , LNoteLibere, DtLetturaTrm, OraLetturaTrm, idLetturista, FlagNoteLetturista, dtProgrammazione, daOraLett, aOraLett FROM utentiOUT WHERE (Stato = 'L' or Stato = 'P') AND Ritrasmetti = 'S'");
        }
        else
        {

            if(ElencoFile.length<1)
                Toast.makeText(MyElencoAttivita.getApplicationContext(), "Non vi sono più foto da sincronizzare", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    protected Void doInBackground(Void... params) {
        Log.i(TAG, "doInBackground");
        dialog.setMax(ElencoFile.length + ElencoFileInternal.length);

        for(Integer i = 0; i < ElencoFile.length; i++)
        {
            try {
                Log.i(TAG, "File: " + ElencoFile[i].toString());
                if(InviaFoto(GPC.FileToByte(ElencoFile[i].getPath()), ElencoFile[i].getName())=="ok")
                {
                    ElencoFile[i].delete();
                    FotoTrasmesse++;
                }
                else
                {
                    FotoErrate++;
                }
                ContaFotoTrasmesse++;
                publishProgress(Integer.toString(ContaFotoTrasmesse));
            }catch(Exception ex)
            {
            }

        }
        for(Integer i = 0; i < ElencoFileInternal.length; i++)
        {
            try {
                Log.i(TAG, "File: " + ElencoFileInternal[i].toString());

                if(InviaFoto(GPC.FileToByte(ElencoFileInternal[i].getPath()), ElencoFileInternal[i].getName())=="ok")
                {
                    ElencoFileInternal[i].delete();
                    FotoTrasmesse++;
                }
                else
                {
                    FotoErrate++;
                }
                ContaFotoTrasmesse++;
                publishProgress(Integer.toString(ContaFotoTrasmesse));
            }catch(Exception ex)
            {
            }

        }
        Log.i(TAG, "doInBackground - Fine invio foto");

        if(!SoloFoto) {
            publishProgress("change");
            res = new ArrayList<String>();
            resCensiti = new ArrayList<String>();
                resAccessori = new ArrayList<String>();
                Integer conta = 0;
                String costruisci = "";
                CDatiLen = CDati.getCount();
                CDati.moveToFirst();
                for (Integer i = 0; i < CDati.getColumnCount(); i++) {
                    if (i != (CDati.getColumnCount() - 1))
                        costruisci = costruisci + CDati.getColumnName(i) + "|";
                    else
                        costruisci = costruisci + CDati.getColumnName(i);
                }

                Log.i(TAG, "doInBackground - Fine Intestazione Utenti");
                res.add(costruisci);
                costruisci = "";

                while (CDati.isAfterLast() == false) {

                    Log.i(TAG, "doInBackground - PopoloRecord");
                    for (Integer i = 0; i < CDati.getColumnCount(); i++) {

                        Log.i(TAG, "doInBackground - PrendoColonna[" + conta + "][" + i + "]");
                        if (i != (CDati.getColumnCount() - 1))
                            if(CDati.getColumnName(i).equals("dtLetturaTrm"))
                                costruisci = costruisci + GPC.ConvertiSoloData(CDati.getString(CDati.getColumnIndex(CDati.getColumnName(i)))) + "|";
                            else if(CDati.getColumnName(i).equals("OraLetturaTrm"))
                                costruisci = costruisci + GPC.ConvertiSoloOra(CDati.getString(CDati.getColumnIndex(CDati.getColumnName(i)))) + "|";
                            else
                                costruisci = costruisci + CDati.getString(CDati.getColumnIndex(CDati.getColumnName(i))) + "|";
                        else
                            costruisci = costruisci + CDati.getString(CDati.getColumnIndex(CDati.getColumnName(i)));
                    }

                    res.add(costruisci);
                    costruisci = "";
                    publishProgress(Integer.toString(conta));
                    conta++;
                    CDati.moveToNext();
                }


                String costruisciCensiti = "";
                CCensiti = dbHelper.query(db, "SELECT * FROM TbDatiCensiti");

                publishProgress("censiti");
                CCensitiLen = CCensiti.getCount();
                CCensiti.moveToFirst();
                for (Integer i = 0; i < CCensiti.getColumnCount(); i++) {
                    if (i != (CCensiti.getColumnCount() - 1))
                        costruisciCensiti = costruisciCensiti + CCensiti.getColumnName(i) + "|";
                    else
                        costruisciCensiti = costruisciCensiti + CCensiti.getColumnName(i) + "|";
                }

                resCensiti.add(costruisciCensiti);
                costruisciCensiti = "";
                Log.i(TAG, "doInBackground - Fine Intestazione Censiti");

                while (CCensiti.isAfterLast() == false) {

                    Log.i(TAG, "doInBackground - PopoloRecordCesniti");
                    for (Integer i = 0; i < CCensiti.getColumnCount(); i++) {

                        Log.i(TAG, "doInBackground - PrendoColonna[" + (conta - CCensitiLen) + "][" + i + "]");
                        if (i != (CCensiti.getColumnCount() - 1))
                            costruisciCensiti = costruisciCensiti + CCensiti.getString(CCensiti.getColumnIndex(CCensiti.getColumnName(i))) + "|";
                        else
                            costruisciCensiti = costruisciCensiti + CCensiti.getString(CCensiti.getColumnIndex(CCensiti.getColumnName(i)));
                    }

                    resCensiti.add(costruisciCensiti);
                    costruisciCensiti = "";
                    publishProgress(Integer.toString(conta));
                    conta++;
                    CCensiti.moveToNext();
                }

                //**************************************
                //**************************************
                String costruisciAccessori = "";
                CAccessori = dbHelper.query(db, "SELECT * FROM DatiAccessori WHERE GlobalConfig = '" + GlobalConfig + "'");

                publishProgress("accessori");
                CAccessoriLen = CAccessori.getCount();
                CAccessori.moveToFirst();
                for (Integer i = 0; i < CAccessori.getColumnCount(); i++) {
                    if (i != (CAccessori.getColumnCount() - 1))
                        costruisciAccessori = costruisciAccessori + CAccessori.getColumnName(i) + "|";
                    else
                        costruisciAccessori = costruisciAccessori + CAccessori.getColumnName(i);
                }

                resAccessori.add(costruisciAccessori);
                costruisciAccessori = "";
                Log.i(TAG, "doInBackground - Fine Intestazione DatiAccessori");

                while (CAccessori.isAfterLast() == false) {

                    Log.i(TAG, "doInBackground - PopoloRecordDatiAccessori");
                    for (Integer i = 0; i < CAccessori.getColumnCount(); i++) {

                        Log.i(TAG, "doInBackground - PrendoColonna[" + (conta - CAccessoriLen) + "][" + i + "]");
                        if (i != (CAccessori.getColumnCount() - 1))
                            costruisciAccessori = costruisciAccessori + CAccessori.getString(CAccessori.getColumnIndex(CAccessori.getColumnName(i))) + "|";
                        else
                            costruisciAccessori = costruisciAccessori + CAccessori.getString(CAccessori.getColumnIndex(CAccessori.getColumnName(i)));
                    }

                    resAccessori.add(costruisciAccessori);
                    costruisciAccessori = "";
                    publishProgress(Integer.toString(conta));
                    conta++;
                    CAccessori.moveToNext();
                }
                //**************************************
                //**************************************

                if (CDatiLen > 0) {
                    AvviaDownload = IntestazioniUtentiOut(res);
                    if (AvviaDownload) {
                        if (CCensitiLen > 0) {
                            AvviaDownload = IntestazioniCensitiOut(resCensiti);
                            if (!AvviaDownload)
                                Toast.makeText(MyElencoAttivita.getApplicationContext(), "Attenzione impossibile inviare i dati censiti!", Toast.LENGTH_LONG).show();
                            else
                                dbHelper.DeleteCensiti(db);

                        }

                        if (CAccessoriLen > 0) {
                            AvviaDownload = IntestazioniAccessoriOut(resAccessori);
                            if (!AvviaDownload)
                                Toast.makeText(MyElencoAttivita.getApplicationContext(), "Attenzione impossibile inviare i dati accessori!", Toast.LENGTH_LONG).show();
                            else
                                dbHelper.DeleteAccessori(db);

                            dbHelper.DeleteutentiOUT(db);
                            dbHelper.Deleteutenti(db);
                        }
                    } else
                    {
                        MyElencoAttivita.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                Toast.makeText(MyElencoAttivita.getApplicationContext(), "Attenzione impossibile inviare i dati di lettura!", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } else
                    AvviaDownload = true;

                if (CAccessoriLen > 0) {
                    AvviaDownload = IntestazioniAccessoriOut(resAccessori);
                    if (!AvviaDownload)
                        Toast.makeText(MyElencoAttivita.getApplicationContext(), "Attenzione impossibile inviare i dati accessori!", Toast.LENGTH_LONG).show();
                    else
                        dbHelper.DeleteAccessori(db);
                }
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

        }
        return null;
    }


    @Override
    protected void onPostExecute(Void result) {
        Log.i(TAG, "onPostExecute");
        System.gc();


        if (dialog.isShowing()) {
            Log.i(TAG, "onPostExecute - Dismissis DIALOG");
            dialog.dismiss();
        }

        Log.i(TAG, "onPostExecute - Finita");

        if (FotoErrate > 0)
            Toast.makeText(MyElencoAttivita.getApplicationContext(), "Non sono state inviate tutte le foto, ti consigliamo di tentare la sola trasmissione foto", Toast.LENGTH_LONG).show();

        if(!SoloFoto) {
            if (AvviaDownload && !AggiornaSveltoDroid) {
                Calendar calendari = Calendar.getInstance();
                try {
                    db.execSQL("DELETE FROM AndroConfig WHERE Type='LastSync'");
                    db.execSQL("INSERT INTO AndroConfig VALUES ('" + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(calendari.getTime()) + "','LastSync')");
                } catch (Exception ex) {
                    Log.i("EXEEEEEAAA", "Ex: " + ex.getMessage().toString());
                }

                Sincro = new AsyncDownload();
                Sincro.MyElencoAttivita = MyElencoAttivita;
                Sincro.operatore = operatore;
                Sincro.SSID = SSID;
                if (GlobalConfig.equals("1") || GlobalConfig.equals("2") || GlobalConfig.equals("11") || GlobalConfig.equals("14"))
                    Sincro.service = "PrelevaInterventiV3";
                else
                    Sincro.service = "PrelevaInterventi";
                Sincro.execute();
            } else if (AggiornaSveltoDroid) {
                Toast.makeText(MyElencoAttivita.getApplicationContext(), "Attenzione devi aggiornare l'APP", Toast.LENGTH_LONG).show();
                MyElencoAttivita.finish();
            } else
                Toast.makeText(MyElencoAttivita.getApplicationContext(), "Trasmissione dati errata - Impossibile aggiornare il carico", Toast.LENGTH_LONG).show();
        }

    }


    protected void onProgressUpdate(String... progress)
    {

        if(progress[0]=="change")
        {
            Log.i(TAG, "onProgressUpdate Change");
            dialog.setMessage("Sincronizzazione dati in corso..");
            dialog.setProgress(0);
            dialog.setMax(CDati.getCount());
            dialog.show();

        }

        else if(progress[0]=="censiti")
        {
            Log.i(TAG, "onProgressUpdate Change");
            dialog.setMessage("Sincronizzazione dati in corso..");
            dialog.setProgress(0);
            dialog.setMax(CCensiti.getCount());
            dialog.show();

        }
        else if(progress[0]=="accessori")
        {
            Log.i(TAG, "onProgressUpdate Change");
            dialog.setMessage("Sincronizzazione dati in corso..");
            dialog.setProgress(0);
            dialog.setMax(CAccessori.getCount());
            dialog.show();

        }
        else
            dialog.setProgress(Integer.parseInt(progress[0]));

    }

    private String InviaFoto(byte[] foto, String nomefoto) {

        service = "InviaFile";
        this.servizio = service;
        String SOAP_ACTION = "http://tempuri.org/" + servizio;
        String METHOD_NAME = servizio;
        String NAMESPACE = "http://tempuri.org/";
        String URL;
        if(SSID.contains("cs1"))
            URL = "http://cs-storage/wsTrmLetture/wsTrmLetture.asmx?wsdl";
        else
            URL = "http://web.comerservizi.net:8083/wsTrmLetture/wsTrmLetture.asmx?wsdl";
        try {
            String appo;
            SoapObject client = null;                        // Its the client petition to the web service
            SoapPrimitive responseBody = null;                    // Contains XML content of dataset
            HttpTransportSE transport = new HttpTransportSE(URL,3000000);
            SoapSerializationEnvelope sse = null;

            sse = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            new MarshalBase64().register(sse);

            sse.dotNet = true;
            sse.implicitTypes = false;

            client = new SoapObject(NAMESPACE, METHOD_NAME);
            client.addProperty("sGlobalConfiguration", GlobalConfig);
            client.addProperty("contenuto", foto);
            client.addProperty("nome", nomefoto);
            sse.setOutputSoapObject(client);
            sse.bodyOut = client;
            transport.call(SOAP_ACTION, sse);

            // This step: get file XML
            responseBody = (SoapPrimitive)sse.getResponse();
            valResponse = responseBody.toString();


            if(valResponse.equals("ok")) {
                Log.i(TAG, "Finito invio file positivo, collegato con il WebService e l'esito è " + valResponse);
                return "ok";
            }
            else {
                Log.i(TAG, "Finito invio file negativo, collegato con il WebService e l'esito è " + valResponse);
                return "no";
            }

        } catch (SocketTimeoutException et) {

            Log.i(TAG, "Finito invio file, TIMEOUT con il WebService e l'esito è ["+et.toString()+"] - " + valResponse);
            return "no";
        } catch (Exception e) {
            Log.i(TAG, "Finito invio file, ERRORE con il WebService e l'esito è ["+e.toString()+"] - " + valResponse);
            return "no";
        }


    }


    private boolean IntestazioniUtentiOut(List<String> ListaRecord)
    {
        Log.i(TAG,"doInBackground - Inizio a provare l'invio");

        if (GlobalConfig.equals("1") || GlobalConfig.equals("2"))
            service = "InviaEsitoALV2";
        else
            service = "InviaEsitoALV3";
        this.servizio = service;
        String SOAP_ACTION = "http://tempuri.org/" + servizio;
        String METHOD_NAME = servizio;
        String NAMESPACE = "http://tempuri.org/";
        String URL;
        if(SSID.contains("cs1"))
            URL = "http://cs-storage/wsTrmLetture/wsTrmLetture.asmx?wsdl";
        else
            URL = "http://web.comerservizi.net:8083/wsTrmLetture/wsTrmLetture.asmx?wsdl";
        try {
            String appo;
            SoapObject client = null;                        // Its the client petition to the web service
            SoapPrimitive responseBody = null;                    // Contains XML content of dataset
            HttpTransportSE transport = new HttpTransportSE(URL,180000);
            SoapSerializationEnvelope sse = null;

            sse = new SoapSerializationEnvelope(SoapEnvelope.VER11);

            sse.dotNet = true;
            sse.implicitTypes = false;
            client = new SoapObject(NAMESPACE, METHOD_NAME);

            SoapObject ArrayWS = new SoapObject(NAMESPACE, "arDati");
            for(String i : ListaRecord)
                ArrayWS.addProperty("string", i);

            client.addSoapObject(ArrayWS);
            client.addProperty("sGlobalConfiguration", GlobalConfig);
            sse.setOutputSoapObject(client);
            sse.bodyOut = client;
            transport.call(SOAP_ACTION, sse);

            // This step: get file XML
            responseBody = (SoapPrimitive)sse.getResponse();
            valResponse = responseBody.toString();

            if(valResponse.contains("ok")) {
                Log.i(TAG, "Finito invio file, collegato con il WebService e l'esito è " + valResponse);
                return true;
            }
            else{
                Log.i(TAG, "Finito invio file, collegato con il WebService e l'esito è " + valResponse);
                return false;
            }


        } catch (SocketTimeoutException et) {

            Log.i(TAG, "Finito invio file, TIMEOUT con il WebService e l'esito è ["+et.toString()+"] - " + valResponse);
            return false;
        } catch (Exception e) {
            Log.i(TAG, "Finito invio file, ERRORE con il WebService e l'esito è ["+e.toString()+"] - " + valResponse);
            return false;
        }

    }


    private boolean IntestazioniCensitiOut(List<String> ListaRecord)
    {
        Log.i(TAG,"doInBackground - Inizio a provare l'invio Censiti");
        service = "InviadatiCensitiAL";
        this.servizio = service;
        String SOAP_ACTION = "http://tempuri.org/" + servizio;
        String METHOD_NAME = servizio;
        String NAMESPACE = "http://tempuri.org/";
        String URL;
        if(SSID.contains("cs1"))
            URL = "http://cs-storage/wsTrmLetture/wsTrmLetture.asmx?wsdl";
        else
            URL = "http://web.comerservizi.net:8083/wsTrmLetture/wsTrmLetture.asmx?wsdl";
        try {
            String appo;
            SoapObject client = null;                        // Its the client petition to the web service
            SoapPrimitive responseBody = null;                    // Contains XML content of dataset
            HttpTransportSE transport = new HttpTransportSE(URL,180000);
            SoapSerializationEnvelope sse = null;

            sse = new SoapSerializationEnvelope(SoapEnvelope.VER11);

            sse.dotNet = true;
            sse.implicitTypes = false;
            client = new SoapObject(NAMESPACE, METHOD_NAME);

            SoapObject ArrayWS = new SoapObject(NAMESPACE, "arDati");
            for(String i : ListaRecord)
                ArrayWS.addProperty("string", i);

            client.addSoapObject(ArrayWS);
            client.addProperty("sGlobalConfiguration", GlobalConfig);
            sse.setOutputSoapObject(client);
            sse.bodyOut = client;
            transport.call(SOAP_ACTION, sse);

            // This step: get file XML
            responseBody = (SoapPrimitive)sse.getResponse();
            valResponse = responseBody.toString();

            if(valResponse.contains("ok")) {
                Log.i(TAG, "Finito invio file Censiti, collegato con il WebService e l'esito è " + valResponse);
                return true;
            }
            else{
                Log.i(TAG, "Finito invio file Censiti, collegato con il WebService e l'esito è " + valResponse);
                return false;
            }


        } catch (SocketTimeoutException et) {

            Log.i(TAG, "Finito invio file Censiti, TIMEOUT con il WebService e l'esito è ["+et.toString()+"] - " + valResponse);
            return false;
        } catch (Exception e) {
            Log.i(TAG, "Finito invio file Censiti, ERRORE con il WebService e l'esito è ["+e.toString()+"] - " + valResponse);
            return false;
        }

    }

    private boolean IntestazioniAccessoriOut(List<String> ListaRecord)
    {
        Log.i(TAG,"doInBackground - Inizio a provare l'invio DatiAccessori");
        service = "InviadatiAccessori";
        this.servizio = service;
        String SOAP_ACTION = "http://tempuri.org/" + servizio;
        String METHOD_NAME = servizio;
        String NAMESPACE = "http://tempuri.org/";
        String URL;
        if(SSID.contains("cs1"))
            URL = "http://cs-storage/wsTrmLetture/wsTrmLetture.asmx?wsdl";
        else
            URL = "http://web.comerservizi.net:8083/wsTrmLetture/wsTrmLetture.asmx?wsdl";
        try {
            String appo;
            SoapObject client = null;                        // Its the client petition to the web service
            SoapPrimitive responseBody = null;                    // Contains XML content of dataset
            HttpTransportSE transport = new HttpTransportSE(URL,180000);
            SoapSerializationEnvelope sse = null;

            sse = new SoapSerializationEnvelope(SoapEnvelope.VER11);

            sse.dotNet = true;
            sse.implicitTypes = false;
            client = new SoapObject(NAMESPACE, METHOD_NAME);

            SoapObject ArrayWS = new SoapObject(NAMESPACE, "arDati");
            for(String i : ListaRecord)
                ArrayWS.addProperty("string", i);

            client.addSoapObject(ArrayWS);
            client.addProperty("sGlobalConfiguration", GlobalConfig);
            sse.setOutputSoapObject(client);
            sse.bodyOut = client;
            transport.call(SOAP_ACTION, sse);

            // This step: get file XML
            responseBody = (SoapPrimitive)sse.getResponse();
            valResponse = responseBody.toString();

            if(valResponse.contains("ok")) {
                Log.i(TAG, "Finito invio file DatiAccessori, collegato con il WebService e l'esito è " + valResponse);
                return true;
            }
            else{
                Log.i(TAG, "Finito invio file DatiAccessori, collegato con il WebService e l'esito è " + valResponse);
                return false;
            }


        } catch (SocketTimeoutException et) {

            Log.i(TAG, "Finito invio file DatiAccessori, TIMEOUT con il WebService e l'esito è ["+et.toString()+"] - " + valResponse);
            return false;
        } catch (Exception e) {
            Log.i(TAG, "Finito invio file DatiAccessori, ERRORE con il WebService e l'esito è ["+e.toString()+"] - " + valResponse);
            return false;
        }

    }
}
