package it.comerservizi.appsveltodroid;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class Act_SegnalazioniSpeciali extends AppCompatActivity {

    Spinner spnSegnSpeciale;
    private List<String> DatiSpinnerSegnalazione;
    private List<String> CodComerSegnalazione;
    private String CodiceUtente;
    private String IDCarico;
    private String IDAzienda;
    private String CategoriaUtente;
    private String CodBiffa;
    private EditText txtAltraSegnalazione;
    String result;
    DatabaseHelper dbHelper;
    SQLiteDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__segnalazioni_speciali);

        try {
            if(Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME) != 1)
            {
                Toast.makeText(getApplicationContext(), "ATTENZIONE HAI DISABILITATO LA DATA E ORA AUTOMATICA - L'APP NON PUO' FUNZIONARE", Toast.LENGTH_LONG).show();
                finish();
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        /* CONTROLLO DATA AUTOMATICA */

        dbHelper = new DatabaseHelper(Act_SegnalazioniSpeciali.this);
        db = dbHelper.getWritableDatabase();
        spnSegnSpeciale = (Spinner) findViewById(R.id.spnSegnSpeciale);
        Bundle extras = getIntent().getExtras();
        if(extras !=null) {
            CodiceUtente = extras.getString("CodiceUtente");
            IDCarico = extras.getString("IdCarico");
            IDAzienda = extras.getString("IdAzienda");
            CodBiffa = extras.getString("CodAzBiffa");
            CategoriaUtente = extras.getString("CategoriaUtente");
        }
        txtAltraSegnalazione = (EditText) findViewById(R.id.txtAltraSegnalazione);

    }

    public void Conferma(View view)
    {
        if(spnSegnSpeciale.getSelectedItemPosition()>0)
        {
            if(CodComerSegnalazione.get(spnSegnSpeciale.getSelectedItemPosition()).toString().trim().equals("altro")) {
                result = txtAltraSegnalazione.getText().toString().trim();
                if(result.toString().trim().length()<1)
                {
                    Toast.makeText(getApplicationContext(), "Con causale altro devi indicare una motivazione", Toast.LENGTH_LONG).show();
                    return;
                }
            }
            else
                result = CodComerSegnalazione.get(spnSegnSpeciale.getSelectedItemPosition());
            Intent returnIntent = new Intent();
            returnIntent.putExtra("result",result);
            setResult(this.RESULT_OK, returnIntent);
            Toast.makeText(getApplicationContext(), "Ricordati di scattare una foto che dimostri la segnalazione selezionata", Toast.LENGTH_LONG).show();
            finish();
        }
        else
            Toast.makeText(getApplicationContext(), "Attenzione non puoi salvare perchè non hai selezionato una causale specifica", Toast.LENGTH_LONG).show();


    }

    public void Annulla(View view)
    {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", result);
        setResult(this.RESULT_CANCELED, returnIntent);
        finish();
    }

    @Override
    public void onBackPressed() {

        Intent returnIntent = new Intent();
        setResult(this.RESULT_CANCELED, returnIntent);
        finish();
    }

    @Override
    protected void onResume(){
        super.onResume();
        dbHelper = new DatabaseHelper(Act_SegnalazioniSpeciali.this);
        db = dbHelper.getWritableDatabase();
        Cursor cursorCambioUbicaz = dbHelper.query(db, "SELECT * FROM tabtipo WHERE BiffaturaDitta LIKE '%-" + CodBiffa + "-%' AND IdDitta ='"+IDAzienda+"' AND Tipo LIKE '%" + CategoriaUtente +"'");
        CodComerSegnalazione = new ArrayList<String>();
        DatiSpinnerSegnalazione = new ArrayList<String>();
        CodComerSegnalazione.add("");
        DatiSpinnerSegnalazione.add("Seleziona Causale Specifica");
        cursorCambioUbicaz.moveToFirst();
        while (cursorCambioUbicaz.isAfterLast() == false)
        {
            CodComerSegnalazione.add(cursorCambioUbicaz.getString(cursorCambioUbicaz.getColumnIndex("CodiceDitta")));
            DatiSpinnerSegnalazione.add(cursorCambioUbicaz.getString(cursorCambioUbicaz.getColumnIndex("Descrizione")).toUpperCase());
            cursorCambioUbicaz.moveToNext();
        }

        cursorCambioUbicaz.close();
        ArrayAdapter<String> SpinnAdapBiff1 = new ArrayAdapter<String>(this, R.layout.spinner_item_cambio1, DatiSpinnerSegnalazione);
        spnSegnSpeciale.setAdapter(SpinnAdapBiff1);

        spnSegnSpeciale.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(CodComerSegnalazione.get(position).toString().trim().equals("altro"))
                    txtAltraSegnalazione.setVisibility(View.VISIBLE);
                else
                    txtAltraSegnalazione.setVisibility(View.INVISIBLE);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                txtAltraSegnalazione.setVisibility(View.INVISIBLE);
            }
        });

    }
}
