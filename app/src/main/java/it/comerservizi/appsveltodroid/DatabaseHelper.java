package it.comerservizi.appsveltodroid;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

/**
 * Created by guido.palumbo on 13/11/2015.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String TAG = "DBHelper";
    public static final String DB_NAME = "test.db";
    public static final int DB_VERS = 21;
    public static final String TABLE = "utenti";
    public static final boolean Debug = false;
    private Integer Letturista = 0;
    private final Context context;
    public ArrayList<String> Entita;

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERS);
        this.context = context;
        //PATH: /data/data/it.comerservizi.appsveltodroid/databases/test.db

        File dbFile = context.getDatabasePath(DB_NAME);
        this.exportDatabse(DB_NAME,dbFile.getAbsolutePath());

        Log.i("PATH DEL DB",dbFile.getAbsolutePath());

    }

    public void SetLetturista(String CodLett)
    {
        this.Letturista = Integer.parseInt(CodLett);
    }

    public Integer GetLetturista()
    {
        return this.Letturista;
    }

    public Cursor query(SQLiteDatabase db, String query) {
        Cursor cursor = db.rawQuery(query, null);
        if (Debug) {
            Log.d(TAG, "Executing Query: "+ query);
        }
        return cursor;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
		/* Create table Logic, once the Application has ran for the first time. */
        //Creazione Tabella Utenti e relativa pulizia
        String sql = String.format("CREATE TABLE utenti (idAzienda TEXT,  IdCarico TEXT, CodiceUtente TEXT,   Nominativo TEXT,  Cognome TEXT,  Toponimo TEXT,  Strada TEXT ,  Civico TEXT,  Isolato TEXT,  Scala TEXT,  Piano TEXT,  Interno TEXT,  Localita TEXT,  CAP TEXT,  CodicePercorso TEXT,  NoteAccesso TEXT,  MatricolaMis TEXT,  CifreContatore TEXT,  CategoriaUtente TEXT,  DataUltLettura TEXT,  UltimaLettura TEXT,  LetturaPrevista TEXT,  telefono TEXT,  IdUtente TEXT,  idLetturista TEXT,  NomeLetturista TEXT,  Terminale TEXT,  DtAffidamento TEXT,  DtRientro TEXT,  Stato TEXT,  NotaAccess TEXT,  PuntoFornitura TEXT,  NotaAccesso100 TEXT,  LetturaDa TEXT,  LetturaA TEXT,  PresCorrettore TEXT,  ClasseContatore TEXT,  dtLimiteInt1 TEXT,  dtLimiteInt2 TEXT,  Ubicazione TEXT,  Periodicita TEXT,  StatoMisuratore TEXT, Provincia TEXT, LCifreLettura TEXT, LDataLettura TEXT, LOraLettura TEXT, LNota1 TEXT, LNota2 TEXT, LNota3 TEXT, LSegnalazione TEXT, LSegnalazione1 TEXT, LCifreM TEXT, LCodTpUtilizzo TEXT, LDesTpUtilizzo TEXT, LUnAbit TEXT, LUnComm TEXT, LNota50 TEXT, LLetturaCorrettore TEXT, LLetturaCorrettore1 TEXT, LLetturaCorrettore2 TEXT, LLetturaCorrettore3 TEXT, LInfo TEXT, Lat_Ditta TEXT, Lon_Ditta TEXT, LLatitudine TEXT, LLongitudine TEXT, LAutolettura TEXT, LNoteLibere TEXT, DtLetturaTrm TEXT, OraLetturaTrm TEXT, MSegnalazione TEXT,dtLimiteInfLett TEXT, DtLimiteLettura TEXT,FotoM_misuratore TEXT, FotoV_convertitoreVm TEXT, FotoC_convertitoreVb TEXT,FotoE_convertitoreEr TEXT,FotoB_LNota1 TEXT,FotoB_LNota2 TEXT, NoteAttivita TEXT, AttivitaSpeciale TEXT, DtPrimoPassaggio TEXT, OraPrimoPassaggio TEXT, FlagNoteLetturista TEXT, LettMinimaCalcolata TEXT, LettMassimaCalcolata TEXT, FDLettura TEXT, ObbNotaUno TEXT, ObbNotaDue TEXT, Ritrasmetti TEXT, Info TEXT,dtProgrammazione TEXT, daOraLett TEXT, aOraLett TEXT, Nota3 TEXT, CONSTRAINT pKey PRIMARY KEY (CodiceUtente,  IdCarico))");
        db.execSQL(sql);
        db.execSQL("delete from utenti");
        //Creazione Tabella UtentiOUT e relativa pulizia
        sql = "";
        sql = String.format("CREATE TABLE utentiOUT (idAzienda TEXT,  IdCarico TEXT, CodiceUtente TEXT,   Nominativo TEXT,  Cognome TEXT,  Toponimo TEXT,  Strada TEXT ,  Civico TEXT,  Isolato TEXT,  Scala TEXT,  Piano TEXT,  Interno TEXT,  Localita TEXT,  CAP TEXT,  CodicePercorso TEXT,  NoteAccesso TEXT,  MatricolaMis TEXT,  CifreContatore TEXT,  CategoriaUtente TEXT,  DataUltLettura TEXT,  UltimaLettura TEXT,  LetturaPrevista TEXT,  telefono TEXT,  IdUtente TEXT,  idLetturista TEXT,  NomeLetturista TEXT,  Terminale TEXT,  DtAffidamento TEXT,  DtRientro TEXT,  Stato TEXT,  NotaAccess TEXT,  PuntoFornitura TEXT,  NotaAccesso100 TEXT,  LetturaDa TEXT,  LetturaA TEXT,  PresCorrettore TEXT,  ClasseContatore TEXT,  dtLimiteInt1 TEXT,  dtLimiteInt2 TEXT,  Ubicazione TEXT,  Periodicita TEXT,  StatoMisuratore TEXT, Provincia TEXT, LCifreLettura TEXT, LDataLettura TEXT, LOraLettura TEXT, LNota1 TEXT, LNota2 TEXT, LNota3 TEXT, LSegnalazione TEXT, LSegnalazione1 TEXT, LCifreM TEXT, LCodTpUtilizzo TEXT, LDesTpUtilizzo TEXT, LUnAbit TEXT, LUnComm TEXT, LNota50 TEXT, LLetturaCorrettore TEXT, LLetturaCorrettore1 TEXT, LLetturaCorrettore2 TEXT, LLetturaCorrettore3 TEXT, LInfo TEXT, Lat_Ditta TEXT, Lon_Ditta TEXT, LLatitudine TEXT, LLongitudine TEXT, LAutolettura TEXT, LNoteLibere TEXT, DtLetturaTrm TEXT, OraLetturaTrm TEXT, MSegnalazione TEXT, dtLimiteInfLett TEXT, DtLimiteLettura TEXT,FotoM_misuratore TEXT, FotoV_convertitoreVm TEXT, FotoC_convertitoreVb TEXT,FotoE_convertitoreEr TEXT,FotoB_LNota1 TEXT,FotoB_LNota2 TEXT, NoteAttivita TEXT, AttivitaSpeciale TEXT, DtPrimoPassaggio TEXT, OraPrimoPassaggio TEXT, FlagNoteLetturista TEXT, LettMinimaCalcolata TEXT, LettMassimaCalcolata TEXT, FDLettura TEXT, ObbNotaUno TEXT, ObbNotaDue TEXT, Ritrasmetti TEXT, Info TEXT,dtProgrammazione TEXT,daOraLett TEXT, aOraLett TEXT, Nota3 TEXT, CONSTRAINT pKey PRIMARY KEY (CodiceUtente,  IdCarico))");
        db.execSQL(sql);
        db.execSQL("delete from utentiOUT");
        //Creazione Tabella tabtipo e relativa pulizia - Esempio descrizioni di Ubicazione
        sql = "";
        sql = String.format("CREATE TABLE tabtipo (IdDitta TEXT, Tipo TEXT, CodiceComer TEXT, Descrizione TEXT, CodiceDitta TEXT, Accessibilita TEXT, BiffaturaDitta TEXT, CONSTRAINT pKey PRIMARY KEY (IdDitta,  Tipo, CodiceComer))");
        db.execSQL(sql);
        db.execSQL("delete from tabtipo");
        //Creazione Tabella ControlloNote e relativa pulizia - Esempio Tabella di validità biffature
        sql = "";
        sql = String.format("CREATE TABLE ControlloNote (IdDitta TEXT, Nota TEXT, Accessibilita TEXT, PresenzaLettura TEXT, NoteAccessibile TEXT, NoteParzAccessibile TEXT, NoteNonAccessibile TEXT, PresenzaFoto TEXT, CONSTRAINT pKey PRIMARY KEY (IdDitta,  Nota))");
        db.execSQL(sql);
        db.execSQL("delete from ControlloNote");
        //Creazione Tabella NoteLettDitta e relativa pulizia - Esempio elenco varie biffature con relative regole
        sql = "";
        sql = String.format("CREATE TABLE NoteLettDitta (IdDitta TEXT, Codice TEXT, CodiceDitta TEXT, DescrSupp TEXT, FatturataDitta TEXT, FatturataLett TEXT, ObbLettura TEXT, ObbData TEXT, ConfrontaPrecLet TEXT, ULettP TEXT, genSegnalazione TEXT, CodSegn TEXT, Descrizione TEXT, SopprimiLett TEXT, mSegnalazione TEXT, UtAssente TEXT, SoppRec TEXT, ScartaFoto TEXT, ValFlagFoto TEXT, ImpostaBiff2 TEXT, FileFoto TEXT, NumNota TEXT, SopprimiDtLett TEXT, ObbRMatricola TEXT, Accorpatore TEXT, Activity TEXT, Autolettura TEXT, RitornaLettura TEXT,  CampoNota TEXT,  ObbFoto TEXT, CONSTRAINT pKey PRIMARY KEY (IdDitta,  Codice))");
        db.execSQL(sql);
        db.execSQL("delete from NoteLettDitta");

        //Creazione Tabella TbDatiCensiti
        sql = "";
        sql = String.format("CREATE TABLE TbDatiCensiti(IdCarico TEXT, CodiceUtente TEXT, RGPSLat TEXT, RGPSLON TEXT, RGPSHH TEXT, RMatricola TEXT, RSiglaMatricola TEXT, RAnnoMatricola TEXT, RSigilloRilevato TEXT, RNotaSigillo TEXT, RNominativo TEXT, RComune TEXT, RIndirizzo TEXT, RCivico TEXT, RIsolato TEXT, RPiano TEXT, RLotto TEXT, RScala TEXT, RInterno TEXT, RNumTelefono TEXT, REstensCivico TEXT, RNumUnitAbitativa TEXT, RNumUnitCommerciali TEXT, RTipoUbicazione TEXT, RNoteUbicazione TEXT, RCifreMisuratore TEXT, CONSTRAINT pKey PRIMARY KEY(CodiceUtente, IdCarico))");
        db.execSQL(sql);
        db.execSQL("delete from TbDatiCensiti");

        sql = "";
        sql = String.format("CREATE TABLE statistiche (IdCarico TEXT, IdAzienda TEXT, LNota1 TEXT, CodiceDitta TEXT, DescrSupp TEXT, FatturataLett TEXT, Totale TEXT, TotLetture TEXT,Periodicita TEXT, Stato TEXT, LAutolettura TEXT, DtLetturaTrm TEXT, LastSync TEXT, NotaAccess TEXT)");
        db.execSQL(sql);
        db.execSQL("delete from statistiche");

        //Creazione Tabella DatiAccessori
        sql = "";
        sql = String.format("CREATE TABLE DatiAccessori (GlobalConfig TEXT, IdCarico TEXT, CodiceUtente TEXT, Etichetta TEXT, Dato TEXT, Tipo TEXT)");
        db.execSQL(sql);
        db.execSQL("delete from DatiAccessori");

        //Creazione Tabella Ripassi
        sql = "";
        sql = String.format("CREATE TABLE Ripassi (idAzienda TEXT,  IdCarico TEXT, CodiceUtente TEXT,LCifreLettura TEXT, LDataLettura TEXT, LOraLettura TEXT, LNota1 TEXT, LNota2 TEXT, LNota3 TEXT, LSegnalazione TEXT, LSegnalazione1 TEXT, LCifreM TEXT, LLetturaCorrettore TEXT, LLetturaCorrettore1 TEXT, LLetturaCorrettore2 TEXT, LLetturaCorrettore3 TEXT, LLatitudine TEXT, LLongitudine TEXT, LAutolettura TEXT, LNoteLibere TEXT, DtLetturaTrm TEXT, OraLetturaTrm TEXT, MSegnalazione TEXT, CONSTRAINT pKey PRIMARY KEY (CodiceUtente,  IdCarico))");
        db.execSQL(sql);
        db.execSQL("delete from DatiAccessori");

        //Creazione Tabella Configurazioni Generiche
        sql = "";
        sql = String.format("CREATE TABLE AndroConfig (IdConfig TEXT, Type TEXT)");
        db.execSQL(sql);
        db.execSQL("delete from AndroConfig");
        db.execSQL("INSERT INTO AndroConfig (IdConfig, Type) VALUES ('1','sGlobalConfiguration')");

        //Creazione Tabella LogErrori

        sql = "";
        sql = String.format("CREATE TABLE LogErrori (Activity TEXT,Terminale TEXT, Dipendente TEXT, Messaggio TEXT, DataOra TEXT)");
        db.execSQL(sql);
        db.execSQL("delete from LogErrori");
        if (Debug) {
            Log.d(TAG, "Create Tabelle");
        }


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Creazione Tabella Configurazioni Generiche

        if(newVersion>1) {
            try {
                String sql;
                sql = "";
                sql = String.format("CREATE TABLE IF NOT EXISTS AndroConfig (IdConfig TEXT, Type TEXT)");
                db.execSQL(sql);
                db.execSQL("delete from AndroConfig");
                db.execSQL("INSERT INTO AndroConfig (IdConfig, Type) VALUES ('1','sGlobalConfiguration')");
                if (Debug) {
                    Log.d(TAG, "Create Tabelle");
                }
            }catch(Exception ex)
            {
                Log.i(TAG,"No aggiornamento VER > 1");
            }
        }
        if(newVersion>2)
        {
            String sql;
            sql = "";
            sql = String.format("ALTER TABLE utenti ADD COLUMN dtLimiteInfLett TEXT");
            try
            {

                db.execSQL(sql);
            }
            catch (Exception e)
            {
                Log.i(TAG,"No aggiornamento VER > 2");
            }
            sql = String.format("ALTER TABLE utenti ADD COLUMN DtLimiteLettura TEXT");
            try
            {

                db.execSQL(sql);
            }
            catch (Exception e)
            {
                Log.i(TAG,"No aggiornamento VER > 2");
            }
            sql = String.format("ALTER TABLE utentiOUT ADD COLUMN dtLimiteInfLett TEXT");
            try
            {

                db.execSQL(sql);
            }
            catch (Exception e)
            {
                Log.i(TAG,"No aggiornamento VER > 2");
            }
            sql = String.format("ALTER TABLE utentiOUT ADD COLUMN DtLimiteLettura TEXT;");
            try
            {

                db.execSQL(sql);
            }
            catch (Exception e)
            {
                Log.i(TAG,"No aggiornamento VER > 2");
            }

        }
        if(newVersion>3)
        {

            try {
                String sql;
                sql = "";
                sql = String.format("ALTER TABLE utenti ADD COLUMN FotoM_misuratore TEXT");
                db.execSQL(sql);
                sql = String.format("ALTER TABLE utenti ADD COLUMN FotoV_convertitoreVm TEXT");
                db.execSQL(sql);
                sql = String.format("ALTER TABLE utenti ADD COLUMN FotoC_convertitoreVb TEXT");
                db.execSQL(sql);
                sql = String.format("ALTER TABLE utenti ADD COLUMN FotoE_convertitoreEr TEXT;");
                db.execSQL(sql);
                sql = String.format("ALTER TABLE utenti ADD COLUMN FotoB_LNota1 TEXT;");
                db.execSQL(sql);
                sql = String.format("ALTER TABLE utenti ADD COLUMN FotoB_LNota2 TEXT;");
                db.execSQL(sql);
                sql = String.format("ALTER TABLE utentiOUT ADD COLUMN FotoM_misuratore TEXT");
                db.execSQL(sql);
                sql = String.format("ALTER TABLE utentiOUT ADD COLUMN FotoV_convertitoreVm TEXT");
                db.execSQL(sql);
                sql = String.format("ALTER TABLE utentiOUT ADD COLUMN FotoC_convertitoreVb TEXT");
                db.execSQL(sql);
                sql = String.format("ALTER TABLE utentiOUT ADD COLUMN FotoE_convertitoreEr TEXT;");
                db.execSQL(sql);
                sql = String.format("ALTER TABLE utentiOUT ADD COLUMN FotoB_LNota1 TEXT;");
                db.execSQL(sql);
                sql = String.format("ALTER TABLE utentiOUT ADD COLUMN FotoB_LNota2 TEXT;");
                db.execSQL(sql);
                sql = String.format("ALTER TABLE tabtipo ADD COLUMN Accessibilita TEXT;");
                db.execSQL(sql);
                sql = String.format("ALTER TABLE tabtipo ADD COLUMN BiffaturaDitta TEXT;");
                db.execSQL(sql);
            }catch(Exception ex)
            {
                Log.i(TAG,"No aggiornamento VER > 3");
            }

        }

        if(newVersion>4)
        {
            try {
                String sql;
                sql = "";
                sql = String.format("CREATE TABLE statistiche (IdCarico TEXT, IdAzienda TEXT, LNota1 TEXT, CodiceDitta TEXT, DescrSupp TEXT, FatturataLett TEXT, Totale TEXT, TotLetture TEXT)");
                db.execSQL(sql);
                db.execSQL("delete from statistiche");
                Log.i(TAG, "aggiornamento VER > 4");
            }catch(Exception ex)
            {
                Log.i(TAG, "newVersion > 4 " + ex.toString());
            }

        }
        if(newVersion>5)
        {
            Log.i(TAG, "Prova aggiornamento VER > 5");
            try {
                String sql;
                sql = String.format("ALTER TABLE utenti ADD COLUMN NoteAttivita TEXT");
                db.execSQL(sql);
                sql = "";
                sql = String.format("ALTER TABLE utentiOUT ADD COLUMN NoteAttivita TEXT");
                db.execSQL(sql);
                sql = "";
                sql = String.format("ALTER TABLE TbDatiCensiti ADD COLUMN RCifreMisuratore TEXT");
                db.execSQL(sql);
                Log.i(TAG, "aggiornamento VER > 5");
            }catch(Exception ex)
            {
                Log.i(TAG,"No aggiornamento VER > 5");
            }
        }
        if(newVersion>6)
        {
            Log.i(TAG, "Prova aggiornamento VER > 6");
            try {
                String sql;
                sql = String.format("ALTER TABLE statistiche ADD COLUMN Periodicita TEXT");
                db.execSQL(sql);
                sql = "";
                sql = String.format("ALTER TABLE statistiche ADD COLUMN Stato TEXT");
                db.execSQL(sql);
                sql = "";
                sql = String.format("ALTER TABLE statistiche ADD COLUMN LAutolettura TEXT");
                db.execSQL(sql);
                sql = "";
                sql = String.format("ALTER TABLE statistiche ADD COLUMN DtLetturaTrm TEXT");
                db.execSQL(sql);
                sql = "";
                sql = String.format("ALTER TABLE statistiche ADD COLUMN LastSync TEXT");
                db.execSQL(sql);
                sql = "";
                sql = String.format("ALTER TABLE statistiche ADD COLUMN NotaAccess TEXT");
                db.execSQL(sql);
                Log.i(TAG, "aggiornamento VER > 6");
            }catch(Exception ex)
            {
                Log.i(TAG,"No aggiornamento VER > 6");
            }
        }
        if(newVersion>7)
        {
            Log.i(TAG, "Prova aggiornamento VER > 7");
            try {
                String sql;
                sql = String.format("ALTER TABLE utenti ADD COLUMN AttivitaSpeciale TEXT");
                db.execSQL(sql);
                sql = "";
                sql = String.format("ALTER TABLE utentiOUT ADD COLUMN AttivitaSpeciale TEXT");
                db.execSQL(sql);

                Log.i(TAG, "aggiornamento VER > 7");
            }catch(Exception ex)
            {
                Log.i(TAG,"No aggiornamento VER > 7");
            }
        }
        if(newVersion>8)
        {
            Log.i(TAG, "Prova aggiornamento VER > 8");
            try {
                String sql;

                sql = String.format("ALTER TABLE utenti ADD COLUMN OraPrimoPassaggio TEXT");
                db.execSQL(sql);
                sql = "";
                sql = String.format("ALTER TABLE utenti ADD COLUMN DtPrimoPassaggio TEXT");
                db.execSQL(sql);
                sql = "";
                sql = String.format("ALTER TABLE utenti ADD COLUMN FlagNoteLetturista TEXT");
                db.execSQL(sql);
                sql = "";
                sql = String.format("ALTER TABLE utenti ADD COLUMN LettMinimaCalcolata TEXT");
                db.execSQL(sql);
                sql = "";
                sql = String.format("ALTER TABLE utenti ADD COLUMN LettMassimaCalcolata TEXT");
                db.execSQL(sql);
                sql = "";
                sql = String.format("ALTER TABLE utentiOUT ADD COLUMN OraPrimoPassaggio TEXT");
                db.execSQL(sql);
                sql = "";
                sql = String.format("ALTER TABLE utentiOUT ADD COLUMN DtPrimoPassaggio TEXT");
                db.execSQL(sql);
                sql = "";
                sql = String.format("ALTER TABLE utentiOUT ADD COLUMN FlagNoteLetturista TEXT");
                db.execSQL(sql);
                sql = String.format("ALTER TABLE utentiOUT ADD COLUMN LettMinimaCalcolata TEXT");
                db.execSQL(sql);
                sql = "";
                sql = String.format("ALTER TABLE utentiOUT ADD COLUMN LettMassimaCalcolata TEXT");
                db.execSQL(sql);

                //Creazione Tabella DatiAccessori
                sql = "";
                sql = String.format("CREATE TABLE DatiAccessori (GlobalConfig TEXT, IdCarico TEXT, CodiceUtente TEXT, Etichetta TEXT, Dato TEXT, Tipo TEXT)");
                db.execSQL(sql);
                db.execSQL("delete from DatiAccessori");

                Log.i(TAG, "aggiornamento VER > 8");
            }catch(Exception ex)
            {
                Log.i(TAG,"No aggiornamento VER > 8");
            }

        }

        if(newVersion>9)
        {
            Log.i(TAG, "Prova aggiornamento VER > 9");
            try {
                String sql;
                sql = "";
                sql = String.format("CREATE TABLE Ripassi (idAzienda TEXT,  IdCarico TEXT, CodiceUtente TEXT,LCifreLettura TEXT, LDataLettura TEXT, LOraLettura TEXT, LNota1 TEXT, LNota2 TEXT, LNota3 TEXT, LSegnalazione TEXT, LSegnalazione1 TEXT, LCifreM TEXT, LLetturaCorrettore TEXT, LLetturaCorrettore1 TEXT, LLetturaCorrettore2 TEXT, LLetturaCorrettore3 TEXT, LLatitudine TEXT, LLongitudine TEXT, LAutolettura TEXT, LNoteLibere TEXT, DtLetturaTrm TEXT, OraLetturaTrm TEXT, MSegnalazione TEXT, CONSTRAINT pKey PRIMARY KEY (CodiceUtente,  IdCarico))");
                db.execSQL(sql);
                db.execSQL("delete from DatiAccessori");
            }catch(Exception ex)
            {
                Log.i(TAG,"No aggiornamento VER > 9");

            }
        }
        if(newVersion>10)
        {

            Log.i(TAG, "Prova aggiornamento VER > 10");
            try {
                String sql;
                sql = "";
                sql = String.format("ALTER TABLE utenti ADD COLUMN FDLettura TEXT");
                db.execSQL(sql);
                sql = "";
                sql = String.format("ALTER TABLE utentiOUT ADD COLUMN FDLettura TEXT");
                db.execSQL(sql);
            }catch(Exception ex)
            {
                Log.i(TAG,"No aggiornamento VER > 10");
            }

        }

        if(newVersion>11)
        {
            Log.i(TAG, "Prova aggiornamento VER > 11");
            try {
                db.execSQL("DELETE FROM TbDatiCensiti WHERE IdCarico < 86");
                db.execSQL("DELETE FROM DatiAccessori WHERE IdCarico < 86");
                String sql;
                sql = "";
                sql = String.format("CREATE TABLE LogErrori (Activity TEXT,idConfig TEXT, Dipendente TEXT, Messaggio TEXT, DataOra TEXT)");
                db.execSQL(sql);
                db.execSQL("delete from LogErrori");
            }catch(Exception ex)
            {
                Log.i(TAG,"No aggiornamento VER > 11");
            }
        }
        if(newVersion>12)
        {
            Log.i(TAG, "Prova aggiornamento VER > 12");
            try {
                String sql;
                sql = String.format("ALTER TABLE NoteLettDitta ADD COLUMN RitornaLettura TEXT");
                db.execSQL(sql);
            }catch(Exception ex)
            {
                Log.i(TAG,"No aggiornamento VER > 12");
            }
        }

        if(newVersion>13)
        {
            Log.i(TAG, "Prova aggiornamento VER > 13");
            try{
                String sql;
                sql = String.format("CREATE TABLE tabtipo_new (IdDitta TEXT, Tipo TEXT, CodiceComer TEXT, Descrizione TEXT, CodiceDitta TEXT, Accessibilita TEXT, BiffaturaDitta TEXT, CONSTRAINT pKey PRIMARY KEY (IdDitta,  Tipo, CodiceComer))");
                db.execSQL(sql);
                db.execSQL("delete from tabtipo_new");
                db.execSQL("INSERT INTO tabtipo_new SELECT * FROM tabtipo");
                db.execSQL("DROP TABLE tabtipo");
                db.execSQL("ALTER TABLE tabtipo_new RENAME TO tabtipo");

            }catch(Exception ex)
            {
                Log.i(TAG,"No aggiornamento VER > 13");

            }
        }
        if(newVersion>14)
        {
            Log.i(TAG, "Prova aggiornamento VER > 14");
            try{
                String sql;
                sql = String.format("ALTER TABLE NoteLettDitta ADD COLUMN CampoNota TEXT");
                db.execSQL(sql);
                sql  = "";
                sql = String.format("ALTER TABLE NoteLettDitta ADD COLUMN ObbFoto TEXT");
                db.execSQL(sql);
                sql  = "";
                sql = String.format("ALTER TABLE utenti ADD COLUMN ObbNotaUno TEXT");
                db.execSQL(sql);
                sql  = "";
                sql = String.format("ALTER TABLE utenti ADD COLUMN ObbNotaDue TEXT");
                db.execSQL(sql);
                sql  = "";
                sql = String.format("ALTER TABLE utentiOUT ADD COLUMN ObbNotaUno TEXT");
                db.execSQL(sql);
                sql  = "";
                sql = String.format("ALTER TABLE utentiOUT ADD COLUMN ObbNotaDue TEXT");
                db.execSQL(sql);

            }catch(Exception ex)
            {
                Log.i(TAG,"No aggiornamento VER > 14");

            }
        }
        if(newVersion>15)
        {
            Log.i(TAG, "Prova aggiornamento VER > 15");
            try{
                String sql;
                sql  = "";
                sql = String.format("ALTER TABLE utenti ADD COLUMN Ritrasmetti TEXT default 'S'");
                db.execSQL(sql);
                sql  = "";
                sql = String.format("ALTER TABLE utentiOUT ADD COLUMN Ritrasmetti TEXT default 'S'");
                db.execSQL(sql);

            }catch(Exception ex)
            {
                Log.i(TAG,"No aggiornamento VER > 15");

            }
        }
        if(newVersion>16)
        {
            Log.i(TAG, "Prova aggiornamento VER > 15");
            try{
                String sql;
                sql  = "";
                sql = String.format("ALTER TABLE utenti ADD COLUMN Ritrasmetti TEXT default 'S'");
                db.execSQL(sql);
                sql  = "";
                sql = String.format("ALTER TABLE utentiOUT ADD COLUMN Ritrasmetti TEXT default 'S'");
                db.execSQL(sql);

            }catch(Exception ex)
            {
                Log.i(TAG,"No aggiornamento VER > 15");

            }
        }
        if(newVersion>17)
        {
            Log.i(TAG, "Prova aggiornamento VER > 17");
            try{
                String sql;
                sql  = "";
                sql = String.format("ALTER TABLE utenti ADD COLUMN Info TEXT default 'S'");
                db.execSQL(sql);
                sql  = "";
                sql = String.format("ALTER TABLE utentiOUT ADD COLUMN Info TEXT default 'S'");
                db.execSQL(sql);

            }catch(Exception ex)
            {
                Log.i(TAG,"No aggiornamento VER > 17");

            }
        }
        if(newVersion>18)
        {
            Log.i(TAG, "Prova aggiornamento VER > 18");
            try {
                String sql;
                sql = "";
                sql = String.format("ALTER TABLE Utenti ADD COLUMN dtProgrammazione TEXT");
                db.execSQL(sql);
                sql = "";
                sql = String.format("ALTER TABLE UtentiOut ADD COLUMN dtProgrammazione TEXT");
                db.execSQL(sql);
                sql = "";
                sql = String.format("ALTER TABLE Utenti ADD COLUMN daOraLett TEXT");
                db.execSQL(sql);
                sql = "";
                sql = String.format("ALTER TABLE UtentiOut ADD COLUMN daOraLett TEXT");
                db.execSQL(sql);
                sql = "";
                sql = String.format("ALTER TABLE Utenti ADD COLUMN aOraLett TEXT");
                db.execSQL(sql);
                sql = "";
                sql = String.format("ALTER TABLE UtentiOut ADD COLUMN aOraLett TEXT");
                db.execSQL(sql);
                sql = "";
                sql = String.format("ALTER TABLE Utenti ADD COLUMN Nota3 TEXT");
                db.execSQL(sql);
                sql = "";
                sql = String.format("ALTER TABLE UtentiOut ADD COLUMN Nota3 TEXT");
                db.execSQL(sql);
            }catch(Exception ex)
            {
                Log.i(TAG,"No aggiornamento VER > 18");
            }
        }
    }

    public String SimpleQuery(SQLiteDatabase db, String Qquery)
    {
        try
        {
            db.execSQL(Qquery);
            return "Ok";

        }catch (Exception e) {
            return e.getMessage() + " - " + e.getCause() + " - " + e.toString();
        }

    }

    public void Deleteutenti(SQLiteDatabase db)
    {
        db.execSQL("delete from utenti");
        if (Debug) {
            Log.d(TAG, "Cancellato utenti");
        }
    }
    public void DeleteCensiti(SQLiteDatabase db)
    {
        db.execSQL("delete from TbDatiCensiti");
        if (Debug) {
            Log.d(TAG, "Cancellato TbDatiCensiti");
        }
    }
    public void DeleteAccessori(SQLiteDatabase db)
    {
        db.execSQL("delete from DatiAccessori");
        if (Debug) {
            Log.d(TAG, "Cancellato DatiAccessori");
        }
    }
    public void DeleteutentiOUT(SQLiteDatabase db)
    {
        db.execSQL("delete from utentiOUT");
        if (Debug) {
            Log.d(TAG, "Cancellato utenti");
        }
    }
    public void Deletetabtipo(SQLiteDatabase db)
    {
        db.execSQL("delete from tabtipo");
        if (Debug) {
            Log.d(TAG, "Cancellato tabtipo");
        }
    }
    public void DeleteControlloNote(SQLiteDatabase db)
    {
        db.execSQL("delete from ControlloNote");
        if (Debug) {
            Log.d(TAG, "Cancellato ControlloNote");
        }
    }
    public void DeleteNoteLettDitta(SQLiteDatabase db)
    {
        db.execSQL("delete from NoteLettDitta");
        if (Debug) {
            Log.d(TAG, "Cancellato NoteLettDitta");
        }
    }

    public void exportDatabse(String databaseName,String CurrentDB) {
        try {
            //File sd = new File(context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS) + File.separator + "BCKPSVLDRD_DB");
            //File data = new File(context.getFilesDir() + File.separator + "BCKPSVLDRD_DB");

            File sd = new File(context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS) + File.separator + "BCKPSVLDRD_DB");
            File data = new File(context.getFilesDir() + File.separator + "BCKPSVLDRD_DB");
            data.setReadable(Boolean.TRUE);
            data.setWritable(Boolean.TRUE);
            sd.setReadable(Boolean.TRUE);
            sd.setWritable(Boolean.TRUE);
            if (!sd.exists()) {
                if (!sd.mkdirs()) {
                    Log.i("DB", "Impossibile creare la folder");
                }
            }
            if (!data.exists()) {
                if (!data.mkdirs()) {
                    Log.i("DB", "Impossibile creare la folder");
                }
            }
            if (sd.canWrite()) {
                String currentDBPath = CurrentDB;
                String backupDBPath = sd + File.separator + "backupnameSveltoDroid.db";
                File currentDB = new File(currentDBPath);
                File backupDB = new File(backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                    Log.i("DB", "BackupEffettuato");
                }
                else
                    Log.i("DB", "CurrentDB non esiste in: " + currentDBPath);
            }
            else
                Log.i("DB", "la sd non è scrivibile");

            if (data.canWrite()) {
                String currentDBPath = CurrentDB;
                String backupDBPath = data + File.separator + "backupnameSveltoDroid.db";
                File currentDB = new File(currentDBPath);
                File backupDB = new File(backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                    Log.i("DB", "BackupEffettuato");
                }
                else
                    Log.i("DB", "CurrentDB non esiste in: " + currentDBPath);
            }
            else
                Log.i("DB", "la sd non è scrivibile");
        } catch (Exception e) {
            Log.i("DB", "BackupErrato: " + e.toString());

        }
    }

}