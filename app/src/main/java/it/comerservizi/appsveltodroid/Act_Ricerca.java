package it.comerservizi.appsveltodroid;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;
import java.util.List;

public class Act_Ricerca extends AppCompatActivity {

    private List<String> ElencoRicerca;
    private GPClass GPC;
    private String GiornoAccesso;
    private Spinner cmbCampi;
    private String CampoRicerca;
    private String CSSID;
    private String ComuneSelezionato;
    private String CaricoSelezionato;
    private TextView txtCaricoSelezionato,txtComuneSelezionato;
    public TextView txtNAttivita;
    private EditText txtFind;
    private Button btTrova;
    private AsyncLoader ElementiAs;
    private Switch swAttivitaConcluse;
    public ListView listView;
    String IDLett="";
    public Integer LastPosition = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act__ricerca);
        GPC = new GPClass();
        ComuneSelezionato = "";
        CaricoSelezionato = "";
        txtCaricoSelezionato = (TextView) findViewById(R.id.txtCaricoSelezionato);
        txtComuneSelezionato = (TextView) findViewById(R.id.txtComuneSelezionato);
        txtNAttivita = (TextView) findViewById(R.id.txtNumAttivita);
        cmbCampi = (Spinner) findViewById(R.id.cmbCampi);
        txtFind = (EditText) findViewById(R.id.txtFind);
        btTrova = (Button) findViewById(R.id.btTrova);
        swAttivitaConcluse = (Switch) findViewById(R.id.swAttivitaConcluse);
        swAttivitaConcluse.setChecked(false);
        Intent I_ActElencoAttivita = getIntent();
        IDLett = I_ActElencoAttivita.getExtras().getString("IDLett");
        GiornoAccesso = I_ActElencoAttivita.getExtras().getString("GiornoAccesso");
        ComuneSelezionato = I_ActElencoAttivita.getExtras().getString("ComuneSelezionato");
        CaricoSelezionato = I_ActElencoAttivita.getExtras().getString("CaricoSelezionato");
        CSSID = I_ActElencoAttivita.getExtras().getString("SSID");
        listView=(ListView)findViewById(R.id.listView1);

        if(ComuneSelezionato.contains("nessuno"))
            txtComuneSelezionato.setText("Tutti");
        else
            txtComuneSelezionato.setText(ComuneSelezionato);

        if(CaricoSelezionato.contains("nessuno"))
            txtCaricoSelezionato.setText("Tutti");
        else
            txtCaricoSelezionato.setText(CaricoSelezionato);



        ElencoRicerca = new ArrayList<String>();
        ElencoRicerca.add("Seleziona il campo di ricerca");
        ElencoRicerca.add("Indirizzo");
        ElencoRicerca.add("Comune");
        ElencoRicerca.add("Nominativo");
        ElencoRicerca.add("Matricola");
        ElencoRicerca.add("Info Aggiuntive Posizione");
        ElencoRicerca.add("Codice Utente / PDR");
        ElencoRicerca.add("Solo Clienti Assenti");
        CampoRicerca = "nulla";

        ArrayAdapter<String> SpinnAdapCarico = new ArrayAdapter<String>(this, R.layout.spinner_item1, ElencoRicerca);
        cmbCampi.setAdapter(SpinnAdapCarico);

        cmbCampi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    if(cmbCampi.getSelectedItem().toString()=="Solo Clienti Assenti" && !swAttivitaConcluse.isChecked())
                    {
                        cmbCampi.setSelection(0);
                        CampoRicerca = "nulla";
                        txtFind.setEnabled(false);
                        btTrova.setEnabled(false);
                        Toast.makeText(getApplicationContext(), "I Clienti assenti possono essere cercati solo per le attività concluse", Toast.LENGTH_LONG).show();
                    }
                    else if(cmbCampi.getSelectedItem().toString()=="Solo Clienti Assenti" && swAttivitaConcluse.isChecked())
                    {
                        CampoRicerca = cmbCampi.getSelectedItem().toString();
                        txtFind.setEnabled(false);
                        btTrova.setEnabled(true);
                    }
                    else {
                        CampoRicerca = cmbCampi.getSelectedItem().toString();
                        txtFind.setEnabled(true);
                        btTrova.setEnabled(true);
                    }
                } else {
                    CampoRicerca = "nulla";
                    txtFind.setEnabled(false);
                    btTrova.setEnabled(false);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                CampoRicerca = "nulla";
                txtFind.setEnabled(false);
                btTrova.setEnabled(false);
            }
        });
    }

    public void ControllaCheck(View view)
    {
        if(cmbCampi.getSelectedItem().toString()=="Solo Clienti Assenti" && !swAttivitaConcluse.isChecked())
        {
            cmbCampi.setSelection(0);
            CampoRicerca = "nulla";
            txtFind.setEnabled(false);
            btTrova.setEnabled(false);
            Toast.makeText(getApplicationContext(), "I Clienti assenti possono essere cercati solo per le attività concluse", Toast.LENGTH_LONG).show();
        }
        else if(cmbCampi.getSelectedItem().toString()=="Solo Clienti Assenti" && swAttivitaConcluse.isChecked())
        {
            CampoRicerca = cmbCampi.getSelectedItem().toString();
            txtFind.setEnabled(false);
            btTrova.setEnabled(true);
        }
        else {
            CampoRicerca = cmbCampi.getSelectedItem().toString();
            txtFind.setEnabled(true);
            btTrova.setEnabled(true);
        }

    }

    @Override
    protected void onResume()
    {
        super.onResume();

        GPC.LastSync(getApplicationContext(), this);
        if(GPC.ControlloGiornoAccesso(getApplicationContext(), this, GiornoAccesso)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                this.finishAffinity();
            }
        }
        else
        {
            if (txtFind.getText().toString().length() > 0 && !CampoRicerca.contains("nulla")) {
                btTrova.setEnabled(false);
                ElementiAs = new AsyncLoader(Act_Ricerca.this, "RicercaDati");
                ElementiAs.Search4 = CampoRicerca;
                ElementiAs.IDLett = IDLett;
                ElementiAs.LastPosition = LastPosition;
                ElementiAs.ActRicerca = this;
                ElementiAs.SSID = CSSID;
                ElementiAs.Search4How = txtFind.getText().toString();
                ElementiAs.Search4Concluse = swAttivitaConcluse.isChecked();
                ElementiAs.Search4Carico = CaricoSelezionato;
                ElementiAs.Search4Comune = ComuneSelezionato;
                ElementiAs.execute();
                btTrova.setEnabled(true);

            }
            else if(cmbCampi.getSelectedItem().toString()=="Solo Clienti Assenti" && swAttivitaConcluse.isChecked())
            {
                btTrova.setEnabled(false);
                ElementiAs = new AsyncLoader(Act_Ricerca.this, "RicercaDati");
                ElementiAs.Search4 = CampoRicerca;
                ElementiAs.IDLett = IDLett;
                ElementiAs.LastPosition = LastPosition;
                ElementiAs.ActRicerca = this;
                ElementiAs.SSID = CSSID;
                ElementiAs.Search4How = txtFind.getText().toString();
                ElementiAs.Search4Concluse = swAttivitaConcluse.isChecked();
                ElementiAs.Search4Carico = CaricoSelezionato;
                ElementiAs.Search4Comune = ComuneSelezionato;
                ElementiAs.execute();
                btTrova.setEnabled(true);
            }
        }
        //if(LastPosition>0)
        //    listView.setSelection(LastPosition);
        Crashlytics.log(null);
        Crashlytics.log("Operatore: " + IDLett );
    }
    public void AvviaRicerca(View view)
    {
        if(txtFind.getText().toString().length()>0 && !CampoRicerca.contains("nulla"))
        {
            InputMethodManager imm = (InputMethodManager) getSystemService(getApplicationContext().INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

            btTrova.setEnabled(false);
            ElementiAs = new AsyncLoader(Act_Ricerca.this, "RicercaDati");
            ElementiAs.Search4 = CampoRicerca;
            ElementiAs.IDLett = IDLett;
            ElementiAs.LastPosition = LastPosition;
            ElementiAs.ActRicerca = this;
            ElementiAs.SSID = CSSID;
            ElementiAs.Search4How = txtFind.getText().toString();
            ElementiAs.Search4Concluse = swAttivitaConcluse.isChecked();
            ElementiAs.Search4Carico = CaricoSelezionato;
            ElementiAs.Search4Comune = ComuneSelezionato;
            ElementiAs.execute();
            btTrova.setEnabled(true);

        }
        else if(cmbCampi.getSelectedItem().toString()=="Solo Clienti Assenti" && swAttivitaConcluse.isChecked())
        {
            btTrova.setEnabled(false);
            ElementiAs = new AsyncLoader(Act_Ricerca.this, "RicercaDati");
            ElementiAs.Search4 = CampoRicerca;
            ElementiAs.IDLett = IDLett;
            ElementiAs.LastPosition = LastPosition;
            ElementiAs.ActRicerca = this;
            ElementiAs.SSID = CSSID;
            ElementiAs.Search4How = txtFind.getText().toString();
            ElementiAs.Search4Concluse = swAttivitaConcluse.isChecked();
            ElementiAs.Search4Carico = CaricoSelezionato;
            ElementiAs.Search4Comune = ComuneSelezionato;
            ElementiAs.execute();
            btTrova.setEnabled(true);
        }
        else if(txtFind.getText().toString().length()<1)
            Toast.makeText(getApplicationContext(), "Devi inserire un valore di ricerca", Toast.LENGTH_LONG).show();
        else if(CampoRicerca.contains("nulla"))
            Toast.makeText(getApplicationContext(), "Devi selezionare un campo di ricerca", Toast.LENGTH_LONG).show();
        else
            Toast.makeText(getApplicationContext(), "Errore sconosciuto [R001]", Toast.LENGTH_LONG).show();
    }
}
