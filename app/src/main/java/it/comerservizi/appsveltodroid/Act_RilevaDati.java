package it.comerservizi.appsveltodroid;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.ComponentCallbacks2;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class Act_RilevaDati extends AppCompatActivity {

    GPSHelper gps;
    GPClass GPC;
    String CodiceUtente;
    String IDCarico;
    String FDLettura;
    String IDAzienda;
    String CategoriaUtente;
    RadioButton rdAutolettura;
    CheckBox rdSigillatura;
    RadioButton rdWhatsapp;
    Button btnFotoVC;
    Button btnFotoB1;
    Button btnFotoB2;
    Button btnCorrettore;
    Button btnProgramma;
    TextView lblMatricola;
    TextView lblCorrettore;
    TextView lblNoteLetturista;
    TextView DescrContatore;
    TextView lblAttivitaSpeciale;
    TextView lblCategoriaUtente;
    ImageView imgCategoriaUtente;
    EditText txtCifreMisuratore;
    EditText txtNoteBiffUno;
    EditText txtNoteBiffDue;
    EditText txtLetturaMis;
    EditText txtNoteLetturista;
    DatabaseHelper dbHelper;
    SQLiteDatabase db;
    String IDDitta;
    String DatiStampaIndirizzo;
    String DatiStampaNominativo;
    String DatiStampaMatricola;
    String DatiStampaLocalita;
    String DatiStampaCodiceUtente;
    Spinner spnBiffUno;
    Spinner spnBiffDue;
    Spinner spnBiffAccessibilita;
    LinearLayout accessibilitaLayout;
    private List<String> SpinnerBiff1;
    private List<String> SpinnerBiff2;
    private List<String> SpinnerAccessibilita;
    ArrayList<String> BiffToActivity;
    ArrayList<String> BiffToActivityDue;
    ArrayList<String> VerAutoLettura;
    ArrayList<String> VerAutoLetturaDue;
    ArrayList<String> VerLetturaUno;
    ArrayList<String> VerLetturaDue;
    ArrayList<String> CodComerBiffatura;
    ArrayList<String> CodAziendaBiffatura;
    ArrayList<String> CodComerBiffaturaDue;
    ArrayList<String> CodAziendaBiffaturaDue;
    ArrayList<String> ObbFotoNotaUno, ObbFotoNotaDue;
    HashMap<String,String> ControlloNoteAccessibili,ControlloNoteParziali,ControlloNoteNonAccessibili;
    HashMap<String,Class> OpenSpecialActivity;
    String DaQualeBiff;
    String IDLett;
    String Latitudine;
    String Longitudine;
    String DefLat, DefLong;
    String FlagDonato;
    String DataAttivita;
    String AttivitaSpeciale;
    String DataAttivitaTrm;
    String DataDAFlusso, DataAFlusso;
    String Accessibilita;
    String OrigToponimo, OrigStrada, OrigCivico;
    List<String> BiffatureAccessibili;
    List<String> BiffatureParziali;
    List<String> BiffatureNonAccessibili;
    List<String> BiffatureSenzaAccessibilita;
    String ObbNotaUno;
    String ObbNotaDue;
    String OraAttivita;
    String OraAttivitaTrm;
    String LetturaVM, LetturaVB, LetturaVE;
    Boolean ObbligoCorrettore = false;
    Boolean ControlloCorrettore = false;
    Boolean ObbligoLettura = true;
    String CodBiffUnoCS, CodBiffDueCS, CodBiffUnoAZ = "", CodBiffDueAZ = "";
    Integer CountPhotoVC, CountPhotoVM, CountPhotoVB, CountPhotoVE, CountPhotoB1, CountPhotoB2,CountPhotoP1;
    Boolean Integrato = false;
    Boolean Censimento = false;
    Calendar CalendarCert;
    boolean statusOfGPS;

    private String CensNominativo;
    private String CensMatricola;
    private String CensSiglaMatricola;
    private String CensAnnoMatricola;
    private String CensImpegnativo;
    private String CensCifreMisuratore;
    private String CensIndirizzo;
    private String CensCivico;
    private String CensBarra;
    private String CensScala;
    private String CensPiano;
    private String CensInterno;
    private String CensComune;
    private String CensNotaSigillo;
    private String CensSigillo;
    private String CensTelefono;
    private String GlobalConfig;
    private String SMATOldNota;
    private String MSegnalazione = "";
    private String LetturaMinimaCalcolata,LetturaMassimaCalcolata;
    private String UltimaLettura, LetturaPrevista;
    private Boolean ValoreLetturaInseritaControllato = false;
    private String DataPrimoPassaggio;
    private String RilascioStampa;
    private Boolean ConfermatoDato;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("response", "Act_RilevaDati onCreate");
        setContentView(R.layout.activity_act__rileva_dati);
        onTrimMemory(ComponentCallbacks2.TRIM_MEMORY_COMPLETE);
        onTrimMemory(ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN);

        ConfermatoDato = false;
        /* CONTROLLO DATA AUTOMATICA */
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                if(Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME) != 1)
                {
                    Toast.makeText(getApplicationContext(), "ATTENZIONE HAI DISABILITATO LA DATA E ORA AUTOMATICA - L'APP NON PUO' FUNZIONARE", Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        /* CONTROLLO DATA AUTOMATICA */
        AttivitaSpeciale = "NESSUNA";
        GPC = new GPClass();
        gps = new GPSHelper();
        gps.GPSContesto(getApplicationContext());
        rdAutolettura = (RadioButton) findViewById(R.id.rdAutolettura);
        rdSigillatura = (CheckBox) findViewById(R.id.rdSigillatura);
        rdWhatsapp = (RadioButton) findViewById(R.id.rdWhatsapp);
        lblMatricola = (TextView) findViewById(R.id.lblMatricola);
        lblAttivitaSpeciale = (TextView) findViewById(R.id.lblAttivitaSpeciale);
        lblCorrettore = (TextView) findViewById(R.id.lblCorrettore);
        lblNoteLetturista = (TextView) findViewById(R.id.lblNoteLetturista);
        txtCifreMisuratore = (EditText) findViewById(R.id.txtCifreMisuratore);
        txtNoteBiffUno = (EditText) findViewById(R.id.txtNoteBiffUno);
        txtNoteBiffDue = (EditText) findViewById(R.id.txtNoteBiffDue);
        txtLetturaMis = (EditText) findViewById(R.id.txtLetturaMis);
        lblCategoriaUtente = (TextView) findViewById(R.id.lblCategoriaUtente);
        imgCategoriaUtente = (ImageView) findViewById(R.id.imgCategoriaUtente);
        accessibilitaLayout = (LinearLayout) findViewById(R.id.accessibilitaLayout);
        CensMatricola = "";



        txtNoteLetturista = (EditText) findViewById(R.id.txtNoteLetturista);
        DescrContatore = (TextView) findViewById(R.id.intestMatricola);
        btnFotoVC = (Button) findViewById(R.id.btnFotoVC);
        btnFotoB1 = (Button) findViewById(R.id.btnFotoB1);
        btnFotoB2 = (Button) findViewById(R.id.btnFotoB2);
        btnProgramma = (Button) findViewById(R.id.btnProgramma);
        btnProgramma.setVisibility(View.INVISIBLE);
        btnCorrettore = (Button) findViewById(R.id.btnCorrettore);
        ControlloNoteAccessibili = new HashMap<String,String>();
        ControlloNoteParziali = new HashMap<String,String>();
        ControlloNoteNonAccessibili = new HashMap<String,String>();
        BiffToActivity = new ArrayList<String>();
        spnBiffUno = (Spinner) findViewById(R.id.spnBiffUno);
        spnBiffDue = (Spinner) findViewById(R.id.spnBiffDue);
        spnBiffAccessibilita = (Spinner) findViewById(R.id.spnAccessibilita);
        CodBiffUnoCS = "000";
        CodBiffDueCS = "000";
        spnBiffDue.setEnabled(false);
        OpenSpecialActivity = new HashMap<String, Class>();
        DefLat = "0";
        DefLong = "0";
        RilascioStampa = "0";
        LetturaVB = "";
        LetturaVE = "";
        LetturaVM = "";
        DataAttivita = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
        DataAttivitaTrm = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
        OraAttivitaTrm = new SimpleDateFormat("HH:mm:ss").format(new Date());
        OraAttivita = new SimpleDateFormat("HH:mm:ss").format(new Date());
        CountPhotoVC=0;
        CountPhotoVM=0;
        CountPhotoVB=0;
        CountPhotoVE=0;
        CountPhotoB1=0;
        CountPhotoB2=0;
        CountPhotoP1=0;
        ObbNotaUno = "S";
        ObbNotaUno = "N";

        CalendarCert = Calendar.getInstance();
        //Prendi il messaggio dall'intent
        Bundle extras = getIntent().getExtras();
        if(extras !=null) {
            CodiceUtente = extras.getString("CodiceUtente");
            IDCarico = extras.getString("IDCarico");
            IDAzienda = extras.getString("IDAzienda");
            IDLett = extras.getString("IDLett");
            Long Var = extras.getLong("DataOraCerta");

            CalendarCert.setTimeInMillis((Var+ SystemClock.currentThreadTimeMillis()));
        }



        dbHelper = new DatabaseHelper(Act_RilevaDati.this);
        db = dbHelper.getWritableDatabase();

        VerificaGPS();

        Cursor c = db.rawQuery("SELECT IdConfig FROM AndroConfig WHERE Type = 'sGlobalConfiguration'", null);

        if (c != null)
            c.moveToFirst();

        GlobalConfig = c.getString(c.getColumnIndex("IdConfig"));
        c.close();



        if(GlobalConfig.equals("1") || GlobalConfig.equals("2")) {
            txtCifreMisuratore.setEnabled(false);
            rdWhatsapp.setVisibility(View.VISIBLE);
        }
        else
        {
            txtCifreMisuratore.setEnabled(true);
            rdWhatsapp.setVisibility(View.INVISIBLE);
        }
        if((IDCarico.equals("233") && GlobalConfig.equals("2")) ||(IDCarico.equals("368") && GlobalConfig.equals("1")))
        {
            rdSigillatura.setVisibility(View.VISIBLE);
        }


        try{
            db.execSQL("DELETE FROM utentiOUT WHERE idAzienda = '"+IDAzienda+"' AND idCarico = '"+IDCarico+"' AND CodiceUtente = '"+CodiceUtente+"'");
            db.execSQL("UPDATE utenti SET Stato = 'A' WHERE CodiceUtente='" + CodiceUtente + "' AND IDCarico='" + IDCarico + "' AND IDAzienda='" + IDAzienda + "' ");
            db.execSQL("INSERT INTO utentiOUT SELECT * FROM utenti WHERE idAzienda = '"+IDAzienda+"' AND idCarico = '"+IDCarico+"' AND CodiceUtente = '"+CodiceUtente+"'");
            GPC._context = getApplicationContext();
            GPC.RimuoviFotoRiaperti(IDCarico.trim(), IDAzienda.trim(), CodiceUtente.trim());
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),"Attenzione impossibile gestire la lavorazione! ["+e.toString()+"]",Toast.LENGTH_LONG).show();
            finish();
        }

        OpenSpecialActivity.put("Act_CambiaIndirizzo", Act_CambiaIndirizzo.class);
        OpenSpecialActivity.put("Act_NuovoContatore", Act_NuovoContatore.class);
        OpenSpecialActivity.put("Act_CambioUbicazione", Act_CambioUbicazione.class);
        OpenSpecialActivity.put("Act_SegnalazioniSpeciali", Act_SegnalazioniSpeciali.class);
        findViewById(R.id.lblMatricola ).requestFocus();

        //******************************************************************************************************
        //******************************************************************************************************
        //*********************************             PER SMAT          **************************************
        //******************************************************************************************************
        //******************************************************************************************************
        if(GlobalConfig.equals("6")) //Se SMAT Torino cambiare "Note Letturista" in "Note Ubicazione" come label
        {
            lblNoteLetturista.setText("Note Ubicazione");
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(80);
            txtNoteLetturista.setFilters(FilterArray);
        }

        //******************************************************************************************************
        //******************************************************************************************************
        //*********************************             PER SMAT          **************************************
        //******************************************************************************************************
        //******************************************************************************************************
        Cursor cursor = dbHelper.query(db, "SELECT idAzienda, MatricolaMis, CifreContatore, PresCorrettore,NotaAccess,LetturaDa,LetturaA,NoteAttivita,AttivitaSpeciale,Nominativo, CAP, Localita, Provincia, Toponimo, Strada, Civico, CodiceUtente,LNoteLibere, LettMinimaCalcolata,LettMassimaCalcolata,DtPrimoPassaggio, MSegnalazione, FDLettura, CategoriaUtente, ObbNotaUno, ObbNotaDue, UltimaLettura, LetturaPrevista,dtProgrammazione, Nota3 FROM utenti WHERE CodiceUtente = '" + CodiceUtente + "' AND idCarico = '"+IDCarico+"' AND  idAzienda = '"+IDAzienda+"'  ");

        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {
            if(!cursor.isNull(cursor.getColumnIndex("AttivitaSpeciale")))
            {
                if(cursor.getString(cursor.getColumnIndex("AttivitaSpeciale")).trim().length()>0)
                    AttivitaSpeciale = cursor.getString(cursor.getColumnIndex("AttivitaSpeciale"));
            }
            if(!cursor.isNull(cursor.getColumnIndex("Nota3")))
            {
                if(cursor.getString(cursor.getColumnIndex("Nota3")).trim().equals("AFF") && cursor.getString(cursor.getColumnIndex("dtProgrammazione")).trim().length()<1) {
                    btnProgramma.setVisibility(View.VISIBLE);
                    AttivitaSpeciale = "PROGRAMMABILE";
                    lblAttivitaSpeciale.setText("PROGRAMMABILE");
                    lblAttivitaSpeciale.setVisibility(View.VISIBLE);
                }
                else {
                    btnProgramma.setVisibility(View.INVISIBLE);
                    lblAttivitaSpeciale.setVisibility(View.INVISIBLE);
                }
            }
            else
                btnProgramma.setVisibility(View.INVISIBLE);
            FlagDonato = cursor.getString(cursor.getColumnIndex("NoteAttivita"));
            DataDAFlusso = cursor.getString(cursor.getColumnIndex("LetturaDa"));
            DataAFlusso  =cursor.getString(cursor.getColumnIndex("LetturaA"));
            lblMatricola.setText(cursor.getString(cursor.getColumnIndex("MatricolaMis")));
            lblCorrettore.setText(cursor.getString(cursor.getColumnIndex("PresCorrettore")));
            DataDAFlusso = cursor.getString(cursor.getColumnIndex("LetturaDa"));
            DataAFlusso  =cursor.getString(cursor.getColumnIndex("LetturaA"));
            MSegnalazione = cursor.getString(cursor.getColumnIndex("MSegnalazione"));
            txtCifreMisuratore.setText(cursor.getString(cursor.getColumnIndex("CifreContatore")));
            IDDitta = cursor.getString(cursor.getColumnIndex("idAzienda"));
            Accessibilita = cursor.getString(cursor.getColumnIndex("NotaAccess"));
            OrigToponimo = cursor.getString(cursor.getColumnIndex("Toponimo"));
            OrigStrada = cursor.getString(cursor.getColumnIndex("Strada"));
            OrigCivico = cursor.getString(cursor.getColumnIndex("Civico"));
            FDLettura = cursor.getString(cursor.getColumnIndex("FDLettura"));
            DatiStampaIndirizzo = cursor.getString(cursor.getColumnIndex("Toponimo")) + " " + cursor.getString(cursor.getColumnIndex("Strada")) + " " + cursor.getString(cursor.getColumnIndex("Civico"));
            DatiStampaLocalita = cursor.getString(cursor.getColumnIndex("CAP")) + " " + cursor.getString(cursor.getColumnIndex("Localita")) + " " + cursor.getString(cursor.getColumnIndex("Provincia"));
            DatiStampaMatricola = cursor.getString(cursor.getColumnIndex("MatricolaMis"));
            DatiStampaNominativo = cursor.getString(cursor.getColumnIndex("Nominativo"));
            DatiStampaCodiceUtente = cursor.getString(cursor.getColumnIndex("CodiceUtente"));
            LetturaMinimaCalcolata = cursor.getString(cursor.getColumnIndex("LettMinimaCalcolata"));
            LetturaMassimaCalcolata = cursor.getString(cursor.getColumnIndex("LettMassimaCalcolata"));
            DataPrimoPassaggio = cursor.getString(cursor.getColumnIndex("DtPrimoPassaggio"));
            CategoriaUtente = cursor.getString(cursor.getColumnIndex("CategoriaUtente"));
            ObbNotaUno = cursor.getString(cursor.getColumnIndex("ObbNotaUno"));
            ObbNotaDue = cursor.getString(cursor.getColumnIndex("ObbNotaDue"));
            UltimaLettura = GPC.NullToEmpty(cursor.getString(cursor.getColumnIndex("UltimaLettura")));
            LetturaPrevista = GPC.NullToEmpty(cursor.getString(cursor.getColumnIndex("LetturaPrevista")));

            //******************************************************************************************************
            //*********************************             PER SMAT          **************************************
            //******************************************************************************************************
            if(GlobalConfig.equals("6")) {
                SMATOldNota = cursor.getString(cursor.getColumnIndex("LNoteLibere"));
                txtNoteLetturista.setText(SMATOldNota);
            }
            //SE LETTURE HERA TOGLIERE IL FLAG AUTOLETTURA
            if(GlobalConfig.equals("11"))
            {
                rdAutolettura.setVisibility(View.INVISIBLE);
                if(CategoriaUtente.equals("11"))
                {
                    imgCategoriaUtente.setImageResource(R.drawable.iconwater01);
                    lblCategoriaUtente.setText("Attività rilevazione Acqua");
                    lblCategoriaUtente.setVisibility(View.VISIBLE);
                    imgCategoriaUtente.setVisibility(View.VISIBLE);
                }
                else if(CategoriaUtente.equals("10"))
                {
                    imgCategoriaUtente.setImageResource(R.drawable.icongas03);
                    lblCategoriaUtente.setText("Attività rilevazione Gas");
                    lblCategoriaUtente.setVisibility(View.VISIBLE);
                    imgCategoriaUtente.setVisibility(View.VISIBLE);
                }
                else if(CategoriaUtente.equals("14"))
                {
                    imgCategoriaUtente.setImageResource(R.drawable.icontelerisc01);
                    lblCategoriaUtente.setText("Attività rilevazione Teleriscaldamenti");
                    lblCategoriaUtente.setVisibility(View.VISIBLE);
                    imgCategoriaUtente.setVisibility(View.VISIBLE);
                }
                else
                {
                    lblCategoriaUtente.setVisibility(View.INVISIBLE);
                    imgCategoriaUtente.setVisibility(View.INVISIBLE);
                }
            }
            else
            {
                rdAutolettura.setVisibility(View.VISIBLE);
                lblCategoriaUtente.setVisibility(View.INVISIBLE);
                imgCategoriaUtente.setVisibility(View.INVISIBLE);

            }

            txtLetturaMis.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        if(GlobalConfig.equals("6") && txtLetturaMis.getText().toString().trim().length()>0 && LetturaMinimaCalcolata!=null && LetturaMassimaCalcolata!=null && !ValoreLetturaInseritaControllato)
                        {
                            AlertDialog.Builder dialogDatoLettura = new AlertDialog.Builder(Act_RilevaDati.this);
                            dialogDatoLettura
                                    .setTitle("CONTROLLA LETTURA")
                                    .setMessage("Attenzione, il consumo indicato potrebbe essere inferiore a quello realmente indicato sul contatore")
                                    .setCancelable(false)
                                    .setPositiveButton("Reinserisci valore", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                            if(Integer.parseInt(txtLetturaMis.getText().toString().trim())<Integer.parseInt(LetturaMinimaCalcolata))
                            {
                                ValoreLetturaInseritaControllato = true;
                                txtLetturaMis.setText("");
                                txtLetturaMis.requestFocus();
                                dialogDatoLettura.setMessage("Attenzione, il consumo indicato potrebbe essere inferiore a quello realmente indicato sul contatore");
                                AlertDialog MsgDatoLettura = dialogDatoLettura.create();
                                MsgDatoLettura.show();
                            }

                        }
                        if(GlobalConfig.equals("6") )
                        {
                            if(txtLetturaMis.getText().toString().trim().length()> Integer.parseInt(txtCifreMisuratore.getText().toString().trim()) )
                            {
                                AlertDialog.Builder dialogDatoLettura = new AlertDialog.Builder(Act_RilevaDati.this);
                                dialogDatoLettura
                                        .setTitle("CONTROLLA LETTURA")
                                        .setMessage("Attenzione, le cifre lettura sono superiori a quanto indicato, controlla la lettura o cambia il valore in Cifre Misuratore")
                                        .setCancelable(false)
                                        .setPositiveButton("Reinserisci valore", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        });
                                AlertDialog MsgDatoLettura = dialogDatoLettura.create();
                                MsgDatoLettura.show();
                            }
                        }
                        if(GlobalConfig.equals("1") || GlobalConfig.equals("2") )
                        {
                            if(txtLetturaMis.getText().toString().trim().length()> Integer.parseInt(txtCifreMisuratore.getText().toString().trim()) )
                            {
                                AlertDialog.Builder dialogDatoLettura = new AlertDialog.Builder(Act_RilevaDati.this);
                                dialogDatoLettura
                                        .setTitle("CONTROLLA LETTURA")
                                        .setMessage("Attenzione, le cifre lettura sono superiori a quanto indicato, controlla la lettura o utilizza la nota di cambio cifre")
                                        .setCancelable(false)
                                        .setPositiveButton("Reinserisci valore", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        });
                                AlertDialog MsgDatoLettura = dialogDatoLettura.create();
                                MsgDatoLettura.show();
                            }
                        }
                        if(UltimaLettura.trim().length()>0 && txtLetturaMis.getText().toString().trim().length()>0 && !ValoreLetturaInseritaControllato)
                        {
                            AlertDialog.Builder dialogDatoLettura = new AlertDialog.Builder(Act_RilevaDati.this);
                            dialogDatoLettura
                                    .setTitle("CONTROLLA LETTURA")
                                    .setMessage("Attenzione, il consumo indicato potrebbe essere inferiore a quello realmente indicato sul contatore")
                                    .setCancelable(false)
                                    .setPositiveButton("Reinserisci valore", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                            txtLetturaMis.requestFocus();
                                            ValoreLetturaInseritaControllato = true;
                                        }
                                    });
                            if(Integer.parseInt(txtLetturaMis.getText().toString().trim())<=Integer.parseInt(UltimaLettura))
                            {
                                ValoreLetturaInseritaControllato = true;
                                txtLetturaMis.setText("");
                                dialogDatoLettura.setMessage("Attenzione, il consumo indicato potrebbe essere inferiore a quello realmente indicato sul contatore");
                                AlertDialog MsgDatoLettura = dialogDatoLettura.create();
                                MsgDatoLettura.show();
                            }
                        }
                        if(LetturaPrevista.trim().length()>0 && txtLetturaMis.getText().toString().trim().length()>0 && !ValoreLetturaInseritaControllato)
                        {
                            AlertDialog.Builder dialogDatoLettura = new AlertDialog.Builder(Act_RilevaDati.this);
                            dialogDatoLettura
                                    .setTitle("CONTROLLA LETTURA")
                                    .setMessage("Attenzione, il consumo indicato potrebbe essere inferiore a quello realmente indicato sul contatore")
                                    .setCancelable(false)
                                    .setPositiveButton("Reinserisci valore", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                            txtLetturaMis.requestFocus();
                                            ValoreLetturaInseritaControllato = true;
                                        }
                                    });
                            if(Integer.parseInt(txtLetturaMis.getText().toString().trim())>Integer.parseInt(LetturaPrevista))
                            {
                                ValoreLetturaInseritaControllato = true;
                                txtLetturaMis.setText("");
                                dialogDatoLettura.setMessage("Attenzione, il consumo indicato potrebbe essere superiore a quello realmente indicato sul contatore");
                                AlertDialog MsgDatoLettura = dialogDatoLettura.create();
                                MsgDatoLettura.show();
                            }
                        }
                    }
                }
            });
            //******************************************************************************************************
            //***********************************     PER SMAT e ITALGAS    ****************************************
            //******************************************************************************************************


            Log.i("NOTA","IDDitta: " + IDDitta);
            if(lblMatricola.getText().length()==16 && (this.GlobalConfig.equals("1") || this.GlobalConfig.equals("2")))
            {
                Integrato = true;
                DescrContatore.setText("Matricola Misuratore INTEGRATO");
            }
            else
            {
                Integrato = false;
                DescrContatore.setText("Matricola Misuratore");
            }
            cursor.moveToNext();
        }
        cursor.close();

        if(AttivitaSpeciale.equals("NESSUNA"))
        {
            lblAttivitaSpeciale.setVisibility(View.INVISIBLE);
        }
        else
        {
            if(AttivitaSpeciale.equals("C"))
            {
                lblAttivitaSpeciale.setText("CENSIMENTO - NOTA 051");
            }
            else if(AttivitaSpeciale.equals("S"))
            {
                lblAttivitaSpeciale.setText("RISIGILLATURA");
                rdSigillatura.setVisibility(View.VISIBLE);
            }
            lblAttivitaSpeciale.setVisibility(View.VISIBLE);
        }
        DataAttivita = ConfrontaData(DataAFlusso, DataDAFlusso, DataAttivita);
        Cursor CScegliBiff = dbHelper.query(db,"SELECT * FROM ControlloNote WHERE IdDitta = '"+IDAzienda+"'");
        BiffatureAccessibili = new ArrayList<String>();
        BiffatureParziali = new ArrayList<String>();
        BiffatureNonAccessibili = new ArrayList<String>();
        BiffatureSenzaAccessibilita = new ArrayList<String>();
        CScegliBiff.moveToFirst();
        String FlagAccess;
        while(CScegliBiff.isAfterLast()==false)
        {
            ControlloNoteAccessibili.put(CScegliBiff.getString(CScegliBiff.getColumnIndex("Nota")), CScegliBiff.getString(CScegliBiff.getColumnIndex("NoteAccessibile")));
            ControlloNoteParziali.put(CScegliBiff.getString(CScegliBiff.getColumnIndex("Nota")),CScegliBiff.getString(CScegliBiff.getColumnIndex("NoteParzAccessibile")));
            ControlloNoteNonAccessibili.put(CScegliBiff.getString(CScegliBiff.getColumnIndex("Nota")),CScegliBiff.getString(CScegliBiff.getColumnIndex("NoteNonAccessibile")));
            FlagAccess = CScegliBiff.getString(CScegliBiff.getColumnIndex("Accessibilita"));
           if(FlagAccess.contains("-Y-"))
               BiffatureAccessibili.add(CScegliBiff.getString(CScegliBiff.getColumnIndex("Nota")));
           if (FlagAccess.contains("-P-"))
               BiffatureParziali.add(CScegliBiff.getString(CScegliBiff.getColumnIndex("Nota")));
           if (FlagAccess.contains("-N-"))
               BiffatureNonAccessibili.add(CScegliBiff.getString(CScegliBiff.getColumnIndex("Nota")));
           if (FlagAccess.contains("--"))
               BiffatureSenzaAccessibilita.add(CScegliBiff.getString(CScegliBiff.getColumnIndex("Nota")));
            CScegliBiff.moveToNext();
        }
        CScegliBiff.close();

        if(lblCorrettore.getText().toString().length()>0) {
            ObbligoCorrettore = true;
            ControlloCorrettore = false;
        }
        else {
            ControlloCorrettore = true;
            ObbligoCorrettore = false;
        }
        /* POPOLAZIONE SPINNER PRIMA/SECONDA BIFFATURA */
        Cursor cursorBiffaturUno = dbHelper.query(db, "SELECT Codice, CodiceDitta, DescrSupp, Activity, Autolettura, ObbLettura, ObbFoto FROM NoteLettDitta WHERE IdDitta = '" + IDDitta + "' AND (CampoNota = '0' or CampoNota = '1') ");
        SpinnerBiff1 = new ArrayList<String>();
        SpinnerBiff1.add("Causale Prima Nota");

        VerAutoLettura = new ArrayList<String>();
        VerLetturaUno = new ArrayList<String>();
        CodComerBiffatura = new ArrayList<String>();
        CodAziendaBiffatura = new ArrayList<String>();
        ObbFotoNotaUno = new ArrayList<String>();
        ObbFotoNotaDue = new ArrayList<String>();
        cursorBiffaturUno.moveToFirst();
        Integer i = 1;
        BiffToActivity.add("");
        CodComerBiffatura.add("");
        CodAziendaBiffatura.add("");
        VerAutoLettura.add("");
        ObbFotoNotaUno.add("");
        ObbFotoNotaDue.add("");
        VerLetturaUno.add("");
        while (cursorBiffaturUno.isAfterLast() == false) {
            if(Accessibilita.contains("Y") || Accessibilita.contains("S")) {
                if (BiffatureAccessibili.contains(cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("CodiceDitta")))) {
                    SpinnerBiff1.add(cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("Codice"))+" - " +cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("DescrSupp")).toUpperCase());
                    VerAutoLettura.add(cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("Autolettura")));
                    BiffToActivity.add(cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("Activity")));
                    VerLetturaUno.add(cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("ObbLettura")));
                    CodComerBiffatura.add(cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("Codice")));
                    CodAziendaBiffatura.add(cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("CodiceDitta")));
                    ObbFotoNotaUno.add(cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("ObbFoto")));
                    i++;
                }
            }
            else if(Accessibilita.contains("P")) {
                if (BiffatureParziali.contains(cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("CodiceDitta")))) {
                    SpinnerBiff1.add(cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("Codice"))+" - " +cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("DescrSupp")).toUpperCase());
                    VerAutoLettura.add(cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("Autolettura")));
                    BiffToActivity.add(cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("Activity")));
                    VerLetturaUno.add(cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("ObbLettura")));
                    CodComerBiffatura.add(cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("Codice")));
                    CodAziendaBiffatura.add(cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("CodiceDitta")));
                    ObbFotoNotaUno.add(cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("ObbFoto")));
                    i++;
                }
            }
            else if(Accessibilita.contains("N")) {
                if (BiffatureNonAccessibili.contains(cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("CodiceDitta")))) {
                    SpinnerBiff1.add(cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("Codice"))+" - " +cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("DescrSupp")).toUpperCase());
                    VerAutoLettura.add(cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("Autolettura")));
                    BiffToActivity.add(cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("Activity")));
                    VerLetturaUno.add(cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("ObbLettura")));
                    CodComerBiffatura.add(cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("Codice")));
                    CodAziendaBiffatura.add(cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("CodiceDitta")));
                    ObbFotoNotaUno.add(cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("ObbFoto")));
                    i++;
                }
            }
            else if(Accessibilita.equals("")) {
                if (BiffatureSenzaAccessibilita.contains(cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("CodiceDitta")))) {
                    SpinnerBiff1.add(cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("Codice"))+" - " +cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("DescrSupp")).toUpperCase());
                    VerAutoLettura.add(cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("Autolettura")));
                    BiffToActivity.add(cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("Activity")));
                    VerLetturaUno.add(cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("ObbLettura")));
                    CodComerBiffatura.add(cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("Codice")));
                    CodAziendaBiffatura.add(cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("CodiceDitta")));
                    ObbFotoNotaUno.add(cursorBiffaturUno.getString(cursorBiffaturUno.getColumnIndex("ObbFoto")));
                    i++;
                }
            }
            cursorBiffaturUno.moveToNext();
        }
        cursorBiffaturUno.close();
        ArrayAdapter<String> SpinnAdapBiff1 = new ArrayAdapter<String>(this, R.layout.spinner_item1, SpinnerBiff1);
        spnBiffUno.setAdapter(SpinnAdapBiff1);
        /* FINE POPOLAZIONE SPINNER PRIMA BIFFATURA */


        spnBiffUno.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                VerificaGPS();
                if (position > 0) {
                    spnBiffDue.setEnabled(true);
                    AttivaSecondaBiffatura(CodAziendaBiffatura.get(position));
                    if (VerAutoLettura.get(position).toString().equals("N") && rdAutolettura.isChecked()) {
                        rdAutolettura.setChecked(false);
                        Toast.makeText(getApplicationContext(), "Attenzione, la nota non è compatibile con l'autolettura", Toast.LENGTH_SHORT).show();
                    }
                    if (VerLetturaUno.get(position).toString().equals("S"))
                        ObbligoLettura = true;
                    else if (VerLetturaUno.get(position).toString().equals("N"))
                    {
                        if(rdWhatsapp.isChecked())
                        {
                            rdWhatsapp.setChecked(false);
                            Toast.makeText(getApplicationContext(), "Whatsapp può essere utilizzato solo in caso di obbligo di lettura", Toast.LENGTH_SHORT).show();
                        }
                        ObbligoLettura = false;
                    }

                    if (BiffToActivity.get(position) != null) {
                        CodBiffUnoCS = CodComerBiffatura.get(position);
                        CodBiffUnoAZ = CodAziendaBiffatura.get(position);
                        DaQualeBiff = "BiffUno";
                        Intent intent = new Intent(getApplicationContext(), OpenSpecialActivity.get(BiffToActivity.get(position)));
                        intent.putExtra("IdCarico", IDCarico);
                        intent.putExtra("CodiceUtente", CodiceUtente);
                        intent.putExtra("IdAzienda", IDAzienda);
                        intent.putExtra("GlobalConfig", GlobalConfig);
                        intent.putExtra("CodAzBiffa", CodBiffUnoAZ);
                        intent.putExtra("CategoriaUtente", CategoriaUtente);
                        if(BiffToActivity.get(position).equals("Act_CambiaIndirizzo"))
                        {
                            intent.putExtra("Toponimo",OrigToponimo);
                            intent.putExtra("Strada",OrigStrada);
                            intent.putExtra("Civico",OrigCivico);
                        }
                        startActivityForResult(intent, 1);
                        txtNoteBiffUno.setEnabled(false);
                        btnFotoB1.setEnabled(true);
                        onPause();
                    } else if (position > 0) {
                        CodBiffUnoCS = CodComerBiffatura.get(position);
                        CodBiffUnoAZ = CodAziendaBiffatura.get(position);
                        txtNoteBiffUno.setText("");
                        txtNoteBiffUno.setEnabled(true);
                        btnFotoB1.setEnabled(true);
                        txtNoteBiffUno.requestFocus();
                    } else {
                        txtNoteBiffUno.setText("");
                        txtNoteBiffUno.setEnabled(true);
                        btnFotoB1.setEnabled(true);
                        txtNoteBiffUno.requestFocus();
                    }

                    //Toast.makeText(getApplicationContext(),"CodBiff: ["+CodBiffUnoCS+"]",Toast.LENGTH_LONG).show();
                } else {
                    txtNoteBiffUno.setText("");
                    txtNoteBiffUno.setEnabled(false);
                    btnFotoB1.setEnabled(false);
                    spnBiffDue.setEnabled(false);
                    spnBiffDue.setSelection(0);
                    txtNoteBiffDue.setText("");
                    txtNoteBiffDue.setEnabled(false);
                    CodBiffUnoCS = "000";
                    CodBiffUnoAZ = null;
                    //Toast.makeText(getApplicationContext(),"CodBiff: ["+CodBiffUnoCS+"]",Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                txtNoteBiffUno.setText("");
                txtNoteBiffUno.setEnabled(false);
                btnFotoB1.setEnabled(false);
            }
        });

    if(Integrato)
        btnCorrettore.setEnabled(false);
    else
        btnCorrettore.setEnabled(true);

        rdSigillatura.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    rdSigillatura.setChecked(true);
                    txtNoteLetturista.setText("eseguita risigillatura");
                    txtNoteLetturista.setEnabled(false);
                }
                else
                {
                    rdSigillatura.setChecked(false);
                    txtNoteLetturista.setText("");
                    txtNoteLetturista.setEnabled(true);
                }

            }
        });

        // Attività vus - GlobalConfig 6
        // mostra lo spinner per censire l'accessibilità solo per l'attività VUS letture, altrimenti il layout accessibilitàLayout resta nascosto
        if(GlobalConfig.trim().equals("14"))
            mostraAccessibilitaVusLetture();

    }

    private void mostraAccessibilitaVusLetture()
    {
        SpinnerAccessibilita = new ArrayList<String>();
        SpinnerAccessibilita.add("Accessibilità");
        SpinnerAccessibilita.add("MA-Accessibile");
        SpinnerAccessibilita.add("PA-Parzialmente Accessibile");
        SpinnerAccessibilita.add("NA-Non Accessibile");
        ArrayAdapter<String> SpinnAdapAccess = new ArrayAdapter<String>(this, R.layout.spinner_item1, SpinnerAccessibilita);
        spnBiffAccessibilita.setAdapter(SpinnAdapAccess);
        accessibilitaLayout.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        VerificaGPS();
        Log.i("response", "Act_RilevaDati onStart");
    }

        @Override
    protected void onPause()
    {
        super.onPause();
        VerificaGPS();
        Log.i("response", "Act_RilevaDati OnPause");
    }

    @Override
    protected void onResume() {
        super.onResume();


        /* CONTROLLO DATA AUTOMATICA */
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                if(Settings.Global.getInt(getContentResolver(), Settings.Global.AUTO_TIME) != 1)
                {
                    Toast.makeText(getApplicationContext(), "ATTENZIONE HAI DISABILITATO LA DATA E ORA AUTOMATICA - L'APP NON PUO' FUNZIONARE", Toast.LENGTH_LONG).show();
                    this.finishAffinity();
                }
                else
                {
                    CalendarCert.setTimeInMillis((CalendarCert.getTimeInMillis() + SystemClock.currentThreadTimeMillis()));
                    Log.i("response", "Act_RilevaDati onResume");
                    findViewById(R.id.lblMatricola).requestFocus();

                    VerificaGPS();
                }
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        /* CONTROLLO DATA AUTOMATICA */
        Crashlytics.log(null);
        Crashlytics.log("GlobalConfig: " + GlobalConfig + " - Operatore: " + IDLett );


        }

    @Override
    protected void onStop() {
        super.onStop();
        VerificaGPS();
        Log.i("response", "Act_RilevaDati onStop");
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
    }

    public void VerificaIntegrato(View view)
    {
        if(Integrato)
        {
            btnFotoVC.setEnabled(false);
            VerificaGPS();
            Intent intent = new Intent(this, Act_Correttore.class);
            intent.putExtra("CodiceUtente", CodiceUtente);
            intent.putExtra("IDCarico", IDCarico);
            intent.putExtra("IDAzienda", IDAzienda);
            intent.putExtra("ObbligoCorrettore", ObbligoCorrettore);
            intent.putExtra("Integrato", Integrato);
            intent.putExtra("DataAttivita", DataAttivita);
            intent.putExtra("OraAttivita", OraAttivita);
            intent.putExtra("Matricola", lblMatricola.getText());
            intent.putExtra("Correttore", lblCorrettore.getText());
            intent.putExtra("LettVM", LetturaVM);
            intent.putExtra("LettVB", LetturaVB);
            intent.putExtra("LettVE", LetturaVE);
            intent.putExtra("operatore", IDLett);
            intent.putExtra("CountPhotoB2", CountPhotoB2);
            intent.putExtra("CountPhotoVM", CountPhotoVM);
            intent.putExtra("CountPhotoVC", CountPhotoVC);
            intent.putExtra("CountPhotoVB", CountPhotoVB);
            intent.putExtra("CountPhotoVE", CountPhotoVE);
            intent.putExtra("GlobalConfig", GlobalConfig);


            startActivityForResult(intent, 3);
        }
        else
        {
            btnFotoVC.setEnabled(true);
        }
    }

    public void DatiCorrettore(View view)
    {

        VerificaGPS();
        Intent intent = new Intent(this, Act_Correttore.class);
        intent.putExtra("CodiceUtente", CodiceUtente);
        intent.putExtra("IDCarico", IDCarico);
        intent.putExtra("IDAzienda", IDAzienda);
        intent.putExtra("ObbligoCorrettore", ObbligoCorrettore);
        intent.putExtra("Integrato", Integrato);
        intent.putExtra("DataAttivita", DataAttivita);
        intent.putExtra("OraAttivita", OraAttivita);
        intent.putExtra("Matricola", lblMatricola.getText());
        intent.putExtra("Correttore", lblCorrettore.getText());
        intent.putExtra("LettVM", LetturaVM);
        intent.putExtra("LettVB", LetturaVB);
        intent.putExtra("LettVE", LetturaVE);
        intent.putExtra("CountPhotoB1", CountPhotoB1);
        intent.putExtra("CountPhotoB2", CountPhotoB2);
        intent.putExtra("CountPhotoVM", CountPhotoVM);
        intent.putExtra("CountPhotoVC", CountPhotoVC);
        intent.putExtra("CountPhotoVB", CountPhotoVB);
        intent.putExtra("CountPhotoVE", CountPhotoVE);
        intent.putExtra("DefLat", DefLat);
        intent.putExtra("DefLat", DefLong);
        intent.putExtra("GlobalConfig",GlobalConfig);
        startActivityForResult(intent, 3);
    }

    public void DatiCensimento(View view)
    {
        VerificaGPS();
        Intent intent = new Intent(this, Act_Censimento.class);
        intent.putExtra("CensNominativo", CensNominativo);
        intent.putExtra("CensMatricola", CensMatricola);
        intent.putExtra("CensAnnoMatricola", CensAnnoMatricola);
        intent.putExtra("CensSiglaMatricola", CensSiglaMatricola);
        intent.putExtra("CensImpegnativo", CensImpegnativo);
        intent.putExtra("CensCifreMisuratore", CensCifreMisuratore);
        intent.putExtra("CensIndirizzo", CensIndirizzo);
        intent.putExtra("CensCivico", CensCivico);
        intent.putExtra("CensBarra", CensBarra);
        intent.putExtra("CensScala", CensScala);
        intent.putExtra("CensPiano", CensPiano);
        intent.putExtra("CensInterno", CensInterno);
        intent.putExtra("CensComune", CensComune);
        intent.putExtra("CensSigillo", CensSigillo);
        intent.putExtra("CensNotaSigillo", CensNotaSigillo);
        intent.putExtra("CensTelefono", CensTelefono);
        startActivityForResult(intent, 4);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {

        Log.i("response", "Act_RilevaDati onActivityResult");
        super.onActivityResult(requestCode, resultCode, data);
        VerificaGPS();
        if (requestCode==1) {
            Log.i("response", "requestCode è relativo alle biffature");
            if(resultCode == this.RESULT_OK){
                Log.i("response", "requestCode è ok");
                String result=data.getStringExtra("result");
                if(DaQualeBiff=="BiffDue")
                    txtNoteBiffDue.setText(result);
                else if(DaQualeBiff=="BiffUno")
                    txtNoteBiffUno.setText(result);

            }
            if (resultCode == this.RESULT_CANCELED) {
                Log.i("response", "requestCode è CANCELLED");
                if(DaQualeBiff=="BiffDue") {
                    spnBiffDue.setSelection(0);
                    txtNoteBiffDue.setText("");
                }
                else if(DaQualeBiff=="BiffUno") {
                    spnBiffUno.setSelection(0);
                    txtNoteBiffUno.setText("");
                }
            }
        }
        else if(requestCode==2)
        {
            String resultB1=data.getStringExtra("B1");
            String resultB2=data.getStringExtra("B2");
            String resultVM=data.getStringExtra("VM");
            String resultVB=data.getStringExtra("VB");
            String resultVE=data.getStringExtra("VE");
            String resultVC=data.getStringExtra("VC");
            CountPhotoB1 = Integer.parseInt(resultB1);
            CountPhotoB2 = Integer.parseInt(resultB2);
            CountPhotoVM = Integer.parseInt(resultVM);
            CountPhotoVC = Integer.parseInt(resultVC);
            CountPhotoVB = Integer.parseInt(resultVB);
            CountPhotoVE = Integer.parseInt(resultVE);
        }
        else if(requestCode==3)
        {
            if(resultCode == this.RESULT_OK) {
                LetturaVM = data.getStringExtra("LettVM");
                LetturaVB = data.getStringExtra("LettVB");
                LetturaVE = data.getStringExtra("LettVE");
                String resultB1=data.getStringExtra("B1");
                String resultB2=data.getStringExtra("B2");
                String resultVM=data.getStringExtra("VM");
                String resultVB=data.getStringExtra("VB");
                String resultVE=data.getStringExtra("VE");
                String resultVC=data.getStringExtra("VC");
                CountPhotoB1 = Integer.parseInt(resultB1);
                CountPhotoB2 = Integer.parseInt(resultB2);
                CountPhotoVM = Integer.parseInt(resultVM);
                CountPhotoVC = Integer.parseInt(resultVC);
                CountPhotoVB = Integer.parseInt(resultVB);
                CountPhotoVE = Integer.parseInt(resultVE);
                ControlloCorrettore = true;
            }
            else
            {
                if(ObbligoCorrettore)
                    ControlloCorrettore = false;
            }
        }
        else if(requestCode==4)
        {
            if(resultCode == this.RESULT_OK)
            {
                Censimento = true;
                CensNominativo = data.getStringExtra("CensNominativo");
                CensMatricola = data.getStringExtra("CensMatricola");
                CensAnnoMatricola = data.getStringExtra("CensAnnoMatricola");
                CensSiglaMatricola = data.getStringExtra("CensSiglaMatricola");
                CensImpegnativo = data.getStringExtra("CensImpegnativo");
                CensCifreMisuratore = data.getStringExtra("CensCifreMisuratore");
                CensIndirizzo = data.getStringExtra("CensIndirizzo");
                CensCivico = data.getStringExtra("CensCivico");
                CensBarra = data.getStringExtra("CensBarra");
                CensScala = data.getStringExtra("CensScala");
                CensPiano = data.getStringExtra("CensPiano");
                CensInterno = data.getStringExtra("CensInterno");
                CensComune = data.getStringExtra("CensComune");
                CensSigillo = data.getStringExtra("CensSigillo");
                CensNotaSigillo = data.getStringExtra("CensNotaSigillo");
                CensTelefono = data.getStringExtra("CensTelefono");
            }
            else
                Censimento = false;

        }
        else if(requestCode==5)
        {
            if(resultCode == this.RESULT_OK)
            {
                finish();
            }
        }
        else
            Log.i("response", "requestCode non classificato");
    }//onActivityResult


    @Override
    public void onBackPressed() {

       /* try{
            db.execSQL("DELETE FROM utentiOUT WHERE idAzienda = '"+IDAzienda+"' AND idCarico = '"+IDCarico+"' AND CodiceUtente = '"+CodiceUtente+"'");
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),"Attenzione impossibile cancellare la lavorazione! ["+e.toString()+"]",Toast.LENGTH_LONG).show();
        }*/
        return;
    }

    public void VerificaGPS()
    {


        statusOfGPS = gps.StatoGPS();
        if(statusOfGPS)
        {
            Latitudine = Double.toString(gps.getLatitude());
            Longitudine = Double.toString(gps.getLongitude());
            if(Latitudine!="0.0" && Latitudine != "0") {
                DefLat = Latitudine;
                DefLong = Longitudine;
                //Toast.makeText(getApplicationContext(), "DA GPS DefLat:" + DefLat + " - DefLong:" + DefLong, Toast.LENGTH_SHORT).show();
            }
            else {
                Cursor Coord = dbHelper.query(db, "SELECT IdConfig, Type FROM AndroConfig WHERE Type = 'Latitudine' OR Type = 'Longitudine'");
                Coord.moveToFirst();
                while (Coord.isAfterLast() == false)
                {
                    if(!Coord.isNull(Coord.getColumnIndex("IdConfig")))
                    {
                        if(Coord.getString(Coord.getColumnIndex("Type")).equals("Latitudine"))
                            DefLat = Coord.getString(Coord.getColumnIndex("IdConfig")).toString();
                        if(Coord.getString(Coord.getColumnIndex("Type")).equals("Longitudine"))
                            DefLong = Coord.getString(Coord.getColumnIndex("IdConfig")).toString();
                    }
                    Coord.moveToNext();
                }
                Coord.close();
                //Toast.makeText(getApplicationContext(), "DA DB DefLat:" + DefLat + " - DefLong:" + DefLong, Toast.LENGTH_SHORT).show();
            }


        }
        else {
            Cursor Coord = dbHelper.query(db, "SELECT IdConfig, Type FROM AndroConfig WHERE Type = 'Latitudine' OR Type = 'Longitudine'");
            Coord.moveToFirst();
            while (Coord.isAfterLast() == false)
            {
                if(!Coord.isNull(Coord.getColumnIndex("IdConfig")))
                {
                    if(Coord.getString(Coord.getColumnIndex("Type")).equals("Latitudine"))
                        DefLat = Coord.getString(Coord.getColumnIndex("IdConfig")).toString();
                    if(Coord.getString(Coord.getColumnIndex("Type")).equals("Longitudine"))
                        DefLong = Coord.getString(Coord.getColumnIndex("IdConfig")).toString();
                }
                Coord.moveToNext();
            }
            Coord.close();
            //Toast.makeText(getApplicationContext(), "DA DB DefLat:" + DefLat + " - DefLong:" + DefLong, Toast.LENGTH_SHORT).show();
        }

    }

    public boolean QualiFotoMancano()
    {
        boolean res = true;
        Log.e("OBBFOTO1",ObbFotoNotaUno.get(spnBiffUno.getSelectedItemPosition()));
        if(spnBiffUno.getSelectedItemPosition()>0) {
            if (CountPhotoB1 == 0 && this.ObbFotoNotaUno.get(spnBiffUno.getSelectedItemPosition()).equals("S")) {
                Toast.makeText(getApplicationContext(), "Mancano le foto Nota Uno [B1]", Toast.LENGTH_SHORT).show();
                res = false;
            }
        }
        if(spnBiffDue.getSelectedItemPosition()>0) {
            if (CountPhotoB2 == 0 && this.ObbFotoNotaDue.get(spnBiffDue.getSelectedItemPosition()).equals("S")) {
                Toast.makeText(getApplicationContext(), "Mancano le foto Nota Due [B2]", Toast.LENGTH_SHORT).show();
                res = false;
            }
        }
        if(CountPhotoVM==0 && LetturaVM.length()>0) {
            Toast.makeText(getApplicationContext(), "Mancano le foto VM", Toast.LENGTH_SHORT).show();
            res = false;
        }
        if(CountPhotoVC==0 && txtLetturaMis.getText().toString().length()>0) {
            Toast.makeText(getApplicationContext(), "Mancano le foto VC", Toast.LENGTH_SHORT).show();
            res = false;
         }
        if(CountPhotoVE==0 && LetturaVE.length()>0) {
            Toast.makeText(getApplicationContext(), "Mancano le foto VE", Toast.LENGTH_SHORT).show();
            res = false;
        }
        if(CountPhotoVB==0 && LetturaVB.length()>0) {
            Toast.makeText(getApplicationContext(), "Mancano le foto VB", Toast.LENGTH_SHORT).show();
            res = false;
        }

        return res;
    }

    public void ReadDatiUtenza(View view)
    {
        Intent intent = new Intent(this, Act_DatiUtenza.class);
        intent.putExtra("CodiceUtente", CodiceUtente);
        intent.putExtra("IDCarico", IDCarico);
        intent.putExtra("IDAzienda", IDAzienda);
        startActivity(intent);
    }

    public boolean ObbligoNote()
    {
        boolean valore = false;
        if(ObbNotaUno.equals("S") && spnBiffUno.getSelectedItemPosition()<1)
            valore = true;
        if(ObbNotaDue.equals("S") && spnBiffDue.getSelectedItemPosition()<1)
            valore = true;

        return valore;
    }

    public void SalvaLavoro(View view) {
        txtLetturaMis.clearFocus();

        if(GlobalConfig.trim().equals("14") && spnBiffAccessibilita.getSelectedItemPosition() == 0)
        {
            Toast.makeText(getApplicationContext(), "Attenzione selezionare un valore per l'accessibilità", Toast.LENGTH_LONG).show();
            return;
        }

        if (ObbligoNote())
        {
            Toast.makeText(getApplicationContext(), "Attenzione per l'attività le note sono obbligatorie", Toast.LENGTH_LONG).show();
            return;
        }
        if(QualiFotoMancano() ) {
            if (!ControlloCorrettore) {
                Toast.makeText(getApplicationContext(), "Attenzione mancano i dati del correttore", Toast.LENGTH_LONG).show();
            }
            else if ((ObbligoLettura && !Integrato) && txtLetturaMis.getText().toString().length() < 1) {
                Toast.makeText(getApplicationContext(), "Attenzione mancano le letture", Toast.LENGTH_LONG).show();
            }
            else if ((!ObbligoLettura && !Integrato) && txtLetturaMis.getText().toString().length() < 1 && CodBiffUnoCS.equals("000")) {
                    Toast.makeText(getApplicationContext(), "Attenzione in assenza di lettura devi inserire almeno una nota", Toast.LENGTH_LONG).show();
            }
            else if ((ObbligoLettura && Integrato) && (LetturaVB.length() < 1 || LetturaVE.length() < 1)) {
                Toast.makeText(getApplicationContext(), "Attenzione mancano le letture VB e VE", Toast.LENGTH_LONG).show();
            }
            else if ((!ObbligoLettura && Integrato) && (LetturaVB.length() < 1 || LetturaVE.length() < 1) && CodBiffUnoCS.equals("000")) {
                Toast.makeText(getApplicationContext(), "Attenzione in assenza di lettura devi inserire almeno una nota", Toast.LENGTH_LONG).show();
            }
            else if ((!ObbligoLettura) && txtLetturaMis.getText().toString().trim().length()>0 && !ConfermatoDato && CodBiffUnoCS!="000" )
            {
                AlertDialog.Builder dialogDatoLettura = new AlertDialog.Builder(Act_RilevaDati.this);
                dialogDatoLettura
                        .setTitle("CONTROLLA LETTURA")
                        .setMessage("Attenzione, hai inserito una lettura con una nota che non ti obbliga ad inserire la lettura. Sei sicuro del dato inserito?")
                        .setCancelable(false)
                        .setPositiveButton("Si, sono sicuro", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                ConfermatoDato = true;
                            }
                        })
                        .setNegativeButton("No, effetturo controllo", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                ConfermatoDato = false;
                            }
                        });
                AlertDialog MsgDatoLettura = dialogDatoLettura.create();
                MsgDatoLettura.show();
            }
            else if ((!ObbligoLettura) && txtLetturaMis.getText().toString().trim().length()>0 && !ConfermatoDato )
            {
                if(Integer.parseInt(txtLetturaMis.getText().toString().trim())==0) {
                    AlertDialog.Builder dialogDatoLettura = new AlertDialog.Builder(Act_RilevaDati.this);
                    dialogDatoLettura
                            .setTitle("CONTROLLA LETTURA")
                            .setMessage("Attenzione, hai inserito una lettura pari a zero. Sei sicuro? Se non hai segnalato un guasto elettronico ti invitiamo a rimuovere lo zero dalla lettura. Grazie!")
                            .setCancelable(true)
                            .setPositiveButton("Verifico io i dati", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    ConfermatoDato = true;
                                }
                            })
                            .setNegativeButton("Cancella lo zero", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    ConfermatoDato = false;
                                    txtLetturaMis.setText("");
                                }
                            });
                    AlertDialog MsgDatoLettura = dialogDatoLettura.create();
                    MsgDatoLettura.show();
                }
            }
            else if ((!ObbligoLettura) && LetturaVB.toString().trim().length()>0 && !ConfermatoDato )
            {
                if(Integer.parseInt(LetturaVB.toString().trim())==0) {
                    AlertDialog.Builder dialogDatoLettura = new AlertDialog.Builder(Act_RilevaDati.this);
                    dialogDatoLettura
                            .setTitle("CONTROLLA LETTURA")
                            .setMessage("Attenzione, hai inserito una lettura VB pari a zero. Sei sicuro? Se non hai segnalato un guasto ti invitiamo a rimuovere lo zero dalla lettura. Grazie!")
                            .setCancelable(true)
                            .setPositiveButton("Verifico io i dati", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    ConfermatoDato = true;
                                }
                            })
                            .setNegativeButton("Cancella lo zero", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    ConfermatoDato = false;
                                    LetturaVB = "";
                                }
                            });
                    AlertDialog MsgDatoLettura = dialogDatoLettura.create();
                    MsgDatoLettura.show();
                }
                else
                {
                    AlertDialog.Builder dialogDatoLettura = new AlertDialog.Builder(Act_RilevaDati.this);
                    dialogDatoLettura
                            .setTitle("CONTROLLA LETTURA")
                            .setMessage("Attenzione, hai inserito una lettura VB con una nota che non ti obbliga ad inserire la lettura. Sei sicuro del dato inserito?")
                            .setCancelable(false)
                            .setPositiveButton("Si, sono sicuro", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    ConfermatoDato = true;
                                }
                            })
                            .setNegativeButton("No, effetturo controllo", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    ConfermatoDato = false;
                                }
                            });
                    AlertDialog MsgDatoLettura = dialogDatoLettura.create();
                    MsgDatoLettura.show();
                }
            }
            else {

                if(GlobalConfig.equals("6"))
                {
                    if(lblMatricola.getText().toString().trim().length()<1)
                    {
                        if(CensMatricola.toString().trim().length()<1 && txtLetturaMis.getText().toString().trim().length()>0) {
                            Toast.makeText(getApplicationContext(), "Attenzione in assenza di matricola devi inserire per forza la matricola del contatore accedendo ai Dati Censiti", Toast.LENGTH_LONG).show();
                            return;
                        }

                    }
                }
                Boolean ControlloUltimoFoto = false;

                //File sdCard = Environment.getExternalStorageDirectory();

                File sdCard = new File(getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS).getAbsolutePath());
                //File sdCard = new File(File.separator + "storage" + File.separator + "sdcard0");

                String ContrNomeFile = CodiceUtente.replace(String.valueOf((char) 160), " ").trim();
                File mediaStorageDir = new File(sdCard.getAbsolutePath() + File.separator + "SveltoDroidPhoto");
                File ContrFile[] = mediaStorageDir.listFiles();
                for (Integer ik = 0; ik < ContrFile.length; ik++) {
                    if (ContrFile[ik].getName().contains(ContrNomeFile))
                        ControlloUltimoFoto = true;
                }

                if (ControlloUltimoFoto) {
                    VerificaGPS();
                    String DefAutoLett = "N";
                    if (rdAutolettura.isChecked())
                        DefAutoLett = "Y";
                    else
                        DefAutoLett = "N";
                    if (rdWhatsapp.isChecked())
                        DefAutoLett = "W";
                    String LettMisuratore = "";
                    String CifreMisuratore = "";
                    String NoteLetturista = "";

                    LettMisuratore = txtLetturaMis.getText().toString();
                    CifreMisuratore = txtCifreMisuratore.getText().toString();
                    NoteLetturista = txtNoteLetturista.getText().toString();
                    if(rdSigillatura.isChecked())
                    {
                        txtNoteLetturista.setText("eseguita risigillatura");
                        txtNoteLetturista.setEnabled(false);
                        NoteLetturista = "eseguita risigillatura";
                    }

                    String sqlUpdate = "UPDATE utentiOUT SET ";
                    sqlUpdate = sqlUpdate + "LCifreLettura ='" + LettMisuratore + "', ";
                    sqlUpdate = sqlUpdate + "LDataLettura ='" + DataAttivita + "', ";
                    sqlUpdate = sqlUpdate + "LOraLettura ='" + OraAttivita + "', ";
                    sqlUpdate = sqlUpdate + "DtLetturaTrm ='" + DataAttivitaTrm + "', ";
                    sqlUpdate = sqlUpdate + "OraLetturaTrm ='" + OraAttivitaTrm + "', ";
                    sqlUpdate = sqlUpdate + "LSegnalazione ='" + txtNoteBiffUno.getText().toString().replace("'", "") + "', ";
                    sqlUpdate = sqlUpdate + "LSegnalazione1 ='" + txtNoteBiffDue.getText().toString().replace("'","") + "', ";
                    sqlUpdate = sqlUpdate + "LNota1 ='" + CodBiffUnoCS + "', ";
                    sqlUpdate = sqlUpdate + "LNota2 ='" + CodBiffDueCS + "', ";
                    if(GlobalConfig.equals("14"))
                        sqlUpdate = sqlUpdate + "LNota3 ='" + SpinnerAccessibilita.get(spnBiffAccessibilita.getSelectedItemPosition()).trim().substring(0,2) + "', ";
                    sqlUpdate = sqlUpdate + "LCifreM ='" + CifreMisuratore + "', ";
                    sqlUpdate = sqlUpdate + "LLetturaCorrettore ='" + LetturaVM + "', ";
                    sqlUpdate = sqlUpdate + "LLetturaCorrettore1 ='" + LetturaVB + "', ";
                    sqlUpdate = sqlUpdate + "LLetturaCorrettore2 ='" + LetturaVE + "', ";
                    sqlUpdate = sqlUpdate + "LLatitudine ='" + DefLat + "', ";
                    sqlUpdate = sqlUpdate + "LLongitudine ='" + DefLong + "', ";
                    sqlUpdate = sqlUpdate + "LAutolettura ='" + DefAutoLett + "', ";
                    sqlUpdate = sqlUpdate + "LNoteLibere ='" + NoteLetturista.replace("'","") + "', ";

                    sqlUpdate = sqlUpdate + "MSegnalazione ='', ";

                    sqlUpdate = sqlUpdate + "Stato ='L' WHERE CodiceUtente='" + CodiceUtente + "' AND IDCarico='" + IDCarico + "' AND IDAzienda='" + IDAzienda + "' ";

                    //Toast.makeText(getApplicationContext(), IDLett, Toast.LENGTH_SHORT).show();
                    try {

                        db.execSQL(sqlUpdate);
                        //sqlUpdate = sqlUpdate.replace("UPDATE utentiOUT SET ", "UPDATE utenti SET ");
                        //db.execSQL(sqlUpdate);
                        //Inutile ricopiare i dati letti anche in utenti, li teniamo solo in utentiOUT
                        //******************************************************************************************************
                        //*********************************             PER SMAT          **************************************
                        //******************************************************************************************************
                        if(GlobalConfig.equals("6"))
                        {
                            String MancaLett1, LNota1CambioCont,CambioContatore,LetturaAttualeCambio,DataLetturaCambio,LetturaForzata,FlagNoteLetturista;
                            if(txtLetturaMis.getText().toString().trim().length()==0)
                                MancaLett1 = "ML";
                            else
                                MancaLett1 = "  ";
                            if( CodBiffUnoAZ!=null && MSegnalazione!=null )
                            {
                                if(this.CodBiffUnoAZ.equals("M") && MSegnalazione.equals("1"))
                                    LNota1CambioCont = "1";
                                else
                                    LNota1CambioCont = " ";
                            }
                            else
                                LNota1CambioCont = " ";

                            if(CodBiffUnoAZ!=null )
                            {
                                if(this.CodBiffUnoAZ.equals("M"))
                                {
                                    CambioContatore = "1";
                                    LetturaAttualeCambio = LettMisuratore;
                                    DataLetturaCambio = DataAttivita;
                                }
                                else {
                                    CambioContatore = MSegnalazione;
                                    LetturaAttualeCambio = "       ";
                                    DataLetturaCambio = "        ";
                                }
                            }
                            else {
                                CambioContatore = MSegnalazione;
                                LetturaAttualeCambio = "       ";
                                DataLetturaCambio = "        ";
                            }
                            if(LetturaMassimaCalcolata!= null && LetturaMinimaCalcolata!= null && ObbligoLettura) {
                                if (Integer.parseInt(LettMisuratore) <= Integer.parseInt(LetturaMassimaCalcolata) && Integer.parseInt(LettMisuratore) >= Integer.parseInt(LetturaMinimaCalcolata)) {
                                    LetturaForzata = "1";
                                }
                                LetturaForzata = "0";
                            }
                            else
                                LetturaForzata = "0";

                            if(!SMATOldNota.equals(NoteLetturista.replace("'","")))
                                FlagNoteLetturista ="1";
                            else
                                FlagNoteLetturista ="0";
                            ArrayList<String> sqlDatiAccessori = new ArrayList<>(7);
                            sqlDatiAccessori.add("INSERT INTO DatiAccessori VALUES ('"+GlobalConfig+"','"+IDCarico+"','"+CodiceUtente+"','MancaLett1','"+MancaLett1+"','')");
                            sqlDatiAccessori.add("INSERT INTO DatiAccessori VALUES ('"+GlobalConfig+"','"+IDCarico+"','"+CodiceUtente+"','LNota1CambioCont','"+LNota1CambioCont+"','')");
                            sqlDatiAccessori.add("INSERT INTO DatiAccessori VALUES ('"+GlobalConfig+"','"+IDCarico+"','"+CodiceUtente+"','CambioContatore','"+CambioContatore+"','')");
                            sqlDatiAccessori.add("INSERT INTO DatiAccessori VALUES ('"+GlobalConfig+"','"+IDCarico+"','"+CodiceUtente+"','LetturaAttualeCambio','"+LetturaAttualeCambio+"','')");
                            sqlDatiAccessori.add("INSERT INTO DatiAccessori VALUES ('"+GlobalConfig+"','"+IDCarico+"','"+CodiceUtente+"','DataLetturaCambio','"+DataLetturaCambio+"','')");
                            sqlDatiAccessori.add("INSERT INTO DatiAccessori VALUES ('"+GlobalConfig+"','"+IDCarico+"','"+CodiceUtente+"','LetturaForzata','"+LetturaForzata+"','')");
                            sqlDatiAccessori.add("INSERT INTO DatiAccessori VALUES ('"+GlobalConfig+"','"+IDCarico+"','" + CodiceUtente + "','FlagNoteLetturista','" + FlagNoteLetturista + "','')");
                            sqlDatiAccessori.add("INSERT INTO DatiAccessori VALUES ('"+GlobalConfig+"','"+IDCarico+"','" + CodiceUtente + "','RilascioStampa','" + RilascioStampa + "','')");
                            sqlDatiAccessori.add("INSERT INTO DatiAccessori VALUES ('"+GlobalConfig+"','"+IDCarico+"','" + CodiceUtente + "','DataSecondoPassaggio','" + DataAttivita + "','')");
                            sqlDatiAccessori.add("INSERT INTO DatiAccessori VALUES ('"+GlobalConfig+"','"+IDCarico+"','" + CodiceUtente + "','OraSecondoPassaggio','" + OraAttivita + "','')");

                            for(int i = 0; i < sqlDatiAccessori.size();i++)
                            {
                                db.execSQL(sqlDatiAccessori.get(i));
                            }
                        }

                        //******************************************************************************************************
                        //*********************************             PER SMAT          **************************************
                        //******************************************************************************************************

                        if(Censimento)
                        {
                            sqlUpdate = "";
                            sqlUpdate = "INSERT INTO TbDatiCensiti VALUES (";
                            sqlUpdate = sqlUpdate + "'" + IDCarico + "', ";
                            sqlUpdate = sqlUpdate + "'" + CodiceUtente + "', ";
                            sqlUpdate = sqlUpdate + "null, ";
                            sqlUpdate = sqlUpdate + "null, ";
                            sqlUpdate = sqlUpdate + "null, ";
                            sqlUpdate = sqlUpdate + "'" + CensMatricola + "', ";
                            sqlUpdate = sqlUpdate + "'" + CensSiglaMatricola.replace("'","") + "', ";
                            sqlUpdate = sqlUpdate + "'" + CensAnnoMatricola.replace("'", "") + "', ";
                            sqlUpdate = sqlUpdate + "'" + CensSigillo.replace("'", "") + "', ";
                            sqlUpdate = sqlUpdate + "'" + CensNotaSigillo.toString().replace("'", "") + "', ";
                            sqlUpdate = sqlUpdate + "'" + CensNominativo.replace("'","") + "', ";
                            sqlUpdate = sqlUpdate + "'" + CensComune.replace("'","") + "', ";
                            sqlUpdate = sqlUpdate + "'" + CensIndirizzo.replace("'", "") + "', ";
                            sqlUpdate = sqlUpdate + "'" + CensCivico.replace("'","") + "', ";
                            sqlUpdate = sqlUpdate + "null, ";
                            sqlUpdate = sqlUpdate + "'" + CensPiano.replace("'","") + "', ";
                            sqlUpdate = sqlUpdate + "null, ";
                            sqlUpdate = sqlUpdate + "'" + CensScala.replace("'","") + "', ";
                            sqlUpdate = sqlUpdate + "'" + CensInterno.replace("'","") + "', ";
                            sqlUpdate = sqlUpdate + "'" + CensTelefono.replace("'","") + "', ";
                            sqlUpdate = sqlUpdate + "'" + CensBarra.replace("'","") + "', ";
                            sqlUpdate = sqlUpdate + "'" + CensImpegnativo.replace("'","") + "', ";
                            sqlUpdate = sqlUpdate + "null, ";
                            sqlUpdate = sqlUpdate + "null, ";
                            sqlUpdate = sqlUpdate + "null, ";
                            sqlUpdate = sqlUpdate + "'" + CensCifreMisuratore.replace("'","") + "') ";
                            db.execSQL(sqlUpdate);
                            Toast.makeText(getApplicationContext(), "Dati Censiti salvati!", Toast.LENGTH_SHORT).show();
                            finish();

                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(), "Attività conclusa!", Toast.LENGTH_SHORT).show();
                            finish();
                        }

                        db.execSQL("UPDATE utenti SET Stato = 'L' WHERE CodiceUtente='" + CodiceUtente + "' AND IDCarico='" + IDCarico + "' AND IDAzienda='" + IDAzienda + "' ");

                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), "Attenzione impossibile Chiudere l'attività [" + e.toString() + "]", Toast.LENGTH_LONG).show();
                    }
                }
                else
                    Toast.makeText(getApplicationContext(), "Attenzione devi riscattare tutte le foto per salvare!", Toast.LENGTH_LONG).show();
            }
        }

    }

    private void AttivaSecondaBiffatura(String FirstBiff)
    {
        Cursor cursorBiffaturDue = dbHelper.query(db, "SELECT Codice, CodiceDitta, DescrSupp, Activity, Autolettura, ObbLettura, ObbFoto FROM NoteLettDitta WHERE IdDitta = '" + IDDitta + "' AND (CampoNota = '0' or CampoNota = '2')");
        SpinnerBiff2 = new ArrayList<String>();
        SpinnerBiff2.add("Causale Seconda Nota");

        VerAutoLetturaDue = new ArrayList<String>();
        VerLetturaDue = new ArrayList<String>();
        CodComerBiffaturaDue = new ArrayList<String>();
        CodAziendaBiffaturaDue = new ArrayList<String>();
        BiffToActivityDue = new ArrayList<String>();
        cursorBiffaturDue.moveToFirst();
        Integer i = 1;
        BiffToActivityDue.add("");
        CodComerBiffaturaDue.add("");
        CodAziendaBiffaturaDue.add("");
        VerAutoLetturaDue.add("");
        VerLetturaDue.add("");
        while (cursorBiffaturDue.isAfterLast() == false) {
            if(Accessibilita.contains("Y") || Accessibilita.contains("S")) {
                if (BiffatureAccessibili.contains(cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("CodiceDitta"))) && ControlloNoteAccessibili.get(FirstBiff).contains("-"+cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("CodiceDitta"))+"-")) {
                    SpinnerBiff2.add(cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("Codice"))+" - " +cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("DescrSupp")).toUpperCase());
                    VerAutoLetturaDue.add(cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("Autolettura")));
                    VerLetturaDue.add(cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("ObbLettura")));
                    BiffToActivityDue.add(cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("Activity")));
                    CodComerBiffaturaDue.add(cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("Codice")));
                    CodAziendaBiffaturaDue.add(cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("CodiceDitta")));
                    ObbFotoNotaDue.add(cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("ObbFoto")));
                    i++;
                }
            }
            else if(Accessibilita.contains("P")) {
                if (BiffatureParziali.contains(cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("CodiceDitta"))) && ControlloNoteParziali.get(FirstBiff).contains("-"+cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("CodiceDitta"))+"-")) {
                    SpinnerBiff2.add(cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("Codice"))+" - " +cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("DescrSupp")).toUpperCase());
                    VerAutoLetturaDue.add(cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("Autolettura")));
                    BiffToActivityDue.add(cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("Activity")));
                    VerLetturaDue.add(cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("ObbLettura")));
                    CodComerBiffaturaDue.add(cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("Codice")));
                    CodAziendaBiffaturaDue.add(cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("CodiceDitta")));
                    ObbFotoNotaDue.add(cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("ObbFoto")));
                    i++;
                }
            }
            else if(Accessibilita.contains("N")) {
                if (BiffatureNonAccessibili.contains(cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("CodiceDitta"))) && ControlloNoteNonAccessibili.get(FirstBiff).contains("-"+cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("CodiceDitta"))+"-")) {
                    SpinnerBiff2.add(cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("Codice"))+" - " +cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("DescrSupp")).toUpperCase());
                    VerAutoLetturaDue.add(cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("Autolettura")));
                    VerLetturaDue.add(cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("ObbLettura")));
                    BiffToActivityDue.add(cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("Activity")));
                    CodComerBiffaturaDue.add(cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("Codice")));
                    CodAziendaBiffaturaDue.add(cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("CodiceDitta")));
                    ObbFotoNotaDue.add(cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("ObbFoto")));
                    i++;
                }
            }
            else if(Accessibilita.equals("")) {
                if (BiffatureSenzaAccessibilita.contains(cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("CodiceDitta")))) {
                    SpinnerBiff2.add(cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("Codice"))+" - " +cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("DescrSupp")).toUpperCase());
                    VerAutoLetturaDue.add(cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("Autolettura")));
                    BiffToActivityDue.add(cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("Activity")));
                    VerLetturaDue.add(cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("ObbLettura")));
                    CodComerBiffaturaDue.add(cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("Codice")));
                    CodAziendaBiffaturaDue.add(cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("CodiceDitta")));
                    ObbFotoNotaDue.add(cursorBiffaturDue.getString(cursorBiffaturDue.getColumnIndex("ObbFoto")));
                    i++;
                }
            }
            cursorBiffaturDue.moveToNext();
        }
        cursorBiffaturDue.close();


        ArrayAdapter<String> SpinnAdapBiff2 = new ArrayAdapter<String>(this, R.layout.spinner_item1, SpinnerBiff2);
        spnBiffDue.setAdapter(SpinnAdapBiff2);
        spnBiffDue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                VerificaGPS();
                if (position > 0) {
                    if (VerAutoLetturaDue.get(position).toString().equals("N") && rdAutolettura.isChecked()) {
                        rdAutolettura.setChecked(false);
                        Toast.makeText(getApplicationContext(), "Attenzione, la nota non è compatibile con l'autolettura", Toast.LENGTH_SHORT).show();
                    }

                    if(VerLetturaDue.get(position).toString().equals("S") )
                        ObbligoLettura = true;
                    else if(VerLetturaDue.get(position).toString().equals("N") )
                    {
                        if(rdWhatsapp.isChecked())
                        {
                            rdWhatsapp.setChecked(false);
                            Toast.makeText(getApplicationContext(), "Whatsapp può essere utilizzato solo in caso di obbligo di lettura", Toast.LENGTH_SHORT).show();
                        }
                        ObbligoLettura = false;
                    }

                    if (BiffToActivityDue.get(position) != null) {
                        CodBiffDueCS = CodComerBiffaturaDue.get(position);
                        CodBiffDueAZ = CodAziendaBiffaturaDue.get(position);
                        DaQualeBiff = "BiffDue";
                        Intent intent = new Intent(getApplicationContext(), OpenSpecialActivity.get(BiffToActivityDue.get(position)));
                        intent.putExtra("IdCarico", IDCarico);
                        intent.putExtra("CodiceUtente", CodiceUtente);
                        intent.putExtra("IdAzienda", IDAzienda);
                        intent.putExtra("GlobalConfig", GlobalConfig);
                        intent.putExtra("CodAzBiffa", CodBiffDueAZ);
                        intent.putExtra("CategoriaUtente", CategoriaUtente);
                        if(BiffToActivityDue.get(position).equals("Act_CambiaIndirizzo"))
                        {
                            intent.putExtra("Toponimo",OrigToponimo);
                            intent.putExtra("Strada",OrigStrada);
                            intent.putExtra("Civico",OrigCivico);
                        }
                        startActivityForResult(intent, 1);
                        txtNoteBiffDue.setEnabled(false);
                        btnFotoB2.setEnabled(true);
                        onPause();
                    } else if (position > 0) {
                        CodBiffDueCS = CodComerBiffaturaDue.get(position);
                        CodBiffDueAZ = CodAziendaBiffaturaDue.get(position);
                        txtNoteBiffDue.setText("");
                        txtNoteBiffDue.setEnabled(true);
                        txtNoteBiffDue.requestFocus();
                        btnFotoB2.setEnabled(true);
                    } else {
                        txtNoteBiffDue.setText("");
                        txtNoteBiffDue.setEnabled(true);
                        txtNoteBiffDue.requestFocus();
                        btnFotoB2.setEnabled(true);
                    }
                } else {
                    txtNoteBiffDue.setText("");
                    txtNoteBiffDue.setEnabled(false);
                    btnFotoB2.setEnabled(false);
                    CodBiffDueCS = "000";
                    CodBiffDueAZ = null;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                txtNoteBiffDue.setText("");
                txtNoteBiffDue.setEnabled(false);
            }


        });

    }

    public void FotoVC(View view)
    {
        txtLetturaMis.clearFocus();
        if (!Integrato )
        {
            VerificaGPS();
            Intent intent;
            if (android.os.Build.VERSION.SDK_INT > 23) {
                intent = new Intent(Act_RilevaDati.this, Act_newCamera.class);
            }
            else
                intent = new Intent(Act_RilevaDati.this, Custom_CameraActivity.class);
            intent.putExtra("CodiceUtente", CodiceUtente);
            intent.putExtra("IDCarico", IDCarico);
            intent.putExtra("IDAzienda", IDAzienda);
            intent.putExtra("DataAttivita", DataAttivita);
            intent.putExtra("OraAttivita", OraAttivita);
            intent.putExtra("Misuratore", lblMatricola.getText());
            intent.putExtra("Correttore", lblCorrettore.getText());
            intent.putExtra("TipoFoto", "VC");
            intent.putExtra("CountPhotoB1", CountPhotoB1);
            intent.putExtra("CountPhotoB2", CountPhotoB2);
            intent.putExtra("CountPhotoVM", CountPhotoVM);
            intent.putExtra("CountPhotoVC", CountPhotoVC);
            intent.putExtra("CountPhotoVB", CountPhotoVB);
            intent.putExtra("CountPhotoVE", CountPhotoVE);
            intent.putExtra("DefLat", DefLat);
            intent.putExtra("DefLong", DefLong);
            intent.putExtra("operatore", IDLett);
            intent.putExtra("GlobalConfig",GlobalConfig);

            startActivityForResult(intent, 2);
        }
        else
            Toast.makeText(getApplicationContext(), "Per piacere re-inserisci la lettura, Grazie", Toast.LENGTH_LONG).show();

    }

    public void FotoB1(View view)
    {

        txtLetturaMis.clearFocus();
        VerificaGPS();
        Intent intent;
        if (android.os.Build.VERSION.SDK_INT > 23) {
            intent = new Intent(Act_RilevaDati.this, Act_newCamera.class);
        }
        else
            intent = new Intent(Act_RilevaDati.this, Custom_CameraActivity.class);
        intent.putExtra("CodiceUtente", CodiceUtente);
        intent.putExtra("IDCarico", IDCarico);
        intent.putExtra("IDAzienda", IDAzienda);
        intent.putExtra("DataAttivita", DataAttivita);
        intent.putExtra("OraAttivita", OraAttivita);
        intent.putExtra("Misuratore", lblMatricola.getText());
        intent.putExtra("Correttore", lblCorrettore.getText());
        intent.putExtra("TipoFoto", "B1");
        intent.putExtra("CountPhotoB1", CountPhotoB1);
        intent.putExtra("CountPhotoB2", CountPhotoB2);
        intent.putExtra("CountPhotoVM", CountPhotoVM);
        intent.putExtra("CountPhotoVC", CountPhotoVC);
        intent.putExtra("CountPhotoVB", CountPhotoVB);
        intent.putExtra("CountPhotoVE", CountPhotoVE);
        intent.putExtra("DefLat", DefLat);
        intent.putExtra("DefLong", DefLong);
        intent.putExtra("operatore", IDLett);
        intent.putExtra("GlobalConfig",GlobalConfig);

        startActivityForResult(intent, 2);

    }

    public void FotoB2(View view)
    {

        txtLetturaMis.clearFocus();
        VerificaGPS();
        Intent intent;
        if (android.os.Build.VERSION.SDK_INT > 23) {
            intent = new Intent(Act_RilevaDati.this, Act_newCamera.class);
        }
        else
            intent = new Intent(Act_RilevaDati.this, Custom_CameraActivity.class);
        intent.putExtra("CodiceUtente", CodiceUtente);
        intent.putExtra("IDCarico", IDCarico);
        intent.putExtra("IDAzienda", IDAzienda);
        intent.putExtra("DataAttivita", DataAttivita);
        intent.putExtra("OraAttivita", OraAttivita);
        intent.putExtra("Misuratore", lblMatricola.getText());
        intent.putExtra("Correttore", lblCorrettore.getText());
        intent.putExtra("TipoFoto", "B2");
        intent.putExtra("CountPhotoB1", CountPhotoB1);
        intent.putExtra("CountPhotoB2", CountPhotoB2);
        intent.putExtra("CountPhotoVM", CountPhotoVM);
        intent.putExtra("CountPhotoVC", CountPhotoVC);
        intent.putExtra("CountPhotoVB", CountPhotoVB);
        intent.putExtra("CountPhotoVE", CountPhotoVE);
        intent.putExtra("DefLat", DefLat);
        intent.putExtra("DefLong", DefLong);
        intent.putExtra("operatore", IDLett);
        intent.putExtra("GlobalConfig",GlobalConfig);

        startActivityForResult(intent, 2);
    }

    public void ControlloWhatsapp(View view) {
        if ((GlobalConfig.equals("1") || GlobalConfig.equals("2")) && rdAutolettura.isChecked() && rdWhatsapp.isChecked()) {
            Toast.makeText(getApplicationContext(), "Attenzione, la lettura via Whatsapp non è compatibile con l'autolettura", Toast.LENGTH_SHORT).show();
            rdAutolettura.setChecked(false);
        }

        if((GlobalConfig.equals("1") || GlobalConfig.equals("2")) && rdWhatsapp.isChecked() && Accessibilita.equals("Y"))
        {
            if(CodBiffUnoAZ!=null) {
                if (!CodBiffUnoAZ.equals("30") && !CodBiffUnoAZ.equals("38") && !CodBiffUnoAZ.equals("16") && !CodBiffUnoAZ.equals("18")) {
                    rdWhatsapp.setChecked(false);
                    Toast.makeText(getApplicationContext(), "In caso di accessibilità, Whatsapp è compatibile solo con note di cambio accessibilità", Toast.LENGTH_SHORT).show();
                }
            }
            else
            {
                rdWhatsapp.setChecked(false);
                Toast.makeText(getApplicationContext(), "In caso di accessibilità, Whatsapp è compatibile solo con note di cambio accessibilità", Toast.LENGTH_SHORT).show();
            }

        }
        if(rdWhatsapp.isChecked() && (!ObbligoLettura && !CodBiffUnoAZ.equals("16") && !CodBiffUnoAZ.equals("18")))
        {
            rdWhatsapp.setChecked(false);
            Toast.makeText(getApplicationContext(), "Whatsapp può essere utilizzato solo in caso di obbligo di lettura", Toast.LENGTH_SHORT).show();

        }
    }

    public void ControlloCheckAutolettura(View view)
    {
        if((GlobalConfig.equals("1") || GlobalConfig.equals("2") ) && rdAutolettura.isChecked() && rdWhatsapp.isChecked())
        {
            Toast.makeText(getApplicationContext(), "Attenzione, l'autolettura non è compatibile con il Whatsapp", Toast.LENGTH_SHORT).show();
            rdWhatsapp.setChecked(false);
        }
        if(spnBiffUno.getSelectedItemPosition()>0) {
            if (VerAutoLettura.get(spnBiffUno.getSelectedItemPosition()).toString().equals("N") && rdAutolettura.isChecked()) {
                rdAutolettura.setChecked(false);
                Toast.makeText(getApplicationContext(), "Attenzione, la nota 1 non è compatibile con l'autolettura", Toast.LENGTH_SHORT).show();
            }
        }
        if(spnBiffDue.getSelectedItemPosition()>0) {
            if (VerAutoLetturaDue.get(spnBiffUno.getSelectedItemPosition()).toString().equals("N") && rdAutolettura.isChecked()) {
                rdAutolettura.setChecked(false);
                Toast.makeText(getApplicationContext(), "Attenzione, la nota 2 non è compatibile con l'autolettura", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void AnnullaLavoro(View view)
    {

        finish();
    }

    public void Programma(View view)
    {

        VerificaGPS();
        Intent intent;
        intent = new Intent(Act_RilevaDati.this, Act_Programmazione.class);
        intent.putExtra("CodiceUtente", CodiceUtente);
        intent.putExtra("IDCarico", IDCarico);
        intent.putExtra("IDAzienda", IDAzienda);
        intent.putExtra("DataAttivita", DataAttivita);
        intent.putExtra("OraAttivita", OraAttivita);
        intent.putExtra("TipoFoto", "P1");
        intent.putExtra("CountPhotoP1", CountPhotoP1);
        intent.putExtra("DefLat", DefLat);
        intent.putExtra("DefLong", DefLong);
        intent.putExtra("operatore", IDLett);
        intent.putExtra("GlobalConfig",GlobalConfig);
        startActivityForResult(intent, 5);
    }

    public void StampaRicevuta (View view)
    {
        txtLetturaMis.clearFocus();
        if(!GlobalConfig.equals("6"))
        {
            Toast.makeText(getApplicationContext(), "Per tale attività non è prevista la stampa", Toast.LENGTH_LONG).show();
            return;
        }
        else if(DataPrimoPassaggio==null)
        {
            Toast.makeText(getApplicationContext(), "La ricevuta è stampabile solo al secondo passaggio", Toast.LENGTH_LONG).show();
            return;
        }
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Toast.makeText(getApplicationContext(), "Attenzione, il bluetooth non funziona", Toast.LENGTH_LONG).show();
        }
        else
        {
            if (mBluetoothAdapter.isEnabled())
            {
                if(GPC.appInstalledOrNot("com.loopedlabs.escposprintservice",getApplicationContext())) {
                    String societa = "";
                    String ditta = "EASY SERVIZI SRL";
                    if (GlobalConfig.equals("6"))
                        societa = "SMAT";
                    else if (GlobalConfig.equals("1") || GlobalConfig.equals("2"))
                        societa = "ITALGAS";
                    else
                        societa = "";

                    Intent i = new Intent();
                    i.setAction("org.escpos.intent.action.PRINT");
                    String txt =  "Societa: " + ditta + System.getProperty("line.separator") ;
                    txt = txt + "Localita: " + DatiStampaLocalita + System.getProperty("line.separator");
                    txt = txt + "PDP: " + DatiStampaCodiceUtente + System.getProperty("line.separator");
                    txt = txt + "Cod. Giro: " + FDLettura + System.getProperty("line.separator");
                    txt = txt + "Indirizzo: " + DatiStampaIndirizzo + System.getProperty("line.separator");
                    txt = txt + "Intestatario: " + DatiStampaNominativo + System.getProperty("line.separator");
                    txt = txt + "Matr. Cont.: " + DatiStampaMatricola + System.getProperty("line.separator");
                    txt = txt + "Data e Ora: " + DataAttivita + " - " + OraAttivita.substring(0,5) + System.getProperty("line.separator");
                    txt = txt + "Operatore N.: " + IDLett + System.getProperty("line.separator");
                    //txt = txt + "Per conto di: " + societa + System.getProperty("line.separator") ;
                    txt = txt + System.getProperty("line.separator") + System.getProperty("line.separator");

                    byte[] d = txt.getBytes();
                    //String a = String.valueOf(new File(File.separator + "storage" + File.separator + "sdcard0"+ File.separator + "SveltoDroidPhotoLOGO"+ File.separator+"Comer.bmp"));
                    //byte[] d = GPC.FileToByte(a);
                    i.putExtra("PRINT_DATA", d);
                    RilascioStampa ="1";
                    startActivity(i);
                }
                else
                    Toast.makeText(getApplicationContext(), "MANCA DRIVER STAMPANTE", Toast.LENGTH_LONG).show();

            }
            else
                Toast.makeText(getApplicationContext(), "DEVI ATTIVARE IL BLUETOOTH", Toast.LENGTH_LONG).show();
        }

    }

    public void MostraGalleria(View view)
    {
        VerificaGPS();
        Intent intent = new Intent(this, Act_Gallery.class);
        intent.putExtra("IDAzienda", IDAzienda);
        intent.putExtra("IDCarico", IDCarico);
        intent.putExtra("CodiceUtente", CodiceUtente);
        intent.putExtra("Contatore", lblMatricola.getText().toString().trim());
        intent.putExtra("Corretore", lblCorrettore.getText().toString().trim());
        if( spnBiffUno.getSelectedItemPosition()>0)
            intent.putExtra("Nota1",SpinnerBiff1.get(spnBiffUno.getSelectedItemPosition()).toString().trim());
        else
            intent.putExtra("Nota1","");
        if( spnBiffDue.getSelectedItemPosition()>0)
            intent.putExtra("Nota2",SpinnerBiff2.get(spnBiffDue.getSelectedItemPosition()).toString().trim());
        else
            intent.putExtra("Nota2","");
        intent.putExtra("operatore", IDLett);
        startActivityForResult(intent, 5);

    }


    private String ConfrontaData(String DataFlussoA, String DataFlussoDa, String DataOggi)
    {
        String resultData;

        if(FlagDonato!=null)
        {
            if(FlagDonato.trim().toLowerCase().equals("donato"))
            {
               try{
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    Date dataDa = sdf.parse(DataFlussoDa);
                    Date dataA = sdf.parse(DataFlussoA);
                    Date today =  sdf.parse(DataOggi);
                    if(dataDa.compareTo(today)<0 && dataA.compareTo(today)>0)
                    {
                        resultData = new SimpleDateFormat("dd/MM/yyyy").format(today);
                    }
                    else if(dataDa.compareTo(today)>0)
                    {
                        resultData = new SimpleDateFormat("dd/MM/yyyy").format(dataDa);
                    }
                    else if(dataA.compareTo(today)<0)
                    {
                        resultData = new SimpleDateFormat("dd/MM/yyyy").format(dataA);
                    }
                    else
                        resultData = new SimpleDateFormat("dd/MM/yyyy").format(today);
                }catch(Exception ex) {

                   Log.i("FLAGDONATO","ERROR");
                    resultData = DataOggi;
                }
            }
            else
                resultData = DataOggi;
        }
        else {

            Log.i("FLAGDONATO","NO");
            resultData = DataOggi;
        }

        return resultData;
    }
}
